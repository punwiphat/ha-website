﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Core.DAL.Models
{
    public partial class CMSdbContext : DbContext
    {
        public CMSdbContext(){}
        public CMSdbContext(DbContextOptions<CMSdbContext> options) : base(options){}
        public virtual DbSet<Tm_Menu> Tm_Menu { get; set; }

        public virtual DbSet<Tm_MenuDetail> Tm_MenuDetail { get; set; }

        public virtual DbSet<MenuDisplay> MenuDisplay { get; set; }

        public virtual DbSet<Role> Role { get; set; }

        public virtual DbSet<RoleGroup> RoleGroup { get; set; }

        public virtual DbSet<Users> Users { get; set; }

        public virtual DbSet<Members> Members { get; set; }

        public virtual DbSet<MediaCategorys> MediaCategorys { get; set; }

        public virtual DbSet<MediaFiles> MediaFiles { get; set; }

        public virtual DbSet<Languages> Languages { get; set; }

        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }

        public virtual DbSet<SettingEmail> SettingEmail { get; set; }

        public virtual DbSet<LogActivities> LogActivities { get; set; }

        public virtual DbSet<SettingFiles> SettingFiles { get; set; }

        public virtual DbSet<Shortcut> Shortcut { get; set; }

        public virtual DbSet<ShortcutDetail> ShortcutDetail { get; set; }

        public virtual DbSet<Banner> Banner { get; set; }

        public virtual DbSet<BannerDetail> BannerDetail { get; set; }

        public virtual DbSet<Calendar> Calendar { get; set; }

        public virtual DbSet<CalendarDetail> CalendarDetail { get; set; }

        public virtual DbSet<Content> Content { get; set; }

        public virtual DbSet<ContentDetail> ContentDetail { get; set; }

        public virtual DbSet<Contact> Contact { get; set; }

        public virtual DbSet<ContactDetail> ContactDetail { get; set; }

        public virtual DbSet<ContactForm> ContactForm { get; set; }

        public virtual DbSet<Posts> Posts { get; set; }

        public virtual DbSet<PostsDetail> PostsDetail { get; set; }

        public virtual DbSet<PostsTag> PostsTag { get; set; }

        public virtual DbSet<RSSFeed> RSSFeed { get; set; }

        public virtual DbSet<RSSFeedDetail> RSSFeedDetail { get; set; }

        public virtual DbSet<Staff> Staff { get; set; }

        public virtual DbSet<StaffDetail> StaffDetail { get; set; }

        public virtual DbSet<DownloadForm> DownloadForm { get; set; }

        public virtual DbSet<DownloadFormDetail> DownloadFormDetail { get; set; }

        public virtual DbSet<JobPosition> JobPosition { get; set; }

        public virtual DbSet<JobPositionDetail> JobPositionDetail { get; set; }

        public virtual DbSet<Faq> Faq { get; set; }

        public virtual DbSet<FaqDetail> FaqDetail { get; set; }

        public virtual DbSet<Category> Category { get; set; }

        public virtual DbSet<Masters> Masters { get; set; }

        public virtual DbSet<SocialLink> SocialLink { get; set; }

        public virtual DbSet<ComplaintType> ComplaintType { get; set; }

        public virtual DbSet<Complaint> Complaint { get; set; }

        public virtual DbSet<WebboardReply> WebboardReply { get; set; }

        public virtual DbSet<WebboardReservedWord> WebboardReservedWord { get; set; }

        public virtual DbSet<WebboardTopic> WebboardTopic { get; set; }

        public virtual DbSet<TopicType> WebboardTopicType { get; set; }

        public virtual DbSet<ChannelComment> ChannelComments { get; set; }

        public virtual DbSet<ChannelCommentType> ChannelCommentTypes { get; set; }

        public virtual DbSet<Projects> Projects { get; set; }

        public virtual DbSet<ProjectsDetail> ProjectsDetail { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")).Build();
                var connectionString = configuration.GetConnectionString("MySqlConnection");

                optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Core
            modelBuilder.Entity<Masters>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("tm_master");
                entity.Property(e => e.ID).HasColumnName("MasterID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.NameTH);
                entity.Property(e => e.NameEN);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            #region Media File
            modelBuilder.Entity<MediaCategorys>(entity =>
            {
                entity.ToTable("media_categorys");
                entity.HasKey(e => e.MediaCategoryID);
                entity.Property(e => e.MediaCategoryID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.MediaCategoryName);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<MediaFiles>(entity =>
            {
                entity.ToTable("media_files");
                entity.HasKey(e => e.MediaFileID);
                entity.Property(e => e.MediaCategoryID);
                entity.Property(e => e.MediaFileID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.RefID);
                entity.Property(e => e.PathFile);
                entity.Property(e => e.FolderName);
                entity.Property(e => e.TitleName);
                entity.Property(e => e.FileNameOri);
                entity.Property(e => e.FileName);
                entity.Property(e => e.FileExtension);
                entity.Property(e => e.FileSize);
                entity.Property(e => e.Downloads);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            #endregion

            #region Menu
            modelBuilder.Entity<Tm_Menu>(entity =>
            {
                entity.ToTable("tm_menu");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID);
                entity.Property(e => e.BelongID);
                entity.Property(e => e.MENU_BACK_ID);
                entity.Property(e => e.MENU_FRONT_ID);
                entity.Property(e => e.MenuTypeCode);
                entity.Property(e => e.section_name_back);
                entity.Property(e => e.menu_name_back);
                entity.Property(e => e.permalink_back);
                entity.Property(e => e.controller_back);
                entity.Property(e => e.action_back);
                entity.Property(e => e.self_back);
                entity.Property(e => e.sort_back);
                entity.Property(e => e.icon_back);
                entity.Property(e => e.show_back);
                entity.Property(e => e.permalink_front);
                entity.Property(e => e.controller_front);
                entity.Property(e => e.action_front);
                entity.Property(e => e.self_front);
                entity.Property(e => e.sort_front);
                entity.Property(e => e.link_front);
                entity.Property(e => e.icon_front);
                entity.Property(e => e.img_front);
                entity.Property(e => e.chk_login_front);
                entity.Property(e => e.chk_popup_front);
                entity.Property(e => e.show_front);
                entity.Property(e => e.levels);
                entity.Property(e => e.Actived);
                entity.Property(e => e.type_show);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
                entity.Property(e => e.IsEdit);
                entity.Property(e => e.Disabled);
            });

            modelBuilder.Entity<Tm_MenuDetail>(entity =>
            {
                entity.ToTable("tm_menu_detail");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Menuname);
                entity.Property(e => e.Permalink);
            });

            modelBuilder.Entity<MenuBackoffice>(entity =>
            {
                entity.HasKey(e => e.MenuBackofficeId);
                entity.ToTable("Menu_Backoffice");
                entity.Property(e => e.MenuBackofficeId).HasColumnName("Menu_Backoffice_Id");
                entity.Property(e => e.SectionMenu).HasColumnName("Section_Menu");
                entity.Property(e => e.MenuName).HasColumnName("Menu_Name");
                entity.Property(e => e.RefMenuId).HasColumnName("Ref_Menu_Id");
                entity.Property(e => e.MenuLevel).HasColumnName("Menu_Level");
                entity.Property(e => e.MenuIcon).HasColumnName("Menu_Icon");
                entity.Property(e => e.MenuTypeId).HasColumnName("Menu_Type_Id");
                entity.Property(e => e.ControllerName).HasColumnName("Controller_Name");
                entity.Property(e => e.ActionName).HasColumnName("Action_Name");
                entity.Property(e => e.Parameter);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived).HasColumnName("Actived");
                entity.Property(e => e.CreatedDate).HasColumnName("Create_Date");
                entity.Property(e => e.CreatedBy).HasColumnName("Create_By");
                entity.Property(e => e.UpdatedDate).HasColumnName("Update_Date");
                entity.Property(e => e.UpdatedBy).HasColumnName("Update_By");
            });

            modelBuilder.Entity<MenuDisplay>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("tm_menu_display");
                entity.Property(e => e.ID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.SortingType);
                entity.Property(e => e.SortingBy);
                entity.Property(e => e.UpdatedDate);
                entity.Property(e => e.UpdatedBy);
            });
            #endregion

            #region Shortcut
            modelBuilder.Entity<Shortcut>(entity =>
            {
                entity.ToTable("shortcut_menu");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("ShortcutID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.ModuleID);
                entity.Property(e => e.ParentID);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<ShortcutDetail>(entity =>
            {
                entity.ToTable("shortcut_menu_detail");
                entity.HasKey(e => e.ShortcutDetailID);
                entity.Property(e => e.ShortcutID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.ServiceName);
                entity.Property(e => e.Permalink);
                entity.Property(e => e.URL);
                entity.Property(e => e.IconType);
                entity.Property(e => e.IconName);
            });
            #endregion

            #region Permission
            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");
                entity.HasKey(e => e.RoleID);
                entity.Property(e => e.RoleID);
                entity.Property(e => e.RoleGroupID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.RoleType);
            });
            modelBuilder.Entity<RoleGroup>(entity =>
            {
                entity.ToTable("role_group");
                entity.HasKey(e => e.RoleGroupID);
                entity.Property(e => e.RoleGroupID);
                entity.Property(e => e.RoleGroupName);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");
                entity.HasKey(e => e.UserID);
                entity.Property(e => e.UserID);
                entity.Property(e => e.Email);
                entity.Property(e => e.Username);
                entity.Property(e => e.FirstName);
                entity.Property(e => e.LastName);
                entity.Property(e => e.Password);
                entity.Property(e => e.RoleGroupID);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<Members>(entity =>
            {
                entity.ToTable("members");
                entity.HasKey(e => e.MemberID);
                entity.Property(e => e.MemberID);
                entity.Property(e => e.TitleID);
                entity.Property(e => e.IdentityType);
                entity.Property(e => e.IdentityNo);
                entity.Property(e => e.Email);
                entity.Property(e => e.Username);
                entity.Property(e => e.Password);
                entity.Property(e => e.FirstName);
                entity.Property(e => e.MiddleName);
                entity.Property(e => e.LastName);
                entity.Property(e => e.Position);
                entity.Property(e => e.Department);
                entity.Property(e => e.TypeMemberID);
                entity.Property(e => e.Address);
                entity.Property(e => e.AddressSend);
                entity.Property(e => e.Actived);
                entity.Property(e => e.Activated);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
                entity.Property(e => e.KeyActivated);
            });

            #endregion

            modelBuilder.Entity<Languages>(entity =>
            {
                entity.ToTable("tm_languages");
                entity.HasKey(e => e.LanguageID);
                entity.Property(e => e.LanguageID);
                entity.Property(e => e.LanguageFlag);
                entity.Property(e => e.LanguageName);
                entity.Property(e => e.LanguageCode);
                entity.Property(e => e.LanguageCulture);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.ToTable("email_template");
                entity.HasKey(e => e.TemplateID);
                entity.Property(e => e.TemplateID);
                entity.Property(e => e.TemplateTypeID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Subject);
                entity.Property(e => e.Descriptions);
                entity.Property(e => e.HTMLContent);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });


            modelBuilder.Entity<SettingEmail>(entity =>
            {
                entity.ToTable("setting_email");
                entity.HasKey(e => e.EmailId);
                entity.Property(e => e.EmailId);
                entity.Property(e => e.EmailAlias);
                entity.Property(e => e.Emailname).HasMaxLength(255);
                entity.Property(e => e.Password).HasMaxLength(255);
                entity.Property(e => e.Smtp).HasMaxLength(255);
                entity.Property(e => e.UseDefaultCredentials).HasColumnName("UseDefaultCredentials");
                entity.Property(e => e.EnabledSSL).HasColumnName("EnabledSSL");
                entity.Property(e => e.UpdatedBy).HasColumnName("UpdatedBy").HasMaxLength(20).IsUnicode(false).IsRequired(true);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime").HasColumnName("UpdatedDate");
            });

            modelBuilder.Entity<SettingFiles>(entity =>
            {
                entity.ToTable("setting_file");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("SettingFileID");
                entity.Property(e => e.AllowTypeFile);
                entity.Property(e => e.MaxSizeFile);
                entity.Property(e => e.SizeFile);
                entity.Property(e => e.MaxQuantityFile);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogActivities>(entity =>
            {
                entity.ToTable("log_activity");
                entity.HasKey(e => e.LogActivityId);
                entity.Property(e => e.LogActivityId).HasColumnName("Log_Activity_Id");
                entity.Property(e => e.Browser);
                entity.Property(e => e.RefId).HasColumnName("Ref_Id");
                entity.Property(e => e.ActivityType).HasColumnName("Activity_Type");
                entity.Property(e => e.MenuID).HasColumnName("Menu_Id");
                entity.Property(e => e.MenuName).HasColumnName("Menu_Name");
                entity.Property(e => e.Action).HasColumnName("Action");
                entity.Property(e => e.ClientIp).HasColumnName("Client_Ip");
                entity.Property(e => e.DeviceName).HasColumnName("Device_Name");
                entity.Property(e => e.Method);
                entity.Property(e => e.Descriptions);
                entity.Property(e => e.ActionDate).HasColumnName("Action_Date");
                entity.Property(e => e.ActionBy).HasColumnName("Action_By");
            });

            #endregion

            #region Contents
            modelBuilder.Entity<Content>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("content");
                entity.Property(e => e.ID).HasColumnName("ContentID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<ContentDetail>(entity =>
            {
                entity.ToTable("content_detail");
                entity.HasKey(e => e.ContentDetailID);
                entity.Property(e => e.ContentDetailID);
                entity.Property(e => e.ContentID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Title);
                entity.Property(e => e.Descriptions);
                entity.Property(e => e.HtmlContent);
            });
            #endregion

            #region Banner
            modelBuilder.Entity<Banner>(entity =>
            {
                entity.ToTable("banner");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("BannerID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<BannerDetail>(entity =>
            {
                entity.ToTable("banner_detail");
                entity.HasKey(e => e.BannerDetailID);
                entity.Property(e => e.BannerDetailID);
                entity.Property(e => e.BannerID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.BannerName);
                entity.Property(e => e.BannerDescription);
                entity.Property(e => e.BannerLink);
                entity.Property(e => e.CountView);
            });

            #endregion

            #region Calendar
            modelBuilder.Entity<Calendar>(entity =>
            {
                entity.ToTable("calendar");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("CalendarID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.EventDate).HasColumnType("datetime");
                entity.Property(e => e.EventEndDate).HasColumnType("datetime");
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

             modelBuilder.Entity<CalendarDetail>(entity =>
             {
                 entity.ToTable("calendar_detail");
                 entity.HasKey(e => e.CalendarDetailID);
                 entity.Property(e => e.CalendarDetailID);
                 entity.Property(e => e.CalendarID);
                 entity.Property(e => e.LangCode);
                 entity.Property(e => e.EventName);
                 entity.Property(e => e.EventDescription);
                 entity.Property(e => e.EventLink);
                 entity.Property(e => e.EventPlace);
             });

            #endregion

            #region Posts
            modelBuilder.Entity<Posts>(entity =>
            {
                entity.ToTable("posts");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("PostID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.OpenComment);
                entity.Property(e => e.OpenRating);
                entity.Property(e => e.MemberOnly);
                entity.Property(e => e.Recommended);
                entity.Property(e => e.Rating);
                entity.Property(e => e.Views);
                entity.Property(e => e.RelatedPost);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<PostsDetail>(entity =>
            {
                entity.ToTable("posts_detail");
                entity.HasKey(e => e.PostDetailID);
                entity.Property(e => e.PostDetailID);
                entity.Property(e => e.PostID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Title);
                entity.Property(e => e.ShortDescriptions);
                entity.Property(e => e.Descriptions);
                entity.Property(e => e.UrlLink);
                entity.Property(e => e.TagKeywords);
                entity.Property(e => e.Views);
            });

            modelBuilder.Entity<PostsTag>(entity =>
            {
                entity.ToTable("posts_tag");
                entity.HasKey(e => e.TagID);
                entity.Property(e => e.TagID);
                entity.Property(e => e.TagName);
                entity.Property(e => e.LangID);
            });
            #endregion

            #region DownloadForm
            modelBuilder.Entity<DownloadForm>(entity =>
            {
                entity.ToTable("download_form");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("DownloadFormID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<DownloadFormDetail>(entity =>
            {
                entity.ToTable("download_form_detail");
                entity.HasKey(e => e.DownloadFormDetailID);
                entity.Property(e => e.DownloadFormDetailID);
                entity.Property(e => e.DownloadFormID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.DownloadFormName);
                entity.Property(e => e.DownloadFormDescription);
                entity.Property(e => e.DownloadFormLink);
            });
            #endregion

            #region JobPosition
            modelBuilder.Entity<JobPosition>(entity =>
            {
                entity.ToTable("jobs_position");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("JobPositionID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<JobPositionDetail>(entity =>
            {
                entity.ToTable("jobs_position_detail");
                entity.HasKey(e => e.JobPositionDetailID);
                entity.Property(e => e.JobPositionDetailID);
                entity.Property(e => e.JobPositionID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.JobPositionName);
                entity.Property(e => e.JobPositionDescription);
                entity.Property(e => e.JobPositionLink);
            });
            #endregion

            #region Faq
            modelBuilder.Entity<Faq>(entity =>
            {
                entity.ToTable("faq");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("FaqID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<FaqDetail>(entity =>
            {
                entity.ToTable("faq_detail");
                entity.HasKey(e => e.FaqDetailID);
                entity.Property(e => e.FaqDetailID);
                entity.Property(e => e.FaqID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Question);
                entity.Property(e => e.Answer);
            });
            #endregion

            #region Contacts
            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("contacts");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("ContactID");
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });
            modelBuilder.Entity<ContactForm>(entity =>
            {
                entity.ToTable("contacts_form");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("ContactFormID");
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Fullname);
                entity.Property(e => e.Telephone);
                entity.Property(e => e.Email);
                entity.Property(e => e.Message);
                entity.Property(e => e.MessageReply);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<ContactDetail>(entity =>
            {
                entity.ToTable("contacts_detail");
                entity.HasKey(e => e.ContactDetailID);
                entity.Property(e => e.ContactDetailID);
                entity.Property(e => e.ContactID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.InstitutionAddress);
                entity.Property(e => e.InstitutionEmail);
                entity.Property(e => e.InstitutionFax);
                entity.Property(e => e.InstitutionTel);
                entity.Property(e => e.InstitutionTitle);
                entity.Property(e => e.institutionSarabun);
                entity.Property(e => e.GoogleMap);
            });
            #endregion

            #region RSSFeed
            modelBuilder.Entity<RSSFeed>(entity =>
            {
                entity.ToTable("rss_feed");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("RSSFeedID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<RSSFeedDetail>(entity =>
            {
                entity.ToTable("rss_feed_detail");
                entity.HasKey(e => e.RSSFeedDetailID);
                entity.Property(e => e.RSSFeedDetailID);
                entity.Property(e => e.RSSFeedID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.RSSFeedName);
                entity.Property(e => e.RSSFeedDescription);
                entity.Property(e => e.RSSFeedLink);
            });
            #endregion

            #region SocialLink
            modelBuilder.Entity<SocialLink>(entity =>
            {
                entity.ToTable("social_media_link");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("SocialID");
                entity.Property(e => e.SocialIcon);
                entity.Property(e => e.SocialIcon2);
                entity.Property(e => e.SocialName);
                entity.Property(e => e.SocialURL);
                entity.Property(e => e.SocialStyle);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });
            #endregion

            #region Category

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("CategoryID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.NameTH);
                entity.Property(e => e.NameEN);
                entity.Property(e => e.EmailTo);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.RSSUrl);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            #endregion

            #region Staff
            modelBuilder.Entity<Staff>(entity =>
            {
                entity.ToTable("staffs");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("StaffID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.Levels);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<StaffDetail>(entity =>
            {
                entity.ToTable("staffs_detail");
                entity.HasKey(e => e.StaffDetailID);
                entity.Property(e => e.StaffDetailID);
                entity.Property(e => e.StaffID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Fullname);
                entity.Property(e => e.Description);
                entity.Property(e => e.Telephone);
                entity.Property(e => e.Email);
                entity.Property(e => e.Position);
            });
            #endregion

            #region Complaint
            modelBuilder.Entity<ComplaintType>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("complaint_type");
                entity.Property(e => e.ID).HasColumnName("ComplainTypeID");
                entity.Property(e => e.NameTH);
                entity.Property(e => e.NameEN);
                entity.Property(e => e.EmailNotification);
                entity.Property(e => e.Actived);
                entity.Property(e => e.Deleted);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.IsDirector);
            });
            modelBuilder.Entity<Complaint>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("complaint");
                entity.Property(e => e.ID).HasColumnName("ComplaintID");
                entity.Property(e => e.ComplainTypeID);
                entity.Property(e => e.Name);
                entity.Property(e => e.Division);
                entity.Property(e => e.Email);
                entity.Property(e => e.Title);
                entity.Property(e => e.Description);
                entity.Property(e => e.CurrentAddr);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.CountryID);
                entity.Property(e => e.ProvinceID);
                entity.Property(e => e.ResponseMessage);

                //entity.Property(e => e.Actived);
                //entity.Property(e => e.Deleted);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);

            });
            #endregion

            #region Webboard
            modelBuilder.Entity<WebboardReservedWord>(entity =>
            {
                entity.ToTable("wb_reserveword");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("ReservedWordID");
                entity.Property(e => e.OffensiveWord);
                entity.Property(e => e.ReplacementWord);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<WebboardTopic>(entity =>
            {
                entity.ToTable("wb_post");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("WBPostID");
                entity.Property(e => e.WBCategoryID);
                entity.Property(e => e.Topic);
                entity.Property(e => e.LangCode);

                entity.Property(e => e.Description);

                entity.Property(e => e.Pined);
                entity.Property(e => e.Views);

                entity.Property(e => e.ReplyCount);
                entity.Property(e => e.TIPAddress).HasColumnName("IPAddress");
                entity.Property(e => e.DeviceName);
                entity.Property(e => e.AliasName);
                entity.Property(e => e.Keywords);
                entity.Property(e => e.RejectReason);
                entity.Property(e => e.ForbiddenReason);

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Approved);
                entity.Property(e => e.Approver);
                entity.Property(e => e.ApprovedDate).HasColumnType("datetime");

                entity.Property(e => e.Forbidden);
                entity.Property(e => e.Stopper);
                entity.Property(e => e.ForbiddenDate).HasColumnType("datetime");

                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
                entity.Property(e => e.TopicTypeID);
            });
            modelBuilder.Entity<WebboardReply>(entity =>
            {
                entity.ToTable("wb_postans");
                entity.HasKey(e => e.WBReplyID);
                entity.Property(e => e.WBReplyID).HasColumnName("WBReplyID");
                entity.Property(e => e.WBPostID);
                entity.Property(e => e.Description);

                entity.Property(e => e.Stopper);

                entity.Property(e => e.TIPAddress).HasColumnName("IPAddress");
                entity.Property(e => e.DeviceName);
                entity.Property(e => e.AliasName);
                entity.Property(e => e.RejectReason);
                entity.Property(e => e.ForbiddenReason);

                entity.Property(e => e.Forbidden);
                entity.Property(e => e.Stopper);
                entity.Property(e => e.ForbiddenDate).HasColumnType("datetime");

                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<TopicType>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("topic_type");
                entity.Property(e => e.ID).HasColumnName("TopicTypeID");
                entity.Property(e => e.NameTH);
                entity.Property(e => e.NameEN);
                entity.Property(e => e.Actived);
                entity.Property(e => e.Deleted);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
            });

            #endregion
           
            #region ChannelComment
            modelBuilder.Entity<ChannelComment>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("channel_comment");
                entity.Property(e => e.ID).HasColumnName("ChannelCommentID");
                entity.Property(e => e.LangCode);
                entity.Property(e => e.Fullname);
                entity.Property(e => e.Telephone);
                entity.Property(e => e.Email);
                entity.Property(e => e.Comment);
                entity.Property(e => e.Address);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
                entity.Property(e => e.TypeID);
            });

            modelBuilder.Entity<ChannelCommentType>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("channel_comment_type");
                entity.Property(e => e.ID).HasColumnName("ChannelCommentTypeID");

                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });

            #endregion ChannelComment

            #region Projects
            modelBuilder.Entity<Projects>(entity =>
            {
                entity.ToTable("projects");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnName("ProjectID");
                entity.Property(e => e.MenuID);
                entity.Property(e => e.CategoryID);
                entity.Property(e => e.Sequence);
                entity.Property(e => e.Actived);
                entity.Property(e => e.CreatedBy);
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
                entity.Property(e => e.UpdatedBy);
                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
                entity.Property(e => e.Deleted);
            });
            modelBuilder.Entity<ProjectsDetail>(entity =>
            {
                entity.ToTable("projects_detail");
                entity.HasKey(e => e.ProjectDetailID);
                entity.Property(e => e.ProjectID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.ProjectName);
                entity.Property(e => e.Descriptions);
                entity.Property(e => e.Hours);
                entity.Property(e => e.Contacts);
                entity.Property(e => e.Department);
                entity.Property(e => e.Others);
            });
            #endregion



            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
