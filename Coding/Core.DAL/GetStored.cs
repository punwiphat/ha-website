﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;
using Core.library;
using System.Threading.Tasks;
using System.Data.Common;

namespace Core.DAL.Models
{
    public static class GetStoredMySql
    {
        public static DbCommand LoadStoredProc(this DbContext context, string storedProcName)
        {
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }
        public static DbCommand WithSqlParam(this DbCommand cmd, string paramName, object paramValue)
        {
            if (string.IsNullOrEmpty(cmd.CommandText))
                throw new InvalidOperationException(
                  "Call LoadStoredProc before using this method");
            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.Value = paramValue;
            cmd.Parameters.Add(param);
            return cmd;
        }

        //public static async Task<List<T>> ExecuteStoredProc<T>(this DbCommand command)
        //{
        //    using (command)
        //    {
        //        if (command.Connection.State == System.Data.ConnectionState.Closed)
        //            command.Connection.Open();
        //        try
        //        {
        //            using (var reader = await command.ExecuteReaderAsync())
        //            {
        //                return reader.MapToList<T>();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            throw (e);
        //        }
        //        finally
        //        {
        //            command.Connection.Close();
        //        }
        //    }
        //}


        public static IDataReader ExecuteReaderPro(string nameProd, MySqlParameter[] param)
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = nameProd;
                command.CommandType = CommandType.StoredProcedure;
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                foreach (MySqlParameter r in param)
                {
                    command.Parameters.Add(r);
                }
                IDataReader reader = command.ExecuteReader();
                return reader;
            }
        }
        public static List<T> GetAllStored<T>(string nameProd, MySqlParameter[] parameter) where T : new()
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = nameProd;
                command.CommandType = CommandType.StoredProcedure;
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                foreach (MySqlParameter r in parameter)
                {
                    command.Parameters.Add(r);
                }
                DataTable dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    dt.Load(reader);
                }
                return dt.ToList<T>();
            }
        }
        public static List<T> GetAllStoredNonparam<T>(string nameProd) where T : new()
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = nameProd;
                command.CommandType = CommandType.StoredProcedure;
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                DataTable dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    dt.Load(reader);
                }
                return dt.ToList<T>();
            }
        }
        public static List<T> GetQuery<T>(string SQLcommand) where T : new()
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = SQLcommand;
                command.CommandType = CommandType.Text;
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                DataTable dt = new DataTable();
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    dt.Load(reader);
                }
                return dt.ToList<T>();
            }
        }
        public static string ExecuteReaderPro_ref(string nameProd, MySqlParameter[] parameter, string Returnparameter)
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = nameProd;
                command.CommandType = CommandType.StoredProcedure;
                foreach (MySqlParameter r in parameter)
                {
                    command.Parameters.Add(r);
                }
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                IDbDataParameter _Parameter = command.CreateParameter();
                _Parameter.ParameterName = Returnparameter;
                _Parameter.Direction = System.Data.ParameterDirection.Output;
                _Parameter.DbType = System.Data.DbType.String;
                _Parameter.Size = 50;
                command.Parameters.Add(_Parameter);
                command.ExecuteNonQuery();
                return _Parameter.Value.ToString();
            }
        }
        public static bool Executenon(string nameProd, MySqlParameter[] parameter)
        {
            using (var Map = new CMSdbContext())
            {
                IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                command.CommandText = nameProd;
                command.CommandType = CommandType.StoredProcedure;
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                foreach (MySqlParameter r in parameter)
                {
                    command.Parameters.Add(r);
                }
               
                command.ExecuteNonQuery();
                return true;
            }
        }
        public static void ExecuteCommand(string SqlCommand)
        {
            try
            {
                using (var Map = new CMSdbContext())
                {
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = SqlCommand;
                    command.CommandType = CommandType.Text;
                    command.CommandTimeout = 0;
                    if (command.Connection.State == System.Data.ConnectionState.Closed)
                        command.Connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

    }
}
