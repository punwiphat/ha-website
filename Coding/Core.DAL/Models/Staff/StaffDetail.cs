﻿using Core.library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace Core.DAL.Models
{
    public partial class StaffDetail : ExMenu
    {
        public int StaffDetailID { get; set; }

        public int StaffID { get; set; }

        public string LangCode { get; set; }

        [NotMapped]
        public string DepartmentName { get; set; }

        public string Fullname { get; set; }

        public string Description { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public string Position { get; set; }

        [NotMapped]
        public virtual string CategoryName { get; set; }

        [NotMapped]
        public int Sequence { get; set; }

        [NotMapped]
        public bool Actived { get; set; }

        [NotMapped]
        public DateTime UpdatedDate { get; set; }

        [NotMapped]
        public string UpdatedBy { get; set; }

        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }

        [NotMapped]
        public string TitleName { get; set; }

        [NotMapped]
        public long Levels { get; set; }

        [NotMapped]
        public string Levels_str { get; set; }

        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public List<MediaFiles> lstFiles { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }

        [NotMapped]
        public virtual string FileName { get; set; }

        [NotMapped]
        public virtual string FilePath { get; set; }

        [NotMapped]
        public virtual string OriginalFileName { get; set; }

        [NotMapped]
        public virtual List<MediaFiles> lstFilesAttach { get; set; }

        [NotMapped]
        public virtual int IsClickDelImg { get; set; }

        [NotMapped]
        public virtual IFormFile Files2 { get; set; }

        [NotMapped]
        public virtual int FileID2 { get; set; }

        [NotMapped]
        public virtual string FileName2 { get; set; }

        [NotMapped]
        public virtual string FilePath2 { get; set; }

        [NotMapped]
        public virtual string OriginalFileName2 { get; set; }
        [NotMapped]
        public virtual int IsClickDelImg2 { get; set; }

    }
    public class StaffByLanguage : Languages
    {
        public virtual List<StaffDetail> lstStaff { get; set; }
        public virtual int MenuDisplayID { get; set; }
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        public virtual string CheckedSortingType
        {
            get
            {
                return SortingType ? "checked=\"checked\"" : "";
            }
        }

    }
}
