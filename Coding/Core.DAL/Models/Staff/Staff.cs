﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Staff : BaseModel
    {
        public int ID { get; set; }
        [NotMapped]
        public int MenuIDCategory { get; set; }

        public int CategoryID { get; set; }
        // [NotMapped]
        public int MenuID { get; set; }
        // public int? DepartmentID { get; set; }
        
        public int? Levels { get; set; }
     
        public int Sequence { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public virtual List<StaffDetail> lstStaff { get; set; }
        
    }

}