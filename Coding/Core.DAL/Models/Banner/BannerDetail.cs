﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;
using Microsoft.AspNetCore.Http;

namespace Core.DAL.Models
{
    public partial class BannerDetail : ExMenu
    {
        public int BannerDetailID { get; set; }
        public int BannerID { get; set; }
        public string LangCode { get; set; }

        [NotMapped]
        public int CategoryID { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        public string BannerName { get; set; }
        public string BannerDescription { get; set; }
        public string BannerLink { get; set; }
        [NotMapped]
        public int Sequence { get; set; }
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
        //[NotMapped]
        public int CountView { get; set; }
        [NotMapped]
        public virtual IFormFile FileAttach { get; set; }

        [NotMapped]
        public virtual int FileAttachID { get; set; }

        [NotMapped]
        public virtual string FileAttachName { get; set; }

        [NotMapped]
        public virtual string FileAttachPath { get; set; }

        [NotMapped]
        public virtual string OriginalFileAttachName { get; set; }
    }
    //public class BannerByLanguage : Languages
    //{
    //    public virtual List<BannerDetail> lstBanner { get; set; }
    //    public virtual int MenuDisplayID { get; set; }
    //    public virtual int SortingBy { get; set; }
    //    public virtual bool SortingType { get; set; }
    //    public virtual string SelectedTabs { get; set; }
    //    public virtual string CheckedSortingType
    //    {
    //        get
    //        {
    //            return SortingType ? "checked=\"checked\"" : "";
    //        }
    //    }

    //}
}
