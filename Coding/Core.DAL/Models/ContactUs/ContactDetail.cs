﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;
using Microsoft.AspNetCore.Http;

namespace Core.DAL.Models
{
    public partial class ContactDetail : ExMenu
    {
        public int ContactDetailID { get; set; }

        public int ContactID { get; set; }

        public string LangCode { get; set; }

        public string InstitutionTitle { get; set; }

        public string InstitutionAddress { get; set; }

        public string InstitutionFax { get; set; }

        public string InstitutionTel { get; set; }

        public string InstitutionEmail { get; set; }

        public string institutionSarabun { get; set; }

        public string GoogleMap { get; set; }

        [NotMapped]
        public DateTime UpdatedDate { get; set; }

        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }

        [NotMapped]
        public virtual string FileName { get; set; }

        [NotMapped]
        public virtual string FilePath { get; set; }

        [NotMapped]
        public virtual string OriginalFileName { get; set; }

    }
    public class ContactByLanguage : Languages
    {
        public virtual List<ContactDetail> lstContact { get; set; }
        public virtual string SelectedTabs { get; set; }
        
    }
}
