﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Contact : BaseModel
    {
        public int ID { get; set; }
        [NotMapped]
        public int MenuID { get; set; }
        [NotMapped]
        public virtual List<ContactDetail> lstContact { get; set; }
    }
}