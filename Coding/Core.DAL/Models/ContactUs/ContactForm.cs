﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class ContactForm : BaseModel
    {
        public int ID { get; set; }

        [NotMapped]
        public int MenuID { get; set; }

        public string LangCode { get; set; }

        public string Fullname { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public string Faculty { get; set; }

        public string Majors { get; set; }

        public string TypePersonals { get; set; }

        public string Message { get; set; }

        public string MessageReply { get; set; }

        public bool Deleted { get; set; }


    }
}