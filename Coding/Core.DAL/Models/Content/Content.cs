﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Content : BaseModel
    {
        public int ID { get; set; }
        public int MenuID { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
        [NotMapped]
        public virtual List<ContentDetail> lstContent { get; set; }
       
    }
}
