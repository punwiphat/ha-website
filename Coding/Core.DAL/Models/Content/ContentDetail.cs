﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
namespace Core.DAL.Models
{
    public partial class ContentDetail : ExMenu
    {
        public int ContentDetailID { get; set; }

        public int ContentID { get; set; }

        public string LangCode { get; set; }

        public string Title { get; set; }

        public string Descriptions { get; set; }

        public string HtmlContent { get; set; }

        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public List<MediaFiles> lstFiles { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }

        [NotMapped]
        public virtual string FileName { get; set; }

        [NotMapped]
        public virtual string FilePath { get; set; }

        [NotMapped]
        public virtual string OriginalFileName { get; set; }

        [NotMapped]
        public virtual List<MediaFiles> Medialst { get; set; }

    }
    
}
