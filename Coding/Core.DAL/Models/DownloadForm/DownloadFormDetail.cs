﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;
using Microsoft.AspNetCore.Http;

namespace Core.DAL.Models
{
    public partial class DownloadFormDetail : ExMenu
    {
        public int DownloadFormDetailID { get; set; }
        public int DownloadFormID { get; set; }
        public string LangCode { get; set; }
        public string DownloadFormName { get; set; }
        public string DownloadFormDescription { get; set; }
        public string DownloadFormLink { get; set; }
        [NotMapped]
        public int CategoryID { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        [NotMapped]
        public int Sequence { get; set; }
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public virtual IFormFile Files { get; set; }
        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
        [NotMapped]
        public virtual string FileExtension { get; set; }
        [NotMapped]
        public virtual float FileSize { get; set; }
        [NotMapped]
        public virtual SettingFiles LimitFile { get; set; }
       

        [NotMapped]
        public List<MediaFiles> lstFilesAttach { get; set; }
        [NotMapped]
        public bool IsRSS { get; set; }
    }
    public class DownloadFormByLanguage : Languages
    {
        public virtual List<DownloadFormDetail> lstDownloadForm { get; set; }
        public virtual int MenuDisplayID { get; set; }
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        public virtual string CheckedSortingType
        {
            get
            {
                return SortingType ? "checked=\"checked\"" : "";
            }
        }

    }
}
