﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;

namespace Core.DAL.Models
{
    public partial class FaqDetail 
    {
        public int FaqDetailID { get; set; }

        public int FaqID { get; set; }

        public string LangCode { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }

        [NotMapped]
        public int CategoryID { get; set; }

        [NotMapped]
        public int Sequence { get; set; }

        [NotMapped]
        public bool Actived { get; set; }

        [NotMapped]
        public DateTime UpdatedDate { get; set; }

        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
    }
    public class FaqByLanguage : Languages
    {
        public virtual List<FaqDetail> lstFaq { get; set; }
        public virtual int MenuDisplayID { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        public virtual string CheckedSortingType
        {
            get
            {
                return SortingType ? "checked=\"checked\"" : "";
            }
        }

    }
}
