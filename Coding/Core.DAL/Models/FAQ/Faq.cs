﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Faq : BaseModel
    {
        public int ID { get; set; }
        public int MenuID { get; set; }
        public int CategoryID { get; set; }

        public int Sequence { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public virtual List<FaqDetail> lstFaq { get; set; }
    }

}