﻿using System;

namespace Core.DAL.Models
{
    public partial class PostsTag
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
        public string LangID { get; set; }
    }
}
