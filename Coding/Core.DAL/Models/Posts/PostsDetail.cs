﻿using Microsoft.AspNetCore.Mvc;
using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace Core.DAL.Models
{
    public partial class PostsDetail : ExMenu
    {
        public int PostDetailID { get; set; }
        public int PostID { get; set; }
        public string LangCode { get; set; }
        public string Title { get; set; }
        public string ShortDescriptions { get; set; }
        public string Descriptions { get; set; }
        public string UrlLink { get; set; }
        public string TagKeywords { get; set; }
        public int Views { get; set; }
        [NotMapped]
        public virtual string Views_Str
        {
            get { return Views.ToString("#,##0"); }
        }
        [NotMapped]
        public virtual List<MediaFiles> MediaLst { get; set; }
        [NotMapped]
        public virtual List<FileStreamResult> Gallerylst { get; set; }
        [NotMapped]
        public List<MediaFiles> lstFiles { get; set; }
        [NotMapped]
        public List<MediaFiles> lstFilesAttach { get; set; }
        [NotMapped]
        public virtual IFormFile Files { get; set; }
        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
        [NotMapped]
        public int Sequence { get; set; }
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public virtual List<PostsDetail> lstRelate { get; set; }

        [NotMapped]
        public bool IsRSS { get; set; }
        [NotMapped]
        public List<string> TagsName
        {
            get { return !string.IsNullOrEmpty(TagKeywords) ? new List<string>(Array.ConvertAll(TagKeywords.Split(','), element => element.ToString())) : new List<string>(); }
            set { }
        }
    }
    public class PostsByLanguage : Languages
    {
        public virtual List<PostsDetail> PostDetailLst { get; set; }
        
        public virtual int MenuDisplayID { get; set; }
       
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
    }
}
