﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class ComplaintType : BaseModel
    {

        public int ID { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public string EmailNotification { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
        public bool IsDirector { get; set; }
    }
}
