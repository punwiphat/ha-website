using System.Collections.Generic;
using System.Text;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace Core.DAL.Models
{
    public class Complaint : BaseModel
    {
        
        public virtual int ID { get; set; }
        [NotMapped]
        public int MenuID { get; set; }
        public virtual int ComplainTypeID { get; set; }
        [NotMapped]
        public virtual string ComplainTypeName { get; set; }
        public virtual string Name { get; set; }
        public virtual string Division { get; set; }
        public virtual string Email { get; set; }
        public virtual string CurrentAddr { get; set; }
        
        //public virtual string Address { get; set; }
        public virtual int? ProvinceID { get; set; }
        public virtual int? CountryID { get; set; }
        [NotMapped]
        public virtual string Province { get; set; }
        [NotMapped]
        public virtual string Country { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string LangCode { get; set; }
        public virtual string ResponseMessage { get; set; }
        [NotMapped]
        public virtual bool Responsed
        {
            get { return !string.IsNullOrEmpty(ResponseMessage); }
        }
        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
        [NotMapped]
        public virtual ContactDetail ObjContact { get; set; }
    }
}
