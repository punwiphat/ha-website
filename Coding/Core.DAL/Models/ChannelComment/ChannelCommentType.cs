﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class ChannelCommentType : BaseModel
    {
        public int ID { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }

        [NotMapped]
        public int MenuID { get; set; }

    }
}