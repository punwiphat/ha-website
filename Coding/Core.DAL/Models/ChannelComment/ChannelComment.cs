﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
namespace Core.DAL.Models
{
    public partial class ChannelComment : BaseModel
    {
        public int ID { get; set; }
        public String LangCode { get; set; }
        public String Fullname { get; set; }
        public String Email { get; set; }
        public String Telephone { get; set; }
        public String Comment { get; set; }
        public String Address { get; set; }
        public bool Deleted { get; set; }
        public int TypeID { get; set; }

        [NotMapped]
        public int MenuID { get; set; }
        [NotMapped]
        public string TypeName { get; set; }

        [NotMapped]
        public virtual IFormFile Files { get; set; }

        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }


    }
}