﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardReports
    {
        public Guid Webboard_Reports_Id { get; set; }
        public Guid Webboard_Topic_Id { get; set; }



        public string Description { get; set; }
        public string IpAddress { get; set; }
        public string DeviceName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        [NotMapped]
        public virtual string CreatedDate_Str
        {
            get { return CreatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreatedDate)) : ""; }
        }
    }
}
