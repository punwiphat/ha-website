﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardReservedWord :BaseModel
    {
        public int ID { get; set; }
        public string OffensiveWord { get; set; }
        public string ReplacementWord { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
    }
}
