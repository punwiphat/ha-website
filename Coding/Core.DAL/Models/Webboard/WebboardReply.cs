﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardReply :BaseModel
    {
        public int WBReplyID { get; set; }
        public int WBPostID { get; set; }
        public string Description { get; set; }
        public string TIPAddress { get; set; }
        public string DeviceName { get; set; }
        public string AliasName { get; set; }
        public string RejectReason { get; set; }
        public string ForbiddenReason { get; set; }
        public bool Forbidden { get; set; }
        public string Stopper { get; set; }
        [NotMapped]
        public string LangCode { get; set; }
        public DateTime? ForbiddenDate { get; set; }
        [NotMapped]
        private string TempForbiddenDate { get; set; }
        [NotMapped]
        public virtual string ForbiddenDate_str
        {
            get { return (ForbiddenDate != null) ? ForbiddenDate.ToDate() : TempForbiddenDate; }
            set { TempForbiddenDate = value; }
        }
        public bool Deleted { get; set; }
        public bool Actived { get; set; }
        [NotMapped]
        public WebboardTopic objTopic { get; set; }
        
    }
   
}
