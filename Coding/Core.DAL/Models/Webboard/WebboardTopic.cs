﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardTopic :BaseModel
    {
        public int ID { get; set; }
        public int WBCategoryID { get; set; }
        [NotMapped]
        public int MenuID { get; set; }
        
        public string LangCode { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public bool Pined { get; set; }
        public int Views { get; set; }
        public int? ReplyCount { get; set; }

        public string TIPAddress { get; set; }
        public string DeviceName { get; set; }
        public string AliasName { get; set; }

        public string Keywords { get; set; }
        public string RejectReason { get; set; }
        public string ForbiddenReason { get; set; }
        public int Sequence { get; set; }
        
        public DateTime? EffectiveDate { get; set; }
        [NotMapped]
        private string TempEffectiveDate { get; set; }
        [NotMapped]
        public virtual string EffectiveDate_str
        {
            get { return (EffectiveDate != null) ? EffectiveDate.ToDate() : TempEffectiveDate; }
            set { TempEffectiveDate = value; }
        }

        public DateTime? EndDate { get; set; }
        [NotMapped]
        private string TempEndDate { get; set; }
        [NotMapped]
        public virtual string EndDate_str
        {
            get { return (EndDate != null) ? EndDate.ToDate() : TempEndDate; }
            set { TempEndDate = value; }
        }


        public bool Approved { get; set; }
        public string Approver { get; set; }


        public DateTime? ApprovedDate { get; set; }
        [NotMapped]
        private string TempApprovedDate { get; set; }
        [NotMapped]
        public virtual string ApprovedDate_str
        {
            get { return (ApprovedDate != null) ? ApprovedDate.ToDate() : TempApprovedDate; }
            set { TempApprovedDate = value; }
        }

        public bool Forbidden { get; set; }
        public string Stopper { get; set; }


        public DateTime? ForbiddenDate { get; set; }
        [NotMapped]
        private string TempForbiddenDate { get; set; }
        [NotMapped]
        public virtual string ForbiddenDate_str
        {
            get { return (ForbiddenDate != null) ? ForbiddenDate.ToDate() : TempForbiddenDate; }
            set { TempForbiddenDate = value; }
        }
        [NotMapped]
        public List<WebboardReply> lstReply { get; set; }
        public bool Deleted { get; set; }
        public bool Actived { get; set; }

        public int TopicTypeID { get; set; }

        [NotMapped]
        public string TopicTypeName { get; set; }


    }
    public class WBTopicByLanguage : Languages
    {
        public virtual List<WebboardTopic> lstWBTopic { get; set; }
        public virtual int MenuDisplayID { get; set; }
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        public virtual string CheckedSortingType
        {
            get
            {
                return SortingType ? "checked=\"checked\"" : "";
            }
        }

    }
}
