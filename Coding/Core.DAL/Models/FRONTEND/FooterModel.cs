﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class FooterModel
    {
        public ContactDetail obj_contact { get; set; }
        public List<Tm_Menu> list_Policy { get; set; }
        public List<SocialLink> list_Social { get; set; }
        public List<Tm_Menu> list_Menu { get; set; }
    }
}
