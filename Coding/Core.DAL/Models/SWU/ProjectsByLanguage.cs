using System.Collections.Generic;
namespace Core.DAL.Models
{
    public partial class ProjectsByLanguage : Languages
	{
		public virtual List<ProjectsDetail> lstProjects { get; set; }

		public virtual int MenuDisplayID { get; set; }

		public virtual int SortingBy { get; set; }

		public virtual bool SortingType { get; set; }

		public virtual string SelectedTabs { get; set; }

		public virtual string CheckedSortingType => SortingType ? "checked=\"checked\"" : "";
	}
}