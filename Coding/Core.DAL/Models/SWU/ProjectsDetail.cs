using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;

namespace Core.DAL.Models
{
    public partial class ProjectsDetail : ExMenu
	{
		public int ProjectDetailID { get; set; }

		public int ProjectID { get; set; }

		public string LangCode { get; set; }

		public string ProjectName { get; set; }

		public string Descriptions { get; set; }

		public string Hours { get; set; }

		public string Contacts { get; set; }

		public string Department { get; set; }

		public string Others { get; set; }

		[NotMapped]
		public int Sequence { get; set; }

		[NotMapped]
		public bool Actived { get; set; }

		[NotMapped]
		public DateTime UpdatedDate { get; set; }

		[NotMapped]
		public string UpdatedBy { get; set; }

		[NotMapped]
		public virtual string UpdatedDate_Str
		{
		   get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
		}

		[NotMapped]
		public string CategoryName { get; set; }
        [NotMapped]
        public int CategoryID { get; set; }
        [NotMapped]
		public List<MediaFiles> lstFiles { get; set; }

		[NotMapped]
		public virtual List<MediaFiles> lstFilesAttach { get; set; }

		[NotMapped]
		public virtual int IsClickDelImg { get; set; }
	}
}