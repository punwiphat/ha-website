﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class SettingFiles:BaseModel
    {
        public int ID { get; set; }
        public string AllowTypeFile { get; set; }
        public int MaxSizeFile { get; set; }
        public int SizeFile { get; set; }
        public int MaxQuantityFile { get; set; }
        [NotMapped]
        public List<int> AllowTypeFileID
        {
            get { return !string.IsNullOrEmpty(AllowTypeFile) ? new List<int>(Array.ConvertAll(AllowTypeFile.Split(','), int.Parse)) : new List<int>(); }
            set { }
        }
        [NotMapped]
        public virtual string AllowFileSet
        {
            get
            {
                string allowfile = "";
                int i = 0;
                foreach (var item in AllowTypeFileID)
                {
                    if(i>0)
                        allowfile += ",";
                    switch (item)
                    {
                        case 1:
                            allowfile += "'jpg', 'jpeg', 'png', 'gif','svg'";
                            break;
                        case 2:
                            allowfile += "'pdf'";
                            break;
                        case 3:
                            allowfile += "'doc','docx'";
                            break;
                        case 4:
                            allowfile += "'xls','xlsx'";
                            break;
                        case 5:
                            allowfile += "'csv'";
                            break;
                        case 6:
                            allowfile += "'ppt','pptx'";
                            break;
                        case 7:
                            allowfile += "'zip'";
                            break;
                        case 8:
                            allowfile += "'txt'";
                            break;
                    }
                    i++;
                }
                return allowfile;
            }
        }
    }
}
