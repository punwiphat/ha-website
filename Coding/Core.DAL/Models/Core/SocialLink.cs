﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class SocialLink : BaseModel
    {
        public int ID { get; set; }
        [NotMapped]
        public int MenuID { get; set; }
        public string SocialIcon { get; set; }
        public string SocialIcon2 { get; set; }
        public string SocialName { get; set; }
        public string SocialURL { get; set; }
        public string SocialStyle { get; set; }
        public bool Actived { get; set; }
    }
}