﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class SettingEmail
    {
        public int EmailId { get; set; }

        public string EmailAlias { get; set; }

        public string Emailname { get; set; }

        public string Smtp { get; set; }

        public int Port { get; set; }

        public string Password { get; set; }

        public bool UseDefaultCredentials { get; set; }

        public bool EnabledSSL { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
    }
}
