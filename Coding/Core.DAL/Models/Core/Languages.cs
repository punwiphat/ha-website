﻿namespace Core.DAL.Models
{
    public partial class Languages
    {
        public int LanguageID { get; set; }
        public string LanguageFlag { get; set; } 
        public string LanguageName { get; set; } 
        public string LanguageCode { get; set; } 
        public string LanguageCulture { get; set; } 
        public int Sequence { get; set; }
        public bool Actived { get; set; }
    }
}
