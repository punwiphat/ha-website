﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class SystemConfigs
    {
        public Guid SystemConfigId { get; set; }
        public string SystemConfigCode { get; set; }
        public int SystemConfigValue { get; set; }
        public string SystemConfigDescriptions { get; set; }
        public bool Activated { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string SystemConfigType { get; set; }
        public int System_Config_Category { get; set; }
        public string System_Config_Remark { get; set; }
        [NotMapped]
        public virtual string CreatedDate_Str
        {
            get { return CreatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreatedDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
    }
}
