﻿namespace Core.DAL.Models
{
    public partial class EmailTemplate : BaseModel
    {
        public int TemplateID { get; set; }
        public int TemplateTypeID { get; set; }
        public string LangCode { get; set; }
        public string Subject { get; set; }
        public string Descriptions { get; set; }
        public string HTMLContent { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
    }
}
