﻿using System;

namespace Core.DAL.Models
{
    public partial class Role
    {
        public int RoleID { get; set; }
        public int RoleGroupID { get; set; }
        public int MenuID { get; set; }
        public string RoleType { get; set; }
    }
}
