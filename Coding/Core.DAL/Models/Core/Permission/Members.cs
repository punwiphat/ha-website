﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Members : BaseModel
    {
        public int MemberID { get; set; }

        public int TitleID { get; set; }

        public int IdentityType { get; set; }

        public string IdentityNo { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        [NotMapped]
        public string ConfirmPassword { get; set; }

        public string Department { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }

        public string Tel { get; set; }

        public string Address { get; set; }

        public string AddressSend { get; set; }

        public int TypeMemberID { get; set; }

        public bool Actived { get; set; }

        public bool Deleted { get; set; }

        public bool Activated { get; set; }

        public string KeyActivated { get; set; }
        [NotMapped]
        public string TypeMemberName { get; set; }
        [NotMapped]
        public string FullName
        {
            get { return String.Concat(FirstName, " ", LastName); }
        }

        [NotMapped]
        public string Msg { get; set; }

    }
}
