﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Users : BaseModel
    {
        public int UserID { get; set; }
        public int RoleGroupID { get; set; }
        //public string TitleNameCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9.]+$", ErrorMessage = "Username can only contain letters, numbers, and dots.")]
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public string RoleGroupName { get; set; }
        [NotMapped]
        public DateTime RoleGroupCreate { get; set; }
        [NotMapped]
        public string FullName
        {
            get { return String.Concat(FirstName, " ", LastName); }
        }

    }
}
