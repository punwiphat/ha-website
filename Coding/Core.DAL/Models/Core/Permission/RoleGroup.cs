﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class RoleGroup : BaseModel
    {

        public int RoleGroupID { get; set; }
        public string RoleGroupName { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
    }
}
