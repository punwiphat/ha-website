﻿using Core.library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace Core.DAL.Models
{
    public partial class ShortcutDetail : ExMenu
    {
        public int ShortcutDetailID { get; set; }
        public int ShortcutID { get; set; }
        public string LangCode { get; set; }
        public string ServiceName { get; set; }
        public string Permalink { get; set; }
        public string URL { get; set; }
        public int IconType { get; set; }
        public string IconName { get; set; }
        [NotMapped]
        public int Sequence { get; set; }
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public virtual IFormFile Files { get; set; }
        [NotMapped]
        public virtual int FileID { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }

    }
    public class ShortcutByLanguage : Languages
    {
        public virtual List<ShortcutDetail> lstDetail { get; set; }
        //public virtual int MenuDisplayID { get; set; }
        //public virtual int SortingBy { get; set; }
        //public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        //public virtual string CheckedSortingType
        //{
        //    get
        //    {
        //        return SortingType ? "checked=\"checked\"" : "";
        //    }
        //}

    }
}
