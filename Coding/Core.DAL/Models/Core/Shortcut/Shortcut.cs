﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Shortcut : BaseModel
    {
        public int ID { get; set; }
        [NotMapped]
        public int MainMenuID { get; set; }
        [NotMapped]
        public int? DDLMenuID { get; set; }
        public int MenuID { get; set; }
        public int ModuleID { get; set; }
        public int ParentID { get; set; }
        public int Sequence { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public virtual List<ShortcutDetail> lstDetail { get; set; }

    }

}