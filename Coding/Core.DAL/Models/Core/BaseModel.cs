﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;

namespace Core.DAL.Models
{
    public partial class BaseModel
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual Int64 RowNumber { get; set; }
        [NotMapped]
        public virtual string CreatedDate_Str
        {
            get { return CreatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreatedDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public string Menuname { get; set; }
        [NotMapped]
        public string GUID { get; set; }
       

    }
    public partial class ResponseJson
    {
        public bool Result { get; set; } = false;
        public string MessageError { get; set; }

    }
    public partial class ExMenu
    {
        [NotMapped]
        public virtual Int64 RowNumber { get; set; }
        [NotMapped]
        public int MenuID { get; set; }
        [NotMapped]
        public string Menuname { get; set; }
        [NotMapped]
        public string SUB_GUID { get; set; }

    }
    public class ddlmaster
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string NameEN { get; set; }

    }
    public class ddlmastername
    {
        public virtual int ID { get; set; }
        public virtual string NameTH { get; set; }
        public virtual string NameEN { get; set; }

    }
    public class ddlmastercode
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }

    }

    public class CountItem
    {
        public virtual Int64 CountRow { get; set; }
    }

    public class ItemConfigValue
    {
        public virtual string ConfigValue { get; set; }
    }
}
