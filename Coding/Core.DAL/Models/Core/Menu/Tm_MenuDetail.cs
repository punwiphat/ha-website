﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Core.DAL.Models
{

    public class Tm_MenuDetail 
    {
        public virtual int ID { get; set; }
        public virtual int MenuID { get; set; }
       // [Required(ErrorMessage = "*???????????????")]
        public virtual string Menuname { get; set; }
        public virtual string Permalink { get; set; }
        //public virtual int Sort_Front { get; set; }
        //public virtual int Sort_Back { get; set; }
        //public virtual int count_back { get; set; }
        //public virtual int count_front { get; set; }
        //public virtual int levels { get; set; }

        public virtual string LangCode { get; set; }
    }
}
