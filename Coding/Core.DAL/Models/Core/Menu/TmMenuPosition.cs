﻿using System;

namespace Core.DAL.Models
{
    public partial class TmMenuPosition
    {
        public Guid MenuPositionId { get; set; }
        public string PositionName { get; set; }
        public bool? Activated { get; set; }
        public bool? ForBackend { get; set; }
    }
}
