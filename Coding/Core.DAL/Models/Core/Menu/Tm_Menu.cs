using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{

    public class Tm_Menu : BaseModel
    {
        public virtual int ID { get; set; }
        
        public virtual int? BelongID { get; set; }
        [NotMapped]
        public virtual int CategoryID { get; set; }
        [NotMapped]
        public virtual int newMenuDownloadID { get; set; }
        
        public virtual int? MENU_FRONT_ID { get; set; }
        public virtual int? MENU_BACK_ID { get; set; }
        
        public virtual string MenuTypeCode { get; set; }
        public virtual string section_name_back { get; set; }
        public virtual string menu_name_back { get; set; }
        public virtual string permalink_back { get; set; }
        public virtual string controller_back { get; set; }
        public virtual string action_back { get; set; }
        public virtual bool self_back { get; set; }
        public virtual int? sort_back { get; set; }
        public virtual string icon_back { get; set; }
        public virtual bool? show_back { get; set; }
        public virtual string controller_front { get; set; }
        public virtual string action_front { get; set; }
        public virtual string link_front { get; set; }
        public virtual bool self_front { get; set; }
        public virtual int? sort_front { get; set; }
        public virtual string icon_front { get; set; }
        public virtual string img_front { get; set; }
        public virtual bool? chk_login_front { get; set; }
        public virtual bool? chk_popup_front { get; set; }
        public virtual bool show_front { get; set; }
        public virtual string permalink_front { get; set; }

        public virtual int? type_show { get; set; }
        public virtual int? levels { get; set; }
        public virtual bool Actived { get; set; }
        public virtual bool Deleted { get; set; }
        public virtual bool IsEdit { get; set; }
        public virtual bool Disabled { get; set; }

        [NotMapped]
        public virtual List<Tm_Menu> Sub_Menu { get;set;}
        [NotMapped]
        public virtual int Sequence { get; set; }
        [NotMapped]
        public virtual string LangCode { get; set; }
        [NotMapped]
        public virtual List<Tm_MenuDetail> lstobj { get; set; }
        [NotMapped]
        public virtual string ParentMenuname { get; set; }
        [NotMapped]
        public virtual string FPermalink { get; set; }
        [NotMapped]
        public virtual string MenuNameTH { get; set; }
        [NotMapped]
        //[RegularExpression("^((?!^Address$)[0-9A-Za-z #,])+$", ErrorMessage = "Menu Name is required and must be properly formatted.")]

        public virtual string MenuNameEN { get; set; }
        [NotMapped]
        public virtual string Role { get; set; }

    }
    public class DataReturn
    {
        public virtual string c_permalink { get; set; }

    }
    public class MenuGenBackoffice
    {
        public virtual string section_name_back { get; set; }
        public virtual List<Tm_Menu> lstobj { get; set; }

    }
    public class MenuType
    {
        public virtual int MenuTypeID { get; set; }
        public virtual string MenuTypeCode { get; set; }
        public virtual string MenuTypeName { get; set; }
        public virtual string Controller_Back { get; set; }
        public virtual string Action_Back { get; set; }
        public virtual string Controller_Front { get; set; }
        public virtual string Action_Front { get; set; }
    }
  
}
