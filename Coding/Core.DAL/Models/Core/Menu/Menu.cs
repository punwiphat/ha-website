﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class TmMenu
    {
        public int MenuID { get; set; }
        public int MenuTypeId { get; set; }
        public string MenuName { get; set; }
        public string Permalink { get; set; }
        public Guid? FrontParentId { get; set; }
        public string FrontController { get; set; }
        public string FrontAction { get; set; }
        public string FrontIcon { get; set; }
        public string FrontUrl { get; set; }
        public bool? FrontClickable { get; set; }
        public int? FrontSequence { get; set; }
        public int? FrontLevel { get; set; }
        public bool? FrontActiveted { get; set; }
        public Guid? BackParentId { get; set; }
        public string BackController { get; set; }
        public string BackAction { get; set; }
        public string BackIcon { get; set; }
        public string BackUrl { get; set; }
        public bool? BackClickable { get; set; }
        public int? BackSequence { get; set; }
        public int? BackLevel { get; set; }
        public bool? BackActiveted { get; set; }

        [NotMapped]
        public List<TmMenu> SubMenu { get; set; }
        [NotMapped]
        public string ActivebMenuName { get; set; }
    }
}
