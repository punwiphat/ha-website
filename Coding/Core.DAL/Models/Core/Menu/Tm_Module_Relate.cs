﻿using System;
using System.Text;
using System.Collections.Generic;


namespace Core.DAL.Models
{

    public class Tm_Module_Relate : BaseModel
    {
        public virtual int ModuleId { get; set; }
        public virtual string MenuID { get; set; }
        public virtual int Sort { get; set; }
    }
}
