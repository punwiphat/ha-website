﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;
namespace Core.DAL.Models
{
    public partial class MenuDisplay
    {
        public int ID { get; set; }
        public int MenuID { get; set; }
        public int CategoryID { get; set; }
        public int? SortingBy { get; set; }
        public bool? SortingType { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
    }
}
