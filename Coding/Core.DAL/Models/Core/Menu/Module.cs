﻿using System;
using System.Collections.Generic;

namespace Core.DAL.Models
{
    public partial class Module
    {
        public int ModuleId { get; set; }
        public string Code { get; set; }
        public string ModuleName { get; set; }
        public string ModuleNameEn { get; set; }
        public bool? IsActive { get; set; }
        public bool IsOnlyAdmin { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public bool? InReport { get; set; }
        public bool? IsNewReport { get; set; }
    }
}
