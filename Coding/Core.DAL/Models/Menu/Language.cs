using System.Collections.Generic; 
using System.Text; 
using System; 


namespace Core.DAL.Models
{

    public class Language : BaseModel
    {
        public Language() { }
        public virtual string LanguageName { get; set; }
        public virtual string LanguageCode { get; set; }
        public virtual string LanguageCulture { get; set; }
        public virtual int Sort { get; set; }
        public virtual string img_flag { get; set; }
    }
    public class ByLanguage_List_and_Content 
    {
        public virtual string menunames { get; set; }
        public virtual List<Content> content { get; set; }
        public virtual List<DownloadByLanguage> lst { get; set; }
    }
    public class ListByLanguage : Language
    {
        public virtual List<Banner_Detail> lstBanner { get; set; }
        public virtual int sorting { get; set; }
        public virtual bool type_sorting { get; set; }
        public virtual string IsCheckedtype_sorting
        {
            get
            {
                return type_sorting ? "checked=\"checked\"" : "";
            }
        }
    }
    public class ListWebboard_ForumLanguage : Language
    {
        public virtual int sort { get; set; }
        public virtual int sorting { get; set; }
        public virtual bool type_sorting { get; set; }
        public virtual int topic_count { get; set; }
        public virtual List<Webboard_Forum_Detail> lstForum { get; set; }
        public virtual string IsCheckedtype_sorting
        {
            get
            {
                return type_sorting ? "checked=\"checked\"" : "";
            }
        }
    }
    public class HospitalsByLanguage : Language
    {
        public virtual List<Hospitals_Detail> lstHospitals { get; set; }
    }
    public class FAQByLanguage : Language
    {
        public virtual List<Faq_Detail> lstFaq { get; set; }
        public virtual int sorting { get; set; }
        public virtual bool type_sorting { get; set; }
        public virtual string IsCheckedtype_sorting
        {
            get
            {
                return type_sorting ? "checked=\"checked\"" : "";
            }
        }
    }
    public class ComplaintByLanguage : Language
    {
        public virtual List<Complaint> lstComplaint { get; set; }
    }
    public class BlogByLanguage : Language
    {
        public virtual List<Blog> lstBlog { get; set; }
    }
    public class Contact_ListByLanguage : Language
    {
        public virtual List<Contact_List> lstContact_List { get; set; }
      
    }
    public class ListByLanguageSurveyor : Language
    {
        public virtual List<Surveyor> lstSurveyor { get; set; }
    }
    public class DownloadByLanguage : Language
    {       
       
        public virtual List<Content> content { get; set; }
        public virtual List<Download_Detail> lstBanner { get; set; }
        public virtual int sorting { get; set; }
        public virtual bool type_sorting { get; set; }
        public virtual string IsCheckedtype_sorting
        {
            get
            {
                return type_sorting ? "checked=\"checked\"" : "";
            }
        }
    }
    public class DatasByLanguage : Language
    {

        public virtual List<Content> content { get; set; }
        public virtual List<Data_Detail> lstBanner { get; set; }
        public virtual int sorting { get; set; }
        public virtual bool type_sorting { get; set; }
        public virtual string IsCheckedtype_sorting
        {
            get
            {
                return type_sorting ? "checked=\"checked\"" : "";
            }
        }
    }
    public class CalendarByLanguage : Language
    {
        public virtual CalendarEvent lstCalerdar { get; set; }
    }
    public class CategoryByLanguage : Language
    {
        public virtual List<Tm_MenuDetail> lstcate { get; set; }
    }


    public class ListTopicLanguage : Language
    {
        public virtual List<Webboard_topic> lst_topic { get; set; }      
    }
}
