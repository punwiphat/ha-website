﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Calendar : BaseModel
    {
        public int ID { get; set; }
        public int MenuID { get; set; }

        public DateTime? InputDate { get; set; }
        [NotMapped]
        private string TempInputDate { get; set; }
        [NotMapped]
        public virtual string InputDate_str
        {
            get { return (InputDate != null) ? InputDate.ToDate() : TempInputDate; }
            set { TempInputDate = value; }
        }
        public DateTime? EffectiveDate { get; set; }
        [NotMapped]
        private string TempEffectiveDate { get; set; }
        [NotMapped]
        public virtual string EffectiveDate_str
        {
            get { return (EffectiveDate != null) ? EffectiveDate.ToDate() : TempEffectiveDate; }
            set { TempEffectiveDate = value; }
        }

        public DateTime? EndDate { get; set; }
        [NotMapped]
        private string TempEndDate { get; set; }
        [NotMapped]
        public virtual string EndDate_str
        {
            get { return (EndDate != null) ? EndDate.ToDate() : TempEndDate; }
            set { TempEndDate = value; }
        }



        public DateTime EventDate { get; set; }
        [NotMapped]
        private string TempEventDate { get; set; }
        [NotMapped]
        public virtual string EventDate_str
        {
            get { return (EventDate != null && EventDate != new DateTime()) ? EventDate.ToDate() : TempEventDate; }
            set { TempEventDate = value; }
        }

        public DateTime EventEndDate { get; set; }
        [NotMapped]
        private string TempEventEndDate { get; set; }
        [NotMapped]
        public virtual string EventEndDate_str
        {
            get { return (EventEndDate != null && EventEndDate != new DateTime()) ? EventEndDate.ToDate() : TempEventEndDate; }
            set { TempEventEndDate = value; }
        }


        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public virtual List<CalendarDetail> lstCalendar { get; set; }
       
    }

}