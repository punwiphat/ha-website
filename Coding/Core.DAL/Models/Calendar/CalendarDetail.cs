﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.library;

namespace Core.DAL.Models
{
    public partial class CalendarDetail : ExMenu
    {
       	public int CalendarDetailID { get; set; }

	public int CalendarID { get; set; }

	public string LangCode { get; set; }

	public string EventName { get; set; }

	public string EventDescription { get; set; }

	public string EventLink { get; set; }

	public string EventPlace { get; set; }
       
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime EventDate { get; set; }
        [NotMapped]
        public DateTime EventEndDate { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        [NotMapped]
        public List<MediaFiles> lstFiles { get; set; }


    }
    public class CalendarByLanguage : Languages
    {
        public virtual List<CalendarDetail> lstCalendar { get; set; }
        public virtual string SelectedTabs { get; set; }

    }
    public class CalendarModel
    {
        
        public virtual int ID { get; set; }
        public virtual string title { get; set; }
        public virtual string start { get; set; }
        public virtual string end { get; set; }
        public virtual string desc { get; set; }
        public virtual bool Actived { get; set; }
        public virtual string color { get; set; }
        public virtual bool allDay { get; set; }
    }
}
