﻿namespace Core.DAL.Models
{
    public partial class MediaFiles : BaseModel
    {
        public int MediaFileID { get; set; }
        public int MediaCategoryID { get; set; }
        public int MenuID { get; set; }
        public string RefID { get; set; }
        public string PathFile { get; set; }
        public string FolderName { get; set; }
        public string TitleName { get; set; }
        public string FileNameOri { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public float? FileSize { get; set; }
        public int? Downloads { get; set; }
        public int? Sequence { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
    }
}
