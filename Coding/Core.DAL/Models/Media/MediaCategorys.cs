﻿namespace Core.DAL.Models
{
    public partial class MediaCategorys :BaseModel
    {
        public int MediaCategoryID { get; set; }
        public int MenuID { get; set; }
        public string MediaCategoryName { get; set; }
        public int? Sequence { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
    }
}
