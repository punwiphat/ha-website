﻿using Core.library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace Core.DAL.Models
{
    public partial class RSSFeedDetail : ExMenu
    {
        public int RSSFeedDetailID { get; set; }
        public int RSSFeedID { get; set; }
        public string LangCode { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        public string RSSFeedName { get; set; }
        public string RSSFeedDescription { get; set; }
        public string RSSFeedLink { get; set; }
        [NotMapped]
        public int Sequence { get; set; }
        [NotMapped]
        public bool Actived { get; set; }
        [NotMapped]
        public DateTime UpdatedDate { get; set; }
        [NotMapped]
        public string UpdatedBy { get; set; }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
        
    }
    public class RSSFeedByLanguage : Languages
    {
        public virtual List<RSSFeedDetail> lstRSSFeed { get; set; }
        public virtual int MenuDisplayID { get; set; }
        public virtual int SortingBy { get; set; }
        public virtual bool SortingType { get; set; }
        public virtual string SelectedTabs { get; set; }
        public virtual string CheckedSortingType
        {
            get
            {
                return SortingType ? "checked=\"checked\"" : "";
            }
        }

    }
}
