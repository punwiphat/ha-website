﻿using System;

namespace Core.DAL.Models
{
    public partial class TmStatus
    {
        public Guid StatusId { get; set; }
        public int? StatusTypeId { get; set; }
        public string StatusName { get; set; }
        public string classStatus { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } 
        public int Mode { get; set; } 
    }
}
