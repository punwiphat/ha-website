﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class TmMasterData
    {
        public Guid MasterId { get; set; }
        public Guid MasterTypeId { get; set; }
        public Guid? MasterRootId { get; set; }
        public Guid? MasterParentId { get; set; }
        public string MasterName { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int? Level { get; set; }
        public string MasterCode { get; set; }
        public bool? Compulsory { get; set; }
        [NotMapped]
        public virtual string CreatedDate_Str
        {
            get { return CreatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreatedDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdatedDate_Str
        {
            get { return UpdatedDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdatedDate)) : ""; }
        }
    }
}
