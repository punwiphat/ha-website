﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Category : BaseModel
    {

        public int ID { get; set; }
        public int MenuID { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public string EmailTo { get; set; }
        public int Sequence { get; set; }
        public string RSSUrl { get; set; }
        public bool Actived { get; set; }
        public bool? Deleted { get; set; }
        [NotMapped]
        public virtual int SortingBy { get; set; }

        [NotMapped]
        public virtual bool SortingType { get; set; }

        [NotMapped]
        public virtual int MenuDisplayID { get; set; }

        [NotMapped]
        public virtual long CountChilds { get; set; }
    }
}
