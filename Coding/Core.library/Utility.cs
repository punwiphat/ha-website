﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Core.library
{
    public static class Utility
    {
        public static DateTime ToDate(this string value)
        {
            CultureInfo ukCulture = new CultureInfo("en-GB");
            return DateTime.ParseExact(value, "dd/MM/yyyy", ukCulture);
        }


        public static string ToDate(this DateTime Obj)
        {
            return Obj.ToString("dd/MM/yyyy", new CultureInfo("en-GB"));
        }
        public static string ToDateTH(this DateTime Obj)
        {
            return Obj.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
        }
        public static string ToDate(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("dd/MM/yyyy", new CultureInfo("en-GB"));
            }
            else
            {
                return "";
            }
        }


        public static string ToThaiFormate(this DateTime Obj)
        {
            return Obj.ToString("dd MMM yy", new CultureInfo("th-TH"));
        }
        public static string ToThaiLongFormate(this DateTime Obj)
        {
            return Obj.ToString("dd MMM yyyy", new CultureInfo("th-TH"));
        }
        public static string ToMonthYearThaiFormate(this DateTime Obj)
        {
            return Obj.ToString("MMM yyyy", new CultureInfo("th-TH"));
        }
        public static string ToThaiLongFormate(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("dd MMM yyyy", new CultureInfo("th-TH"));
            }
            else
            {
                return "";
            }
        }
        public static string ToThaiLongFormateToIntStr(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("yyyyMMdd", new CultureInfo("th-TH"));
            }
            else
            {
                return "";
            }
        }
        public static string ToThaiLongFormateWithDash(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("yyyy-MM-dd", new CultureInfo("th-TH"));
            }
            else
            {
                return "";
            }
        }
        public static string ToEngFormateWithDash(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("yyyy-MM-dd", new CultureInfo("en-GB"));
            }
            else
            {
                return "";
            }
        }
        public static string ToThaiFormate(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("dd MMM yy", new CultureInfo("th-TH"));
            }
            else
            {
                return "";
            }
        }

      
        public static string ToThaiFormateAndTime(this DateTime Obj)
        {
            return Obj.ToString("dd MMM yy เวลา HH : mm", new CultureInfo("th-TH"));
        }
        public static string ToThaiFormateAndTime(this DateTime? Obj)
        {
            if (Obj != null)
            {
                return Obj.Value.ToString("dd MMM yy เวลา HH : mm", new CultureInfo("th-TH"));
            }
            else
            {
                return "-";
            }
        }
       
        public static DateTime? todateEn(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                CultureInfo ukCulture = new CultureInfo("en-GB");
                ukCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                string tem = string.Format("{0:dd/MM/yyyy}", value);
                string[] dateString = tem.Split('/');
                int iYear = int.Parse(dateString[2].Substring(0, 4));
                string sTime = dateString[2].Substring(4, dateString[2].Length - 4);
                int NowYear = DateTime.Now.Year;
                if (Math.Abs(iYear - NowYear) > 100)
                    iYear = iYear - 543;

                DateTime result = DateTime.ParseExact(dateString[0] + "/" + dateString[1] + "/" + iYear + sTime,  "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-GB")); 
                return result;
            }
            else
                return null;
        }

        public static DateTime? todateTH(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                CultureInfo ukCulture = new CultureInfo("en-GB");
                ukCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                string tem = string.Format("{0:dd/MM/yyyy}", value);
                string[] dateString = tem.Split('/');
                int iYear = int.Parse(dateString[2].Substring(0, 4));
                string sTime = dateString[2].Substring(4, dateString[2].Length - 4);
                int NowYear = DateTime.Now.Year;
                if (Math.Abs(iYear - NowYear) < 100)
                    iYear = iYear + 543;

                DateTime result = Convert.ToDateTime(dateString[0] + "/" + dateString[1] + "/" + iYear + sTime);
                return result;
            }
            else
                return null;
        }
        public static int dateEnToInt(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string tem = string.Format("{0:dd/MM/yyyy}", value);
                string[] dateString = tem.Split('/');
                int iYear = int.Parse(dateString[2].Substring(0, 4));
                string sTime = dateString[2].Substring(4, dateString[2].Length - 4);
                int NowYear = DateTime.Now.Year;
                if (Math.Abs(iYear - NowYear) > 100)
                    iYear = iYear - 543;

                string result = iYear + dateString[1] + dateString[0];

                return int.Parse(result);
            }
            else
                return -1;
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public static string ToPermalink(string input)
        {
            //var replacements = new[] {"!", "*", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]" };
            var replacements = new[] { "'", "!", "#", "$", "%", "&", "\\","'","(",")","*","+",",","-",".","/","@",":",";","<","=",">","[","]","^","_","`","{","|","}","~","\"" };//,@"""
            foreach (var word in replacements)
            {
                input = input.Contains(word) ? input.Replace(word, "") : input;
            }

            return input;// Regex.Replace(input, @"[\W]", "-");
        }
        static public object GetValObjDy(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }
    }

}
