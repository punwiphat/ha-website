﻿using System;
using System.Globalization;

namespace Core.library
{

    public class Formatter
    {
        #region Convert format Date
        public class GetDateMonth
        {
            public DateTime firstDate { get; set; }
            public DateTime lastDate { get; set; }
        }
        public static DateTime todate(string value, string Culture = null)
        {
            return !string.IsNullOrEmpty(Culture) ? DateTime.ParseExact(value, "dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH")) : DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        public static GetDateMonth DateOfMonth(DateTime date)
        {
            GetDateMonth obj = new GetDateMonth();
            obj.firstDate = new DateTime(date.Year, date.Month, 1);
            obj.lastDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            return obj;
        }
        public static string format_th_short(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd MMM HH:mm", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_th_short(DateTime date)
        {
            return date.ToString("dd MMM HH:mm", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_th(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd MMM yyyy HH:mm", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_th(DateTime date)
        {
            return date.ToString("dd MMM yyyy HH:mm", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_day_th(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dddd", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_day_th(DateTime date)
        {
            return date.ToString("dddd", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_time(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("HH:mm", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_time(DateTime date)
        {
            return date.ToString("HH:mm", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_th_slash(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_th_slash(DateTime date)
        {
            return date.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_th_datt(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd-MM-yyyy", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_th_datt(DateTime date)
        {
            return date.ToString("dd-MM-yyyy", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string format_th_slash_short(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd/MM/yy", new System.Globalization.CultureInfo("th-TH")) : "";
        }
        public static string format_th_slash_short(DateTime date)
        {
            return date.ToString("dd/MM/yy", new System.Globalization.CultureInfo("th-TH"));
        }

        public static string format_en_slash(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "";
        }
        public static string format_en_slash(DateTime date)
        {
            return date.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
        }
        public static string ToDate_Culture(DateTime date)
        {
            return date.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name));
        }
        public static string ToDate_Culture(DateTime? date)
        {
            return (date != null) ? date.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name)) : "";
        }

        #endregion
        public static String TimeAgo(DateTime dateTime)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(dateTime);
            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("เมื่อสักครู่", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} นาทีที่แล้ว", timeSpan.Minutes) :
                    "1 นาทีที่แล้ว";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} ชั่วโมงที่แล้ว", timeSpan.Hours) :
                    "1 ชั้่วโมงที่แล้ว";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ? (timeSpan.Days > 8 ?
                    format_th_short(dateTime) : (format_day_th(dateTime) + " " + format_time(dateTime))) :
                    "เมื่อวาน " + format_time(dateTime);
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    format_th(dateTime) :
                    format_th_short(dateTime);
            }
            return result;
        }
        public static string Truncate(string value, int maxLength, string truncationSuffix = "…")
        {
            return value?.Length > maxLength
                ? value.Substring(0, maxLength) + truncationSuffix
                : value;
        }
    }

}