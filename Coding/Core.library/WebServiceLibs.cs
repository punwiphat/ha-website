﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

namespace Core.library
{
    public class WebServiceLibs
    {
        public WebServiceLibs()
        {
            decompressionMethods = DecompressionMethods.GZip;
            Querystring = new Dictionary<string, object>();
            ItemsHeader = new Dictionary<string, string>();
            ItemsBody = new Dictionary<string, object>();
        }
        public DecompressionMethods decompressionMethods { get; set; }
        // ค่า Query String
        public Dictionary<string, object> Querystring { get; set; }
        //ค่า Header
        public Dictionary<string, string> ItemsHeader { get; set; }
        // ค่า JsonBody
        public Dictionary<string, Object> ItemsBody { get; set; }

        public string UrlSetting { get; set; }
        public class HeaderSetting
        {
            public string HeaderName { get; set; }
            public string Value { get; set; }
        }
        public enum Http
        {
            CONNECT,
            GET,
            HEAD,
            MKCOL,
            POST,
            PUT
        }
        public enum AcceptType
        {
            Json = 1,
            urlencoded = 2
        }
        private static string ChkTypeAccept(string value)
        {
            string _type = "application/json;charset=utf-8";
            switch (value)
            {
                case "Json":
                    _type = "application/json;charset=utf-8";
                    break;
                case "urlencoded": _type = "application/x-www-form-urlencoded"; break;
                default: _type = "application/json;charset=utf-8"; break;
            }
            return _type;
        }
        public static string QueryString(IDictionary<string, object> dict)
        {
            var list = new List<string>();
            foreach (var item in dict)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return string.Join("&", list);
        }
        public T CallWebservice<T>(Http Method, AcceptType acceptType)
        {
            return JsonConvert.DeserializeObject<T>(CallWebservice(Method, acceptType));
        }
        public string CallWebservice(Http Method, AcceptType acceptType)
        {
            if (string.IsNullOrEmpty(UrlSetting))
            {
                return "กรุณาระบุค่า UrlSetting ที่ใช้ดึงข้อมูล";

            }
            if (Querystring.Count > 0)
            {
                UrlSetting  = UrlSetting+ "?" +WebServiceLibs.QueryString(Querystring);
            }

            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UrlSetting);
           
            request.ContentType = "application/json;charset=utf-8";// ****
           
            //request.ContentType = "application/x-www-form-urlencoded";// ****
            foreach (var Items in ItemsHeader)
            {
                request.Headers.Add(Items.Key, Items.Value);
            }

            //List<string> postString = new List<string>();
            //foreach (var Items in ItemsBody)
            //{
            //    postString.Add("\"" + Items.Key + "\":" + "\"" + Items.Value.ToString() + "\"");

            //}
            //string postData = String.Join(",", postString);
            //var data = Encoding.ASCII.GetBytes("{" + postData + "}");

            request.Method = Method.ToString();
            request.Accept = ChkTypeAccept(acceptType.ToString());

            if (ItemsBody.Count > 0)
            {
                var data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(ItemsBody));
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            request.AutomaticDecompression = decompressionMethods;
            string responseFromServer = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseFromServer = reader.ReadToEnd();
            }
            return responseFromServer;
        }

        public T CallWebserviceCustom<T>(Http Method, AcceptType acceptType)
        {
            return JsonConvert.DeserializeObject<T>(CallWebserviceCustom(Method, acceptType));
        }
        public string CallWebserviceCustom(Http Method, AcceptType acceptType)
        {
            if (string.IsNullOrEmpty(UrlSetting))
            {
                return "กรุณาระบุค่า UrlSetting ที่ใช้ดึงข้อมูล";

            }
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UrlSetting);
            request.ContentType = "application/x-www-form-urlencoded;charset=utf-8";// ****


            request.Method = Method.ToString();

            List<string> postString = new List<string>();
            foreach (var Items in ItemsBody)
            {
                postString.Add(Items.Key + "=" + Items.Value.ToString());

            }
            string postData = String.Join("&", postString);
            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }


            foreach (var Items in ItemsHeader)
            {
                request.Headers.Add(Items.Key, Items.Value);
            }
            request.Accept = ChkTypeAccept(acceptType.ToString());
            request.AutomaticDecompression = decompressionMethods;
            string responseFromServer = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseFromServer = reader.ReadToEnd();
            }
            return responseFromServer;
        }

        public T CallWebserviceFile<T>(Http Method, AcceptType acceptType)
        {
            return JsonConvert.DeserializeObject<T>(CallWebserviceFile(Method, acceptType));
        }
        public string CallWebserviceFile(Http Method, AcceptType acceptType)
        {
            if (string.IsNullOrEmpty(UrlSetting))
            {
                return "กรุณาระบุค่า UrlSetting ที่ใช้ดึงข้อมูล";

            }
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UrlSetting);
            request.ContentType = "application/json;charset=utf-8";// ****


            request.Method = Method.ToString();

            List<string> postString = new List<string>();
            foreach (var Items in ItemsBody)
            {
                postString.Add("\"" + Items.Key + "\":" + "\"" + Items.Value.ToString() + "\"");

            }
            string postData = String.Join(",", postString);
            var data = Encoding.ASCII.GetBytes("{" + postData + "}");
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }


            foreach (var Items in ItemsHeader)
            {
                request.Headers.Add(Items.Key, Items.Value);
            }
            request.Accept = ChkTypeAccept(acceptType.ToString());
            request.AutomaticDecompression = decompressionMethods;
            string responseFromServer = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseFromServer = reader.ReadToEnd();
            }
            return responseFromServer;
        }
    }
}
