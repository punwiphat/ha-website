﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Microsoft.Extensions.Configuration;

namespace Core.library
{
    public class SendEmail
    {
        public SendEmail(string fromAliasEmail, string fromEmail, string password, string host, int Port,Boolean EnableSsl, Boolean UseDefaultCredentials) {

            _fromAliasEmail = fromAliasEmail;
            _fromEmail = fromEmail;
            _password =  password;
            _host = host;
            _Port = Port;
            _EnableSsl = EnableSsl;
            _UseDefaultCredentials= UseDefaultCredentials;
        }

        public static string _fromAliasEmail { get; set; }
        public static string _fromEmail { get; set; }
        public static string _password { get; set; }
        public static string _host  { get; set; }
        public static int   _Port { get; set; }
        public static bool _EnableSsl { get; set; }
        public static bool _UseDefaultCredentials { get; set; }

        private static bool MailActive()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")).Build();
            return bool.Parse(configuration["MailActive"]);
        }
        public bool SendMail(string str_to_address, string Subject, string Content)
        {

            try
            {
                if (!MailActive())
                {
                    return true;
                }

                MailMessage mailMessage = new MailMessage();
                var smtpClient = new SmtpClient(_host);
                smtpClient.Host = _host;
                smtpClient.Port = _Port;
                smtpClient.EnableSsl = _EnableSsl;
                if (smtpClient.EnableSsl)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                        { return true; };
                }
                // smtpClient.ConnectType = SmtpConnectType.ConnectSSLAuto;
                if (_host.ToLower() == "smtp.office365.com") {
                    smtpClient.TargetName = "STARTTLS/smtp.office365.com"; 
                }
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = _UseDefaultCredentials;
                //smtpClient.Credentials = null;
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //smtpClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                NetworkCredential MyCredentials = new NetworkCredential(_fromEmail, _password);
                smtpClient.Credentials = MyCredentials;
                smtpClient.Timeout = 10000;

                //mailMessage.To.Add(str_to_address);

                mailMessage.From = new MailAddress(_fromEmail, _fromAliasEmail);
                mailMessage.Priority = MailPriority.High;
                mailMessage.Subject = Subject;
                mailMessage.Body = Content;
                mailMessage.IsBodyHtml = true;

                if (SetMailAddressCollection(mailMessage.To, str_to_address))
                {
                    smtpClient.Send(mailMessage);
                }
            }
            catch(Exception ex)
            {                string s = ex.Message;

            }
            return true;
        }
        private static bool SetMailAddressCollection(MailAddressCollection toAddresses, string mailId)
        {
            bool successfulAddressCreation = true;
            toAddresses.Add(new MailAddress(mailId));
            return successfulAddressCreation;
        }
    }
}
