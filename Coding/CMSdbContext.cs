﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Core.DAL.Models
{
    public partial class CMSdbContext : DbContext
    {
        public CMSdbContext()
        {
        }
        public CMSdbContext(DbContextOptions<CMSdbContext> options)
            : base(options)
        {
        }
        #region Menu
        public virtual DbSet<Tm_Menu> Tm_Menu { get; set; }
        public virtual DbSet<Tm_MenuDetail> Tm_MenuDetail { get; set; }
        public virtual DbSet<Tm_Module> Tm_Module { get; set; }
        public virtual DbSet<Tm_Module_Relate> Tm_Module_Relate { get; set; }
        public virtual DbSet<Language> Language { get; set; }

        #endregion

        public virtual DbSet<Banner> Banner { get; set; }
        public virtual DbSet<BannerDetail> BannerDetail { get; set; }


        //public virtual DbSet<Attachment_File_Config> Attachment_File_Config { get; set; }
        //public virtual DbSet<ContentType> ContentType { get; set; }
        //public virtual DbSet<Contents> Contents { get; set; }
        //public virtual DbSet<DocumentCategorys> DocumentCategorys { get; set; }
        //public virtual DbSet<DocumentFiles> DocumentFiles { get; set; }

        //public virtual DbSet<Events> Events { get; set; }


        //public virtual DbSet<Module> Module { get; set; }

        //public virtual DbSet<Posts> Posts { get; set; }
        //public virtual DbSet<PostsComment> PostsComment { get; set; }
        //public virtual DbSet<PostsTag> PostsTag { get; set; }

        //public virtual DbSet<SystemConfigs> SystemConfigs { get; set; }
        //public virtual DbSet<TmMasterData> TmMasterData { get; set; }
        //public virtual DbSet<TmMasterDataType> TmMasterDataType { get; set; }
        public virtual DbSet<EmailConfig> EmailConfig { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<MenuBackoffice> MenuBackoffice { get; set; }
        //public virtual DbSet<TmMenu> TmMenu { get; set; }
        //public virtual DbSet<TmMenuPosition> TmMenuPosition { get; set; }
        //public virtual DbSet<TmMenuRelated> TmMenuRelated { get; set; }

        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RoleGroup> RoleGroup { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        //public virtual DbSet<TmStatus> TmStatus { get; set; }

        //public virtual DbSet<WebboardCategory> WebboardCategory { get; set; }
        //public virtual DbSet<WebboardReply> WebboardReply { get; set; }
        //public virtual DbSet<WebboardReports> WebboardReports { get; set; }
        //public virtual DbSet<WebboardReservedWord> WebboardReservedWord { get; set; }
        //public virtual DbSet<WebboardTopic> WebboardTopic { get; set; }

        //public virtual DbSet<Projects> Projects { get; set; }

        //public virtual DbSet<Notifications> Notifications { get; set; }
        //public virtual DbSet<Tm_Positions> TmPositions { get; set; }

        //public virtual DbSet<Log_auto_noti> Logautonoti { get; set; }

        // Setting Core app //
        public virtual DbSet<LogActivities> LogActivities { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
          
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")).Build();
                var connectionString = configuration.GetConnectionString("MySqlConnection");
                optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Banner
            modelBuilder.Entity<Banner>(entity =>
            {
                entity.HasKey(e => e.BannerID);
                entity.Property(e => e.BannerID);
                entity.Property(e => e.MenuID);
                entity.Property(e => e.InputDate).HasColumnType("datetime");
                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");
                entity.Property(e => e.EndDate).HasColumnType("datetime");
                entity.Property(e => e.SortingType);
                entity.Property(e => e.SortingBy);
                entity.Property(e => e.CreateBy);
                entity.Property(e => e.CreateDate).HasColumnType("datetime");
                entity.Property(e => e.UpdateBy);
                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
                entity.Property(e => e.Actived);
                entity.Property(e => e.Deleted);
            });

            modelBuilder.Entity<BannerDetail>(entity =>
            {
                entity.HasKey(e => e.BannerDetailID);
                entity.Property(e => e.BannerDetailID);
                entity.Property(e => e.BannerID);
                entity.Property(e => e.LangCode);
                entity.Property(e => e.BannerName);
                entity.Property(e => e.BannerDescription);
                entity.Property(e => e.BannerLink);
                
                entity.Property(e => e.Sequence);
              
                entity.Property(e => e.CreateBy);
                entity.Property(e => e.CreateDate).HasColumnType("datetime");
                entity.Property(e => e.UpdateBy);
                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

            });
            #endregion

            //modelBuilder.Entity<Attachment_File_Config>(entity =>
            //{
            //    entity.HasKey(e => e.Attachment_File_ID)
            //        .HasName("PK__Attachme__125EB7614E278889");

            //    entity.ToTable("Attachment_File_Config");

            //    entity.Property(e => e.Attachment_File_ID)
            //        .HasColumnName("Attachment_File_ID")
            //       ;

            //    entity.Property(e => e.Size_limit).HasColumnName("Size_limit");

            //    entity.Property(e => e.Size_limit)
            //        .HasColumnName("Size_limit_Type")
            //        .HasMaxLength(2);

            //    entity.Property(e => e.Type_limit)
            //        .HasColumnName("Type_limit")
            //        .HasMaxLength(255);

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        ;
            //});



            //modelBuilder.Entity<ContentType>(entity =>
            //{
            //    entity.Property(e => e.ContentTypeId).HasDefaultValueSql("(newid())");

            //    entity.Property(e => e.ContentTypeName).HasMaxLength(200);

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");
            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");


            //    entity.Property(e => e.CreateBy)
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.UpdateBy)
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Contents>(entity =>
            //{
            //    entity.HasKey(e => e.ContentId)
            //        .HasName("PK__CONTENTS__3214EC2754AF3159");

            //    entity.HasComment("ตารางข้อมูลเนื้อหาเว็บไซต์");

            //    entity.Property(e => e.ContentId)
            //        .HasColumnName("Content_Id")
            //       
            //        .HasComment("รหัสหลักแบบไม่ซ้ำโดยระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.Descriptions).HasComment("คำอธิบายรายะเอียด");

            //    entity.Property(e => e.MenuId)
            //        .HasColumnName("Menu_Id")
            //        .HasComment("รหัสหลักข้อมูลเมนู");

            //    entity.Property(e => e.ShortDescriptions)
            //        .HasColumnName("Short_Descriptions")
            //        .HasComment("คำอธิบายโดยย่อ");

            //    entity.Property(e => e.TopicName)
            //        .HasColumnName("Topic_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อเรื่อง");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<DocumentCategorys>(entity =>
            //{
            //    entity.HasKey(e => e.DocumentCategoryId)
            //        .HasName("PK__media_ca__1EACB97F46B15FFC");

            //    entity.ToTable("Document_Categorys");

            //    entity.HasComment("ตารางข้อมูลหมวดหมู่ไฟล์เอกสารแนบ");

            //    entity.Property(e => e.DocumentCategoryId)
            //        .HasColumnName("Document_Category_Id")
            //       
            //        .HasComment("รหัสหลักหมวดหมู่ไฟล์เอกสารแนบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.DocumentCategoryName)
            //        .HasColumnName("Document_Category_Name")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อหมวดหมู่");

            //    entity.Property(e => e.MenuId)
            //        .HasColumnName("Menu_Id")
            //        .HasComment("รหัสหลักข้อมูลเมนู");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.UpdateBy)
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<DocumentFiles>(entity =>
            //{
            //    entity.HasKey(e => e.DocumentFileId)
            //        .HasName("PK_media_files");

            //    entity.ToTable("Document_Files");

            //    entity.HasComment("ตารางข้อมูลไฟล์เอกสารแนบ");

            //    entity.Property(e => e.DocumentFileId)
            //        .HasColumnName("Document_File_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลไฟล์เอกสารแนบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.DocumentCategoryId)
            //        .HasColumnName("Document_Category_Id")
            //        .HasComment("รหัสหลักหมวดหมู่ไฟล์เอกสารแนบ");

            //    entity.Property(e => e.Downloads).HasComment("จำนวครั้งที่ดาวน์โหลด");

            //    entity.Property(e => e.FileExtension)
            //        .HasColumnName("File_Extension")
            //        .HasMaxLength(20)
            //        .HasComment("นามสกุลไฟล์");

            //    entity.Property(e => e.FileName)
            //        .HasColumnName("File_Name")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อไฟล์และนามสกุล");

            //    entity.Property(e => e.FileNameOri)
            //        .HasColumnName("File_Name_Ori")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อไฟล์ (ต้นฉบับ)");

            //    entity.Property(e => e.FileSize)
            //        .HasColumnName("File_Size")
            //        .HasComment("ขนาดไฟล์");

            //    entity.Property(e => e.FolderName)
            //        .HasColumnName("Folder_Name")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อแฟ้ม");

            //    entity.Property(e => e.PathFile)
            //        .HasColumnName("Path_File")
            //        .HasMaxLength(255)
            //        .HasComment("ที่อยู่ไฟล์เอกสารแนบ");

            //    entity.Property(e => e.RefId)
            //        .HasColumnName("Ref_Id")
            //        .HasComment("รหัสอ้างอิงข้อมูล");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.TitleName)
            //        .HasColumnName("Title_Name")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อเอกสาร");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.ToTable("Email_Template");

                entity.HasComment("ตารางข้อมูลรูปแบบอีเมล์");

                entity.Property(e => e.EmailTemplateId)
                    .HasColumnName("Email_Template_Id")
                    .HasComment("รหัสหลักข้อมูลรูปแบบอีเมล์");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("Create_By")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime")
                    ;

                entity.Property(e => e.EmailDescriptions)
                    .HasColumnName("Email_Descriptions")
                    .HasComment("รายละเอียดอีเมล์");

                entity.Property(e => e.EmailSubject)
                    .HasColumnName("Email_Subject")
                    .HasMaxLength(255)
                    .HasComment("ชื่อเรื่องการส่งอีเมล์");

                entity.Property(e => e.UpdateBy)
                    .IsRequired()
                    .HasColumnName("Update_By")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasColumnType("datetime")
                    ;
            });

            //modelBuilder.Entity<Events>(entity =>
            //{
            //    entity.HasKey(e => e.EventId)
            //        .HasName("PK__EVENTS__3214EC279222FFC7");

            //    entity.HasComment("ตารางข้อมูลกิจกรรม");

            //    entity.Property(e => e.EventId)
            //        .HasColumnName("Event_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลกิจกรรม");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.EndDate)
            //        .HasColumnName("EndDate")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่สิ้นสุดกิจกรรม");

            //    entity.Property(e => e.StartDate)
            //        .HasColumnName("StartDate")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่เริ่มกิจกรรม");

            //    entity.Property(e => e.s_hour)
            //      .HasColumnName("SHour")               
            //      .HasComment("เวลาที่เริ่มกิจกรรม");

            //    entity.Property(e => e.s_min)
            //      .HasColumnName("SMinute")            
            //      .HasComment("เวลาที่เริ่มกิจกรรม");

            //    entity.Property(e => e.e_hour)
            //    .HasColumnName("EHour")
            //    .HasComment("เวลาที่สิ้นสุดกิจกรรม");

            //    entity.Property(e => e.e_min)
            //      .HasColumnName("EMinute")
            //      .HasComment("เวลาที่สิ้นสุดกิจกรรม");


            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.Descriptions).HasComment("คำอธิบายรายะเอียด");

            //    entity.Property(e => e.EventColor)
            //        .HasColumnName("Event_Color")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("สีของกิจกรรม");

            //    entity.Property(e => e.LocationName)
            //        .HasColumnName("Location_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อสถานที่จัดกิจกรรม");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.TopicName)
            //        .HasColumnName("Topic_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อกิจกรรม");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UrlLink)
            //        .HasColumnName("Url_Link")
            //        .HasMaxLength(255)
            //        .HasComment("ลิงค์ที่เกี่ยวข้อง");
            //});

            modelBuilder.Entity<LogActivities>(entity =>
            {

                entity.ToTable("log_activity");
                entity.HasKey(e => e.LogActivityId);
                entity.Property(e => e.LogActivityId);
                entity.Property(e => e.ActivityType).HasColumnName("Activity_Type");
                entity.Property(e => e.MenuId).HasColumnName("Menu_Id");
                entity.Property(e => e.MenuName).HasColumnName("Menu_Name");
                entity.Property(e => e.Action).HasColumnName("Action");
                entity.Property(e => e.ClientIp).HasColumnName("ClientIp");
                entity.Property(e => e.DeviceName).HasColumnName("Device_Name");
                entity.Property(e => e.Method);
                entity.Property(e => e.Descriptions);

                entity.Property(e => e.ActionDate).HasColumnName("Action_Date");
                entity.Property(e => e.ActionBy).HasColumnName("Action_By");
            });

            modelBuilder.Entity<EmailConfig>(entity =>
            {
                entity.HasKey(e => e.EmailId);
                entity.Property(e => e.EmailId).HasColumnName("Email_Id");
                entity.Property(e => e.Emailname).HasMaxLength(255);
                entity.Property(e => e.Password).HasMaxLength(255);
                entity.Property(e => e.Smtp).HasMaxLength(255);
                entity.Property(e => e.UseDefaultCredentials).HasColumnName("Use_Default_Credentials");
                entity.Property(e => e.EnableSSL).HasColumnName("EnableSSL");

                entity.Property(e => e.UpdateBy)
                    .IsRequired()
                    .HasColumnName("Update_By")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasColumnType("datetime")
                    ;

            });

            //modelBuilder.Entity<Module>(entity =>
            //{
            //    entity.Property(e => e.ModuleId).ValueGeneratedNever();

            //    entity.Property(e => e.Code)
            //        .HasMaxLength(5)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Icon)
            //        .HasColumnName("ICON")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.InReport)
            //        .HasDefaultValueSql("((0))")
            //        .HasComment("สถานะการแสดงในระบบ Report Design 0= ไม่แสดง และ 1 = แสดง");

            //    entity.Property(e => e.IsActive)
            //        .IsRequired()
            //        .HasDefaultValueSql("((1))");

            //    entity.Property(e => e.IsNewReport)
            //        .HasDefaultValueSql("((0))")
            //        .HasComment("สถานะการเพิ่ม Report 0=ไม่สามารถเพิ่มได้ และ 1= สามารถเพิ่มได้");

            //    entity.Property(e => e.ModuleName)
            //        .IsRequired()
            //        .HasMaxLength(50);

            //    entity.Property(e => e.ModuleNameEn)
            //        .IsRequired()
            //        .HasColumnName("ModuleNameEN")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Url)
            //        .HasColumnName("URL")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<Posts>(entity =>
            //{
            //    entity.HasKey(e => e.PostId)
            //        .HasName("PK__Posts__5875F7ADE552EF3A");

            //    entity.HasComment("ตารางข้อมูลข่าวประชาสัมพันธ์");

            //    entity.Property(e => e.PostId)
            //        .HasColumnName("Post_Id")
            //       
            //        .HasComment("รหัสหลักแบบไม่ซ้ำโดยระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.DateInput)
            //        .HasColumnName("Date_Input")
            //        .HasColumnType("datetime")
            //        .HasComment("วันที่นำเข้าข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.Descriptions).HasComment("คำอธิบายรายะเอียด");

            //    entity.Property(e => e.EffectiveDate)
            //        .HasColumnName("Effective_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันที่เริ่มแสดงผล");

            //    entity.Property(e => e.EndDate)
            //        .HasColumnName("End_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันที่สิ้นสุดการแสดงผล");

            //    entity.Property(e => e.MemberOnly)
            //        .HasColumnName("Member_Only")
            //        .HasDefaultValueSql("((0))")
            //        .HasComment("ข้อมูลเฉพาะสมาชิก = true");

            //    entity.Property(e => e.MenuId)
            //        .HasColumnName("Menu_Id")
            //        .HasComment("รหัสหลักข้อมูลเมนู");

            //    entity.Property(e => e.OpenComment)
            //        .HasColumnName("Open_Comment")
            //        .HasComment("เปิดให้สามาระแสดงความคิดเห็นได้");

            //    entity.Property(e => e.OpenRating)
            //        .HasColumnName("Open_Rating")
            //        .HasComment("เปิดให้สามารถใส่ระดับความพึงพอใจบทความได้");

            //    entity.Property(e => e.PostRelatedId)
            //        .HasColumnName("Post_Related_Id")
            //        .HasComment("รหัสหลักข้อมูลบทความที่เกี่ยวข้อง");

            //    entity.Property(e => e.Rating).HasComment("ระดับความชื่อชอบบทความ");

            //    entity.Property(e => e.Recommended).HasComment("สถานะการแนะนำ");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.ShortDescriptions)
            //        .HasColumnName("Short_Descriptions")
            //        .HasComment("คำอธิบายโดยย่อ");

            //    entity.Property(e => e.StyleSort)
            //        .HasColumnName("Style_Sort")
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("ลักษณะการแสดงผล true=มากไปน้อย ,false = น้อยไปมาก");

            //    entity.Property(e => e.TopicName)
            //        .HasColumnName("Topic_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อเรื่อง");

            //    entity.Property(e => e.TypeSort)
            //        .HasColumnName("Type_Sort")
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("รูปแบบการเรียงลำดับ 1=เรียงจากวันที่แก้ไข,2=เรียงจากชื่อ,3=กำหนดเอง");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UrlLink)
            //        .HasColumnName("Url_Link")
            //        .HasComment("ลิงค์ที่เกี่ยวข้อง");

            //    entity.Property(e => e.Views).HasComment("จำนวนผู้เข้าชม");

            //    entity.Property(e => e.Vote).HasComment("จำนวนที่โหวต");
            //});

            //modelBuilder.Entity<PostsComment>(entity =>
            //{
            //    entity.HasKey(e => e.CommentId)
            //        .HasName("PK__POSTS_CO__3214EC2734A081B3");

            //    entity.ToTable("Posts_Comment");

            //    entity.HasComment("ตารางข้อมูลการแสดงความคิดเห็น");

            //    entity.Property(e => e.CommentId)
            //        .HasColumnName("Comment_Id")
            //       
            //        .HasComment("รหัสหลักแบบไม่ซ้ำโดยระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.Approved).HasComment("สถานะการอนุญาตให้แสดงผล");

            //    entity.Property(e => e.Comment)
            //        .IsRequired()
            //        .HasColumnType("ntext")
            //        .HasComment("รายละเอียดความคิดเห็น");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.IpAddress)
            //        .HasColumnName("Ip_Address")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("ที่อยู่ IP ของผู้แสดงความคิดเห็น");

            //    entity.Property(e => e.PostId)
            //        .HasColumnName("Post_Id")
            //        .HasComment("รหัสหลักข้อมูลบทความ");

            //    entity.Property(e => e.ReplyId)
            //        .HasColumnName("Reply_Id")
            //        .HasComment("รหัสหลักข้อมูลการแสดงความคิดเห็น");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<PostsTag>(entity =>
            //{
            //    entity.HasKey(e => e.TagId)
            //        .HasName("PK__Posts_Ta__D0AC5C1305367224");

            //    entity.ToTable("Posts_Tag");

            //    entity.HasComment("ตารางข้อมูลคำค้นหา");

            //    entity.Property(e => e.TagId)
            //        .HasColumnName("Tag_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลคำค้นหา");

            //    entity.Property(e => e.TagName)
            //        .IsRequired()
            //        .HasColumnName("Tag_Name")
            //        .HasMaxLength(255)
            //        .HasComment("คำหลัก");
            //});

            //modelBuilder.Entity<SystemConfigs>(entity =>
            //{
            //    entity.HasKey(e => e.SystemConfigId)
            //        .HasName("PK__system_c__7DF5307506DD7F9E");

            //    entity.ToTable("System_Configs");

            //    entity.HasComment("ตารางข้อมูลการตั้งค่าระบบ");

            //    entity.Property(e => e.SystemConfigId)
            //        .HasColumnName("System_Config_Id")
            //       
            //        .HasComment("รหัสข้อมูลการตั้งค่าระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.System_Config_Category).HasColumnName("System_Config_Category");

            //    entity.Property(e => e.SystemConfigCode)
            //        .IsRequired()
            //        .HasColumnName("System_Config_Code")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("ประเภทการตั้งค่า");

            //    entity.Property(e => e.SystemConfigDescriptions)
            //        .HasColumnName("System_Config_Descriptions")
            //        .HasMaxLength(255)
            //        .HasComment("รายละเอียดการตั้งค่า");

            //    entity.Property(e => e.System_Config_Remark)
            //        .HasColumnName("System_Config_Remark")
            //        .HasMaxLength(255);

            //    entity.Property(e => e.SystemConfigType)
            //        .HasColumnName("System_Config_Type")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.SystemConfigValue)
            //        .HasColumnName("System_Config_Value")
            //        .HasComment("ความหมายการตั้งค่า");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<TmMasterData>(entity =>
            //{
            //    entity.HasKey(e => e.MasterId)
            //        .HasName("PK__master_d__25E2A55208048C89");

            //    entity.ToTable("Tm_Master_Data");

            //    entity.HasComment("ตารางข้อมูลหลักของระบบ");

            //    entity.Property(e => e.MasterId)
            //        .HasColumnName("Master_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลหลักระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.Compulsory).HasComment("สถานะภาคบังคับ");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.Level).HasComment("ลำดับชั้นข้อมูล");

            //    entity.Property(e => e.MasterCode)
            //        .HasColumnName("Master_Code")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสข้อมูลหลักระบบ");

            //    entity.Property(e => e.MasterName)
            //        .IsRequired()
            //        .HasColumnName("Master_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อข้อมูลหลักระบบ");

            //    entity.Property(e => e.MasterParentId)
            //        .HasColumnName("Master_Parent_Id")
            //        .HasComment("รหัสข้อมูลสังกัด");

            //    entity.Property(e => e.MasterRootId)
            //        .HasColumnName("Master_Root_Id")
            //        .HasComment("รหัสข้อมูลที่สังกัดลำดับแรก");

            //    entity.Property(e => e.MasterTypeId)
            //        .HasColumnName("Master_Type_Id")
            //        .HasComment("ประเภทข้อมูลหลักระบบ");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<TmMasterDataType>(entity =>
            //{
            //    entity.HasKey(e => e.MasterTypeId)
            //        .HasName("PK__master_d__ADDE57F3485645FC");

            //    entity.ToTable("Tm_Master_Data_Type");

            //    entity.HasComment("ตารางประเภทข้อมูลหลักของระบบ");

            //    entity.Property(e => e.MasterTypeId)
            //        .HasColumnName("Master_Type_Id")
            //       
            //        .HasComment("รหัสหลักแบบไม่ซ้ำโดยระบบ");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.MasterTypeName)
            //        .IsRequired()
            //        .HasColumnName("Master_Type_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อข้อมูลหลักระบบ");

            //    entity.Property(e => e.MasterTypeParentId)
            //        .HasColumnName("Master_Type_Parent_Id")
            //        .HasComment("รหัสข้อมูลสังกัด");

            //    entity.Property(e => e.MasterTypeRootId)
            //        .HasColumnName("Master_Type_Root_Id")
            //        .HasComment(" รหัสข้อมูลที่สังกัดลำดับแรก");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});
            modelBuilder.Entity<MenuBackoffice>(entity =>
            {
                entity.HasKey(e => e.MenuBackofficeId);
                entity.ToTable("Menu_Backoffice");
                entity.Property(e => e.MenuBackofficeId).HasColumnName("Menu_Backoffice_Id");
                entity.Property(e => e.SectionMenu).HasColumnName("Section_Menu");
                entity.Property(e => e.MenuName).HasColumnName("Menu_Name");
                entity.Property(e => e.RefMenuId).HasColumnName("Ref_Menu_Id");
                entity.Property(e => e.MenuLevel).HasColumnName("Menu_Level");
                entity.Property(e => e.MenuIcon).HasColumnName("Menu_Icon");
                entity.Property(e => e.MenuTypeId).HasColumnName("Menu_Type_Id");
                entity.Property(e => e.ControllerName).HasColumnName("Controller_Name");
                entity.Property(e => e.ActionName).HasColumnName("Action_Name");
                entity.Property(e => e.Parameter).HasColumnName("Parameter");
                entity.Property(e => e.Sequence).HasColumnName("Sequence");
                entity.Property(e => e.Actived).HasColumnName("Actived");
                entity.Property(e => e.CreateDate).HasColumnName("Create_Date");
                entity.Property(e => e.CreateBy).HasColumnName("Create_By");
                entity.Property(e => e.UpdateDate).HasColumnName("Update_Date");
                entity.Property(e => e.UpdateBy).HasColumnName("Update_By");
            });
            //modelBuilder.Entity<TmMenu>(entity =>
            //{
            //    entity.HasKey(e => e.MenuId)
            //        .HasName("PK__tm_menus__69E723383FDC0464");

            //    entity.ToTable("Tm_Menu");
            //        //entity.ToTable("Tm_Menu_copy");

            //        entity.HasComment("ตารางข้อมูลเมนู");

            //    entity.Property(e => e.MenuId)
            //        .HasColumnName("Menu_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลเมนู");

            //    entity.Property(e => e.BackAction)
            //        .HasColumnName("Back_Action")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อ Action (หลังบ้าน)");

            //    entity.Property(e => e.BackActiveted)
            //        .HasColumnName("Back_Activeted")
            //        .HasComment("สถานะการใช้งาน (หลังบ้าน)");

            //    entity.Property(e => e.BackClickable)
            //        .HasColumnName("Back_Clickable")
            //        .HasComment("สถานะการให้คลิกได้ (หลังบ้าน) ");

            //    entity.Property(e => e.BackController)
            //        .HasColumnName("Back_Controller")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อ Controller (หลังบ้าน)");

            //    entity.Property(e => e.BackIcon)
            //        .HasColumnName("Back_Icon")
            //        .HasMaxLength(50)
            //        .HasComment("คลาสไอคอน (หลังบ้าน) ");

            //    entity.Property(e => e.BackLevel)
            //        .HasColumnName("Back_Level")
            //        .HasComment("ระดับของเมนู (หลังบ้าน)");

            //    entity.Property(e => e.BackParentId)
            //        .HasColumnName("Back_Parent_Id")
            //        .HasComment("รหัสหลักเมนูที่สังกัด (หลังบ้าน) ");

            //    entity.Property(e => e.BackSequence)
            //        .HasColumnName("Back_Sequence")
            //        .HasComment("ลำดับการแสดงผล (หลังบ้าน) ");

            //    entity.Property(e => e.BackUrl)
            //        .HasColumnName("Back_Url")
            //        .HasMaxLength(200)
            //        .HasComment("ลิงค์  (หลังบ้าน)");

            //    entity.Property(e => e.FrontAction)
            //        .HasColumnName("Front_Action")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อ Action (หน้าบ้าน)");

            //    entity.Property(e => e.FrontActiveted)
            //        .HasColumnName("Front_Activeted")
            //        .HasComment("สถานะการใช้งาน (หน้าบ้าน)");

            //    entity.Property(e => e.FrontClickable)
            //        .HasColumnName("Front_Clickable")
            //        .HasComment("สถานะการให้คลิกได้ (หน้าบ้าน)");

            //    entity.Property(e => e.FrontController)
            //        .HasColumnName("Front_Controller")
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อ Controller (หน้าบ้าน)");

            //    entity.Property(e => e.FrontIcon)
            //        .HasColumnName("Front_Icon")
            //        .HasMaxLength(50)
            //        .HasComment("คลาสไอคอน (หน้าบ้าน)");

            //    entity.Property(e => e.FrontLevel)
            //        .HasColumnName("Front_Level")
            //        .HasComment("ระดับของเมนู (หน้าบ้าน)");

            //    entity.Property(e => e.FrontParentId)
            //        .HasColumnName("Front_Parent_Id")
            //        .HasComment("รหัสหลักเมนูที่สังกัด (หน้าบ้าน)");

            //    entity.Property(e => e.FrontSequence)
            //        .HasColumnName("Front_Sequence")
            //        .HasComment("ลำดับการแสดงผล (หน้าบ้าน)");

            //    entity.Property(e => e.FrontUrl)
            //        .HasColumnName("Front_Url")
            //        .HasMaxLength(200)
            //        .HasComment("ลิงค์ (หน้าบ้าน)");

            //    entity.Property(e => e.MenuName)
            //        .HasColumnName("Menu_Name")
            //        .HasMaxLength(100)
            //        .HasComment("ชื่อเมนู");

            //    entity.Property(e => e.MenuTypeId)
            //        .HasColumnName("Menu_Type_Id")
            //        .HasComment("รหัสหลักข้อมูลประเภทเมนู");

            //    entity.Property(e => e.Permalink)
            //        .HasMaxLength(50)
            //        .HasComment("ชื่อแทน url");
            //});

            //modelBuilder.Entity<TmMenuPosition>(entity =>
            //{
            //    entity.HasKey(e => e.MenuPositionId)
            //        .HasName("PK_Tm_Module");

            //    entity.ToTable("Tm_Menu_Position");

            //    entity.HasComment("ตารางข้อมูลตำแหน่งแสดงผลเมนู");

            //    entity.Property(e => e.MenuPositionId)
            //        .HasColumnName("Menu_Position_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลโมโดูล");

            //    entity.Property(e => e.ForBackend).HasColumnName("For_Backend");

            //    entity.Property(e => e.PositionName)
            //        .HasColumnName("Position_name")
            //        .HasMaxLength(50);
            //});

            //modelBuilder.Entity<TmMenuRelated>(entity =>
            //{
            //    entity.HasKey(e => e.MenuRelateId)
            //        .HasName("PK__Tm_Modul__E1C65640DFB95F13");

            //    entity.ToTable("Tm_Menu_Related");

            //    entity.HasComment("ตารางข้อมูลความสัมพันธ์ระหว่างตำแหน่งและเมนู");

            //    entity.Property(e => e.MenuRelateId)
            //        .HasColumnName("Menu_Relate_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลความสัมพันธ์ตำแหน่งกับเมนู");

            //    entity.Property(e => e.MenuId)
            //        .HasColumnName("Menu_Id")
            //        .HasComment("รหัสเมนู");

            //    entity.Property(e => e.MenuPositionId)
            //        .HasColumnName("Menu_Position_Id")
            //        .HasComment("รหัสตำแหน่งเมนู");

            //    entity.Property(e => e.Mode).HasComment("สิทธิ์การใช้งาน 0= ไม่มีสิทธิ์ , 1 = ดูเท่านั้น , 2 = จัดการ");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");
            //});

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("Tm_Role");

                entity.HasComment("ตารางข้อมูลกำหนดบทบาทของกลุ่มสิทธิ์การใช้งาน");

                entity.Property(e => e.RoleId).HasColumnName("Role_Id");
                entity.Property(e => e.RoleGroupId).HasColumnName("Role_Group_Id");
                entity.Property(e => e.MenuId).HasColumnName("Menu_Id");
                entity.Property(e => e.RoleType).HasColumnName("Role_Type");
            });

            modelBuilder.Entity<RoleGroup>(entity =>
            {
                entity.HasKey(e => e.RoleGroupId);

                entity.ToTable("Role_Group");

                entity.HasComment("ตารางข้อมูลกลุ่มสิทธิ์การใช้งาน");

                entity.Property(e => e.RoleGroupId)
                    .HasColumnName("Role_Group_Id")
                    .HasComment("รหัสหลักกลุ่มบทบาทการทำงาน");

                entity.Property(e => e.Actived).HasComment("สถานะการใช้งาน");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("Create_By")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("รหัสผู้สร้างข้อมูล");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime")
                    .HasComment("วันเวลาที่สร้างข้อมูล");

                entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

                entity.Property(e => e.RoleGroupName)
                    .HasColumnName("Role_Group_Name")
                    .HasMaxLength(255)
                    .HasComment("ชื่อกลุ่มบทบาทการทำงาน");

                entity.Property(e => e.UpdateBy)
                    .IsRequired()
                    .HasColumnName("Update_By")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasColumnType("datetime")
                    .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            });

            //modelBuilder.Entity<TmStatus>(entity =>
            //{
            //    entity.HasKey(e => e.StatusId)
            //        .HasName("PK__tm_statu__519009AC0974BD5F");

            //    entity.ToTable("Tm_Status");

            //    entity.HasComment("ตารางข้อมูลสถานะการทำงาน");

            //    entity.Property(e => e.StatusId)
            //        .HasColumnName("Status_ID")
            //       
            //        .HasComment("รหัสหลักข้อมูลสถานะการทำงาน");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.classStatus)
            //       .HasColumnName("Class")
            //       .HasMaxLength(255)
            //       .HasComment("");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");
            //    entity.Property(e => e.Sequence).HasComment("ลำดับการทำงาน");
            //    entity.Property(e => e.Mode).HasComment("โหมด");

            //    entity.Property(e => e.StatusName)
            //        .IsRequired()
            //        .HasColumnName("Status_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อข้อมูลหลักระบบ");

            //    entity.Property(e => e.StatusTypeId)
            //        .HasColumnName("Status_Type_ID")
            //        .HasComment("รหัสหลักประเภทสถานะการดำเนินงาน");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);
                entity.Property(e => e.UserId) .HasColumnName("User_Id");
                entity.Property(e => e.Email).HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("อีเมล์สำหรับติดต่อ");

                entity.Property(e => e.FirstName)
                    .HasColumnName("First_Name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("ชื่อจริงของผู้ใช้งาน");

                entity.Property(e => e.LastName)
                    .HasColumnName("Last_Name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("นามสกุลของผู้ใช้งาน");

              

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .HasComment("รหัสผ่าน");

                entity.Property(e => e.RoleGroupId)
                    .HasColumnName("Role_Group_Id")
                    .HasComment("รหัสหลักกลุ่มสิทธิ์");

                //entity.Property(e => e.TitleNameCode)
                //    .HasColumnName("Title_Name_Code")
                //    .HasMaxLength(20)
                //    .IsUnicode(false)
                //    .HasComment("รหัสคำนำหน้า");

                entity.Property(e => e.Username)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("ชื่อผู้ใช้งาน");

                entity.Property(e => e.Actived).HasComment("สถานะการใช้งาน");

                entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

                entity.Property(e => e.CreateBy)
              .IsRequired()
              .HasColumnName("Create_By")
              .HasMaxLength(20)
              .IsUnicode(false)
              .HasComment("รหัสผู้สร้างข้อมูล");
                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime")
                    
                    .HasComment("วันเวลาที่สร้างข้อมูล");
                entity.Property(e => e.UpdateBy)
                    .IsRequired()
                    .HasColumnName("Update_By")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");
                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasColumnType("datetime")
                    
                    .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            });

            //modelBuilder.Entity<WebboardCategory>(entity =>
            //{
            //    entity.ToTable("Webboard_Category");

            //    entity.HasComment("ตารางข้อมูลหมวดหมู่กระทู้");
            //    entity.HasKey(e => e.WebboardCategoryId);
            //    entity.Property(e => e.WebboardCategoryId)
            //        .HasColumnName("Webboard_Category_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลหมวดหมู่กระทู้");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.Approved).HasComment("สถานะการอนุญาตเผยแพร่ข้อมูล");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.Descriptions).HasComment("รายละเอียดกระทู้");

            //    entity.Property(e => e.MemberOnly)
            //        .HasColumnName("Member_Only")
            //        .HasDefaultValueSql("((0))")
            //        .HasComment("ข้อมูลเฉพาะสมาชิก = true");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.TopicName)
            //        .HasColumnName("Topic_Name")
            //        .HasMaxLength(255)
            //        .HasComment("ชื่อเรื่องกระทู้");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<WebboardReply>(entity =>
            //{
            //    entity.HasKey(e => e.ReplyId)
            //        .HasName("PK__webboard__3214EC278FBF0547");

            //    entity.ToTable("Webboard_Reply");

            //    entity.HasComment("ตารางข้อมูลการตอบกระทู้");

            //    entity.Property(e => e.ReplyId)
            //        .HasColumnName("Reply_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลการตอบกระทู้");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.DeviceName)
            //        .HasColumnName("Device_Name")
            //        .HasComment("ชื่ออุปกรณ์");

            //    entity.Property(e => e.IpAddress)
            //        .IsRequired()
            //        .HasColumnName("Ip_Address")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("ที่อยู่ไอพี");

            //    entity.Property(e => e.ReplyDescriptions)
            //        .IsRequired()
            //        .HasColumnName("Reply_Descriptions")
            //        .HasColumnType("ntext")
            //        .HasComment("ข้อความการตอบกระทู้");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.WebboardTopicId)
            //        .HasColumnName("Webboard_Topic_Id")
            //        .HasComment("รหัสข้อมูลกระทู้");
            //});

            //modelBuilder.Entity<WebboardReports>(entity =>
            //{
            //    entity.ToTable("Webboard_Reports");
            //    entity.HasKey(e => e.Webboard_Reports_Id);
            //    entity.Property(e => e.Webboard_Reports_Id)
            //        .HasColumnName("Webboard_Reports_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลกระทู้");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Description).HasMaxLength(255);

            //    entity.Property(e => e.DeviceName)
            //        .HasColumnName("Device_Name")
            //        .HasComment("ชื่ออุปกรณ์");

            //    entity.Property(e => e.IpAddress)
            //        .IsRequired()
            //        .HasColumnName("Ip_Address")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("ที่อยู่ไอพี");

            //    entity.Property(e => e.Webboard_Topic_Id).HasColumnName("Webboard_Topic_Id");
            //});

            //modelBuilder.Entity<WebboardReservedWord>(entity =>
            //{
            //    entity.HasKey(e => e.ReservedWordId)
            //        .HasName("PK__reservew__3214EC27FB3BA89E");

            //    entity.ToTable("Webboard_Reserved_Word");

            //    entity.HasComment("ตารางข้อมูลคำสงวน");
            //    entity.HasKey(e => e.ReservedWordId);
            //    entity.Property(e => e.ReservedWordId)
            //        .HasColumnName("Reserved_Word_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลคำสงวน");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.ReplaceWord)
            //        .HasColumnName("Replace_Word")
            //        .HasMaxLength(50)
            //        .HasComment("คำแทนที่คำสงวน");

            //    entity.Property(e => e.ReservedWord)
            //        .HasColumnName("Reserved_Word")
            //        .HasMaxLength(50)
            //        .HasComment("คำสงวน");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});

            //modelBuilder.Entity<WebboardTopic>(entity =>
            //{
            //    entity.ToTable("Webboard_Topic");

            //    entity.HasComment("ตารางข้อมูลกระทู้");
            //    entity.HasKey(e => e.WebboardTopicId);
            //    entity.Property(e => e.WebboardTopicId)
            //        .HasColumnName("Webboard_Topic_Id")
            //       
            //        .HasComment("รหัสหลักข้อมูลกระทู้");

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.AliasName)
            //        .HasColumnName("Alias_Name")
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .HasComment("นามแฝงผู้สร้างกระทู้");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

            //    entity.Property(e => e.DeviceName)
            //        .HasColumnName("Device_Name")
            //        .HasComment("ชื่ออุปกรณ์");

            //    entity.Property(e => e.IpAddress)
            //        .IsRequired()
            //        .HasColumnName("Ip_Address")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("ที่อยู่ไอพี");

            //    entity.Property(e => e.IsAlias)
            //        .HasColumnName("Is_Alias")
            //        .HasComment("ต้องการใช้นามแฝง");

            //    entity.Property(e => e.Islock)
            //        .HasColumnName("Is_lock")
            //        .HasComment("เปิดให้แสดงความคิดเห็น");

            //    entity.Property(e => e.Keywords).HasMaxLength(2000);

            //    entity.Property(e => e.Pin).HasComment("ปักหมุดให้อยุ่ด้านหน้าเสมอ");

            //    entity.Property(e => e.Reply).HasComment("จำนวนข้อความที่ตอบกระทู้");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.StyleSort)
            //        .HasColumnName("Style_Sort")
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("ลักษณะการแสดงผล true=มากไปน้อย ,false = น้อยไปมาก");

            //    entity.Property(e => e.TypeSort)
            //        .HasColumnName("Type_Sort")
            //        .HasDefaultValueSql("((1))")
            //        .HasComment("รูปแบบการเรียงลำดับ 1=เรียงจากวันที่แก้ไข,2=เรียงจากชื่อ,3=กำหนดเอง");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.Views).HasComment("จำนวนครั้งที่เข้าอ่านกระทู้");

            //    entity.Property(e => e.WebboardCategoryId)
            //        .HasColumnName("Webboard_Category_Id")
            //        .HasComment("รหัสหลักข้อมูลกระทู้");

            //    entity.Property(e => e.WebboardTopicDecriptions)
            //        .IsRequired()
            //        .HasColumnName("Webboard_Topic_Decriptions")
            //        .HasColumnType("ntext")
            //        .HasComment("รายละเอียดกระทู้");

            //    entity.Property(e => e.WebboardTopicName)
            //        .IsRequired()
            //        .HasColumnName("Webboard_Topic_Name")
            //        .HasComment("ชื่อกระทู้");
            //});
            
            //modelBuilder.Entity<Projects>(entity =>
            //{
            //    entity.HasKey(e => e.ProjectId);

            //    entity.ToTable("Projects");
            //    entity.Property(e => e.ProjectId)
            //       .HasColumnName("Project_Id")
            //      ;
            //    entity.HasComment("ตารางข้อมูลโครงการ");
            //    entity.Property(e => e.YearBudget)
            //    .HasColumnName("Year_Budget");
            //    entity.Property(e => e.ProjectNo)
            //       .IsRequired()
            //       .HasColumnName("Project_No")
            //       .HasMaxLength(50);
            //    entity.Property(e => e.ProjectName)
            //     .IsRequired()
            //     .HasColumnName("Project_Name");
            //    entity.Property(e => e.ProjectDescription)
            //      .HasColumnName("Project_Description");
            //    entity.Property(e => e.ProjectType)
            //   .HasColumnName("Project_Type");
            //    entity.Property(e => e.OrgId)
            //    .HasColumnName("Org_Id");
              

            //    entity.Property(e => e.Activated).HasComment("สถานะการใช้งาน");

            //    entity.Property(e => e.CreateBy)
            //        .IsRequired()
            //        .HasColumnName("Create_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");

            //    entity.Property(e => e.Deleted).HasComment("สถานะการลบข้อมูล");

                


            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //});
          
            ////  public virtual DbSet<Log_auto_noti> Logautonoti { get; set; }
            //modelBuilder.Entity<Log_auto_noti>(entity =>
            //{
            //    entity.ToTable("Log_auto_noti");
            //    entity.HasKey(e => e.ID);
            //    entity.Property(e => e.ID)
            //        .HasColumnName("ID"); 
            //    entity.Property(e => e.StampDate).HasColumnName("StampDate");
            //    entity.Property(e => e.Remark).HasColumnName("Remark");
            //    entity.Property(e => e.Issuccess).HasColumnName("Issuccess");

                

            //});

           
            //modelBuilder.Entity<Tm_Positions>(entity =>
            //{
            //    entity.ToTable("Tm_Positions");
            //    entity.HasKey(e => e.Position_Id);
            //    entity.Property(e => e.Position_Id)
            //        .HasColumnName("Position_Id")
            //       ;

            //    entity.Property(e => e.Position_Name).HasColumnName("Position_Name");

            //    entity.Property(e => e.Position_Level).HasColumnName("Position_Level");

            //    entity.Property(e => e.Sequence).HasComment("ลำดับการแสดงผล");

            //    entity.Property(e => e.UpdateBy)
            //        .IsRequired()
            //        .HasColumnName("Update_By")
            //        .HasMaxLength(20)
            //        .IsUnicode(false)
            //        .HasComment("รหัสผู้ที่แก้ไขข้อมูลล่าสุด");

            //    entity.Property(e => e.UpdateDate)
            //        .HasColumnName("Update_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่แก้ไขข้อมูลล่าสุด");
            //    entity.Property(e => e.CreateBy)
            //       .IsRequired()
            //       .HasColumnName("Create_By")
            //       .HasMaxLength(20)
            //       .IsUnicode(false)
            //       .HasComment("รหัสผู้สร้างข้อมูล");

            //    entity.Property(e => e.CreateDate)
            //        .HasColumnName("Create_Date")
            //        .HasColumnType("datetime")
            //        
            //        .HasComment("วันเวลาที่สร้างข้อมูล");
            //});

           
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
