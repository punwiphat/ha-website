﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Core.Common;
using Core.DAL.Models;
using Core;
using Core.library;

namespace Core.Backoffice.Controllers
{
    public class LanguageTabViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public LanguageTabViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!_cache.TryGetValue("LanguageList", out List<Languages> obj))
            {
                obj = LanguageData.GetByActive();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(365), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(365) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                options.RegisterPostEvictionCallback(Callback, "Some state info");
                _cache.Set("LanguageList", obj, TimeSpan.FromDays(365));
           }
            return await Task.FromResult((IViewComponentResult)View("Default", obj));
        }
        private void Callback(object key, object value, EvictionReason reason, object state)
        {
            Console.WriteLine("Evicted");
        }
    }
}
