﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Backoffice.Controllers
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public MenuViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int UserRoleGroupId, int Username)
        {
            //if (!_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> obj))
            //{
            List<Tm_Menu> list_menu = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
            var role = RoleData.Get.RoleByGroupID(UserRoleGroupId).Where(o => o.RoleType != "N").ToList();
            List<Tm_Menu> obj = (from o in list_menu
                               join r in role on o.ID equals r.MenuID
                               where r.RoleType != "N"
                               select new
                               {
                                   o.ID
                                   ,
                                   o.img_front
                                   ,
                                   o.IsEdit
                                   ,
                                   o.LangCode
                                   ,
                                   o.levels
                                   ,
                                   o.link_front
                                   ,
                                   o.lstobj
                                   ,
                                   o.MENU_BACK_ID
                                   ,
                                   o.MENU_FRONT_ID
                                   ,
                                   o.menu_name_back
                                   ,
                                   o.Menuname
                                   ,
                                   o.MenuNameEN
                                   ,
                                   o.MenuNameTH
                                   ,
                                   o.MenuTypeCode
                                   ,
                                   o.newMenuDownloadID
                                   ,
                                   o.ParentMenuname
                                   ,
                                   o.permalink_back
                                   ,
                                   o.permalink_front
                                   ,
                                   o.RowNumber
                                   ,
                                   o.section_name_back
                                   ,
                                   o.self_back
                                   ,
                                   o.self_front
                                   ,
                                   o.Sequence
                                   ,
                                   o.show_back
                                   ,
                                   o.show_front
                                   ,
                                   o.sort_back
                                   ,
                                   o.sort_front
                                   ,
                                   o.Sub_Menu
                                   ,
                                   o.type_show
                                   ,
                                   o.UpdatedBy
                                   ,
                                   o.UpdatedDate
                                   ,
                                   o.action_back
                                   ,
                                   o.action_front
                                   ,
                                   o.Actived
                                   ,
                                   o.BelongID
                                   ,
                                   o.CategoryID
                                   ,
                                   o.controller_back
                                   ,
                                   o.controller_front
                                   ,
                                   o.CreatedBy
                                   ,
                                   o.CreatedDate

                                   ,
                                   o.Disabled
                                   ,
                                   o.FPermalink
                                   ,
                                   o.icon_back
                                   ,
                                   o.icon_front

                               }).ToList_list<Tm_Menu>();
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //                                                                // SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    //options.RegisterPostEvictionCallback(Callback, "Some state info");
            //    _cache.Set("MenuList" + UserRoleGroupId.ToString(), obj, TimeSpan.FromDays(1));
            //}


            List<MenuGenBackoffice> result_menu = new List<MenuGenBackoffice>();
            result_menu = obj.GroupBy(o => o.section_name_back).Select(g => new { section_name_back = g.Key, lstobj = g.ToList() }).ToList_list<MenuGenBackoffice>();
            return await Task.FromResult((IViewComponentResult)View("Default", result_menu));
        }
       
    }
    //public class LogsTabViewComponent : ViewComponent
    //{
    //    public async Task<IViewComponentResult> InvokeAsync(string LangID, string Menuname)
    //    {
    //        return View("~/Views/PanelSystem/Logs/_list.cshtml");
    //    }
    //}
  
}
