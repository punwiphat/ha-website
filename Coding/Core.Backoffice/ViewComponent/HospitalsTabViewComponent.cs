﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Backoffice.Controllers
{
    public class HospitalsTabViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public HospitalsTabViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            ViewBag.LangID = LangID;
            return View("~/Views/Hospital/Hospitals/_list.cshtml");
        }
    }
    public class LogsTabViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public LogsTabViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            ViewBag.LangID = LangID;
            return View("~/Views/PanelSystem/Logs/_list.cshtml");
        }
    }
}
