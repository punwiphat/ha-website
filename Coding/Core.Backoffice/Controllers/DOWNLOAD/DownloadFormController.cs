﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class DownloadFormController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public DownloadFormController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<DownloadFormByLanguage> lstobj = new List<DownloadFormByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                //TempData[MenuID+"_" + a.LanguageCode] = "";
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                // Session.Remove(MenuID + "_" + a.LanguageCode);
                DownloadFormByLanguage obj = new DownloadFormByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstDownloadForm = DownloadFormData.Get.ByMenu(MenuID, a.LanguageCode);

                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            return View(lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                ViewBag.MenuID = MenuID;
                SettingFiles LimitSetting = SettingFileData.Get.All();
                //if (!_cache.TryGetValue("SettingFile" + UserRoleGroupId.ToString(), out SettingFiles LimitSetting))
                //{
                //    LimitSetting = SettingFileData.Get.All();
                //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                //    {
                //        AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                //        SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                //    };
                //    _cache.Set("SettingFile", LimitSetting, TimeSpan.FromDays(365));
                //}
                var main = !string.IsNullOrEmpty(ID) ? DownloadFormData.Get.MainDownloadFormById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new DownloadForm();
                var obj = DownloadFormData.Get.DownloadFormById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                main = main == null ? new DownloadForm() : main;
                main.MenuID = MenuID;
                main.lstDownloadForm = obj;
                main.Menuname = menuname;
                foreach (var item in obj)
                {
                    item.LimitFile = LimitSetting;
                }
                var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
                main.GUID = Guid.NewGuid().ToString();
                main.CategoryDLLID = MenuDisplay.CategoryID;
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");

                // ViewBag.DDLCategory = CategoryData.Get.ByMenuIdActive(MenuDisplay.CategoryID);
                return View("~/Views/DownloadForm/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View(new DownloadForm());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(DownloadForm DownloadForm)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = DownloadFormData.Post.AddOrUpdate(DownloadForm, UserName, IpAddress, Browser);
                    foreach (var o in DownloadForm.lstDownloadForm)
                    {
                        o.DownloadFormID = main.ID;
                    }

                    var detail = DownloadFormData.Post.DetailAddOrUpdate(DownloadForm.lstDownloadForm, UserName, IpAddress, Browser);
                    main.lstDownloadForm = detail;
                    foreach (var item in detail)
                    {
                        #region uploadfile
                        string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                        List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                          JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                        if (lstUploads.Count() > 0)
                        {
                            foreach (var item2 in lstUploads)
                            {
                                var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.DownloadFormDetailID.ToString(), UserName, IpAddress, Browser);
                            }
                        }
                        #endregion
                    }
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //DownloadForm.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", DownloadForm);
                }
            }
            //DownloadForm.lstobj = Contents;
            return View("~/Views/DownloadForm/Manage.cshtml", DownloadForm);
        }
    }
}
