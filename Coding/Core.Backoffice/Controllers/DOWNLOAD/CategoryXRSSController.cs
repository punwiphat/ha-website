﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class CategoryXRSSController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public CategoryXRSSController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            var Obj = CategoryData.Get.ByMenuID(MenuID);
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            ViewBag.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
            ViewBag.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;

            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/CategoryRSS/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname, int ID)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            Category Obj = new Category();
            if (ID != 0)
                Obj = CategoryData.Get.ByID(ID);
            Obj.MenuID = MenuID;
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/CategoryRSS/Manage.cshtml", Obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Category Obj, string menuname)
        {
            ViewBag.MenuID = Obj.MenuID;
            ViewBag.Title = Obj.Menuname;
            try
            {
                var items = CategoryData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                return Json(new { status = true, location = Url.Action("Index", "CategoryXRSS") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

    }
}
