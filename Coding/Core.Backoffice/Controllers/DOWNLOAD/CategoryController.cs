﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class CategoryController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public CategoryController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            var Obj = CategoryData.Get.ByMenuID(MenuID);
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            ViewBag.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
            ViewBag.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;

            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/Category/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname, int ID)
        {
            int refID = ID;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                Category Obj = new Category();
                if (ID != 0)
                    Obj = CategoryData.Get.ByID(ID);
                Obj.MenuID = MenuID;

                string logDescription = "";
                if (Obj !=null)
                {
                    logDescription = "ดูรายละเอียดข้อมูล Category : " + Obj.NameTH;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
                return View("~/Views/Category/Manage.cshtml", Obj);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
            }
            return RedirectToAction("Index", new { Menuname = menuname });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Category Obj, string menuname)
        {
            ViewBag.MenuID = Obj.MenuID;
            ViewBag.Title = Obj.Menuname;
            try
            {
                var items = CategoryData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                return Json(new { status = true, location = Url.Action("Index", "Category") });
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(Obj.ID, LogTransaction.Get.ActivityType.Error, Obj.Menuname, UserName, ex.Message, IpAddress, Browser);
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

    }
}
