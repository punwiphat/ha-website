﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class PersonalsController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public PersonalsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<StaffByLanguage> lstobj = new List<StaffByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                StaffByLanguage obj = new StaffByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstStaff = StaffData.Get.ByMenuID(MenuID, a.LanguageCode);

                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            return View("~/Views/Personals/index.cshtml", lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = !string.IsNullOrEmpty(ID) ? StaffData.Get.MainStaffById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Staff();
                var obj = StaffData.Get.StaffById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                main = main == null ? new Staff() : main;
                main.MenuID = MenuID;
                main.lstStaff = obj;
                main.Menuname = menuname;
                // main._Levels = main.Levels ?? 0;
                main.GUID = Guid.NewGuid().ToString();
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");

                return View("~/Views/Personals/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View("~/Views/Personals/Manage.cshtml", new Staff());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Staff Staff)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = StaffData.Post.AddOrUpdate(Staff, UserName, IpAddress, Browser);
                    foreach (var o in Staff.lstStaff)
                    {
                        o.StaffID = main.ID;
                    }
                    var detail = StaffData.Post.DetailAddOrUpdate(Staff.lstStaff, UserName, IpAddress, Browser, 10);
                    main.lstStaff = detail;
                    foreach (var item in detail)
                    {
                        #region uploadfile
                        string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                        List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                          JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                        if (lstUploads.Count() > 0)
                        {
                            foreach (var item2 in lstUploads)
                            {
                                var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.StaffDetailID.ToString(), UserName, IpAddress, Browser);
                            }
                        }

                        #endregion
                    }
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                    // return View("~/Views/Staff/Index.cshtml", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //Staff.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", Staff);
                }
            }
            return View("~/Views/Personals/Manage.cshtml", Staff);
        }
    }
}
