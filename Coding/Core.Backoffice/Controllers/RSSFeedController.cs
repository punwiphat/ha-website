﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class RSSFeedController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        
        public RSSFeedController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector,IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<RSSFeedByLanguage> lstobj = new List<RSSFeedByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                RSSFeedByLanguage obj = new RSSFeedByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstRSSFeed = RSSFeedData.Get.ByMenu(MenuID, a.LanguageCode);

                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            return View("~/Views/RSSFeed/index.cshtml", lstobj);
        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
                {
                    lang = LanguageData.GetByActive();
                }
                var obj = RSSFeedData.Get.RSSFeedById(!string.IsNullOrEmpty(ID)?int.Parse(ID):0).ToList();
                
                var main = !string.IsNullOrEmpty(ID)?RSSFeedData.Get.MainRSSFeedById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new RSSFeed();
                main = main == null ? new RSSFeed() : main;
                main.MenuID = MenuID;
                main.lstRSSFeed = obj;
                main.Menuname = menuname;
                var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
                main.MenuIDCategory = MenuDisplay != null ? MenuDisplay.CategoryID:0;
                return View("~/Views/RSSFeed/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
               // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/RSSFeed/Manage.cshtml");
            }
                return View(new RSSFeed());
        }
        [HttpPost]
        public ActionResult Manage(RSSFeed RSSFeed)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = RSSFeedData.Post.AddOrUpdate(RSSFeed, UserName, IpAddress, Browser);
                    foreach (var o in RSSFeed.lstRSSFeed)
                    {
                        o.RSSFeedID = main.ID;
                    }
                    var detail = RSSFeedData.Post.DetailAddOrUpdate(RSSFeed.lstRSSFeed, UserName, IpAddress, Browser);
                    main.lstRSSFeed = detail;
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //RSSFeed.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/RSSFeed/Manage.cshtml", RSSFeed);
                }
            }
            return View("~/Views/RSSFeed/Manage.cshtml", RSSFeed);
        }
    }
}
