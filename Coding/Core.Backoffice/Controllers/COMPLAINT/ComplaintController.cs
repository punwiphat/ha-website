﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ComplaintController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ComplaintController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            var Obj = ComplaintData.Get.AllByCheckRoleAdmin(UserRoleGroupId);
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/Complaint/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname, int ID)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            Complaint Obj = new Complaint();
            if (ID != 0)
            {
                Obj = ComplaintData.Get.ByID(ID);
            }
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/Complaint/Manage.cshtml", Obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Complaint Obj)
        {
            try
            {
                ViewBag.Title = Obj.Menuname;
                var items = ComplaintData.Post.Responsed(Obj.ID, Obj.ResponseMessage, UserName, IpAddress, Browser);
                if (items)
                {
                    return Json(new { status = true, location = Url.Action("Index", "Complaint") });
                }
                else
                {
                    return Json(new { status = false, message = "ไม่สามารถบันทึกข้อมูลได้" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }



    }
}
