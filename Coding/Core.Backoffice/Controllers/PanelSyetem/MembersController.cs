﻿using System;
using Core.Common;
using Core.Common.Models;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class MembersController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public MembersController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        //public async Task<IActionResult> Index()
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            var obj = MemberData.Get.GetAll();
            ViewBag.MenuID = 30;

            return View("~/Views/PanelSystem/Members/Index.cshtml", obj);// await PaginatedList<Members>.CreateAsync(obj, pageNumber ?? 1, Constants.Config.pageSize));
        }

        public IActionResult Itemslist()
        {
            var obj = MemberData.Get.GetAll();
            return PartialView("~/Views/PanelSystem/Members/_Itemslist.cshtml", obj);
        }

        [Authorize]
        public IActionResult Manage(string menuname,int MemberID)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            Members Obj = new Members();
            Obj.Actived = true;
            ViewBag.MenuID = 30;
            if (MemberID != 0)
            {
                Obj = MemberData.Get.ByID(MemberID);
            }
            return View("~/Views/PanelSystem/Members/Manage.cshtml", Obj);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Members Obj)
        {
            try
            {
                bool mode = false;
                if (Obj.MemberID != 0)
                {
                    mode = true;
                }
                var user = MemberData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                //if (!mode && user.Actived)
                //{
                //    //var Template = Get_Email_Template.Get.ByID(Get_Email_Template.Get.MasterType.register);
                //    //string _html = Template.EmailDescriptions.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://www.osmeplegal.com");
                //    //var _mail = Get_MailConfig.Get.All();
                //    //SendEmail sendEmail = new SendEmail(_mail.AliasName, _mail.Mailname, Encryption.Decrypt(_mail.Password), _mail.Smtp, _mail.Port, _mail.EnableSsl, _mail.UseDefaultCredentials);
                //    //sendEmail.SendMail(user.Email, Template.EmailSubject, _html);
                //}
                return Json(new { status = true, location = Url.Action("Index", "Members") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }


        [HttpPost]
        [Authorize]
        public JsonResult Delete(int MemberID, string menuName)
        {
            try
            {
                return Json(new { status = true });
                var Items = MemberData.Post.Delete(MemberID, UserName);
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult resetpass(int MemberID)
        {
            try
            {
                var user = MemberData.Post.resetpassword(MemberID, UserName);
                var Template = EmailTemplateData.Get.ByID(TemplateNum.B_ResetPassword, "TH");
                string _html = Template.HTMLContent.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://www.ha.or.th");
                // string _html = ""
                var _mail = SettingEmailData.Get.All();
                string pwd = Encryption.Decrypt(_mail.Password);
                SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                sendEmail.SendMail(user.Email, Template.Subject, _html);
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }



        [HttpPost]
        [Authorize]
        public JsonResult UpdateStatus(int MemberID, Boolean Status)
        {
            try
            {
                var Items = MemberData.Post.Status(MemberID, Status, UserName);
                return Json(new { status = true, Items });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }

        [Authorize]
        public IActionResult Profile(int MemberID)
        {
            return View("~/Views/PanelSystem/Members/Profile.cshtml", MemberData.Get.ByID(MemberID));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Profile(Members Obj)
        {
            try
            {
                MemberData.Post.AddOrUpdate_profile(Obj, UserName);
                return Json(new { status = true, location = Url.Action("Profile", "Members") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
        [HttpGet]
        public JsonResult emailvalidation(int MemberID, string Email)
        {
            try
            {

                return Json(new { status = MemberData.Get.EmailValidation(Email, MemberID) });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }



        [Authorize]
        public IActionResult changepass()
        {
            return View("~/Views/PanelSystem/Members/changepass.cshtml", new ChangPasswordModel() { });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult changepass(ChangPasswordModel Obj, int MemberID)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (MemberData.Post.AddOrUpdate_password(MemberID, Obj.CurrentPassword, Obj.ConfirmPassword, UserName))
                    {
                        return Json(new { status = true, location = Url.Action("changepass", "Members") });
                    }
                    else
                    {
                        return Json(new { status = false, message = "ข้อมูลไม่ถูกต้อง" });
                    }
                }
                else
                {
                    return Json(new { status = false, message = "ข้อมูลไม่ถูกต้อง" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

    }
}
