﻿using System;
using System.Collections.Generic;
using Core.Common;
using Core.DAL.Models;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class RoleGroupController : BaseController
    {
        public static string menuname = "กลุ่มสิทธิ์";
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        public RoleGroupController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            //ViewBag.MenuID = 28;
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            var Obj = RoleData.Get.All(null);
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/RoleGroup/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname,int ID)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            RoleGroupModel Obj = new RoleGroupModel();
            Obj.Actived = true;
            if (ID != 0)
            {
                var _result = RoleData.Get.ByID(ID);
                Obj.Actived = _result.Actived;
                Obj.RoleGroupID = _result.RoleGroupID;
                Obj.RoleGroupName = _result.RoleGroupName;
            }
            ViewBag.MenuID = 28;
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/RoleGroup/Manage.cshtml", Obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(RoleGroupModel Obj, List<MenuModel> menulist)
        {
            try
            {
                var items = RoleData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                if (RoleData.Post.AddRoleMenu(items.RoleGroupID, menulist, UserName, IpAddress, Browser))
                {
                    return Json(new { status = true, location = Url.Action("Index", "RoleGroup") });
                }
                else
                {
                    return Json(new { status = false, message = "ไม่สามารถบันทึกข้อมูลได้" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
    }
}
