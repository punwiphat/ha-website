﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Core.DAL;
using Core.library;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Core.Backoffice.Controllers
{
    public class LoginController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IMemoryCache _cache;
        public LoginController(ILogger<HomeController> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _cache = memoryCache;
        }
        public IActionResult Index()
        {
            if (UserID != 0)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        } 
        [HttpPost]
        public IActionResult Index(LoginViewModel objUser,string returnUrl)
        {

            if (ModelState.IsValid)
            {
                var user = UsersData.Get.Login(objUser.UserName, objUser.Password);
                if (user != null)
                {
                    var claims = new List<Claim>();

                    claims.Add(new Claim("FullName", user.FullName));
                    claims.Add(new Claim("UserId", user.UserID.ToString()));
                    claims.Add(new Claim("username", user.Username));
                    claims.Add(new Claim("RoleGroupID", user.RoleGroupID.ToString()));
                    claims.Add(new Claim("RoleGroupName", user.RoleGroupName));
                    var grandmaIdentity =
                         new ClaimsIdentity(claims, "User Identity"); 
                    var userPrincipal = new ClaimsPrincipal(new[] { grandmaIdentity });
                    HttpContext.SignInAsync(userPrincipal); 
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    var error = string.Join(", ", "ข้อมูลไม่ถูกต้อง");
                    this.ModelState.AddModelError("Password", error);
                } 
            }

            return View(objUser);
        }
        public IActionResult Forget()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Forget(string email)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = UsersData.Get.By_Email(email);
                    if (user != null)
                    {
                        var Template = EmailTemplateData.Get.ByID(Common.TemplateNum.ForgotPassword, "TH");
                        string _html = Template.HTMLContent.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://backend.ha.or.th");
                        var _mail = SettingEmailData.Get.All();
                        string pwd = Encryption.Decrypt(_mail.Password);
                        SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                        sendEmail.SendMail(user.Email, Template.Subject, _html);
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        return RedirectToAction("NoData", "Login");
                    }

                }
                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Maintenance", "Login", ex.Message);
            }
        }


        public IActionResult Signout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("index", "Login");
        }
    }
}
