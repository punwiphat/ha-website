﻿using System;
using System.Collections.Generic;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class SettingFileController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private readonly IMemoryCache _cache;
        public SettingFileController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            ViewBag.Title = "ตั้งค่าไฟล์";
            ViewBag.MenuID = 70;
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            return View("~/Views/PanelSystem/SettingFile/Index.cshtml", SettingFileData.Get.All());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public JsonResult Index(SettingFiles Obj, List<int> AllowTypeFileID)
        {
            try
            {
                Obj.AllowTypeFile = String.Join(",", AllowTypeFileID);
                SettingFileData.save.AddOrUpdate(Obj, UserName, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());

                //_cache.Set("SettingFile", SettingFileData.Get.All(), TimeSpan.FromDays(365));
                return Json(new { status = true, location = Url.Action("Index", "SettingFile") });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }

    }
}