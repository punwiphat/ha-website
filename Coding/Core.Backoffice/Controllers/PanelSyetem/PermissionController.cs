﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class PermissionController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IMemoryCache _cache;
        public PermissionController(ILogger<HomeController> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index()
        {

            return View();
        }

    }
}
