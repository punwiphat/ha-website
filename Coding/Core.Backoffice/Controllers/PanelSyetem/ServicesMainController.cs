﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ServicesMainController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ServicesMainController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            int ModuleID = 1;
            string view = "~/Views/PanelSystem/ServiceMain/index.cshtml";
            switch (MenuID)
            {
                case 95:
                    ModuleID = 1;
                    view = "~/Views/PanelSystem/ServiceMain/index1.cshtml";
                    break;
                case 9:
                    ModuleID = 5;
                    view = "~/Views/PanelSystem/ServiceMain/index.cshtml";
                    break;
                case 71:
                    ModuleID = 6;
                    view = "~/Views/PanelSystem/ServiceMain/index2.cshtml";
                    break;
                case 102:
                    ModuleID = 7;
                    view = "~/Views/PanelSystem/ServiceMain/index.cshtml";
                    break;
                case 97:
                    ModuleID = 8;
                    view = "~/Views/PanelSystem/ServiceMain/index1.cshtml";
                    break;
            }

            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<ShortcutByLanguage> lstobj = new List<ShortcutByLanguage>();
            foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                ShortcutByLanguage obj = new ShortcutByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstDetail = MenuFrontData.Get.ServiceManageList(ModuleID, a.LanguageCode);
                lstobj.Add(obj);
            }
            return View(view, lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = !string.IsNullOrEmpty(ID) ? MenuFrontData.Get.MainById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Shortcut();
                var obj = MenuFrontData.Get.ShortcutById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                if (MenuID == 95)

                    main = main == null ? new Shortcut() : main;
                main.MainMenuID = MenuID;
                switch (MenuID)
                {
                    case 95:
                        main.ModuleID = 1;
                        break;
                    case 9:
                        main.ModuleID = 5;
                        break;
                    case 71:
                        main.ModuleID = 6;
                        break;
                    case 102:
                        main.ModuleID = 7;
                        break;
                    case 97:
                        main.ModuleID = 8;
                        break;
                }

                main.lstDetail = obj;
                main.Menuname = menuname;

                return View("~/Views/PanelSystem/ServiceMain/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }

            return View("~/Views/PanelSystem/ServiceMain/Manage.cshtml", new ShortcutDetail());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Shortcut Obj)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    int size = 0;
                    switch (Obj.MainMenuID)
                    {

                        case 9:
                            size = 10;
                            break;
                        case 102:
                            size = 5;
                            break;
                    }
                    Obj.MenuID = Obj.DDLMenuID != null ? Obj.DDLMenuID.Value : 0;
                    var main = MenuFrontData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                    foreach (var o in Obj.lstDetail)
                    {
                        o.ShortcutID = main.ID;
                        o.MenuID = main.MainMenuID;

                    }

                    var detail = MenuFrontData.Post.DetailAddOrUpdate(Obj.lstDetail, UserName, IpAddress, Browser, size);
                    main.lstDetail = detail;

                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                    //return View("~/Views/banner_and_link/Banner/Index.cshtml", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //banner.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", banner);
                }
            }

            return View("~/Views/PanelSystem/ServiceMain/Manage.cshtml", new Shortcut());
        }
    }
}
