﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class MenuPanelController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private readonly IConfiguration _configuration;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        private readonly string _domain;
        public MenuPanelController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
            _configuration = configuration;
            _domain = _configuration.GetValue<string>("Domain");

        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            ViewBag.Domain = _domain;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            var Obj = MenuBackofficeData.Get.GetMenuManageList();

            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, name_menu, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/MenuPanel/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname, int ID, int ParentID)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            Tm_Menu Obj = new Tm_Menu();
            if (ID != 0)
                Obj = MenuBackofficeData.Get.ByID(ID);
            else
            {
                Obj.MENU_BACK_ID = ParentID;
            }

            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/MenuPanel/Manage.cshtml", Obj);
        }
        [HttpPost]
        [Authorize]
        public JsonResult UpdateOrder([FromBody] List<MenuOrder> order)
        {
            bool result = true;
            foreach (var item in order)
            {
                result = MenuBackofficeData.Post.DragUpdateSequence(item.id, item.level, item.position, item.parentId);
            }
            return Json(new { success = result, message = "Order updated successfully" });
        }
        public class MenuOrder
        {
            public int id { get; set; }
            public int position { get; set; }
            public int? parentId { get; set; }
            public int level { get; set; }
        }
        public static void UpdateMenuOrderAndParent(int id, int position, int? parentId)
        {
            using (var db = new CMSdbContext())
            {
                var menu = db.Tm_Menu.Find(id);
                if (menu != null)
                {
                    menu.Sequence = position;
                    menu.MENU_BACK_ID = parentId;
                    menu.MENU_FRONT_ID = parentId;
                    db.SaveChanges();
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Tm_Menu Obj, string menuname)
        {
            try
            {
                Tm_Menu items = new Tm_Menu();
                Obj.section_name_back = "เนื้อหาเว็บไซต์";
                if (!Obj.Disabled)
                {
                    if (Obj.MenuTypeCode == "DOWNLOAD" && Obj.ID == 0)
                    {
                        var itemsDownload = MenuBackofficeData.Post.ADDCATE(Obj, UserName, IpAddress, Browser);
                    }
                    else
                    {
                         items = MenuBackofficeData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                    }
                }
                else
                {
                     items = MenuBackofficeData.Post.UpdateDisabled(Obj, UserName, IpAddress, Browser);

                }
               
                SetNewMenu(UserRoleGroupId);
                return Json(new { status = true, location = Url.Action("Index", "MenuPanel") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
        public IActionResult Refresh(string menuname)
        {
            SetNewMenu(UserRoleGroupId);
            return RedirectToAction("Index", new { menuname = menuname });
        }
        public void SetNewMenu(int UserRoleGroupId)
        {
            _cache.Remove("MenuList" + UserRoleGroupId.ToString());

            if (!_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> obj))
            {
                List<Tm_Menu> list_menu = new List<Tm_Menu>();
                obj = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
                var role = RoleData.Get.RoleByGroupID(UserRoleGroupId);
                list_menu = (from o in obj
                             join r in role on o.ID equals r.MenuID
                             where r.RoleType != "N"
                             select new
                             {
                                 o
                             }).ToList_list<Tm_Menu>();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                                                                            // SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set("MenuList" + UserRoleGroupId.ToString(), obj, TimeSpan.FromDays(1));
            }
        }
    }
}
