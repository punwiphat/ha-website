﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class SettingEmailController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        public SettingEmailController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            return View("~/Views/PanelSystem/Setting/Index.cshtml", SettingEmailData.Get.All());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public JsonResult Index(SettingEmail Obj)
        {
            try
            {
                SettingEmailData.save.AddOrUpdate(Obj, UserName, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());
                return Json(new { status = true, location = Url.Action("Index", "SettingEmail") });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }

    }
}