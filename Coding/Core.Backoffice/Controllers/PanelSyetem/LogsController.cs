﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class LogsController : BaseController
    {
        public async Task<IActionResult> Index(string menuname)
        {
            //ViewBag.Title = "รายงานใช้ระบบ System Log";
            //int MenuID = 33;
            //ViewBag.MenuID = MenuID;
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            return View("~/Views/PanelSystem/Logs/Index.cshtml");

        }
        [HttpPost]
        public IActionResult ListLogs(string SearchString,int MenuID=0, string LangID = "TH")
        {

            List<LogActivities> obj = new List<LogActivities>();
            obj = LogTransaction.Get.all(SearchString, MenuID);
            return PartialView("~/Views/PanelSystem/Logs/_Itemslist.cshtml", obj);
        }
        public JsonResult getLogs(string SearchString="", int MenuID = 0)
        {
            // var logs = JsonSerializer.Serialize(LogTransaction.Get.all());
            return new JsonResult(LogTransaction.Get.all(SearchString, MenuID));
        }
    }
}
