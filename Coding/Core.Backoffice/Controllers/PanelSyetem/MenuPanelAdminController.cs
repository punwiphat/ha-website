﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Wangkanai.Detection;
using Core.Models;
using Core.Common;
using Core.DAL.Models;
using Microsoft.Extensions.Caching.Memory;
using Core.library;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class MenuPanelAdminController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public MenuPanelAdminController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string menuname)
        {
            ViewBag.Title = menuname;
            int MenuID = 0;
            //if (_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> MenuObj))
            //    MenuID = MenuObj.Where(o => o.permalink_back == menuname).FirstOrDefault()?.ID ?? 0;
            MenuID = MenuBackofficeData.Get.GetMenuBackOfficeListActiveByname(menuname).FirstOrDefault()?.ID ?? 0;

            ViewBag.MenuID = MenuID;
            var Obj = MenuBackofficeData.Get.GetMenuBackOfficeList();

            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/MenuPanelAdmin/Index.cshtml", Obj);
        }
        public IActionResult Manage(string menuname, int ID)
        {
            ViewBag.Title = menuname;
            int MenuID = 0;
            //if (_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> MenuObj))
            //{
            //    MenuID = MenuObj.Where(o => o.permalink_back == menuname).FirstOrDefault()?.ID ?? 0;
            //}
            MenuID = MenuBackofficeData.Get.GetMenuBackOfficeListActiveByname(menuname).FirstOrDefault()?.ID ?? 0;

            ViewBag.MenuID = MenuID;
            Tm_Menu Obj = new Tm_Menu();
            if (ID != 0)
                Obj = MenuBackofficeData.Get.ByID(ID);

           
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, menuname, UserName, "", IpAddress, Browser);
            return View("~/Views/PanelSystem/MenuPanelAdmin/Manage.cshtml", Obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Tm_Menu Obj, string menuname)
        {
            try
            {
                var items = MenuBackofficeData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                SetNewMenu(UserRoleGroupId);
                return Json(new { status = true, location = Url.Action("Index", "MenuPanel") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
        public void SetNewMenu(int UserRoleGroupId)
        {

            List<Tm_Menu> list_menu = new List<Tm_Menu>();
            List<Tm_Menu> obj = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
            var role = RoleData.Get.RoleByGroupID(UserRoleGroupId);
            list_menu = (from o in obj
                         join r in role on o.ID equals r.MenuID
                         where r.RoleType != "N"
                         select new
                         {
                             o
                         }).ToList_list<Tm_Menu>();
            MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            };
           _cache.Set("MenuNavList_" + UserRoleGroupId.ToString(), obj, TimeSpan.FromHours(24));
        }
    }
}
