﻿using System;
using System.Threading.Tasks;
using Core.Common;
using Core.Common.Models;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class UsersController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        public UsersController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
        }
        public async Task<IActionResult> Index(string menuname)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                var obj = UsersData.Get.GetAll();
               // ViewBag.MenuID = 29;
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, "ผู้ใช้งาน", UserName, "", IpAddress, Browser);
                return View("~/Views/PanelSystem/Users/Index.cshtml", obj);
            }
            catch(Exception ex)
            {
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "ผู้ใช้งาน", UserName, ex.Message, IpAddress, Browser);
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
        }

        public IActionResult Itemslist()
        {
            var obj = UsersData.Get.GetAll();
            ViewBag.MenuID = 29;
            return PartialView("~/Views/PanelSystem/Users/_Itemslist.cshtml", obj);
        }

        [Authorize]
        public IActionResult Manage(string menuname,int UserID)
        {
            int refID = UserID;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                Users Obj = new Users();
                //ViewBag.MenuID = 29;
                Obj.Actived = true;
                if (UserID != 0)
                {
                    Obj = UsersData.Get.ByID(UserID);
                }
                string logDescription = "";
                if (Obj != null)
                {
                    logDescription = "ดูรายละเอียดข้อมูล Username : " + Obj.Username;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/PanelSystem/Users/Manage.cshtml", Obj);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
            }
            //return View("~/Views/banner_and_link/Banner/Manage.cshtml", new Banner());
            return RedirectToAction("Index", new { Menuname = name_menu });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(Users Obj)
        {
            try
            {
                bool mode = false;
                if (Obj.UserID != 0)
                {
                    mode = true;
                }
                var user = UsersData.save.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                if (!mode && user.Actived)
                {
                    var Template = EmailTemplateData.Get.ByID(TemplateNum.B_Register, "TH");
                    string _html = Template.HTMLContent.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://grad.swu.ac.th/SWU.BACKEND");
                    // string _html = ""
                    var _mail = SettingEmailData.Get.All();
                    string pwd = Encryption.Decrypt(_mail.Password);
                    string usnUpdate = UserName;
                    Task.Run(() =>
                    {
                        try
                        {
                            SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                            sendEmail.SendMail(user.Email, Template.Subject, _html);
                            LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Sendemail, "ส่งอีเมลสำเร็จ", usnUpdate, "บันทึกข้อมูลผู้ใช้ อีเมลถูกส่งไปที่ " + user.Email, IpAddress, Browser);
                        }
                        catch (Exception emailEx)
                        {
                            LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Error, "ส่งอีเมลไม่สำเร็จ", usnUpdate, "บันทึกข้อมูลผู้ใช้ อีเมลไม่ถูกส่งไปที่ " + user.Email+" "+emailEx.Message, IpAddress, Browser);
                        }
                    });
                    //SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                    //sendEmail.SendMail(user.Email, Template.Subject, _html);
                }

                return Json(new { status = true, location = Url.Action("Index", "Users") });
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(Obj.UserID, LogTransaction.Get.ActivityType.Error, "ผู้ใช้งาน", UserName, ex.Message, IpAddress, Browser);

                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

        public IActionResult Forget()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Forget(string email)
        {
            if (ModelState.IsValid)
            {
                var user = UsersData.save.resetpassword(UserID, UserName);
                if (user != null)
                {
                    var Template = EmailTemplateData.Get.ByID(TemplateNum.B_ResetPassword, "TH");
                    string _html = Template.HTMLContent.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://www.ha.or.th");
                    // string _html = ""
                    var _mail = SettingEmailData.Get.All();
                    string pwd = Encryption.Decrypt(_mail.Password);
                    string usnUpdate = UserName;
                    Task.Run(() =>
                    {
                        try
                        {
                            SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                            sendEmail.SendMail(user.Email, Template.Subject, _html);
                            LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Sendemail, "ส่งอีเมลสำเร็จ", usnUpdate, "Forgot Password อีเมลถูกส่งไปที่ " + user.Email, IpAddress, Browser);
                        }
                        catch (Exception emailEx)
                        {
                            LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Error, "ส่งอีเมลไม่สำเร็จ", usnUpdate, "Forgot Password อีเมลไม่ถูกส่งไปที่ " + user.Email +" "+emailEx.Message, IpAddress, Browser);
                        }
                    });
                    //SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                    //sendEmail.SendMail(user.Email, Template.Subject, _html);
                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    return RedirectToAction("NoData", "Login");
                }

            }
            return RedirectToAction("Index", "Login");

        }
        [HttpPost]
        [Authorize]
        public JsonResult Delete(int UserID)
        {
            try
            {
                var Items = UsersData.save.Delete(UserID, UserName);
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult resetpass(int UserID)
        {
            try
            {
                var user = UsersData.save.resetpassword(UserID, UserName);
                var Template = EmailTemplateData.Get.ByID(TemplateNum.B_ResetPassword, "TH");
                string _html = Template.HTMLContent.Replace("[@FullName]", user.FirstName + " " + user.LastName).Replace("[@UserName]", user.Username).Replace("[@Password]", Encryption.Decrypt(user.Password)).Replace("[@LINK]", @"https://www.ha.or.th");
                // string _html = ""
                var _mail = SettingEmailData.Get.All();
                string pwd = Encryption.Decrypt(_mail.Password);
                string usnUpdate = UserName;
                Task.Run(() =>
                {
                    try
                    {
                        SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                        sendEmail.SendMail(user.Email, Template.Subject, _html);
                        LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Sendemail, "ส่งอีเมลสำเร็จ", usnUpdate, "resetpass อีเมลถูกส่งไปที่ " + user.Email, IpAddress, Browser);
                    }
                    catch (Exception emailEx)
                    {
                        LogTransaction.SaveLog(user.UserID, LogTransaction.Get.ActivityType.Error, "ส่งอีเมลไม่สำเร็จ", usnUpdate, "resetpass อีเมลถูกไม่ส่งไปที่ " + user.Email+" "+emailEx.Message, IpAddress, Browser);
                    }
                });
                //SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, pwd, _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                //sendEmail.SendMail(user.Email, Template.Subject, _html);
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }



        [HttpPost]
        [Authorize]
        public JsonResult UpdateStatus(int UserID, Boolean Status)
        {
            try
            {
                var Items = UsersData.save.Status(UserID, Status, UserName);
                return Json(new { status = true, Items });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }

        [Authorize]
        public IActionResult Profile()
        {
            return View("~/Views/PanelSystem/Users/Profile.cshtml", UsersData.Get.ByID(UserID));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Profile(Users Obj)
        {
            try
            {
                UsersData.save.AddOrUpdate_profile(Obj, UserName);
                return Json(new { status = true, location = Url.Action("Profile", "Users") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
        [HttpGet]
        public JsonResult emailvalidation(int UserID, string Email)
        {
            try
            {

                return Json(new { status = UsersData.Get.EmailValidation(Email, UserID) });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }



        [Authorize]
        public IActionResult changepass()
        {
            return View("~/Views/PanelSystem/Users/changepass.cshtml", new ChangPasswordModel() { });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult changepass(ChangPasswordModel Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (UsersData.save.AddOrUpdate_password(UserID, Obj.CurrentPassword, Obj.ConfirmPassword, UserName))
                    {
                       // return Json(new { status = true, location = Url.Action("changepass", "Users") });
                        return Json(new { status = true, message = "เปลี่ยนรหัสผ่านสำเร็จ" });
                    }
                    else
                    {
                        return Json(new { status = false, message = "ข้อมูลไม่ถูกต้อง" });
                    }
                }
                else
                {
                    return Json(new { status = false, message = "ข้อมูลไม่ถูกต้อง" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

    }
}
