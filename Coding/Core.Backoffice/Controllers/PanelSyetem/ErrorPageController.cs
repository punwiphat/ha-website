﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ErrorPageController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IMemoryCache _cache;
        public ErrorPageController(ILogger<HomeController> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index(string Code)
        {
            return View();
        }

    }
}
