﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class PostsController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public PostsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (!_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(365),
                    SlidingExpiration = TimeSpan.FromDays(365)
                };
                _cache.Set("LanguageList", lang, TimeSpan.FromDays(365));
            }
            List<PostsByLanguage> lstobj = new List<PostsByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                PostsByLanguage obj = new PostsByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.PostDetailLst = PostData.Get.ByMenu(MenuID, a.LanguageCode);
                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingForPostBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");
            return View(lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var obj = PostData.Get.PostsById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();

                var main = !string.IsNullOrEmpty(ID) ? PostData.Get.MainPostsById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Posts();
                main = main == null ? new Posts() : main;
                if (main.InputDate == null)
                    main.InputDate = DateTime.Now;
                main.PostLst = obj;
                main.Menuname = menuname;
                main.MenuID = MenuID;
                main.GUID = Guid.NewGuid().ToString();
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");
                return View("~/Views/Posts/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(int.Parse(ID), LogTransaction.Get.ActivityType.Error, menuname, UserName, ex.Message, IpAddress, Browser);
            }
            return View(new Posts());
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Posts Posts, List<int> RelatedID)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    Posts.RelatedPost = String.Join(",", RelatedID);

                    var main = PostData.Post.AddOrUpdate(Posts, UserName, IpAddress, Browser);
                    foreach (var o in Posts.PostLst)
                    {
                        o.PostID = main.ID;
                        if(o.TagsName !=null && o.TagsName.Count() >0)
                        o.TagKeywords = String.Join(",", o.TagsName);
                    }
                    var detail = PostData.Post.DetailAddOrUpdate(Posts.PostLst, UserName, IpAddress, Browser);
                    foreach (var item in detail)
                    {
                        #region uploadfile
                        string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                        List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                          JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                        if (lstUploads.Count() > 0)
                        {
                            foreach (var item2 in lstUploads)
                            {
                                var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.PostDetailID.ToString(), UserName, IpAddress, Browser);
                            }
                        }

                        #endregion
                    }
                    main.PostLst = detail;

                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //Posts.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", Posts);
                }
            }
            //Posts.lstobj = Contents;
            return View("~/Views/Posts/Manage.cshtml", Posts);
        }
        public JsonResult GetTags(string term)
        {
            var result = PostData.Get.TagsAll(term.ToLower());
            return Json(new { status = true, result });
        }

    }
}
