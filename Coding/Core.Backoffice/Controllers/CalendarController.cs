﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;
using Calendar = Core.DAL.Models.Calendar;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class CalendarController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public CalendarController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            return View();
        }
        public IActionResult Manage(string menuname, string ID)
        {
            ViewBag.Add = string.IsNullOrEmpty(ID) ? true : (int.Parse(ID) == 0) ? true : false;
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = !string.IsNullOrEmpty(ID) ? CalendarData.Get.MainCalendarById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Calendar();
                var obj = CalendarData.Get.CalendarById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                main = main == null ? new Calendar() : main;
                main.MenuID = MenuID;
                main.lstCalendar = obj;
                main.Menuname = menuname;

                main.GUID = Guid.NewGuid().ToString();
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");

                ViewBag.ID = main.ID;

                return View("~/Views/Calendar/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View(new Calendar());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Calendar Calendar)
        {
            ViewBag.Add = Calendar.ID == 0 ? true : false;
            ViewBag.ID = Calendar.ID;
            ViewBag.Title = Calendar.Menuname;
            ViewBag.Menuname = Calendar.Menuname;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = CalendarData.Post.AddOrUpdate(Calendar, UserName, IpAddress, Browser);
                    foreach (var o in Calendar.lstCalendar)
                    {
                        o.CalendarID = main.ID;
                    }
                    var detail = CalendarData.Post.DetailAddOrUpdate(Calendar.lstCalendar, UserName, IpAddress, Browser);

                    foreach (var item in detail)
                    {
                        if (!string.IsNullOrEmpty(item.SUB_GUID))
                        {
                            #region uploadfile
                            string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                            List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                              JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                            if (lstUploads.Count() > 0)
                            {
                                foreach (var item2 in lstUploads)
                                {
                                    var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.CalendarDetailID.ToString(), UserName, IpAddress, Browser);
                                }
                            }

                            #endregion
                        }
                    }
                    main.lstCalendar = detail;

                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //Calendar.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", Calendar);
                }
            }
            return View("~/Views/Calendar/Manage.cshtml", Calendar);
        }
        public JsonResult GetEvents(int MenuID)
        {
            try
            {
                var Items = CalendarData.Get.ByMenu(MenuID, "TH");

                return Json(Items.ToArray());
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message.ToString() });
            }
        }
        public JsonResult UpdateCalendarEventByDragDrop(string ID, string Title, string DateTimeStart, string DateTimeEnd)
        {
            DateTime DateStart = DateTime.Parse(DateTimeStart, null, DateTimeStyles.RoundtripKind);
            DateStart = DateStart.AddDays(1);
            DateTime DateEnd = DateTime.Parse(DateTimeEnd, null, DateTimeStyles.RoundtripKind);
            var ResponseData = CalendarData.Post.UpdateEvent(int.Parse(ID), DateStart, DateEnd, UserName, IpAddress, Browser);
            return Json(ResponseData);
            //return 
        }
        public bool DeleteCalendarEvent(string Id)
        {
            return true;// CalendarDao.Delete_Calendar_Event(Id);
        }

        [HttpPost]
        public JsonResult DeleteEvent(int ID, string MenuName)
        {
            ResponseJson Res = CalendarData.Post.Delete(ID, UserName, IpAddress, Browser, MenuName);
            return Json(Res);
        }
    }
}
