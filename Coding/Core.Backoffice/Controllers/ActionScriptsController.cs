﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ActionScriptsController :  BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ActionScriptsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        [HttpPost]
        public JsonResult UpdateSorting(string MenuDisplayID, string MenuID, string SortingBy,int ActionType)
        {
            bool Results = false;
            if(ActionType ==1)
                Results = MenuDisplayData.Post.UpdateSortingBy(Int32.Parse(MenuDisplayID), Int32.Parse(MenuID), Int32.Parse(SortingBy),UserName, IpAddress, Browser);
            else
                Results = MenuDisplayData.Post.UpdateSortingType(Int32.Parse(MenuDisplayID), Int32.Parse(MenuID), Boolean.Parse(SortingBy), UserName, IpAddress, Browser);

            return Json(new { status = Results, message = "พบข้อผิดพลาดกรุณาลองใหม่ หรือ ติดต่อผู้ดูแลระบบ"});
        }

        [HttpPost]
        public JsonResult Delete(int RoleGroupID)
        {
            try
            {
                var Items = RoleData.Post.Delete(RoleGroupID, UserName, IpAddress, Browser);
                return Json(new { status = true, Items });
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        [HttpPost]
        public JsonResult UpdateSequence(int ID, string Menuname, string MenuType, string Seq)
        {
            ResponseJson ResponseData = new ResponseJson();
            try
            {
                switch (MenuType)
                {
                    case "BANNER":
                        ResponseData = BannerData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "JOBS":
                        ResponseData = JobPositionData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "POSTS":
                        ResponseData = PostData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "FAQ":
                        ResponseData = FaqData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "DOWNLOAD":
                        ResponseData = DownloadFormData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "RSSFeed":
                        ResponseData = RSSFeedData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "STAFF":
                        ResponseData = StaffData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "WBTOPIC":
                        ResponseData = WBTopicData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "SHORTCUT":
                        ResponseData = MenuFrontData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MENU":
                        ResponseData = MenuBackofficeData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        SetNewMenu(UserRoleGroupId);
                        break;
                    case "CATEGORY":
                        ResponseData = CategoryData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MASTER":
                        ResponseData = MasterData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "PROJECTS":
                        ResponseData = ProjectsData.Post.UpdateSequence(ID, int.Parse(Seq), UserName, IpAddress, Browser, Menuname);
                        break;
                        
                    default:
                        ResponseData.Result = false;
                        ResponseData.MessageError = "Default";
                        break;
                }
                return Json(ResponseData);
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        [HttpPost]
        public JsonResult UpdateStatus(int ID,string Menuname, string MenuType, string state)
        {
            ResponseJson ResponseData = new ResponseJson();
            try
            {
                
                switch (MenuType)
                {
                    case "CATEGORY":    ResponseData = CategoryData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "BANNER":
                                        ResponseData = BannerData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "JOBS":
                                        ResponseData = JobPositionData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "POSTS":
                                        ResponseData = PostData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "FAQ":
                                        ResponseData = FaqData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "DOWNLOAD":
                                        ResponseData = DownloadFormData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "RSSFeed":
                                        ResponseData = RSSFeedData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                                        break;
                    case "STAFF":
                        ResponseData = StaffData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                 
                    case "WBTOPIC":
                        ResponseData = WBTopicData.Post.Forbidden(ID, bool.Parse(state),"", UserName, IpAddress, Browser, Menuname);
                        break;
                    case "WBWAITTING":
                        ResponseData = WBTopicData.Post.Approved(ID, bool.Parse(state),"", UserName, IpAddress, Browser, Menuname);
                        break;
                     case "COMPLAINTTYPE":
                        ResponseData = ComplaintTypeData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "SHORTCUT":
                        ResponseData = MenuFrontData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MENU":
                        ResponseData = MenuBackofficeData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        SetNewMenu(UserRoleGroupId);
                        break;
                    case "ROLEGROUP":
                        ResponseData = RoleData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                 
                    case "MEMBER":
                        ResponseData = MemberData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "WBREPLY":
                        ResponseData = WBReplyData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "CHANNELCOMMENTTYPE":
                        ResponseData = ChannelCommentTypeData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MASTER":
                        ResponseData = MasterData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "USER":
                        ResponseData = UsersData.save.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    case "PROJECTS":
                        ResponseData = ProjectsData.Post.UpdateStatus(ID, bool.Parse(state), UserName, IpAddress, Browser, Menuname);
                        break;
                    default:
                                        ResponseData.Result = false;
                                        ResponseData.MessageError = "Default";
                                        break;
                }
                return Json(ResponseData);
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        [HttpPost]
        public JsonResult Deleted(int ID,string Menuname, string MenuType, string state)
        {
            ResponseJson ResponseData = new ResponseJson();
            try
            {
                switch (MenuType)
                {
                    case "CATEGORY":
                        ResponseData = CategoryData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "BANNER":
                        ResponseData = BannerData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "JOBS":
                        ResponseData = JobPositionData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "POSTS":
                        ResponseData = PostData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "FAQ":
                        ResponseData = FaqData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "DOWNLOAD":
                        ResponseData = DownloadFormData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "CALENDAR":
                        ResponseData = CalendarData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "RSSFeed":
                        ResponseData = RSSFeedData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "STAFF":
                        ResponseData = StaffData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "WBRESERVED":
                        ResponseData = WBReservedWordData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                   
                    case "WBTOPIC":
                        ResponseData = WBTopicData.Post.Delete(ID,  UserName, IpAddress, Browser, Menuname);
                        break;
                       
                    case "COMPLAINTTYPE":
                        ResponseData = ComplaintTypeData.Post.Delete(ID,  UserName, IpAddress, Browser, Menuname);
                        break;
                    case "SHORTCUT":
                        ResponseData = MenuFrontData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MENU":
                        ResponseData = MenuBackofficeData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        SetNewMenu(UserRoleGroupId);
                        break;
                    case "ROLEGROUP":
                        ResponseData = RoleData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                 
                    case "MEMBER":
                         ResponseData = MemberData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "WBREPLY":
                        ResponseData = WBReplyData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "CONTACTFORM":
                        ResponseData = ContactData.Post.DeleteContactForm(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "CHANNELCOMMENT":
                        ResponseData = ChannelCommentData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "CHANNELCOMMENTTYPE":
                        ResponseData = ChannelCommentTypeData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "MASTER":
                        ResponseData = MasterData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "USER":
                        ResponseData = UsersData.save.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    case "PROJECTS":
                        ResponseData = ProjectsData.Post.Delete(ID, UserName, IpAddress, Browser, Menuname);
                        break;
                    default:
                        ResponseData.Result = false;
                        ResponseData.MessageError = "Default";
                        break;
                }
                return Json(ResponseData);
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex.Message.ToString() });
            }
        }
        public void SetNewMenu(int UserRoleGroupId)
        {
            _cache.Remove("MenuList" + UserRoleGroupId.ToString());

            if (!_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> obj))
            {
                List<Tm_Menu> list_menu = new List<Tm_Menu>();
                obj = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
                var role = RoleData.Get.RoleByGroupID(UserRoleGroupId);
                list_menu = (from o in obj
                             join r in role on o.ID equals r.MenuID
                             where r.RoleType != "N"
                             select new
                             {
                                 o
                             }).ToList_list<Tm_Menu>();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                                                                            // SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set("MenuList" + UserRoleGroupId.ToString(), obj, TimeSpan.FromDays(1));
            }

        }
        #region File
        public JsonResult TempUpload()
        {
            IFormFile file = Request.Form.Files[2];
            return Json(null);

        }
        public JsonResult DeleteFiles(string MediaFileID)
        {
            if (!string.IsNullOrEmpty(MediaFileID))
            {
                var p = MediaFilesData.Post.DeleteByFile(int.Parse(MediaFileID), UserName, IpAddress, Browser);
            }
            return Json(true);
        }

        public JsonResult UploadFile(string RefID, int MenuID, int CateID)//, string MenuID, string Menuname, string CateID,)
        {
            var aa = Request.Form.Files.Count;
            string tempUploads = TempData[RefID] != null ? Convert.ToString(TempData[RefID]) : "";

            //List<IFormFile> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                //JsonConvert.DeserializeObject<List<IFormFile>>(tempUploads) : new List<IFormFile>();
            List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
               JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
            for (int i = 0; i < aa; i++)
            {
                if (Request.Form.Files[i].Length > 0)
                {
                    lstUploads.Add(UploadFiles.UploadFileState(Request.Form.Files[i], MenuID, RefID, CateID, 0, ""));
                    //lstUploads.Add(Request.Form.Files[i]);
                }
                else
                {

                    return Json(new { status = true, message = Request.Form.Files[i].ToString() });

                }
            }
            var json = JsonConvert.SerializeObject(lstUploads); 
            TempData[RefID] = json;
            return Json(true);
        }
        public JsonResult RemoveFile(int i,string RefID) // string MenuID, string CateID, 
        {
            string tempUploads = TempData[RefID] != null ? Convert.ToString(TempData[RefID]) : "";
            //List<IFormFile> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
            //JsonConvert.DeserializeObject<List<IFormFile>>(tempUploads) : new List<IFormFile>();
            List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
           JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
            if (lstUploads != null && lstUploads.Count() > 0)
            {
                lstUploads.RemoveAt(i);
            }
            var json = JsonConvert.SerializeObject(lstUploads); 
            TempData[RefID] = json;
            return Json(true);
        }

       
        #endregion
    }
}
