﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class PopupController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        
        public PopupController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector,IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<BannerByLanguage> lstobj = new List<BannerByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
             foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                BannerByLanguage obj = new BannerByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstBanner = BannerData.Get.ByMenu(MenuID, a.LanguageCode);
                
                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy !=null ? MenuDisplay.SortingBy.Value: 1):1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false):false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, name_menu, UserName, "", IpAddress, Browser);

            return View("~/Views/banner_and_link/Popup/index.cshtml", lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            int refID = !string.IsNullOrEmpty(ID) ? Convert.ToInt32(ID) : 0;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = !string.IsNullOrEmpty(ID)?BannerData.Get.MainBannerById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Banner();
                var obj = BannerData.Get.BannerById(!string.IsNullOrEmpty(ID)?int.Parse(ID):0).ToList();
                main = main == null ? new Banner() : main;
                main.MenuID = MenuID;
                main.lstBanner = obj;
                string logDescription = "";
                if (obj.Count > 0)
                {
                    logDescription = "ดูรายละเอียดข้อมูล Popup Name : " + obj.FirstOrDefault().BannerName;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);

                return View("~/Views/banner_and_link/Popup/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
            }
                //return View("~/Views/banner_and_link/Popup/Manage.cshtml", new Banner());
                return RedirectToAction("Index", new { Menuname = menuname });
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Banner banner)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = BannerData.Post.AddOrUpdate(banner,UserName, IpAddress, Browser);
                    foreach(var o in banner.lstBanner)
                    {
                        o.BannerID = main.ID;
                    }
                    var detail = BannerData.Post.DetailAddOrUpdate(banner.lstBanner, UserName,12, IpAddress, Browser);
                    main.lstBanner = detail;
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(banner.ID, LogTransaction.Get.ActivityType.Error, banner.Menuname, UserName, ex.Message, IpAddress, Browser);
                }
            }
                return View("~/Views/banner_and_link/Popup/Manage.cshtml", banner);
        }
    }
}
