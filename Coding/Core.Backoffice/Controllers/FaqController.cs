﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class FaqController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        
        public FaqController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector,IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<FaqByLanguage> lstobj = new List<FaqByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
             foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                FaqByLanguage obj = new FaqByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstFaq = FaqData.Get.ByMenu(MenuID, a.LanguageCode);
                
                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy !=null ? MenuDisplay.SortingBy.Value: 1):1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false):false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            return View(lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
                ViewBag.DDLCategory = CategoryData.Get.ByMenuIdActive(MenuDisplay.CategoryID);
            
                var main = !string.IsNullOrEmpty(ID)?FaqData.Get.MainFaqById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Faq();
                var obj = FaqData.Get.FaqById(!string.IsNullOrEmpty(ID)?int.Parse(ID):0).ToList();
                main = main == null ? new Faq() : main;
                main.MenuID = MenuID;
                main.lstFaq = obj;
                main.Menuname = menuname;
                return View("~/Views/Faq/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
               // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
                return View(new Faq());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Faq Faq)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = FaqData.Post.AddOrUpdate(Faq,UserName);
                    foreach(var o in Faq.lstFaq)
                    {
                        o.FaqID = main.ID;
                    }
                     var detail = FaqData.Post.DetailAddOrUpdate(Faq.lstFaq, UserName);
                    main.lstFaq = detail;
                        //if (a.MENU_ID == 72)
                        //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, FaqDetail.GUID, 150, 100, true);
                        //else if (a.MENU_ID == 119)
                        //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, FaqDetail.GUID, 1280, 1024, true);
                        //else if (a.MENU_ID == 71)
                        //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, FaqDetail.GUID, 0, 0, true);
                        //else
                        //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, FaqDetail.GUID, 666, 422, false);

                    //StampTransaction.SaveTransaction(state_GUID, obj.createby, int.Parse(MYSession.Current.User_Id));
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, state, "ได้ทำการ" + (state == "EDIT" ? "แก้ไขข้อมูล " : "เพิ่มข้อมูล ") + Contents.FirstOrDefault().title + "เรียบร้อย");
                    return RedirectToAction("Index",new { Menuname = main.Menuname});
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //Faq.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", Faq);
                }
            }
            //Faq.lstobj = Contents;
            return View("~/Views/Faq/Manage.cshtml", Faq);
        }
    }
}
