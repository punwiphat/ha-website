﻿using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using Core.library;
using Core.Common;
using Core.DAL.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;
using MySqlConnector;

namespace Core.Backoffice.Controllers
{
    public class BaseController : Controller
    {
        public static readonly string DATE_INPUT_FORMAT = "dd/MM/yyyy";
        public static bool hasManageRole = false;
        public static string MenuID = "0";
        public static string Username = "";

        public BaseController()//IMemoryCache memoryCache)
        {
            hasManageRole = false;
          


        }
        
        #region selectlist common
        // ปี
        public static SelectList Get_Year()
        {
            List<Yearlist> Obj = new List<Yearlist>();
            int NowYear = DateTime.Now.Year + 5;
            if (DateTime.Now.Month > 10)
            {
                NowYear++;
            }
            int Start = 2016;
            while (Start <= NowYear)
            {
                Obj.Add(new Yearlist() { Year = Start + 543 });
                Start++;
            }
            return new SelectList(Obj, "Year", "Year");
        }
        protected class Yearlist
        {
            public int Year { get; set; }

        }
        private class _value
        {
            public string value { get; set; }
            public string value2 { get; set; }
        }
        // คำนำหน้า
        public static SelectList GetTitle(string selectedValues = "")
        {
            List<ddlmastername> Obj = new List<ddlmastername>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_MenuID", 411);
            Obj = GetStoredMySql.GetAllStored<ddlmastername>("B_MASTER_BY_MENUID_ACTIVE", param).ToList();
            //return Obj;

            //List<ddlmaster> Obj = new List<ddlmaster>();
            //Obj.Add(new ddlmaster() { ID = 1, Name = "นาย", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 2, Name = "นางสาว", NameEN = "Miss" });
            //Obj.Add(new ddlmaster() { ID = 3, Name = "นาง", NameEN = "Mrs." });

            return new SelectList(Obj, "ID", "NameTH", selectedValues);
        }
        public static SelectList GetIcon(string selectedValues = "")
        {
            List<ddlmastername> Obj = new List<ddlmastername>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_MenuID", 422);
            Obj = GetStoredMySql.GetAllStored<ddlmastername>("B_MASTER_BY_MENUID_ACTIVE", param).ToList();
            //return Obj;

            //List<ddlmaster> Obj = new List<ddlmaster>();
            //Obj.Add(new ddlmaster() { ID = 1, Name = "นาย", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 2, Name = "นางสาว", NameEN = "Miss" });
            //Obj.Add(new ddlmaster() { ID = 3, Name = "นาง", NameEN = "Mrs." });

            return new SelectList(Obj, "NameTH", "NameTH", selectedValues);
        }
        public static SelectList GetLevels(string selectedValues = "")
        {
            List<ddlmaster> Obj = new List<ddlmaster>();
            Obj.Add(new ddlmaster() { ID = 1, Name = "Level 1", NameEN = "Level 1" });
            Obj.Add(new ddlmaster() { ID = 2, Name = "Level 2", NameEN = "Level 2" });
            Obj.Add(new ddlmaster() { ID = 3, Name = "Level 3", NameEN = "Level 3" });

            return new SelectList(Obj, "ID", "Name", selectedValues);
        }
        public static SelectList GetExtension(List<int> selectedValues)
        {


            List<ddlmastername> Obj = new List<ddlmastername>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_MenuID", 457);
            Obj = GetStoredMySql.GetAllStored<ddlmastername>("B_MASTER_BY_MENUID_ACTIVE", param).ToList();

            //List<ddlmaster> Obj = new List<ddlmaster>();
            //Obj.Add(new ddlmaster() { ID = 1, Name = "IMAGE", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 2, Name = "PDF", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 3, Name = "WORD", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 4, Name = "EXCEL", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 5, Name = "CSV", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 6, Name = "POWERPOINT", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 7, Name = "ZIP", NameEN = "Mr." });
            //Obj.Add(new ddlmaster() { ID = 8, Name = "TEXT", NameEN = "Mr." });
            return new SelectList(Obj, "ID", "NameTH", selectedValues);
        }
        //public static SelectList GetTitle(string selectedValues = "")
        //{
        //    List<_value> Obj = new List<_value>();
        //    Obj.Add(new _value() { value = "นาย" });
        //    Obj.Add(new _value() { value = "นางสาว" });
        //    Obj.Add(new _value() { value = "นาง" });
        //    return new SelectList(Obj, "value", "value", selectedValues);
        //}
        public static SelectList GetMenuType(string selectedValues = "")
        {
            List<_value> Obj = new List<_value>();
            Obj.Add(new _value() { value = "1", value2 = "เมนูหลัก" });
            Obj.Add(new _value() { value = "2", value2 = "ลิงค์ภายนอก" });
            //Obj.Add(new _value() { value = "3", value2 = "เนื้อหาเว็บไซต์" });
            return new SelectList(Obj, "value", "value2", selectedValues);
        }
        public static SelectList GetMenuCate4User(string selectedValues = "")
        {
            List<ddlmastercode> lst = new List<ddlmastercode>();
            lst = GetStoredMySql.GetAllStoredNonparam<ddlmastercode>("menutypelist_user").ToList();
            return new SelectList(lst, "Code", "Name");
        }
        public static SelectList GetMenuCate(string selectedValues = "")
        {
            List<ddlmastercode> lst = new List<ddlmastercode>();
            lst = GetStoredMySql.GetAllStoredNonparam<ddlmastercode>("menutypelist").ToList();
            return new SelectList(lst, "Code", "Name");
        }
        public static SelectList GetMenuManageRoot(int level, int? selectedValues = 0)
        {
            var lst = MenuBackofficeData.Get.MenuEnabledBelong(level);
            return new SelectList(lst, "ID", "menu_name_back", selectedValues);
        }

        public static SelectList GetMenuRoot(int level, int? selectedValues = 0)
        {
            var lst = MenuBackofficeData.Get.GetMenuListActive(level);
            return new SelectList(lst, "ID", "menu_name_back", selectedValues);
        }
        public static SelectList GetMenuSection(string selectedValues = "")
        {
            List<ddlmaster> lst = new List<ddlmaster>();
            lst = GetStoredMySql.GetAllStoredNonparam<ddlmaster>("menusectionlist").ToList();
            return new SelectList(lst, "Name", "Name", selectedValues);
        }
        public static SelectList GetCateByMenu(int MenuID, string selectedValues = "")
        {
            List<Category> Obj = new List<Category>();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Category.Where(o => o.MenuID == MenuID && o.Deleted != true && o.Actived == true).ToList();
                return new SelectList(Obj, "ID", "NameTH", selectedValues);
            }
        }
        public static SelectList GetCateOfMenu(string selectedValues = "")
        {
            List<Tm_Menu> Obj = new List<Tm_Menu>();
            var myInClause = new int[] { 11, 14 };

            using (var Map = new CMSdbContext())
            {
                Obj = MenuBackofficeData.Get.GetByAllMenu().Where(o => myInClause.Contains(o.ID)).ToList();
                return new SelectList(Obj, "ID", "menu_name_back", selectedValues);
            }
        }
        
        public static SelectList GetProvince(string selectedValues = "")
        {
            List<ddlmaster> Obj = new List<ddlmaster>();
            Obj = GetStoredMySql.GetAllStoredNonparam<ddlmaster>("province_list").ToList();
            return new SelectList(Obj, "ID", "Name", selectedValues);
        }
        public static SelectList GetRegion(string selectedValues = "")
        {
            List<ddlmaster> Obj = new List<ddlmaster>();
            Obj = GetStoredMySql.GetAllStoredNonparam<ddlmaster>("region_list").ToList();
            return new SelectList(Obj, "ID", "Name", selectedValues);
        }
        [NonAction]
        public SelectList DDLSortingForPostBy(string selectedValues)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem
            {
                Text = "เรียงจากวันที่แก้ไข",
                Value = "1",
                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "1" ? true : false : true
            });
            listItems.Add(new SelectListItem
            {
                Text = "เรียงจากวันที่ของข้อมูล",
                Value = "4",
                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "1" ? true : false : true
            });
            listItems.Add(new SelectListItem
            {
                Text = "เรียงจากชื่อ",
                Value = "2",

                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "2" ? true : false : false
            });
            listItems.Add(new SelectListItem
            {
                Text = "กำหนดเอง",
                Value = "3",

                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "3" ? true : false : false
            });
            return new SelectList(listItems, "Value", "Text", selectedValues);

        }
        [NonAction]
        public SelectList DDLSortingBy(string selectedValues)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem
            {
                Text = "เรียงจากวันที่แก้ไข",
                Value = "1",
                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "1" ? true : false : true
            });
            listItems.Add(new SelectListItem
            {
                Text = "เรียงจากชื่อ",
                Value = "2",

                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "2" ? true : false : false
            });
            listItems.Add(new SelectListItem
            {
                Text = "กำหนดเอง",
                Value = "3",

                Selected = !string.IsNullOrEmpty(selectedValues) ? selectedValues == "3" ? true : false : false
            });
            return new SelectList(listItems, "Value", "Text", selectedValues);

        }
        public static SelectList GetRelatePost(int MenuID, string LangID, List<int> selectedValues)
        {

            List<PostsDetail> Obj = new List<PostsDetail>();
            Obj = PostData.Get.ByMenu(MenuID, LangID);
            return new SelectList(Obj, "PostID", "Title", selectedValues);
        }
        public static SelectList GetTagsPost(string LangID, List<string> selectedValues)
        {
            var lst = PostData.Get.TagsLangAll("", LangID);
            SelectList Lists = new SelectList(lst, "TagName", "TagName", selectedValues);
            return Lists;

        }
        public static SelectList GetCateDownload(int MenuID, int? selectedValues = 0)
        {
            var lst = CategoryData.Get.ByMenuIdActive(MenuID);
            return new SelectList(lst, "ID", "NameTH", selectedValues);
        }
        #endregion

        public class _returndata
        {
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public Object data { get; set; }
        }

        public List<Tm_Menu> GetMenuWithRole()
        {
            List<Tm_Menu> list_menu_x_role = new List<Tm_Menu>();
            List<Tm_Menu> MenuList = MenuBackofficeData.Get.GetMenuBackOfficeList();
            var role = RoleData.Get.RoleByGroupID(UserRoleGroupId);
            list_menu_x_role = (from o in MenuList
                                join r in role on o.ID equals r.MenuID
                                where r.RoleType != "N"
                                select new
                                {
                                    o
                                }).ToList_list<Tm_Menu>();
            return list_menu_x_role;
        }
        public int UserID
        {
            get
            {
                int user_id = 0;
                if (HttpContext.User.Claims != null)
                {
                    var u = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "UserId");
                    user_id = u != null ? Int32.Parse(u.Value) : 0;
                }
                return user_id;

            }
        }
        public string FullName
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "FullName")?.Value : ""; }
        }
        public string UserName
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "username")?.Value : ""; }
        }

        public int UserRoleGroupId
        {
            get { return HttpContext.User.Claims != null ? Int32.Parse(HttpContext.User.Claims.FirstOrDefault(x => x.Type == "RoleGroupID")?.Value) : 0; }
        }

        public string UserRoleGroupName
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "RoleGroupName")?.Value : ""; }
        }

        //protected UserSingleton _mySingletonServiceInstance;
        protected string Getdevice_client()
        {
            return Request.Headers["User-Agent"].ToString();
        }
        protected string GetIpAddress()
        {
            return HttpContext.Connection.RemoteIpAddress.ToString();
        }
        private static string GetMachineNameFromIPAddress(string ipAdress)
        {
            string machineName = string.Empty;
            try
            {
                System.Net.IPHostEntry hostEntry = System.Net.Dns.GetHostEntry(ipAdress);
                machineName = hostEntry.HostName;
            }
            catch (Exception ex)
            {
                //log here
            }
            return machineName;
        }
        //public void SaveActivityLog(int transactionId, string function, string process, string msg, string username, string IP, string browser)
        //{
        //    try
        //    {
        //        LogTransaction.SaveLog(transactionId, process, function, username, IP, browser + ",   " + msg);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        public static bool AuthorizeMenu(string menuId, int roleId)
        {
            if (roleId == 999999999)
                return true;

            if (string.IsNullOrEmpty(menuId))
                return true;

            //var sRolelst = RoleData.Get.Menurole(roleId);
            //if (sRolelst.Count > 0)
            //{
            //    var objMenu = StaticMenu.GetMenuById(Guid.Parse(menuId));
            //    if (objMenu.FrontLevel == 3)
            //    {
            //        objMenu = StaticMenu.GetMenuById((Guid)objMenu.FrontParentId);
            //        menuId = objMenu.MenuID.ToString();
            //        var objSel = sRolelst.Where(s => s.MenuID.ToString() == menuId && s.RoleType == 2).FirstOrDefault();
            //        if (objSel != null)
            //            return true;
            //        else
            //            return false;
            //    }
            //    else
            //    {
            //        var objSel = sRolelst.Where(s => s.MenuID.ToString() == menuId && s.RoleType == 0).FirstOrDefault();
            //        if (objSel != null)
            //            return false;
            //        else
            //            return true;
            //    }
            //}
            return false;
        }
    }

    //public class GetMenuAttribute : ActionFilterAttribute
    //{
    //    private readonly IMemoryCache _cache;

    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        //var userClaims = claims as ClaimsIdentity;
    //        //bool havePer = userClaims.HasClaim(claimValue, claimValue);
    //        int GroupIG = BaseController.UserRoleGroupId;
    //        if (!_cache.TryGetValue("MenuNavList_" + _UserRoleGroupId, out List<Tm_Menu> obj))
    //        {
    //            List<Tm_Menu> list_menu = new List<Tm_Menu>();
    //            obj = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
    //            var role = RoleData.Get.RoleByGroupID(BaseController.UserRoleGroupId);
    //            list_menu = (from o in obj
    //                         join r in role on o.ID equals r.MenuID
    //                         where r.RoleType != "N"
    //                         select new
    //                         {
    //                             o
    //                         }).ToList_list<Tm_Menu>();
    //            MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
    //            {
    //                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
    //                SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
    //            };
    //            _cache.Set("MenuNavList_" + BaseController.UserRoleGroupId.ToString(), obj, TimeSpan.FromHours(2));
    //        }


    //        //List<MenuGenBackoffice> result_menu = new List<MenuGenBackoffice>();
    //        //result_menu = obj.GroupBy(o => o.section_name_back).Select(g => new { section_name_back = g.Key, lstobj = g.ToList() }).ToList_list<MenuGenBackoffice>();
    //        //if (BaseController.Username != null)
    //        //{
    //        //    filterContext.Result = new RedirectResult("~/Login/Index");
    //        //    return;
    //        //}
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (BaseController.Username == null)
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
    public class ActiveUserFilter : ActionFilterAttribute
    {
      
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(BaseController.Username))
            {
                var u = UsersData.Get.ActiveByID(BaseController.Username);
                if (u == null)
                {
                    filterContext.Result = new RedirectResult("~/Login/Signout");
                    return;
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }

public static class SessionExtensions
    {
        public static T GetComplexData<T>(this ISession session, string key)
        {
            var data = session.GetString(key);
            if (data == null)
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(data);
        }
        public static void SetComplexData(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
        //public static string Filter(this string input)
        //{
        //    string[] badWords = Get_Webboard.Get.Reserved().Select(o => o.ReservedWord).ToArray();
        //    var re = new Regex(
        //        @"\b("
        //        + string.Join("|", badWords.Select(word =>
        //            string.Join(@"\s*", word.ToCharArray())))
        //        + @")\b", RegexOptions.IgnoreCase);
        //    return re.Replace(input, match =>
        //    {
        //        return new string('*', match.Length);
        //    });
        //}
    }
}
