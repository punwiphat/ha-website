﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ContentsController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public ContentsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }

        public IActionResult Manage(string menuname)
        {
            int refID = 0;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if(menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
                {
                    lang = LanguageData.GetByActive();
                }
                var main = MenuID != 0 ? ContentData.Get.MainByMenuId(MenuID) : new Content();
                var obj = ContentData.Get.ContentById(main.ID).ToList();
                main = main == null ? new Content() : main;
                main.MenuID = MenuID;
                main.lstContent = obj;
                main.Menuname = menuname;
                main.GUID = Guid.NewGuid().ToString();
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");

                string logDescription = "";
                if (obj.Count > 0)
                {
                    logDescription = "ดูรายละเอียดข้อมูล : " + obj.FirstOrDefault().Title;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/Content/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, menuname, UserName, ex.Message, IpAddress, Browser);
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(Content Content)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            if (ModelState.IsValid)
            {
                try
                {
                    var main = ContentData.Post.AddOrUpdate(Content, UserName, IpAddress, Browser);
                    foreach (var d in Content.lstContent)
                    {
                        d.ContentID = main.ID;
                    }
                    var detail = ContentData.Post.DetailAddOrUpdate(Content.lstContent, UserName, IpAddress, Browser);
                    main.lstContent = detail;
                    foreach (var item in detail)
                    {
                        if (!string.IsNullOrEmpty(item.SUB_GUID))
                        {
                            #region uploadfile
                            string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                            List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                              JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                            if (lstUploads.Count() > 0)
                            {
                                foreach (var item2 in lstUploads)
                                {
                                    var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.ContentDetailID.ToString(), UserName, IpAddress, Browser);
                                }
                            }

                            #endregion
                        }
                    }
                    return RedirectToAction("Manage", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(Content.ID, LogTransaction.Get.ActivityType.Error, Content.Menuname, UserName, ex.Message, IpAddress, Browser);

                }
            }
            return View("~/Views/Content/Manage.cshtml", Content);
        }

    }
}
