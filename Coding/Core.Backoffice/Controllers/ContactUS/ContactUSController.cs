﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ContactUSController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ContactUSController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = ContactData.Get.Content();
                main = main != null ? main : new Contact();
                var obj = ContactData.Get.ContactById(main.ID).ToList();
                main = main == null ? new Contact() : main;
                main.MenuID = MenuID;
                main.lstContact = obj;
                main.Menuname = menuname;
                return View("~/Views/ContactUs/Index.cshtml", main);
            }
            catch (Exception ex)
            {
                
            }
            return View("~/Views/ContactUs/Index.cshtml", new Banner());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Index(Contact contacts)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = ContactData.Post.AddOrUpdate(contacts, UserName, IpAddress, Browser);
                    foreach (var o in contacts.lstContact)
                    {
                        o.ContactID = main.ID;
                    }
                    var detail = ContactData.Post.DetailAddOrUpdate(contacts.lstContact, UserName, IpAddress, Browser);
                    main.lstContact = detail;
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //banner.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", banner);
                }
            }
            return View("~/Views/ContactUs/Index.cshtml", contacts);
        }

        public IActionResult List(string menuname)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = ContactData.Get.ContactFormList();
                main = main != null ? main : new List<ContactForm>();

                return View("~/Views/ContactUs/List.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View("~/Views/ContactUs/List.cshtml", new Banner());
        }

        public IActionResult Manage(string menuname, string ID)
        {
            ViewBag.Title = menuname;
          
            try
            {
                int MenuID = 0;
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalink(menuname);
                MenuID = menuObj?.ID ?? 0;
                string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                var item = ContactData.Get.ContactFormById(string.IsNullOrEmpty(ID) ? 0 : int.Parse(ID));
                return View("~/Views/ContactUs/ContactUsForm/Manage.cshtml", item);
            }
            catch (Exception ex)
            {
                return View("~/Views/ContactUs/ContactUsForm/Manage.cshtml", new ContactForm());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(ContactForm obj)
        {
            try
            {
                var result = ContactData.Post.UpdateContactFormReply(obj, UserName);
                try
                {
                    var Template = EmailTemplateData.Get.ByID(TemplateNum.B_ContactUSStaff, (!string.IsNullOrEmpty(result.LangCode) ? "TH" : result.LangCode));
                    string _html = Template.HTMLContent.Replace("[@FullName]", result.Fullname).Replace("[@Message]", result.Message).Replace("[@MessageReply]", result.MessageReply);
                    var _mail = SettingEmailData.Get.All();
                    SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, Encryption.Decrypt(_mail.Password), _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                    sendEmail.SendMail(result.Email, Template.Subject, _html);
                }
                catch (Exception ex)
                {
                    obj.MessageReply = "";
                    var resultError = ContactData.Post.UpdateContactFormReply(obj, UserName);
                    return Json(new { status = false, message = "บันทึกข้อมูลwไม่สำเร็จ" });
                }
                return Json(new { status = true, location = Url.Action("List", "ContactUS") });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }


    }
}
