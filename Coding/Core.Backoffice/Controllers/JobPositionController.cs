﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class JobPositionController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public JobPositionController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<JobPositionByLanguage> lstobj = new List<JobPositionByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                //TempData[MenuID+"_" + a.LanguageCode] = "";
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                // Session.Remove(MenuID + "_" + a.LanguageCode);
                JobPositionByLanguage obj = new JobPositionByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstJobPosition = JobPositionData.Get.ByMenu(MenuID, a.LanguageCode);

                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");

            return View(lstobj);

        }
        public IActionResult Manage(string menuname, string ID)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                //int MenuID = GetMenuIDBack(menuname, "VIEW", null);
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                ViewBag.MenuID = MenuID;
                var main = !string.IsNullOrEmpty(ID) ? JobPositionData.Get.MainJobPositionById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new JobPosition();
                var obj = JobPositionData.Get.JobPositionById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                main = main == null ? new JobPosition() : main;
                main.MenuID = MenuID;
                main.lstJobPosition = obj;
                main.Menuname = menuname;
                main.GUID = Guid.NewGuid().ToString();
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");
                return View("~/Views/JobPosition/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View(new JobPosition());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Manage(JobPosition JobPosition)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = JobPositionData.Post.AddOrUpdate(JobPosition, UserName);
                    foreach (var o in JobPosition.lstJobPosition)
                    {
                        o.JobPositionID = main.ID;
                    }
                    var detail = JobPositionData.Post.DetailAddOrUpdate(JobPosition.lstJobPosition, UserName);
                    main.lstJobPosition = detail;
                    foreach (var item in detail)
                    {
                        if (!string.IsNullOrEmpty(item.SUB_GUID))
                        {
                            #region uploadfile
                            string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                            List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                              JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                            if (lstUploads.Count() > 0)
                            {
                                foreach (var item2 in lstUploads)
                                {
                                    var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.JobPositionDetailID.ToString(), UserName, IpAddress, Browser);
                                }
                            }

                            #endregion
                        }
                    }
                    //if (a.MENU_ID == 72)
                    //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, JobPositionDetail.GUID, 150, 100, true);
                    //else if (a.MENU_ID == 119)
                    //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, JobPositionDetail.GUID, 1280, 1024, true);
                    //else if (a.MENU_ID == 71)
                    //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, JobPositionDetail.GUID, 0, 0, true);
                    //else
                    //    manage_file(a.menuname, a.Files, MenuID + "_" + a.LANG_ID, JobPositionDetail.GUID, 666, 422, false);

                    //StampTransaction.SaveTransaction(state_GUID, obj.createby, int.Parse(MYSession.Current.User_Id));
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, state, "ได้ทำการ" + (state == "EDIT" ? "แก้ไขข้อมูล " : "เพิ่มข้อมูล ") + Contents.FirstOrDefault().title + "เรียบร้อย");
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //JobPosition.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", JobPosition);
                }
            }
            //JobPosition.lstobj = Contents;
            return View("~/Views/JobPosition/Manage.cshtml", JobPosition);
        }
    }
}
