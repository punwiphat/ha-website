﻿using System;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class GoToFileController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        
        public GoToFileController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector,IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                Banner getMain = BannerData.Get.MainBannerByMenuId(MenuID);
                var main = getMain!=null ? getMain : new Banner();
                int ID = main.ID;
                var obj = BannerData.Get.BannerById(ID).ToList();
                main = main == null ? new Banner() : main;
                main.MenuID = MenuID;
                main.lstBanner = obj;
                string logDescription = "";
                if(obj.Count > 0)
                {
                    logDescription = "ดูรายละเอียดข้อมูล ดาวน์โหลดไฟล์ Name : "+obj.FirstOrDefault().Menuname;
                }
                LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/GoToFile/Index.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
            }
            return RedirectToAction("Index", new { Menuname = menuname });
        }
        [HttpPost]
        public ActionResult Index(Banner banner)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = BannerData.Post.AddOrUpdate(banner,UserName, IpAddress, Browser);
                    foreach(var o in banner.lstBanner)
                    {
                        o.BannerID = main.ID;
                    }
                    var detail = BannerData.Post.DetailAddOrUpdate(banner.lstBanner, UserName,2, IpAddress, Browser);
                    main.lstBanner = detail;
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(banner.ID, LogTransaction.Get.ActivityType.Error, banner.Menuname, UserName, ex.Message, IpAddress, Browser);
                   
                }
            }
                return View("~/Views/GoToFile/Index.cshtml", banner);
        }
    }
}
