﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class CoursesController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        
        public CoursesController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector,IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
            if (menuObj.Role == "N")
                return RedirectToAction("Index", "Permission", new { MenuName = "401" });

            int MenuID = menuObj?.ID ?? 0;
            name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuname;
            ViewBag.Role = menuObj.Role;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            if (_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
            }
            List<ProjectsByLanguage> lstobj = new List<ProjectsByLanguage>();
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            foreach (var a in lang)
            {
                TempData.Remove(MenuID + "_" + a.LanguageCode);
                ProjectsByLanguage obj = new ProjectsByLanguage();
                obj.SelectedTabs = a.LanguageCode == "TH" ? "active" : "";
                obj.LanguageCode = a.LanguageCode;
                obj.lstProjects = ProjectsData.Get.ByMenuID(MenuID, a.LanguageCode);

                obj.MenuDisplayID = MenuDisplay != null ? MenuDisplay.ID : 0;
                obj.SortingBy = MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value : 1) : 1;
                obj.SortingType = MenuDisplay != null ? (MenuDisplay.SortingType != null ? MenuDisplay.SortingType.Value : false) : false;
                lstobj.Add(obj);
            }
            ViewBag.Getsorting = DDLSortingBy(MenuDisplay != null ? (MenuDisplay.SortingBy != null ? MenuDisplay.SortingBy.Value.ToString() : "1") : "1");
            LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, name_menu, UserName, "", IpAddress, Browser);
            return View("~/Views/SWU/Courses/index.cshtml", lstobj);
        }
        public IActionResult Manage(string menuname, string ID)
        {
            int refID = !string.IsNullOrEmpty(ID) ? Convert.ToInt32(ID) : 0;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

                var main = !string.IsNullOrEmpty(ID) ? ProjectsData.Get.MainProjectsById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0) : new Projects();
                var obj = ProjectsData.Get.ProjectsById(!string.IsNullOrEmpty(ID) ? int.Parse(ID) : 0).ToList();
                main = main == null ? new Projects() : main;
                main.MenuID = MenuID;
                main.lstProjects = obj;
                main.Menuname = menuname;
                var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
                main.GUID = Guid.NewGuid().ToString();
                main.MenuIDCategory = MenuDisplay.CategoryID;
                TempData.Remove(main.GUID + "_TH");
                TempData.Remove(main.GUID + "_EN");

                string logDescription = "";
                if (obj.Count > 0)
                {
                    logDescription = "ดูรายละเอียดข้อมูล"+ name_menu+" : " + obj.FirstOrDefault().ProjectName;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/SWU/Courses/Manage.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
            }
            return RedirectToAction("Index", new { Menuname = menuname });


        }
        [HttpPost]
        public ActionResult Manage(Projects Projects)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = ProjectsData.Post.AddOrUpdate(Projects, UserName, IpAddress, Browser);
                    foreach (var o in Projects.lstProjects)
                    {
                        o.ProjectID = main.ID;
                    }
                    var detail = ProjectsData.Post.DetailAddOrUpdate(Projects.lstProjects, UserName, IpAddress, Browser, 10);
                    main.lstProjects = detail;
                    foreach (var item in detail)
                    {
                        #region uploadfile
                        string tempUploads = TempData[item.SUB_GUID] != null ? Convert.ToString(TempData[item.SUB_GUID]) : "";
                        List<MediaFiles> lstUploads = !string.IsNullOrEmpty(tempUploads) ?
                          JsonConvert.DeserializeObject<List<MediaFiles>>(tempUploads) : new List<MediaFiles>();
                        if (lstUploads.Count() > 0)
                        {
                            foreach (var item2 in lstUploads)
                            {
                                var p = MediaFilesData.Post.UpdateStatus(item2.MediaFileID, item.ProjectDetailID.ToString(), UserName, IpAddress, Browser);
                            }
                        }
                        #endregion
                    }
                    return RedirectToAction("Index", new { Menuname = main.Menuname });
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(Projects.ID, LogTransaction.Get.ActivityType.Error, Projects.Menuname, UserName, ex.Message, IpAddress, Browser);
                }
            }
            return View("~/Views/SWU/Courses/Manage.cshtml", Projects);
        }
    }
}
