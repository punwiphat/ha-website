﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class SocialLinkController : BaseController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;

        public SocialLinkController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });


                var obj = SocialLinkData.Get.ListSocialLink().ToList();
                foreach (var item in obj)
                {
                    item.MenuID = MenuID;
                    item.Menuname = menuname;
                }

                return View("~/Views/SocialLink/Index.cshtml", obj);
            }
            catch (Exception ex)
            {
                // LogModifyPage("", menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการอ่านข้อมูล (สำหรับหน้าจัดการ) ได้เนื่องจาก" + ex.Message);
                //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml");
            }
            return View("~/Views/SocialLink/Index.cshtml", new Banner());
        }
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Index(List<SocialLink> lstSocialList)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    var main = SocialLinkData.Post.AddOrUpdate(lstSocialList, UserName, IpAddress, Browser);

                    return RedirectToAction("Index", new { Menuname = lstSocialList.FirstOrDefault().Menuname });
                }
                catch (Exception ex)
                {
                    //LogModifyPage(state_GUID, Contents.FirstOrDefault().menuname, "ERROR :" + ex.GetType().Name, "ไม่สามารถทำการบันทึกข้อมูลได้เนื่องจาก" + ex.Message);
                    //banner.lstobj = Contents;
                    //return View("~/Views/" + Config_static.Common.theme_name + "/Linker/Manage.cshtml", banner);
                }
            }
            return View("~/Views/SocialLink/Index.cshtml", lstSocialList);
        }
    }
}
