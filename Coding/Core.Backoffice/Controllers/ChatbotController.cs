﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ChatbotController : Controller
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private readonly IConfiguration _configuration;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        private readonly string _linkChatbot;
        public ChatbotController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
            _configuration = configuration;
            _linkChatbot = _configuration.GetValue<string>("Link_Chatbot");

        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return Redirect(_linkChatbot);
        }
    }
}
