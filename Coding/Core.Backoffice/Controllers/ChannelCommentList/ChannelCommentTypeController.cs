﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ChannelCommentTypeController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ChannelCommentTypeController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string menuname)
        {
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalink(menuname);
                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                var main = ChannelCommentTypeData.Get.ALL();

                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, name_menu, UserName, "", IpAddress, Browser);
                return View("~/Views/ChannelCommentList/ChannelCommentType/Index.cshtml", main);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
        }


        public IActionResult Manage(string menuname, int ID)
        {
            int refID = ID;
            string name_menu = "";
            try
            {
                ViewBag.Menuname = menuname;
                ViewBag.Title = menuname;
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalink(menuname);
                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                ChannelCommentType Obj = new ChannelCommentType();
                Obj.Actived = true;
                if (ID != 0)
                {
                    Obj = ChannelCommentTypeData.Get.ByID(ID);
                }
                Obj.MenuID = MenuID;
                Obj.Menuname = menuname;
                string logDescription = "";
                if (Obj != null)
                {
                    logDescription = "ดูรายละเอียดข้อมูล Channel Comment Type ID : " + Obj.ID;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/ChannelCommentList/ChannelCommentType/Manage.cshtml", Obj);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Manage(ChannelCommentType Obj, string menuname)
        {
            ViewBag.MenuID = Obj.MenuID;
            ViewBag.Title = Obj.Menuname;
            ViewBag.Menuname = Obj.Menuname;
            try
            {
                var items = ChannelCommentTypeData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                return Json(new { status = true, location = Url.Action("Index", "ChannelCommentType") });
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(Obj.ID, LogTransaction.Get.ActivityType.Error, Obj.Menuname, UserName, ex.Message, IpAddress, Browser);
                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
    }
}
