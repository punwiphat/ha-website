﻿using System;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class ChannelCommentController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ChannelCommentController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult List(string menuname)
        {
            try
            {
                string name_menu = "";
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                var main = ChannelCommentData.Get.List();
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.View, name_menu, UserName, "", IpAddress, Browser);
                return View("~/Views/ChannelCommentList/List.cshtml", main);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
        }


        public IActionResult Manage(string menuname, int ID)
        {
            int refID = ID;
            string name_menu = "";
            try
            {
                Tm_Menu menuObj = MenuBackofficeData.Get.ByPermalinkWithRole(menuname, UserName);
                if (menuObj.Role == "N")
                    return RedirectToAction("Index", "Permission", new { MenuName = "401" });

                int MenuID = menuObj?.ID ?? 0;
                name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
                ViewBag.Title = name_menu;
                ViewBag.MenuID = MenuID;
                ViewBag.Menuname = menuname;
                ViewBag.Role = menuObj.Role;
                if (MenuID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                ChannelComment Obj = new ChannelComment();
                if (ID != 0)
                {
                    Obj = ChannelCommentData.Get.ByID(ID);
                }
                string logDescription = "";
                if (Obj!=null)
                {
                    logDescription = "ดูรายละเอียดข้อมูล Channel Comment ID : " + Obj.ID;
                }
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.View, name_menu, UserName, logDescription, IpAddress, Browser);
                return View("~/Views/ChannelCommentList/Manage.cshtml", Obj);
            }
            catch (Exception ex)
            {
                LogTransaction.SaveLog(refID, LogTransaction.Get.ActivityType.Error, name_menu, UserName, ex.Message, IpAddress, Browser);
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            }
            //return View("~/Views/ChannelCommentList/Manage.cshtml", new ChannelComment());
        }
    }
}
