﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Core.Backoffice.Models;
using Core.Common;
using Core.DAL.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
using Core.library;

namespace Core.Backoffice.Controllers
{
    [Authorize]
    [ActiveUserFilter]
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IMemoryCache _cache;
        public HomeController(ILogger<HomeController> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index()
        {
            ViewBag.MenuID = 1;
            if (!_cache.TryGetValue("MenuList" + UserRoleGroupId.ToString(), out List<Tm_Menu> MenuObj))
            {
                //List<Tm_Menu> list_menu = new List<Tm_Menu>();
                //MenuObj = MenuBackofficeData.Get.GetByAllMenu();

                //MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                //{
                //    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                //    SlidingExpiration = TimeSpan.FromDays(1) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                //};
                //_cache.Set("MenuList", MenuObj, TimeSpan.FromDays(1));
                List<Tm_Menu> list_menu = new List<Tm_Menu>();
                MenuObj = MenuBackofficeData.Get.GetMenuBackOfficeListActive();
                var role = RoleData.Get.RoleByGroupID(UserRoleGroupId);
                list_menu = (from o in MenuObj
                             join r in role on o.ID equals r.MenuID
                             where r.RoleType != "N"
                             select new
                             {
                                 o
                             }).ToList_list<Tm_Menu>();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                                                                            // SlidingExpiration = TimeSpan.FromMinutes(30) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                //options.RegisterPostEvictionCallback(Callback, "Some state info");
                _cache.Set("MenuList" + UserRoleGroupId.ToString(), MenuObj, TimeSpan.FromDays(1));
            }
            if (!_cache.TryGetValue("LanguageList", out List<Languages> lang))
            {
                lang = LanguageData.GetByActive();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(365),
                    SlidingExpiration = TimeSpan.FromDays(365)
                };
                _cache.Set("LanguageList", lang, TimeSpan.FromDays(365));
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error500()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
