﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core.Backoffice.Models;
using Core.Models;
using PagedList.Core;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.FileSystemGlobbing.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using Wangkanai.Detection;
using Core.library;
using Microsoft.AspNetCore.Authorization;

namespace Core.Backoffice.Controllers
{
    public class BannersController : BaseController
    {

        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;

        public BannersController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector)
        {

            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
        }


        [Authorize]
        public async Task<IActionResult> Index(string title, string sortOrder, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            string sPath = Directory.GetCurrentDirectory();
            ViewBag.Title = string.IsNullOrEmpty(title) ? ViewData["Currenttitle"] : title;


            Guid menuId = (Guid)StaticMenu.GetMenuIdByName(title);
            ViewBag.MenuId = menuId;

            var obj = BannerData.SearchListByMenu(searchString, menuId);
            obj = obj.OrderByDescending(s => s.UpdateDate);

            foreach (var it in obj)
            {
                var thumbfile = UploadFiles.GetThumbnailFileByrowId(it.BannerId, "Banner");
                if (thumbfile != null)
                {
                    it.FileName = thumbfile.FileName;
                    it.FilePath = thumbfile.PathFile;
                    it.OriginalFileName = thumbfile.FileNameOri;
                }

            }
            ViewData["Currenttitle"] = title;
            ViewData["CurrentFilter"] = searchString;
            ViewData["UserName"] = UserName;

            //save log
            if (pageNumber == null)
            {


                SaveActivityLog(menuId, title, "View", "", UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());
            }

            return View(await PaginatedList<Banners>.CreateAsync(obj, pageNumber ?? 1, Constants.Config.pageSize));

        }
        [HttpGet]
        [Authorize]
        public IActionResult Manage(Guid? bannerId, string title)
        {
            ViewBag.Title = title;
            TempData["Message"] = "";
            var obj = new Core.DAL.Models.Banners();
            if (bannerId == null)
            {
                obj.MenuId = (Guid)StaticMenu.GetMenuIdByName(title);
                obj.Activated = true;

                //save log
                SaveActivityLog(obj.MenuId, title, "View", obj.MenuId.ToString(), UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());

            }
            else
            {

                obj = BannerData.GetBannerById((Guid)bannerId);

                obj.MenuId = (Guid)StaticMenu.GetMenuIdByName(title);
                try
                {
                    obj.EffectiveDate = obj.EffectiveDate.ToString().todateTH();
                    obj.EndDate = obj.EndDate.ToString().todateTH();
                }
                catch { }//server can not change to thai format


                var thumbfile = UploadFiles.GetThumbnailFileByrowId((Guid)bannerId, "Banner");
                if (thumbfile != null)
                {
                    obj.FileName = thumbfile.FileName;
                    obj.FilePath = thumbfile.PathFile;
                    obj.OriginalFileName = thumbfile.FileNameOri;
                }
                //save log
                SaveActivityLog(obj.BannerId, title, "View", "", UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());


            }
            obj.CreateBy = UserName;
            obj.UpdateBy = UserName;
            ViewBag.MenuId = obj.MenuId;

            return View(obj);
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Manage(Core.DAL.Models.Banners savedata, IFormFile TopicFile, List<IFormFile> Gallery)
        {
            var objMenu = StaticMenu.GetMenuById(savedata.MenuId);
            ViewBag.Title = objMenu.MenuName;
            string sProcess = "Add";
            try
            {

                if (ModelState.IsValid)
                {


                    var objResult = BannerData.AddOrUpdate(savedata);


                    if (savedata.BannerId.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        sProcess = "Edit";
                    }

                    if (TopicFile != null)
                    {
                        if (TopicFile.Length > 0)
                        {
                            UploadFiles.DeleteFile(objResult.BannerId, "Banner", objResult.UpdateBy);
                        }

                        if (objMenu.MenuName == "จัดการป้ายโฆษณา")
                        {
                            UploadFiles.AddUploadCarouselFile(TopicFile, objResult.BannerId.ToString(), "Banner", objResult.CreateBy);
                        }
                        else if (objMenu.MenuName == "จัดการเว็บไซต์ที่เกี่ยวข้อง")
                        {
                            UploadFiles.AddUploadRelateLinkFile(TopicFile, objResult.BannerId.ToString(), "Banner", objResult.CreateBy);
                        }
                        else if (objMenu.MenuName == "จัดการประกาศพิเศษ")
                        {
                            UploadFiles.AddUploadLandingFile(TopicFile, objResult.BannerId.ToString(), "Banner", objResult.CreateBy);
                        }
                        else
                        {
                            UploadFiles.AddUploadThumbnailFile(TopicFile, objResult.BannerId.ToString(), "Banner", objResult.CreateBy);
                        }

                    }
                    ////multi files
                    //foreach (IFormFile file in Gallery)
                    //{
                    //    UploadFiles.AddUploadGalleryFile(file, objResult.BannerId.ToString(), "Gallery", objResult.CreateBy);
                    //}

                    //save log
                    SaveActivityLog(objResult.BannerId, objMenu.MenuName, sProcess, "", UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());
                    ViewBag.MenuId = objResult.MenuId;
                    TempData["Message"] = "Success";
                    return RedirectToAction("Index", "Banners", new { title = objMenu.MenuName });
                }

                return View(savedata);
            }
            catch (Exception ex)
            {
                ViewBag.MenuId = savedata.MenuId;
                //save log
                SaveActivityLog(savedata.BannerId, objMenu.MenuName, sProcess, "Error:" + ex.Message, UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());
                TempData["Message"] = "Error";
                return RedirectToAction("Index", "Banners", new { title = objMenu.MenuName });
                //throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public JsonResult UpdateActivateStatus(Guid bannerId, string updateby)
        {
            try
            {
                var objResult = BannerData.UpdateActiveStatus(bannerId, updateby);


                //save log
                var Obj = BannerData.GetBannerById(bannerId);
                var objMenu = StaticMenu.GetMenuById(Obj.MenuId);
                SaveActivityLog(bannerId, objMenu.MenuName, "Edit", "Uupdate Status", UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());
                TempData["Message"] = "Success";
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                var Obj = BannerData.GetBannerById(bannerId);
                var objMenu = StaticMenu.GetMenuById(Obj.MenuId);
                SaveActivityLog(bannerId, objMenu.MenuName, "Edit", "Error:" + ex.Message, UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());

                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }
        [Authorize]
        public JsonResult DeleteBanner(Guid bannerId, string updateby)
        {
            try
            {
                var objResult = BannerData.DeleteBanner(bannerId, updateby);
                //save log
                var Obj = BannerData.GetBannerById(bannerId);
                var objMenu = StaticMenu.GetMenuById(Obj.MenuId);
                SaveActivityLog(bannerId, objMenu.MenuName, "Delete", "Delete Data", UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());

                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                var Obj = BannerData.GetBannerById(bannerId);
                var objMenu = StaticMenu.GetMenuById(Obj.MenuId);
                SaveActivityLog(bannerId, objMenu.MenuName, "Delete", "Error:" + ex.Message, UserId, _accessor.HttpContext.Connection.RemoteIpAddress.ToString(), _browser.Browser.Type.ToString());

                return Json(new { status = false, message = ex.Message.ToString() });
            }
        }

        [HttpGet]
        public IActionResult GetThumbFile(Guid bannerId)
        {
            try
            {

                var objFile = UploadFiles.RenderByFileName(bannerId, "Banner");
                if (objFile == null)
                    return null;

                string sFilePaht = _environment.ContentRootPath + @"\" + objFile.PathFile;

                byte[] FileBytes = System.IO.File.ReadAllBytes(sFilePaht);
                return File(FileBytes, objFile.FileExtension);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
