﻿$(function () {
   
    $("body").on("change", ".DDLArea", function (event) {
        var AreaId = $(this).val();
        var ele = $(this).attr('rel');
        renderProvince(AreaId, ele);

    });
    $("body").on("change", ".DDLProvince", function (event) {
        var ProvinceID = $(this).val();

        var ele = $(this).attr('rel');

        renderDistrict(ProvinceID, ele);

    });
    $("body").on("change", ".DDLDistrict", function (event) {
        var DistrictId = $(this).val();
        var ele = $(this).attr('rel');
        renderSubDistrict(DistrictId, ele);

    });
    $("body").on("change", ".DDLSubDistrict", function (event) {
        var SubDistrictId = $(this).val();
        //renderSubDistrict(DistrictId);

    });
    $("body").on("change", "#filladdress", function (event) {
       // console.log($('#filladdress').is(":checked"));
        if ($('#filladdress').is(":checked"))
        {
            var addr = $("#Addr1_P").val();
            var ProvinceCode = $("#ProvinceCode_P").val();
            var DistrictCode = $("#DistrictCode_P").val();
            var SubDistrictCode = $("#SubDistrictCode_P").val();
            var PostCode = $("#PostCode_P").val();
            $("#Addr1_C").val(addr);

            $("#ProvinceCode_C").val(ProvinceCode).trigger("change");

            $("#DistrictCode_C").val(DistrictCode);//.delay(3000).trigger("change");
            $("#SubDistrictCode_C").val(SubDistrictCode);//.delay(3000).trigger("change");
            $("#PostCode_C").val(PostCode);//.delay(3000).trigger("change");
        }
        else {
            $("#Addr1_C").val('');
            $("#ProvinceCode_C").val('');
            $("#DistrictCode_C").val('');
            $("#SubDistrictCode_C").val('');
            $("#PostCode_C").val('');
        }

    });
})

function renderProvince(ele) {
    $.ajax({
        url: "/Base/GetJsonDllProvince",
       // data: {},
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.ProvinceID_' + ele);

            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกจังหวัด</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.code,
                        text: data.name
                    }).html(data.name).appendTo('.ProvinceID_' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}
function renderDistrict(ProvinceID, ele) {
    $.ajax({
        url: "/Base/GetJsonDllDistrict",
        data: { ProvinceCode: ProvinceID },
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.DistrictId_' + ele);
            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกอำเภอ</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.code,
                        text: data.name
                    }).html(data.name).appendTo('.DistrictId_' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}
function renderSubDistrict(DistrictId, ele) {

    $.ajax({
        //url: "@Url.Action("GetJsonDllSubDistrict", "Base")",

        url: "/Base/GetJsonDllSubDistrict",
        data: { DistrictCode: DistrictId },
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.SubDistrictId_' + ele);
            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกตำบล</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.code,
                        text: data.name
                    }).html(data.name).appendTo('.SubDistrictId_' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}