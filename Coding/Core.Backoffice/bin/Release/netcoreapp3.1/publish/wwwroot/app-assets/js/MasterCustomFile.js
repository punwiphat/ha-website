﻿$(document).ready(function () {

    $('body').on('click', '.bt-popup', function () {
        var $this = $(this);
        $.get($this.attr('data-href'), function (result) {
            $($this.attr('data-name')).html(result);
        });
    });
    $('body').on('click', '.bt-edit', function () {
        var $this = $(this);
        location.href = $this.attr('data-href');
    });

})

function keypresshandle(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        document.getElementById("fmSearch").submit();
    }
} 