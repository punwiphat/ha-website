﻿$(document).ready(function () {
    $('body').on('click', '.bt-readed', function () {
        var $this = $(this);
         var readed = $this.attr('data-val');
         var id = $this.attr('data-id');

        if (readed == "active") {
            $this.removeClass(readed);
            var number = $("#noti-count").text();
            if (number > 1) {
                number = number - 1;
                $("#noti-count").text(number);
            }
            else {
                $("#noti-count").hide();
            }
            alert('@Url.Action("Readed", "NotifyConfigs")');
            $.ajax({
                url: '/NotifyConfigs/Readed',
                data: { NotiId: id },
                cache: false,
                success: function (html) {
                    location.href = $this.attr('data-href');
                }
            });

        }
        else {
            location.href = $this.attr('data-href');
        }
    });
    $('body').on('click', '.bt-readedAll', function () {
       
            $.ajax({
                url: '/NotifyConfigs/ReadedAll',
                cache: false,
                success: function (html) {
                    $("#parentNotiId").find("a.active").removeClass('active');
                 
                    $("#noti-count").hide();
                }
            });

    });
    
    $('body').on('click', '.bt-cancel', function () {
        var $this = $(this);
        Swal.fire({
            title: 'คุณต้องการยกเลิกคำร้องใช่หรือไม่ ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน !',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-default ml-1',
            cancelButtonText: 'ยกเลิก !',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
               
                $.post($this.attr('data-href'), function (data) {
                    if (data.status) {
                        success_alert('ยกเลิกคำร้องสำเร็จ', 'ยกเลิกคำร้องเรียบร้อยแล้ว');
                        var row = $this.closest('tr');
                        row.remove()
                    }
                    else {
                        Error_alert('ยกเลิกคำร้องไม่สำเร็จ', data.message);
                    }
                });
            }
        })
    });
   
   
});
