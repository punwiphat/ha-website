﻿//$(".addItempersonals").click(function () {
//    var $this = $(this).attr('rel');
//    $.ajax({
//        url: '@Url.Action("AddDefendant", "Form")',
//        data: { menuID: '11', tableID: '11' },
//        cache: false,
//        success: function (html) {
//            $("#" + $this).append(html);
//            if (('.select2').length > 0) {
//                $('.select2').select2({
//                    dropdownAutoWidth: true,
//                    width: '100%'
//                });
//            }
//        }
//    });
//    return false;
//});

$('body').on('click', '.addItempersonals', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/Form/AddDefendant',
        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            $("#" + $this).append(html);
            if (('.select2').length > 0) {
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }
        }
    });
    return false;
})
$('body').on('click', '.addItemCivil', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/CourtForm/AddCivilApp',
        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            $("#" + $this).append(html);
            if (('.select2').length > 0) {
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }
        }
    });
    return false;
})
$('body').on('click', '.addItemAppeal', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/CourtForm/AddAppealApp',
        //url: '@Url.Action("AddCivilApp", "CourtForm")',
        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            $("#" + $this).append(html);
            if (('.select2').length > 0) {
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }
        }
    });
    return false;
})
$('body').on('click', '.addItemSupreme', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/CourtForm/AddSupremeApp',
        //url: '@Url.Action("AddCivilApp", "CourtForm")',
        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            if (('.select2').length > 0) {
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }
            $("#" + $this).append(html);
        }
    });
    return false;
})
$('body').on('click', '.addItemprosecutors', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/AttorneyForm/AddProsecutor',
        //url: '@Url.Action("AddProsecutor", "CourtForm")',
        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            $("#" + $this).append(html);
        }
    });
    return false;
})
$('body').on('click', '.addItemAuctionSchedule', function () {
    var $this = $(this).attr('rel');
    $.ajax({
        url: '/ExecutionForm/AddAuctionApp',
        //url: '@Url.Action("AddProsecutor", "CourtForm")',

        data: { menuID: '11', tableID: '11' },
        cache: false,
        success: function (html) {
            $("#" + $this).append(html);
        }
    });
    return false;
})


$('body').on('click', 'a.deletePersonalRowM', function () {
    var thisObj = $(this).parents().parents("div.editorPersonalM:first");

    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});


$('body').on('click', 'a.deleteRowM', function () {
    var thisObj = $(this).parents().parents("div.deleteRowM:first");
    //console.log('deleteRowM');
    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
           // thisObj.fadeOut("slow");
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});
$('body').on('click', 'a.deleteAppealRowM', function () {
    var thisObj = $(this).parents().parents("div.editorAppealM:first");
    // console.log(thisObj);

    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
       //  console.log(result);
        if (result.value) {
            thisObj.fadeOut(300, function () { $(this).remove(); });
          //  thisObj.fadeOut("slow");
            return false;
        }
    })

    return false;
});
$('body').on('click', 'a.deleteCivilRowM', function () {
   // console.log('ss');
   // console.log('deleteRowM');
    var thisObj = $(this).parents().parents("div.editorCilvilM:first");
    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        //console.log(result);
       // console.log(result.value);

        if (result.value) {
            //thisObj.fadeOut("slow");
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});
$('body').on('click', 'a.deleteSupremeRowM', function () {
    var thisObj = $(this).parents().parents("div.editorSupremeM:first");
    // console.log(thisObj);

    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        // console.log(result);
        if (result.value) {
           // thisObj.fadeOut("slow");
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});
$(function () {
    if ($("div.editorPersonalM a.deletePersonalRowM").length > 0) {
        $("div.editorPersonalM a.deletePersonalRowM").first().hide();
    }
    if ($("div.editorCilvilM a.deleteCivilRowM").length > 0) {
        $("div.editorCilvilM a.deleteCivilRowM").first().hide();
    }
    if ($("div.editorAppealM a.deleteAppealRowM").length > 0) {
        $("div.editorAppealM a.deleteAppealRowM").first().hide();
    }
    if ($("div.editorSupremeM a.deleteSupremeRowM").length > 0) {
        $("div.editorSupremeM a.deleteSupremeRowM").first().hide();
    }
    if ($("div.editorProsecutorM a.deleteProsecutorRowM").length > 0) {
        $("div.editorProsecutorM a.deleteProsecutorRowM").first().hide();
    }
    if ($("div.editorAuctionM a.deleteAuctionRowM").length > 0) {
        $("div.editorAuctionM a.deleteAuctionRowM").first().hide();
    }

})