﻿$(function () {
    $("body").on("change", ".DDLArea", function (event) {
        var AreaId = $(this).val();
        var ele = $(this).attr('rel');
        renderProvince(AreaId, ele);

    });
    $("body").on("change", ".DDLProvince", function (event) {
        var ProvinceID = $(this).val();
        var ele = $(this).attr('rel');
        renderDistrict(ProvinceID, ele);

    });
    $("body").on("change", ".DDLDistrict", function (event) {
        var DistrictId = $(this).val();
        var ele = $(this).attr('rel');
        renderSubDistrict(DistrictId, ele);

    });
    $("body").on("change", ".DDLSubDistrict", function (event) {
        var SubDistrictId = $(this).val();
        //renderSubDistrict(DistrictId);

    });
})
function renderProvince(AreaId, ele) {
    $.ajax({
        url: "/Base/GetJsonDllProvince",
        data: { AreaId: AreaId },
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.ProvinceID' + ele);
            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกจังหวัด</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.provinceId,
                        text: data.provinceNameTh
                    }).html(data.provinceNameTh).appendTo('.ProvinceID' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}
function renderDistrict(ProvinceID, ele) {
    $.ajax({
        url: "/Base/GetJsonDllDistrict",
        data: { ProvinceID: ProvinceID },
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.DistrictId' + ele);
            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกอำเภอ</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.districtId,
                        text: data.districtNameTh
                    }).html(data.districtNameTh).appendTo('.DistrictId' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}
function renderSubDistrict(DistrictId, ele) {

    $.ajax({
        //url: "@Url.Action("GetJsonDllSubDistrict", "Base")",
        url: "/Base/GetJsonDllSubDistrict",
        data: { DistrictId: DistrictId },
        type: "post",
        cache: false,
        success: function (result) {
            var modelDropDown = $('.SubDistrictId' + ele);
            modelDropDown.empty();
            modelDropDown.append("<option value=''>เลือกตำบล</option>");
            $.each(result, function (i, data) {
                $('<option>',
                    {
                        value: data.subDistrictId,
                        text: data.subDistrictNameTh
                    }).html(data.subDistrictNameTh).appendTo('.SubDistrictId' + ele);
            });
            modelDropDown.trigger("chosen:updated");
        },
        error: function () {
        }
    });
}