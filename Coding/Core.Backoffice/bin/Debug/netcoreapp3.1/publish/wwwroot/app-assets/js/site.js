﻿$(function () {
    if ($('.table').length > 0) {
        $.extend(true, $.fn.dataTable.defaults, {
            'order': [
                [0, 'asc']
            ],
            "paging": true,
            "info": false
        });
        $.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
            return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
                return $('input', td).val() * 1;
            });
        }
        $.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
            return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
                return $('input', td).prop('checked') ? '1' : '0';
            });
        }
        $.fn.dataTable.ext.order['dom-select'] = function (settings, col) {
            return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
                return $('select', td).val();
            });
        }

    }
    //if ($('.tree').length > 0) {
    //    $('.tree').treegrid();
    //}

    $("#EffectiveDate").datepicker().on('changeDate', (selected) => {
        var minDate = new Date(selected.date.valueOf());
      //  //console.log(minDate);
        $('#EndDate').datepicker('setStartDate', minDate);
    });
    //$("#EndDate").datepicker().on('changeDate', (selected) => {
    //    var minDate = new Date(selected.date.valueOf());
    //    $('#EffectiveDate').datepicker('setEndDate', minDate);
    //});

    if (('.select2').length > 0) {
        $('.select2').select2({
            placeholder: "กรุณาเลือก",
            dropdownAutoWidth: true,
            width: '100%',
            allowClear: true
        });
    }
    if ($(".select2-tags").length > 0) {
        $(".select2-tags").select2({
            dropdownAutoWidth: true,
            width: '100%',
            tags: true,
            allowClear: true
        });
    }
    if ($(".select2-relate").length > 0) {
        $(".select2-relate").select2({
            dropdownAutoWidth: true,
            width: '100%',
            allowClear: true
        });
    }
    $(".StatePublish").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: '<i class="la la-eye"></i> PUBLISH',
        offText: '<i class="la la-eye-slash"></i> DRAFT',
        labelText: '&nbsp',
        handleWidth: '80',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    $(".StatePublishSmall").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: '<i class="la la-eye"></i>',
        offText: '<i class="la la-eye-slash"></i>' ,
        labelText: '&nbsp',
        handleWidth: '180',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    $(".StateComment").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: '<i class="la la-comments-o"></i> ON',
        offText: '<i class="la la-remove"></i> OFF',
        labelText: '&nbsp',
        handleWidth: '120',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    $(".bt-menustatus").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: 'ACTIVE',
        offText: 'INACTIVE',
        labelText: '&nbsp',
        handleWidth: '30',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });

    $(".bt-status").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: 'ACTIVE',
        offText: 'INACTIVE',
        labelText: '&nbsp',
        handleWidth: '120',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    $(".switchForbidden").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'secondary',
        offColor: 'success',
        onText: 'INACTIVE',
        offText: 'ACTIVE',
        labelText: '&nbsp',
        handleWidth: '120',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    $(".switchACTIVE").bootstrapSwitch({
        animate: true,
        size: 'small',
        onColor: 'success',
        offColor: 'secondary',
        onText: '<i class="la la-check"></i> ACTIVE',
        offText: '<i class="la la-remove"></i> INACTIVE',
        labelText: '&nbsp',
        handleWidth: '80',
        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'
    });
    //if ($(".switchBootstrap").length > 0) {
    //    $('.switchBootstrap').bootstrapSwitch('size', 'small');
    //    $('.switchBootstrap').bootstrapSwitch('onColor', 'info');
    //    $('.switchBootstrap').bootstrapSwitch('onText', 'เผยแพร่');
    //    $('.switchBootstrap').bootstrapSwitch('offText', 'บันทึกร่าง');
    //    $('.switchBootstrap').bootstrapSwitch('handleWidth', '1000');
    //}
    //if ($(".switchON").length > 0) {
    //    $('.switchON').bootstrapSwitch('size', 'small');
    //    $('.switchON').bootstrapSwitch('onColor', 'info');
    //    $('.switchON').bootstrapSwitch('onText', 'ON');
    //    $('.switchON').bootstrapSwitch('offText', 'OFF');
    //}
    //if ($(".switchACTIVE").length > 0) {
    //    $('.switchACTIVE').bootstrapSwitch('size', 'small');
    //    $('.switchACTIVE').bootstrapSwitch('onColor', 'info');
    //    $('.switchACTIVE').bootstrapSwitch('onText', 'ACTIVE');
    //    $('.switchACTIVE').bootstrapSwitch('offText', 'INACTIVE');
    //    $('.switchACTIVE').bootstrapSwitch('handleWidth', '150');
    //}

    $(".SortingType").bootstrapSwitch({
        // animate: true,
        size: null,

        onColor: 'warning',
        offColor: 'info',
        onText: '<i class="la la-sort-alpha-desc"></i> ',
        offText: '<i class="la la-sort-alpha-asc"></i> ',
        labelText: '&nbsp',
        handleWidth: '100',

        labelWidth: 'auto',
        baseClass: 'bootstrap-switch',
        wrapperClass: 'wrapper'

    });
    if ($('.skin-square input').length > 0) {
        $('.skin-square input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });
    }
    //if ($('.textareaHtml').length > 0) {
    //    CKEDITOR.replace('HtmlContent');
    //}
    if ($('.ImageCover').length >0) {
        $(".ImageCover").fileinput({
            overwriteInitial: true,
            showUpload: false,
            elErrorContainer: "#errorBlock",
            maxFileCount: 1,
            allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif', 'svg'],
            maxFileSize: 10240,
            mainClass: " file-caption  kv-fileinput-caption"
        });
    }
    $('.number').keyup(function (e) {
        if (/\D/g.test(this.value)) {
        }
    });   
})

function copyToClipboard(copyText) {

    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(copyText).select();
    document.execCommand("copy");
    $temp.remove();

    toastr.info('Copy To Clipboard');
   
}

