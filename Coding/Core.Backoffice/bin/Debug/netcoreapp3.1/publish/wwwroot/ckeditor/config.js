/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
CKEDITOR.dtd.$removeEmpty.i = 0;
CKEDITOR.dtd.$removeEmpty.span = 0;
CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'document', groups: ['mode', 'document', 'doctools', 'Preview'] },
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'styles',groups: ['Format', 'Font', 'FontSize']        },
        { name: 'colors',groups: ['TextColor', 'BGColor']         },
        { name: 'basicstyles',groups: ['basicstyles', 'cleanup']},
        //{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align', 'bidi'] //,
            //items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
        },
        { name: 'links', groups: ['Link', 'Unlink'] },
		{ name: 'editing', groups: ['spellchecker'] }, //'find', 'selection', 
		//{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'others' },
		
		//{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		//{ name: 'colors' },
	];
    config.removeButtons = 'Underline,Subscript,Superscript,Anchor,Paste,Pastetext,Font';
	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
		config.removeDialogTabs = 'link:target;link:advanced;link:Upload;image:Link;image:advanced;image:Upload';
	//config.width = '100%';
    //config.height = 500;

	config.resize_enabled = true;
	config.filebrowserBrowseUrl = '/BACKOFFICE/ckfinder/ckfinder.html';
	config.filebrowserUploadUrl = '/BACKOFFICE/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
	config.smiley_path = '../Emotion/';
	config.smiley_images = ['onion-head-emoticon-01.gif', 'onion-head-emoticon-02.gif', 'onion-head-emoticon-03.gif', 'onion-head-emoticon-04.gif', 'onion-head-emoticon-05.gif', 'onion-head-emoticon-06.gif', 'onion-head-emoticon-07.gif', 'onion-head-emoticon-08.gif', 'onion-head-emoticon-09.gif', 'onion-head-emoticon-10.gif', 'onion-head-emoticon-100.gif', 'onion-head-emoticon-101.gif', 'onion-head-emoticon-102.gif', 'onion-head-emoticon-103.gif', 'onion-head-emoticon-104.gif', 'onion-head-emoticon-105.gif', 'onion-head-emoticon-107.gif', 'onion-head-emoticon-109.gif', 'onion-head-emoticon-110.gif', 'onion-head-emoticon-11.gif', 'onion-head-emoticon-111.gif', 'onion-head-emoticon-112.gif', 'onion-head-emoticon-113.gif', 'onion-head-emoticon-114.gif', 'onion-head-emoticon-115.gif', 'onion-head-emoticon-116.gif', 'onion-head-emoticon-117.gif', 'onion-head-emoticon-118.gif', 'onion-head-emoticon-119.gif', 'onion-head-emoticon-12.gif', 'onion-head-emoticon-120.gif', 'onion-head-emoticon-121.gif', 'onion-head-emoticon-123.gif', 'onion-head-emoticon-125.gif', 'onion-head-emoticon-127.gif', 'onion-head-emoticon-13.gif', 'onion-head-emoticon-14.gif', 'onion-head-emoticon-15.gif', 'onion-head-emoticon-16.gif', 'onion-head-emoticon-17.gif', 'onion-head-emoticon-18.gif', 'onion-head-emoticon-19.gif', 'onion-head-emoticon-21.gif'];

	config.smiley_descriptions = ['haha'];
	config.font_names = 'Kanit;' + config.font_names;
	config.extraPlugins = 'font,btgrid,btbutton,ckawesome,colordialog,embed,autoembed,image2,autogrow,pastefromword,justify,smiley,colorbutton,panelbutton,templates,imagepaste,div';
	//config.autoGrow_minHeight = 200;
	//config.autoGrow_maxHeight = 600;
	config.autoGrow_bottomSpace = 50;
	config.height = 500;        // 500 pixels high.
	config.height = '25em';     // CSS unit (em).
    // Load the default contents.css file plus customizations for this sample.
	config.allowedContent = true;
	config.contentsCss = [CKEDITOR.basePath + 'contents.css', 'https://sdk.ckeditor.com/samples/assets/css/widgetstyles.css'

      //  ,'Backend/Content/font-awesome.css'
     , 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'
        , 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
	];
    // Setup content provider. See https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_media_embed
	config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';

    // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
    // resizer (because image size is controlled by widget styles or the image takes maximum
    // 100% of the editor width).
   
	config.image2_alignClasses = ['text-left', 'text-center', 'text-right'];
	config.image2_disableResizer = true;
	//config.uploadUrl = _file;
};

