﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Core
{
    public static class TruncateString
    {
        public static HtmlString Truncate(this IHtmlHelper helper, string text, int maxLength = 100)
        {
            if (text == null) return new HtmlString("");

            if (text.Length > maxLength)
            {
                text = text.Substring(0, maxLength) + "...";
            }
            return new HtmlString($"{text}");
        }
    }
}
