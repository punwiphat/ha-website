﻿
var subdomain = '';
$(document).ready(function () {
    $('body').on('change', '#SortingBy', function () {
        var MenuID = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = $(this).val();
        $.ajax({
            url: subdomain + "/ActionScripts/" + MenuID+"/UpdateSorting",
            type: 'POST',
            data: { MenuDisplayID: ID, MenuID: MenuID, SortingBy: Val, ActionType: 1 },
            success: function (data) {
                if (data.status) {
                    toastr.success('ทำการจัดลำดับการแสดงผลเรียบร้อย');
                    //$("#tab_TH").addClass("active");
                    //location.reload();
                }
                else {
                    toastr.warning(data.message);
                }
            },
            error: function (xhr) {
                toastr.warning('ทำการจัดลำดับการแสดงผลล้มเหลว !')
            }
        });
    })
    $('#SortingType').on('switchChange.bootstrapSwitch', function (event, state) {
        var MenuID = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = state;
        $.ajax({
            url: subdomain + "/ActionScripts/UpdateSorting/UpdateSorting",
            type: 'POST',
            data: { MenuDisplayID: ID, MenuID: MenuID, SortingBy: Val, ActionType: 2 },
            success: function (data) {
                if (data.status) {
                    toastr.success('ทำการจัดลำดับการแสดงผลเรียบร้อย');
                 //   $("#tab_TH").addClass("active");
                  //  location.reload();
                }
                else {
                    toastr.warning(data.message);
                }
            },
            error: function (xhr) {
                toastr.warning('ทำการจัดลำดับการแสดงผลล้มเหลว !')
            }
        });
    });
    $('body').on('change', '.SortOrder_N', function () {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = $(this).val();
        $.ajax({
            url: subdomain + "/ActionScripts/" + Menuname + "/UpdateSequence",
            type: 'POST',
            data: { ID: ID, Menuname: Menuname, MenuType: Type, Seq: Val},
            success: function (data) {
                if (data.result) {
                    success_alert('', 'ทำการจัดลำดับการแสดงผลเรียบร้อย');
                }
                else {
                    Error_alert('', data.messageError);
                }
            },
            error: function (xhr) {
                Error_alert('', 'ทำการจัดลำดับการแสดงผลล้มเหลว !');
            }
        });
    })
    $('.bt-menustatus').on('switchChange.bootstrapSwitch', function (event, state) {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = state;
        $.ajax({
            url: subdomain + "/ActionScripts/" + Menuname + "/UpdateStatus",
            type: 'POST',
            data: { ID: ID, Menuname: Menuname, MenuType: Type, state: Val },
            success: function (data) {
                if (data.result) {
                    success_alert('', 'ทำการเปลี่ยนสถานะเรียบร้อยแล้ว');
                }
                else {
                    Error_alert('', data.messageError);
                }
            },
            error: function (xhr) {
                Error_alert('', 'ทำการเปลี่ยนสถานะล้มเหลว !');
            }
        });
    });
    $('body').on('switchChange.bootstrapSwitch', '.bt-status', function (event, state) {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = state;
        $.ajax({
            url: subdomain + "/ActionScripts/" + Menuname + "/UpdateStatus",
            type: 'POST',
            data: { ID: ID, Menuname: Menuname, MenuType: Type, state: Val },
            success: function (data) {
                if (data.result) {
                    success_alert('', 'ทำการเปลี่ยนสถานะเรียบร้อยแล้ว');
                }
                else {
                    Error_alert('', data.messageError);
                }
            },
            error: function (xhr) {
                Error_alert('', 'ทำการเปลี่ยนสถานะล้มเหลว !');
            }
        });
    });
    $('.switchForbidden').on('switchChange.bootstrapSwitch', function (event, state) {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var Val = state;
        $.ajax({
            url: subdomain + "/ActionScripts/" + Menuname + "/UpdateStatus",
            type: 'POST',
            data: { ID: ID, Menuname: Menuname, MenuType: Type, state: Val },
            success: function (data) {
                if (data.result) {
                    success_alert('', 'ทำการเปลี่ยนสถานะเรียบร้อยแล้ว');
                }
                else {
                    Error_alert('', data.messageError);
                }
            },
            error: function (xhr) {
                Error_alert('', 'ทำการเปลี่ยนสถานะล้มเหลว !');
            }
        });
    });
    $('body').on('click', '.bt-delete', function () {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var $this = $(this);
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน !',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary ml-1',
            cancelButtonText: 'ยกเลิก !',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: subdomain + "/ActionScripts/" + Menuname + "/Deleted",
                    type: 'POST',
                    data: { ID: ID, Menuname: Menuname, MenuType: Type},
                    success: function (data) {
                        if (data.result) {
                            var row = $this.closest('tr');
                            row.remove()
                            $('li').filter('[data-id="' + ID +'"]').remove();
                            success_alert('', 'ลบข้อมูลรียบร้อยแล้ว');
                        }
                        else {
                            Error_alert('', data.messageError);
                        }
                    },
                    error: function (xhr) {
                        Error_alert('', 'ทำการลบข้อมูลล้มเหลว !');
                    }
                });
            }
        })
    });
    $('body').on('click', '.bt-menudelete', function () {
        var Type = $(this).attr("data-type");
        var Menuname = $(this).attr("data-menu");
        var ID = $(this).attr("data-id");
        var $this = $(this);
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน !',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary ml-1',
            cancelButtonText: 'ยกเลิก !',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: subdomain + "/ActionScripts/" + Menuname + "/Deleted",
                    type: 'POST',
                    data: { ID: ID, Menuname: Menuname, MenuType: Type },
                    success: function (data) {
                        if (data.result) {
                            var row = $this.closest('li');
                            row.remove()
                            success_alert('', 'ลบข้อมูลรียบร้อยแล้ว');
                        }
                        else {
                            Error_alert('', data.messageError);
                        }
                    },
                    error: function (xhr) {
                        Error_alert('', 'ทำการลบข้อมูลล้มเหลว !');
                    }
                });
            }
        })
    });
    $("#logout").click(function () {
        swal({
            title: "ต้องการออกจากระบบใช่หรือไม่?",
            text: "หากต้องการให้กดใช่ ไม่ต้องการกด ไม่ใช่!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn btn-confirm mt-2",
            cancelButtonClass: "btn btn-cancel ml-2 mt-2",
            confirmButtonText: "ใช่!",
            cancelButtonText: "ไม่ใช่",
            preConfirm: function (t) {
                return new Promise(function (n, e) {
                    setTimeout(function () {
                        window.location.replace(subdomain + "/Login/Signout/Signout");
                    }, 500)
                })
            }
        })
    })
});


function Success(data) {
    if (data.status) {
        success_alert('บันทึกข้อมูลสำเร็จ', 'บันทึกข้อมูลเรียบร้อยแล้ว');
    }
    else {
        Error_alert('บันทึกข้อมูลไม่สำเร็จ', data.message);
    }
}
function Success_redirect(data) {
    //console.log('Success_redirect');
    //console.log(data);
    if (data.status) {
        success_alert('บันทึกข้อมูลสำเร็จ', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        window.location = data.location;
    }
    else {
        Error_alert('บันทึกข้อมูลไม่สำเร็จ', data.message);
    }
}
function redirect(data) {
    if (data.status) {
        success_alert('บันทึกข้อมูลสำเร็จ', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        window.location = data.location;
    }
    else {
        Error_alert('บันทึกข้อมูลไม่สำเร็จ', data.message);
    }
}
function Reload(data) {
    if (data.status) {
        success_alert('บันทึกข้อมูลสำเร็จ', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        location.reload();
    }
    else {
        Error_alert('บันทึกข้อมูลไม่สำเร็จ', data.message);
    }
}