
﻿
﻿//$(".addItempersonals").click(function () {
//    var $this = $(this).attr('rel');
//    $.ajax({
//        url: '@Url.Action("AddDefendant", "Form")',
//        data: { menuID: '11', tableID: '11' },
//        cache: false,
//        success: function (html) {
//            $("#" + $this).append(html);
//            if (('.select2').length > 0) {
//                $('.select2').select2({
//                    dropdownAutoWidth: true,
//                    width: '100%'
//                });
//            }
//        }
//    });
//    return false;
//});

//$('body').on('click', '.addItempersonals', function () {
//    var $this = $(this).attr('rel');
//    $.ajax({
//        url: '/Form/AddDefendant',
//        data: { menuID: '11', tableID: '11' },
//        cache: false,
//        success: function (html) {
//            $("#" + $this).append(html);
//            if (('.select2').length > 0) {
//                $('.select2').select2({
//                    dropdownAutoWidth: true,
//                    width: '100%'
//                });
//            }
//        }
//    });
//    return false;
//})

$('body').on('click', 'a.deletePersonalRowM', function () {
    var thisObj = $(this).parents().parents("div.editorPersonalM:first");

    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});
$('body').on('click', 'a.deleteEventsRowM', function () {
    var thisObj = $(this).parents().parents("div.editorEventsM:first");

    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});


$('body').on('click', 'a.deleteRowM', function () {
    var thisObj = $(this).parents().parents("div.deleteRowM:first");
    //console.log('deleteRowM');
    Swal.fire({
        title: 'คุณต้องการลบข้อมูลใช่หรือไม่ ?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน !',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-light ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
           // thisObj.fadeOut("slow");
            thisObj.fadeOut(300, function () { $(this).remove(); });

            return false;
        }
    })

    return false;
});
$(function () {
    if ($("div.editorPersonalM a.deletePersonalRowM").length > 0) {
        $("div.editorPersonalM a.deletePersonalRowM").first().hide();
    }
})