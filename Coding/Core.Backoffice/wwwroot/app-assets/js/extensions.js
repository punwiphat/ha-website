﻿
$('.confirmbtn').on('click', function () {
    Swal.fire({
        title: 'คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?',
        text: 'กรุณายืนยันอีกครั้งเพื่อลบข้อมูลนี้ !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่ !',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-default ml-1',
        cancelButtonText: 'ยกเลิก !',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
        }
    })
});
function success_alert(tltle,detail) {
    toastr.success(detail, tltle
        , { "showMethod": "slideDown", "hideMethod": "slideUp", timeOut: 20000 }); 
}
function Error_alert(tltle, detail) {
    toastr.error(detail, tltle
        , { "showMethod": "slideDown", "hideMethod": "slideUp", timeOut: 20000 });

}
function DualListbox(cls) {
    // Custom Text Support
    $(cls).bootstrapDualListbox({
        moveOnSelect: true,
        filterTextClear: "แสดงทั้งหมด",
        filterPlaceHolder: "ค้นหา...",
        infoText: 'เลือก {0} รายการ',
        infoTextFiltered: '<span class="badge badge-info">รายการค้นหา</span> {0} จาก {1}',
        infoTextEmpty: 'ไม่มีรายการ',
    });
}
function loadStaticDataTable(id) {

    $.fn.dataTable.ext.order['dom-input'] = function (settings, col) {
        return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            return $('input', td).val();
        });
    }
    $(id).DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': false,
        'ordering': true,
        'order': [],
        'info': true,
        'autoWidth': false,
        'language': {
            "sEmptyTable": "ไม่มีข้อมูลในตาราง",
            "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
            "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 รายการ",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกรายการ)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "แสดง _MENU_ รายการ",
            "sLoadingRecords": "กำลังโหลดข้อมูล...",
            "sProcessing": "กำลังดำเนินการ...",
            "sSearch": "ค้นหา: ",
            "sZeroRecords": "ไม่พบข้อมูล",
            "oPaginate": {
                "sFirst": "หน้าแรก",
                "sPrevious": "ก่อนหน้า",
                "sNext": "ถัดไป",
                "sLast": "หน้าสุดท้าย"
            }
        },
        'columnDefs': [{
            "targets": 0, // Column index to apply the custom sorting
            "orderDataType": "dom-text",
            "type": "num",
            "orderSequence": ["asc", "desc"]
        }],
        "fnDrawCallback": function () {
            $(".bt-status").bootstrapSwitch({
                animate: true,
                size: 'small',
                onColor: 'success',
                offColor: 'secondary',
                onText: 'ACTIVE',
                offText: 'INACTIVE',
                labelText: '&nbsp',
                handleWidth: '120',
                labelWidth: 'auto',
                baseClass: 'bootstrap-switch',
                wrapperClass: 'wrapper'
            });
        }
    })
   
    $('[data-toggle="tooltip"]').tooltip();
    UnloadingUI();
}
function onSuccessLoadDataTable(tableid) {

    loadStaticDataTable('#' + tableid);
}
function loadStaticGDataTable(id, groupColumn,span) {
   
    $(id).DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': false,
        'ordering': true,
        'order': [
            [1, '']
        ],
        'info': true,
        'autoWidth': false,
        'language': {
            "sEmptyTable": "ไม่มีข้อมูลในตาราง",
            "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
            "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 รายการ",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกรายการ)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "แสดง _MENU_ รายการ",
            "sLoadingRecords": "กำลังโหลดข้อมูล...",
            "sProcessing": "กำลังดำเนินการ...",
            "sSearch": "ค้นหา: ",
            "sZeroRecords": "ไม่พบข้อมูล",
            "oPaginate": {
                "sFirst": "หน้าแรก",
                "sPrevious": "ก่อนหน้า",
                "sNext": "ถัดไป",
                "sLast": "หน้าสุดท้าย"
            }
        },
        "columnDefs": [
            { "visible": false, "targets": groupColumn },
            {
                "targets": [4],
                "orderable": false
            }
        ],
        "fnDrawCallback": function () {
            $(".bt-status").bootstrapSwitch({
                animate: true,
                size: 'small',
                onColor: 'success',
                offColor: 'secondary',
                onText: 'ACTIVE',
                offText: 'INACTIVE',
                labelText: '&nbsp',
                handleWidth: '120',
                labelWidth: 'auto',
                baseClass: 'bootstrap-switch',
                wrapperClass: 'wrapper'
            });
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;
            api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="'+span+'">' + group + '</td></tr>'
                    );
                    last = group;
                }
            });
        }
    })
    $.fn.dataTable.ext.order['dom-input'] = function (settings, col) {
        return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            return $('input', td).val();
        });
    }
    $('[data-toggle="tooltip"]').tooltip();
    UnloadingUI();
}
function onSuccessLoadGDataTable(tableid, groupColumn, span) {

    loadStaticGDataTable('#' + tableid, groupColumn, span);
}
function UnloadingUI() {
    $('.blockUI').fadeOut();
}
function loadingDIV() {
    var block_ele = $(this).closest('section');

    $(block_ele).block({
        message:
            '<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; Loading ...</div>',
        fadeIn: 1000,
        fadeOut: 1000,
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: "10px 15px",
            color: "#fff",
            width: "auto",
            backgroundColor: "#333"
        }
    });
}
function loadingUI() {
    var block_ele = $(this).closest('.tab-pane');

    $(block_ele).block({
        message:
            '<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; Loading ...</div>',
        fadeIn: 1000,
        fadeOut: 1000,
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: "10px 15px",
            color: "#fff",
            width: "auto",
            backgroundColor: "#333"
        }
    });
}