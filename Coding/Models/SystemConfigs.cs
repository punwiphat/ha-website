﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class SystemConfigs
    {
        public Guid SystemConfigId { get; set; }
        public string SystemConfigCode { get; set; }
        public int SystemConfigValue { get; set; }
        public string SystemConfigDescriptions { get; set; }
        public bool Activated { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string SystemConfigType { get; set; }
        public int System_Config_Category { get; set; }
        public string System_Config_Remark { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
