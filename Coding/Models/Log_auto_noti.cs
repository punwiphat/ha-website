﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class Log_auto_noti
    {
        public int ID { get; set; }
        public DateTime StampDate { get; set; }
        public string Remark { get; set; } 
        public Boolean Issuccess { get; set; }
    }
}
