﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardReply
    {
        public Guid ReplyId { get; set; }
        public Guid WebboardTopicId { get; set; }
        public string ReplyDescriptions { get; set; }
        public string IpAddress { get; set; }
        public string DeviceName { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
