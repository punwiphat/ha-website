﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class WebboardTopic
    {
        [Key]
        public Guid WebboardTopicId { get; set; }
        public Guid WebboardCategoryId { get; set; }
        public string WebboardTopicName { get; set; }
        public string WebboardTopicDecriptions { get; set; }
        public bool Pin { get; set; }
        public int Views { get; set; }
        public int? Reply { get; set; }
        public Boolean Reports { get; set; }
        public string IpAddress { get; set; }
        public string DeviceName { get; set; }
        public bool Islock { get; set; }
        public bool? IsAlias { get; set; }
        public string AliasName { get; set; }
        public int? TypeSort { get; set; }
        public bool? StyleSort { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string Keywords { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
