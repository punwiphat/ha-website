﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Notifications
    {
        [Key]
        public Guid Notification_ID { get; set; }
        public Guid User_ID { get; set; }
        public int Notification_Type { get; set; }
        public string Notification_Title { get; set; }
        public string Notification_Body { get; set; }
        public string Url_Detail { get; set; }
        public Boolean Readed { get; set; }
        public DateTime? Readed_Time { get; set; }
        public DateTime? Notification_Time { get; set; } 
    }
}
