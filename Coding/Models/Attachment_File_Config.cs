﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Attachment_File_Config
    {
        [Key]
        public Guid Attachment_File_ID { get; set; }
        public int Size_limit { get; set; }
        public string Size_limit_Type { get; set; }
        public string Type_limit { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
       
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
