﻿using System;

namespace Core.DAL.Models
{
    public partial class Role
    {
        public int RoleId { get; set; }
        public int RoleGroupId { get; set; }
        public int MenuId { get; set; }
        public int RoleType { get; set; }
    }
}
