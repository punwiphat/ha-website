﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class EmailConfig
    {
        [Key]
        public Guid EmailId { get; set; }
        public string Emailname { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public bool EnableSSL { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
