﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Users :BaseModel
    {
        public int UserId { get; set; }
        public int RoleGroupId { get; set; }
        //public string TitleNameCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Actived { get; set; }
        public bool Deleted { get; set; }

        [NotMapped]
        public string RoleGroupName { get; set; }
        [NotMapped]
        public DateTime RoleGroupCreate { get; set; }
        [NotMapped]
        public string FullName
        {
            get { return String.Concat(FirstName, " ", LastName); }
        }

    }
}
