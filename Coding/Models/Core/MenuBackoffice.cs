﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class MenuBackoffice : BaseModel
    {
        public int MenuBackofficeId { get; set; }
        public string SectionMenu { get; set; }
        public string MenuName { get; set; }
        public int? RefMenuId { get; set; }
        public int MenuLevel { get; set; }
        public string MenuIcon { get; set; }
        public int? MenuTypeId { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Parameter { get; set; }
        public int Sequence { get; set; }
        public bool? Actived { get; set; }

        [NotMapped]
        public List<MenuBackoffice> SubMenu { get; set; }
        [NotMapped]
        public string ActiveMenuName { get; set; }
    }
}
