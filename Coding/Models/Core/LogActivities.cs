﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class LogActivities
    {
        public int LogActivityId { get; set; }
        public int RefId { get; set; }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string ClientIp { get; set; }
        public string DeviceName { get; set; }

        public string ActivityType { get; set; }
        public string Action { get; set; }
        public string Descriptions { get; set; }
        public string Method { get; set;  }

        public DateTime ActionDate { get; set; }
        public string ActionBy { get; set; }
        public string Fullname { get; set; }
        [NotMapped]
        public string GroupName { get; set; }
        
        [NotMapped]
        public virtual string ActionDate_Str
        {
            get { return ActionDate != null ? Formatter.TimeAgo(ActionDate) : ""; }
        }
        [NotMapped]
        public virtual DateTime ActionDate_Only
        {
            get { return ActionDate.Date;  }
        }
    }
   
}
