﻿using System;

namespace Core.DAL.Models
{
    public partial class TmMenuRelated
    {
        public Guid MenuRelateId { get; set; }
        public Guid MenuPositionId { get; set; }
        public Guid MenuId { get; set; }
        public int Sequence { get; set; }
        public int Mode { get; set; }
    }
}
