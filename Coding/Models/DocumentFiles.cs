﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class DocumentFiles
    {
        public Guid DocumentFileId { get; set; }
        public Guid? DocumentCategoryId { get; set; }
        public Guid? RefId { get; set; }
        public string PathFile { get; set; }
        public string FolderName { get; set; }
        public string TitleName { get; set; }
        public string FileNameOri { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public float? FileSize { get; set; }
        public int? Downloads { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool? Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
