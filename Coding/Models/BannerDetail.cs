﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.DAL.Models
{
    public partial class BannerDetail : BaseModel
    {
        public int BannerDetailID { get; set; }
        public int BannerID { get; set; }
        public string LangCode { get; set; }
        public string BannerName { get; set; }
        public string BannerDescription { get; set; }
        public string BannerLink { get; set; }
        public int? Sequence { get; set; }
       
        [NotMapped]
        public virtual Guid FileId { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
    }
}
