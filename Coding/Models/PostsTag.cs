﻿using System;

namespace Core.DAL.Models
{
    public partial class PostsTag
    {
        public Guid TagId { get; set; }
        public string TagName { get; set; }
    }
}
