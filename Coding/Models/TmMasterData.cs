﻿using Core.library;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class TmMasterData
    {
        public Guid MasterId { get; set; }
        public Guid MasterTypeId { get; set; }
        public Guid? MasterRootId { get; set; }
        public Guid? MasterParentId { get; set; }
        public string MasterName { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public int? Level { get; set; }
        public string MasterCode { get; set; }
        public bool? Compulsory { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
