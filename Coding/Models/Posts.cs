﻿using Microsoft.AspNetCore.Mvc;
using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Posts
    {
        public Guid PostId { get; set; }
        public Guid MenuId { get; set; }
        public string PostRelatedId { get; set; }
        public string TopicName { get; set; }
        public string ShortDescriptions { get; set; }

        public string Descriptions { get; set; }
        public string UrlLink { get; set; }
        public string Keywords { get; set; }
        public DateTime? DateInput { get; set; }
        [NotMapped]
        public virtual string DateInput_str
        {
            get { return (DateInput != null) ? DateInput.ToDate() : TempDateInput; }
            set { TempDateInput = value; }
        }
        [NotMapped]
        private string TempDateInput
        {
            get; set;
        }

        public DateTime? EffectiveDate { get; set; }
        [NotMapped]
        public virtual string EffectiveDate_str
        {
            get { return (EffectiveDate != null) ? EffectiveDate.ToDate() : TempEffectiveDate; }
            set { TempEffectiveDate = value; }
        }
        [NotMapped]
        private string TempEffectiveDate
        {
            get; set;
        }
        public DateTime? EndDate { get; set; }
        [NotMapped]
        public virtual string EndDate_str
        {
            get { return (EndDate != null) ? EndDate.ToDate() : TempEndDate; }
            set { TempEndDate = value; }
        }
        [NotMapped]
        private string TempEndDate
        {
            get; set;
        }
        public int? TypeSort { get; set; }
        public bool? StyleSort { get; set; }
        public bool? OpenComment { get; set; }
        public bool? OpenRating { get; set; }
        public bool MemberOnly { get; set; }
        public bool? Recommended { get; set; }
        public int? Views { get; set; }
        public int? Vote { get; set; }
        public double? Rating { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        [NotMapped]
        public virtual string DateInputShort_Str
        {
            get { return string.Format("{0:dd MMM yyyy}", DateInput); }
        }
        [NotMapped]
        public virtual string EffectiveDateShort_Str
        {
            get { return string.Format("{0:dd MMM yyyy}", EffectiveDate); }
        }
        [NotMapped]
        public virtual string EndDateShort_Str
        {
            get { return string.Format("{0:dd MMM yyyy}", EndDate); }
        }
        [NotMapped]
        public virtual Guid FileId { get; set; }
        [NotMapped]
        public virtual string FileName { get; set; }
        [NotMapped]
        public virtual string FilePath { get; set; }
        [NotMapped]
        public virtual string OriginalFileName { get; set; }
        [NotMapped]
        public virtual List<DocumentFiles> Documentlst { get; set; }
        [NotMapped]
        public virtual List<FileStreamResult> Gallerylst { get; set; }
        [NotMapped]
        public List<Posts> News { get; set; } = new List<Posts>();
        [NotMapped]
        public List<Guid> RelatedId { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }
}
