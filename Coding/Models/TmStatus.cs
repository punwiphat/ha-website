﻿using System;

namespace Core.DAL.Models
{
    public partial class TmStatus
    {
        public Guid StatusId { get; set; }
        public int? StatusTypeId { get; set; }
        public string StatusName { get; set; }
        public string classStatus { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; } 
        public int Mode { get; set; } 
    }
}
