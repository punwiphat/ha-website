﻿using Core.library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DAL.Models
{
    public partial class Events
    {
        public Guid EventId { get; set; }
        public string TopicName { get; set; }
        public string Descriptions { get; set; }
        public string LocationName { get; set; }
        public string EventColor { get; set; }
        public string UrlLink { get; set; }
        public DateTime? StartDate { get; set; }
        //[NotMapped]
        //public virtual string StartDate_Str
        //{
        //    get { return string.Format("{0:dd/MM/yyyy}", StartDate); }
        //}
        //public DateTime? DateInput { get; set; }

        [NotMapped]
        public virtual string StartDate_Str
        {
            get { return (StartDate != null) ? StartDate.ToDate() : TempStartDateInput; }
            set { TempStartDateInput = value; }
        }
        [NotMapped]
        private string TempStartDateInput
        {
            get; set;
        }

        public virtual string StartDateTime
        {
            get { return ((DateTime)StartDate).ToString("yyyy-MM-dd") + "T" + s_time; }
        }
        public virtual DateTime? EndDate { get; set; }
        [NotMapped]
        public virtual string EndDate_Str
        {
            get { return (EndDate != null) ? EndDate.ToDate() : TempEndDateInput; }
            set { TempEndDateInput = value; }
        }
        [NotMapped]
        private string TempEndDateInput
        {
            get; set;
        }
        //[NotMapped]
        //public virtual string EndDate_Str
        //{
        //    get { return string.Format("{0:dd/MM/yyyy}", EndDate); }
        //}

        public virtual string EndDateTime
        {
            get { return ((DateTime)EndDate).ToString("yyyy-MM-dd") + "T" + s_time; }
        }
        [NotMapped]
        public virtual string s_hour { get; set; }
        [NotMapped]
        public virtual string s_min { get; set; }
        [NotMapped]
        public virtual string s_time
        {
            get { return !string.IsNullOrEmpty(s_hour) && !string.IsNullOrEmpty(s_min) ? s_hour.Trim() + ":" + s_min.Trim() : ""; }
        }
        [NotMapped]
        public virtual string e_hour { get; set; }
        [NotMapped]
        public virtual string e_min { get; set; }
        [NotMapped]
        public virtual string e_time
        {
            get { return !string.IsNullOrEmpty(e_hour) && !string.IsNullOrEmpty(e_min) ? e_hour.Trim() + ":" + e_min.Trim() : ""; }
        }
        public int? Sequence { get; set; }
        public bool Activated { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        [NotMapped]
        public virtual string CreateDate_Str
        {
            get { return CreateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(CreateDate)) : ""; }
        }
        [NotMapped]
        public virtual string UpdateDate_Str
        {
            get { return UpdateDate != null ? Formatter.TimeAgo(Convert.ToDateTime(UpdateDate)) : ""; }
        }
    }

    public class CalendarEventViewModel
    {

        public int id { get; set; }
        public Int64 ROW_NUMBERs { get; set; }

        public string name { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string place { get; set; }
        public string description { get; set; }
        public string color { get; set; }
        public bool allDay { get; set; }
        public string call { get; set; }
        public int IsContact { get; set; }
        public string IsContactStr { get; set; }


    }
    public class EventViewModel
    {
        public string YearMonth { get; set; }
    }
}
