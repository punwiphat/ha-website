using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Core.DAL.Models
{

    public class Tm_Module : BaseModel
    {
        [Required(ErrorMessage="*")]
        public virtual string ModuleName { get; set; }
        public virtual string Keygen { get; set; }
    }
}
