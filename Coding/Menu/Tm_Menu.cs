using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Core.DAL.Models
{

    public class Tm_Menu : BaseModel
    {
        public virtual int? MENU_FRONT_ID { get; set; }
        public virtual int? MENU_BACK_ID { get; set; }
        public virtual string controller_back { get; set; }
        public virtual string name  { get; set; }
        public virtual string name_en { get; set; }
        public virtual string permalink_back { get; set; }
        public virtual string permalink_front { get; set; }
        
        public virtual string action_back { get; set; }
        public virtual string link_back { get; set; }
        public virtual bool self_back { get; set; }
        public virtual int sort_back { get; set; }
        public virtual string icon_back { get; set; }
        public virtual string img_back { get; set; }
        public virtual bool chk_login_back { get; set; }
        public virtual bool chk_popup_back { get; set; }


        public virtual string controller_front { get; set; }
        public virtual string action_front { get; set; }
        public virtual string link_front { get; set; }
        public virtual bool self_front { get; set; }
        public virtual int sort_front { get; set; }
        public virtual string icon_front { get; set; }
        public virtual string img_front { get; set; }
        public virtual bool chk_login_front { get; set; }
        public virtual bool chk_popup_front { get; set; }
        public virtual bool show_front { get; set; }
        public virtual bool show_back { get; set; }
        public virtual string name_back { get; set; }
        public virtual string name_front { get; set; }
        public virtual List<Tm_Menu> Sub_Menu { get;set;}
        [Required(ErrorMessage = "*???????????????")]
        public virtual int type_show { get; set; }
        public virtual int levels { get; set; }

        
        public virtual string Lang { get; set; }
        public virtual List<Tm_MenuDetail> lstobj { get; set; }
       

    }
}
