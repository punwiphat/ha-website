﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;

namespace Core.DAL.Models
{
    public static class GetStoredSqlServer 
    {
            public static IDataReader ExecuteReaderPro(string nameProd, SqlParameter[] param)
            {
                using (var Map = new CMSdbContext())
                {
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = nameProd;
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter r in param)
                    {
                        command.Parameters.Add(r);
                    }
                    IDataReader reader = command.ExecuteReader();
                    return reader;
                }
            }
            public static List<T> GetAllStored<T>(string nameProd, SqlParameter[] parameter) where T : new()
            {
                using (var Map = new CMSdbContext())
                {
                    //IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = nameProd;
                    command.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter r in parameter)
                    {
                        command.Parameters.Add(r);
                    }
                    IDataReader reader = command.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    return dt.ToList<T>();

                }
            }
            public static List<T> GetAllStoredNonparam<T>(string nameProd) where T : new()
            {
                //var connection = (SqlConnection)context.Database.AsSqlServer().Connection.DbConnection;
                using (var Map = new CMSdbContext())
                {
                    // IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = nameProd;
                    command.CommandType = CommandType.StoredProcedure;
                    IDataReader reader = command.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    return dt.ToList<T>();
                }
            }
            public static List<T> GetQuery<T>(string SQLcommand) where T : new()
            {
                using (var Map = new CMSdbContext())
                {
                    //IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = SQLcommand;
                    command.CommandType = CommandType.Text;
                    IDataReader reader = command.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    return dt.ToList<T>();
                }
            }
            public static string ExecuteReaderPro_ref(string nameProd, SqlParameter[] parameter, string Returnparameter)
            {
                using (var Map = new CMSdbContext())
                {
                    //IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = nameProd;
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter r in parameter)
                    {
                        command.Parameters.Add(r);
                    }

                    IDbDataParameter _Parameter = command.CreateParameter();
                    _Parameter.ParameterName = Returnparameter;
                    _Parameter.Direction = System.Data.ParameterDirection.Output;
                    _Parameter.DbType = System.Data.DbType.String;
                    _Parameter.Size = 50;
                    command.Parameters.Add(_Parameter);
                    command.ExecuteNonQuery();
                    return _Parameter.Value.ToString();
                }
            }
            public static bool Executenon(string nameProd, SqlParameter[] parameter)
            {
                using (var Map = new CMSdbContext())
                {
                    //IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                    IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                    command.CommandText = nameProd;
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter r in parameter)
                    {
                        command.Parameters.Add(r);
                    }
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            public static void ExecuteCommand(string SqlCommand)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        //IDbCommand command = SessionFactory.GetCurrentSession().Connection.CreateCommand();
                        IDbCommand command = Map.Database.GetDbConnection().CreateCommand();
                        command.CommandText = SqlCommand;
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 0;
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
     
        public static List<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            List<T> result = new List<T>();

            foreach (var row in table.Rows)
            {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
        }
        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(System.DayOfWeek))
                {
                    DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), row[property.Name].ToString());
                    property.SetValue(item, day, null);
                }
                else
                {
                    if (row.Table.Columns.Contains(property.Name))
                    {
                        object value = row[property.Name];
                        if (value == DBNull.Value)
                            value = null;
                        if (value != null)
                        {
                            if (value.GetType() == typeof(System.TimeSpan))
                            {
                                property.SetValue(item, Convert.ToString(value), null);
                            }
                            else
                            {
                                property.SetValue(item, value, null);
                            }
                        }
                        else
                        {
                            property.SetValue(item, value, null);
                        }
                    }
                }
            }
            return item;
        }

    }
}
