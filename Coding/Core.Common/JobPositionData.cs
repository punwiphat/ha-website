﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class JobPositionData
    {
        public class Get
        {
            public static JobPosition MainJobPositionById(int ID)
            {
                JobPosition Obj = new JobPosition();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.JobPosition.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static JobPosition MainJobPositionByMenuId(int MenuID)
            {
                JobPosition Obj = new JobPosition();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.JobPosition.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<JobPositionDetail> ByMenu(int MenuID,string LangCode)
            {
                List<JobPositionDetail> Obj = new List<JobPositionDetail>();
               
                using (var Map = new CMSdbContext())
                {
                    Obj = (from Main in Map.JobPosition
                           join JobPositions in Map.JobPositionDetail on Main.ID equals JobPositions.JobPositionID
                           where Main.Deleted != true && JobPositions.LangCode == LangCode
                           && Main.MenuID == MenuID
                           select new
                           {
                                JobPositions.JobPositionID
                               ,JobPositions.JobPositionDetailID
                               ,JobPositions.JobPositionLink
                               ,JobPositions.JobPositionName
                               ,JobPositions.JobPositionDescription
                               ,Sequence = Main.Sequence
                               ,UpdatedDate = Main.UpdatedDate
                               ,UpdatedBy = Main.UpdatedBy
                               ,Actived = Main.Actived
                               //,FileID =    Media != null ? Media.Media_File_Id : 0
                               //,FileName =  Media != null ? Media.FileName      : ""
                               //,FilePath =  Media != null ? Media.PathFile      : ""
                               //,OriginalFileName  =  Media != null ? Media.FileNameOri : ""          
                            }).ToList_list<JobPositionDetail>();


                }
               

                return !Obj.Any()? Obj.OrderByDescending(o => o.UpdatedDate).ToList(): Obj;
            }
            public static List<JobPositionDetail> JobPositionById(int ID,string LangCode = null)
            {
                try
                {
                    List<JobPositionDetail> Obj = new List<JobPositionDetail>();
                    //if (Id != 0)
                    //{

                    //    using (var Map = new CMSdbContext())
                    //    {
                    //        Obj = (from Lang in Map.Languages
                    //               join JobPositions in Map.JobPositionDetail on Lang.LanguageCode equals JobPositions.LangCode into _JobPositions
                    //               from JobPositions in _JobPositions.DefaultIfEmpty()
                    //               join Job in Map.JobPosition on JobPositions.JobPositionID equals Job.ID
                    //               join Media in Map.MediaFiles on
                    //               new
                    //               {
                    //                   Key1 = JobPositions.JobPositionDetailID.ToString()
                    //                   ,
                    //                   Key2 = Job.MenuID
                    //               } equals
                    //               new
                    //               {
                    //                   Key1 = Media.RefID
                    //                   ,
                    //                   Key2 = Media.MenuID

                    //               }
                    //               into _it
                    //               from Media in _it.DefaultIfEmpty()
                    //               where Media.Deleted != true
                    //                && Media.Actived == true
                    //               && JobPositions.JobPositionID == Id
                    //               && Lang.Actived == true
                    //               select new
                    //               {
                    //                   LangCode = Lang.LanguageCode
                    //                   ,
                    //                   JobPositions.JobPositionID
                    //                   ,
                    //                   JobPositions.JobPositionDetailID
                    //                   ,
                    //                   JobPositions.JobPositionLink
                    //                   ,
                    //                   JobPositions.JobPositionName
                    //                   ,
                    //                   JobPositions.JobPositionDescription
                    //                   ,
                    //                   JobPositions.Sequence
                    //                   ,
                    //                   FileID = Media != null ? Media.MediaFileID : 0
                    //                   ,
                    //                   FileName = Media != null ? Media.FileName : string.Empty
                    //                   ,
                    //                   FilePath = Media != null ? Media.PathFile : string.Empty
                    //                   ,
                    //                   OriginalFileName = Media != null ? Media.FileNameOri : string.Empty
                    //               }).ToList_list<JobPositionDetail>();



                    //    }
                    //}
                    //else
                    //{
                    //    var lang = LanguageData.GetByActive();
                    //    if (!string.IsNullOrEmpty(LangCode))
                    //        lang = lang.Where(o => o.LanguageCode == LangCode).ToList();
                    //    foreach (var l in lang)
                    //    {
                    //        JobPositionDetail o = new JobPositionDetail();
                    //        o.LangCode = l.LanguageCode;
                    //        Obj.Add(o);
                    //    }
                    //}
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_JobID", ID);
                    Obj = GetStoredMySql.GetAllStored<JobPositionDetail>("B_JOB_BY_MAINID", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            JobPositionDetail o = new JobPositionDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    foreach (var item in Obj)
                    {
                        item.lstFiles = MediaFilesData.Get.ByCategoryMenu(4, item.MenuID).ToList();
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<JobPosition> JobPositionByActive(int MenuID)
            {
                List<JobPosition> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.JobPosition.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<JobPosition> SearchListByMenu(string searchString, int MenuID)
            {
                IQueryable<JobPositionDetail> Obj;
                CMSdbContext context = new CMSdbContext();
                List<JobPosition> JobPosition = new List<JobPosition>();
                JobPosition = context.JobPosition.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                return JobPosition;
            }
            public static List<JobPosition> GetJobPositionList()
            {
                CMSdbContext context = new CMSdbContext();
                var objResult = context.JobPosition.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;
            }
            public static List<JobPositionDetail> FListPosition(int MenuID,string LangCode)
            {
                List<JobPositionDetail> Lists = new List<JobPositionDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Lists = GetStoredMySql.GetAllStored<JobPositionDetail>("F_JOBPOSITION", param).ToList();
                return Lists;
            }
        }
        public class Post
        {
            public static JobPosition AddOrUpdate(JobPosition Obj,string Username)
            {
                bool mode = false;
                JobPosition saveObj = new JobPosition();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.JobPosition.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        saveObj.MenuID = Obj.MenuID;
                        var lmax = Map.JobPosition.Where(o=>o.MenuID == Obj.MenuID);
                        var max = lmax.Count()>0? lmax.Max(o => o.Sequence)+1:1;
                        saveObj.Sequence = max > 0 ? max : 1;
                    }
                    
                    saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                    saveObj.EndDate = Obj.EndDate_str.todateEn();
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    saveObj.GUID = Obj.GUID;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<JobPositionDetail> DetailAddOrUpdate(List<JobPositionDetail> item, string Username)
            {

                try
                {
                    bool mode = false;
                    List<JobPositionDetail> saveLst = new List<JobPositionDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            JobPositionDetail saveObj = new JobPositionDetail();
                            if (Obj.JobPositionDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.JobPositionDetail.Find(Obj.JobPositionDetailID);
                            }
                            //else
                            //{
                            //    saveObj.Deleted = false;
                            //    saveObj.CreatedDate = DateTime.Now;
                            //    saveObj.CreatedBy = Obj.CreatedBy;
                            //}
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.JobPositionID = Obj.JobPositionID;
                            }
                            saveObj.JobPositionName = Obj.JobPositionName;
                            saveObj.JobPositionLink = Obj.JobPositionLink;
                            saveObj.JobPositionDescription = Obj.JobPositionDescription;
                            saveObj.SUB_GUID = Obj.SUB_GUID;
                            //saveObj.UpdatedDate = DateTime.Now;
                            //saveObj.UpdatedBy = Username;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.JobPosition.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.JobPosition.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.JobPosition.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
