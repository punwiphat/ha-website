using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Common;
using Core.DAL.Models;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;

public static class ProjectsData
{
	public class Get
	{
		public static Projects MainProjectsById(int ID)
		{
            Projects Obj = new Projects();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Projects.Find(ID);
            }
            return Obj;
		}

		public static Projects MainProjectsList()
		{
            Projects Obj = new Projects();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Projects.Where(o=>o.Deleted!=true).FirstOrDefault();
            }
            return Obj;
            
		}
        public static List<ProjectsDetail> ByMenuID(int MenuID, string LangCode)
        {
            List<ProjectsDetail> Obj = new List<ProjectsDetail>();
            MySqlParameter[] param = new MySqlParameter[2];
            param[0] = new MySqlParameter("p_MenuID", MenuID);
            param[1] = new MySqlParameter("p_LangCode", LangCode);
            Obj = GetStoredMySql.GetAllStored<ProjectsDetail>("B_PROJECT_BY_MENUID", param).ToList();
            return Obj;
        }

        public static List<ProjectsDetail> ProjectsById(int ID, string LangCode = null)
        {
            List<ProjectsDetail> Obj = new List<ProjectsDetail>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_ProjectID", ID);
            Obj = GetStoredMySql.GetAllStored<ProjectsDetail>("B_PROJECT_BY_ID", param).ToList();
            if (Obj.Count == 0)
            {
                foreach (Languages l in LanguageData.GetByActive())
                {
                    ProjectsDetail o = new ProjectsDetail();
                    o.LangCode = l.LanguageCode;
                    Obj.Add(o);
                }
            }
			if (!string.IsNullOrEmpty(LangCode))
				{
					Obj = Obj.Where((ProjectsDetail o) => o.LangCode == LangCode).ToList();
				}
            foreach (ProjectsDetail it in Obj)
            {
                it.lstFilesAttach = MediaFilesData.Get.FListMedia(it.MenuID, 4, it.ProjectDetailID);
            }
            return Obj;
        }
       

		public static List<Projects> ProjectsByActive(int MenuID)
		{
            List<Projects> Obj = new List<Projects>();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Projects.Where(o=>o.Deleted != true && o.Actived == true).OrderByDescending(o=>o.Sequence).ToList();
            }
            return Obj;
        
		}

		public static List<Projects> SearchListByMenu(string searchString, int MenuID)
		{
		
            List<Projects> Obj = new List<Projects>();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Projects.Where(o => o.Deleted != true).ToList();
            }
            return Obj;
        }

		public static List<Projects> GetProjectsList()
		{
            List<Projects> Obj = new List<Projects>();
            using (var Map = new CMSdbContext())
            {
                Obj = Map.Projects.Where(o => o.Deleted != true ).OrderByDescending(o => o.UpdatedDate).ToList();
            }
            return Obj;
        }

		public static List<ProjectsDetail> FListProjects(int MenuID, string LangID, string searchString)
		{
            List<ProjectsDetail> Obj = new List<ProjectsDetail>();
            MySqlParameter[] param = new MySqlParameter[3];
            param[0] = new MySqlParameter("p_LangCode", LangID);
            param[1] = new MySqlParameter("p_searchString", searchString);
            param[2] = new MySqlParameter("p_MenuID", MenuID);
            Obj = GetStoredMySql.GetAllStored<ProjectsDetail>("F_PROJECT", param).ToList();
           
			if (Obj.Count() > 0)
			{
                foreach (ProjectsDetail it in Obj)
                {
                    it.lstFilesAttach = MediaFilesData.Get.FListMedia(MenuID, 4, it.ProjectDetailID);
                }
            }
			return Obj;
		}
	}

	public class Post
	{
		public static Projects AddOrUpdate(Projects Obj, string Username, string IPaddress, string Browser)
		{
			bool mode = false;
			Projects saveObj = new Projects();
			CMSdbContext Map = new CMSdbContext();
			try
			{
				if (Obj.ID != 0)
				{
					mode = true;
					saveObj = Map.Projects.Find(new object[1] { Obj.ID });
				}
				else
				{
					saveObj.MenuID = Obj.MenuID;
					saveObj.Deleted = false;
					saveObj.CreatedDate = DateTime.Now;
					saveObj.CreatedBy = Username;
					List<Projects> lmax = ((IQueryable<Projects>)Map.Projects).Where((Projects o) => o.MenuID == Obj.MenuID).ToList();
					int max = ((lmax.Count() <= 0) ? 1 : (lmax.Max((Projects o) => o.Sequence) + 1));
					saveObj.Sequence = ((max <= 0) ? 1 : max);
				}
				saveObj.CategoryID = Obj.CategoryID;
				saveObj.UpdatedDate = DateTime.Now;
				saveObj.UpdatedBy = Username;
				saveObj.Actived = Obj.Actived;
				saveObj.Menuname = Obj.Menuname;
				if (mode)
				{
					((DbContext)Map).Update<Projects>(saveObj);
				}
				else
				{
					((DbContext)Map).Add<Projects>(saveObj);
				}
				((DbContext)Map).SaveChanges();
				return saveObj;
			}
			finally
			{
				((IDisposable)Map)?.Dispose();
			}
		}

		public static List<ProjectsDetail> DetailAddOrUpdate(List<ProjectsDetail> item, string Username, string IPaddress, string Browser, int size = 14)
		{
			try
			{
				bool mode = false;
				List<ProjectsDetail> saveLst = new List<ProjectsDetail>();
				CMSdbContext Map = new CMSdbContext();
				try
				{
					foreach (ProjectsDetail Obj in item)
					{
						ProjectsDetail saveObj = new ProjectsDetail();
						if (Obj.ProjectDetailID != 0)
						{
							mode = true;
							saveObj = Map.ProjectsDetail.Find(new object[1] { Obj.ProjectDetailID });
						}
						else
						{
							saveObj.LangCode = Obj.LangCode;
							saveObj.ProjectID = Obj.ProjectID;
						}
						saveObj.ProjectName = Obj.ProjectName;
						saveObj.Descriptions = Obj.Descriptions;
						saveObj.Department = Obj.Department;
						saveObj.Hours = Obj.Hours;
						saveObj.Contacts = Obj.Contacts;
						saveObj.Others = Obj.Contacts;
						saveObj.MenuID = Obj.MenuID;
						saveObj.lstFilesAttach = Obj.lstFilesAttach;
						saveObj.SUB_GUID = Obj.SUB_GUID;
						if (mode)
						{
							((DbContext)Map).Update<ProjectsDetail>(saveObj);
						}
						else
						{
							((DbContext)Map).Add<ProjectsDetail>(saveObj);
						}
						saveObj.IsClickDelImg = Obj.IsClickDelImg;
						saveLst.Add(saveObj);
					}
					((DbContext)Map).SaveChanges();
				}
				finally
				{
					((IDisposable)Map)?.Dispose();
				}
				return saveLst;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson Res = new ResponseJson();
			try
			{
				CMSdbContext Map = new CMSdbContext();
				try
				{
					Projects Obj = Map.Projects.Find(new object[1] { ID });
					if (Obj == null)
					{
						Res.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						Obj.Sequence = Seq;
						((DbContext)Map).Update<Projects>(Obj);
						((DbContext)Map).SaveChanges();
						Res.Result = true;
					}
				}
				finally
				{
					((IDisposable)Map)?.Dispose();
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Sequence, Menuname, Username, Res.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				Res.Result = false;
				Res.MessageError = ex.Message;
			}
			return Res;
		}

		public static ResponseJson UpdateStatus(int ID, bool Active, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson Res = new ResponseJson();
			try
			{
				CMSdbContext Map = new CMSdbContext();
				try
				{
					Projects Obj = Map.Projects.Find(new object[1] { ID });
					if (Obj == null)
					{
						Res.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						Obj.Actived = Active;
						((DbContext)Map).Update<Projects>(Obj);
						((DbContext)Map).SaveChanges();
						Res.Result = true;
					}
				}
				finally
				{
					((IDisposable)Map)?.Dispose();
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.ChangeStatus, Menuname, Username, Res.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				Res.Result = false;
				Res.MessageError = ex.Message;
			}
			return Res;
		}

		public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson Res = new ResponseJson();
			try
			{
				CMSdbContext Map = new CMSdbContext();
				try
				{
					Projects Obj = Map.Projects.Find(new object[1] { ID });
					if (Obj == null)
					{
						Res.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						Obj.Deleted = true;
						Obj.UpdatedBy = Username;
						Obj.UpdatedDate = DateTime.Now;
						((DbContext)Map).Update<Projects>(Obj);
						((DbContext)Map).SaveChanges();
						Res.Result = true;
					}
				}
				finally
				{
					((IDisposable)Map)?.Dispose();
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				Res.MessageError = ex.Message;
			}
			return Res;
		}
	}
}
