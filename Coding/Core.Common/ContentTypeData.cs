﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
namespace Core.Common
{
    public static class ContentTypeData
    {


        public static IQueryable<ContentType> SearchList(string searchString)
        {
            var context = new CMSdbContext();
            if (!string.IsNullOrEmpty(searchString))
                return context.ContentType.Where(s => s.ContentTypeName.Contains(searchString) && s.Deleted == false).OrderBy(o => o.UpdateDate);
            else
                return context.ContentType.Where(s => s.Deleted == false).OrderByDescending(o => o.UpdateDate);

        }

        public static bool UpdateActiveStatus(Guid contentTypeId, string updateBy)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.ContentType.Find(contentTypeId);

                Obj.Activated = !Obj.Activated;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                context.Update(Obj);
                context.SaveChanges();
                return true;
            }

        }

        public static ContentType AddOrUpdate(ContentType update)
        {

            ContentType saveObj = new ContentType();
            using (var context = new CMSdbContext())
            {
                Boolean mode = false;
                if (update.ContentTypeId.ToString() != "00000000-0000-0000-0000-000000000000")
                {
                    mode = true;
                    saveObj = context.ContentType.Find(update.ContentTypeId);
                }
                else
                {

                    saveObj.Deleted = false;
                    saveObj.CreateDate = DateTime.Now;
                    saveObj.CreateBy = update.CreateBy;
                }
                saveObj.ContentTypeName = update.ContentTypeName;

                saveObj.Activated = update.Activated;
                saveObj.UpdateDate = DateTime.Now;
                saveObj.UpdateBy = update.CreateBy;
                if (mode)
                {
                    context.Update(saveObj);
                }
                else
                {
                    context.Add(saveObj);
                }
                context.SaveChanges();
            }
            return saveObj;
        }

        public static ContentType GetContentTypeById(Guid Id)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.ContentType.Find(Id);


                return Obj;
            }


        }

        public static bool DeleteContentType(Guid Id, string updateBy)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.ContentType.Find(Id);
                Obj.Deleted = true;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                context.Update(Obj);
                context.SaveChanges();
                return true;
            }
        }
        public static List<ContentType> GetContentTypeByActive()
        {

            using (var context = new CMSdbContext())
            {
                var Obj = context.ContentType.Where(o => o.Deleted == false && o.Activated == true).OrderByDescending(o => o.ContentTypeName).ToList();


                return Obj;
            }


        }
        public static List<ContentType> GetContentTypeList()
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.ContentType.Where(o => o.Deleted == false && o.Activated == true).OrderByDescending(o => o.ContentTypeName).ToList();


                return Obj;
            }

        }


    }
}
