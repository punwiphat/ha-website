﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public static class StaffData
    {
        public class Get
        {
            public static Staff MainStaffById(int ID)
            {
                Staff Obj = new Staff();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Staff.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static Staff MainStaffList()
            {
                Staff Obj = new Staff();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Staff.Where(o => o.Deleted != true).FirstOrDefault();
                }
                return Obj;
            }
            public static List<StaffDetail> ByMenuID(int MenuID,string LangCode)
            {
                List<StaffDetail> Obj = new List<StaffDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<StaffDetail>("B_STAFF_BY_MENUID", param).ToList();
                return Obj;
            }
            public static List<StaffDetail> StaffById(int ID,string LangCode = null)
            {
                try
                {
                    List<StaffDetail> Obj = new List<StaffDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_StaffID", ID);
                    Obj = GetStoredMySql.GetAllStored<StaffDetail>("B_STAFF_BY_ID", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            StaffDetail o = new StaffDetail();
                            o.LangCode = l.LanguageCode;
                            
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var it in Obj)
                        {
                            it.lstFilesAttach = MediaFilesData.Get.FListMedia(it.MenuID, 4, it.StaffDetailID);
                        }
                    }
                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<Staff> StaffByActive(int MenuID)
            {
                List<Staff> Obj;
                CMSdbContext context = new CMSdbContext();
                Obj = context.Staff.Where(s => s.Deleted != true && s.Actived == true).OrderByDescending(o => o.Sequence).ToList();
                return Obj;
            }
            public static List<Staff> SearchListByMenu(string searchString, int MenuID)
            {
                IQueryable<StaffDetail> Obj;
                CMSdbContext context = new CMSdbContext();
                List<Staff> Staff = new List<Staff>();
                Staff = context.Staff.Where(o => o.Deleted == false).ToList();
                return Staff;
            }
            public static List<Staff> GetStaffList()
            {

                CMSdbContext context = new CMSdbContext();
                var objResult = context.Staff.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;

            }
            public static List<StaffDetail> FListStaff(int MenuID,string LangID,string searchString)
            {
                List<StaffDetail> Obj = new List<StaffDetail>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_LangCode", LangID);
                param[1] = new MySqlParameter("p_searchString", searchString);
                param[2] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<StaffDetail>("F_STAFF", param).ToList();
                return Obj;
            }
        }
        public class Post
        {
            public static Staff AddOrUpdate(Staff Obj,string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                Staff saveObj = new Staff();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.Staff.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.MenuID = Obj.MenuID;
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        var lmax = Map.Staff.ToList();
                        var max = lmax.Count()>0? lmax.Max(o => o.Sequence)+1:1;
                        saveObj.Sequence = max > 0 ? max : 1;
                    }
                    saveObj.Levels = Obj.Levels??1;
                    saveObj.CategoryID = Obj.CategoryID;
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<StaffDetail> DetailAddOrUpdate(List<StaffDetail> item, string Username, string IPaddress, string Browser,int size = 5)
            {

                try
                {
                    bool mode = false;
                    List<StaffDetail> saveLst = new List<StaffDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            StaffDetail saveObj = new StaffDetail();
                            if (Obj.StaffDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.StaffDetail.Find(Obj.StaffDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.StaffID = Obj.StaffID;
                            }
                            saveObj.Fullname = Obj.Fullname;
                            saveObj.Telephone = Obj.Telephone;
                            saveObj.Position = Obj.Position;
                            saveObj.Email = Obj.Email;
                            saveObj.Files = Obj.Files;
                            saveObj.Files2 = Obj.Files2;
                            saveObj.MenuID = Obj.MenuID;
                            saveObj.lstFilesAttach = Obj.lstFilesAttach;
                            saveObj.SUB_GUID = Obj.SUB_GUID;

                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveObj.IsClickDelImg = Obj.IsClickDelImg;
                            saveObj.IsClickDelImg2 = Obj.IsClickDelImg2;
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                        foreach (var itemsave in saveLst)
                        {
                            if (itemsave.Files != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.StaffDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.StaffDetailID, 2, size, Username);
                            }
                            else
                            {
                                if(itemsave.IsClickDelImg == 1) {
                                    var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.StaffDetailID.ToString(), Username, IPaddress, Browser);
                                }
                            }
                            if (itemsave.Files2 != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 5, itemsave.StaffDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.Files2, itemsave.MenuID, itemsave.StaffDetailID, 5, size, Username);
                            }
                            else
                            {
                                if (itemsave.IsClickDelImg2 == 1)
                                {
                                    var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 5, itemsave.StaffDetailID.ToString(), Username, IPaddress, Browser);
                                }
                            }
                        }
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Staff.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Staff.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Staff.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
