﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common
{
    public static class TextRegister
    {
        public class TH
        {
            public const string TXT_ID_CARD = "เลขที่บัตรประชาชน";

            public const string TXT_TITLE = "คำนำหน้าชื่อ";

            public const string TXT_FIRST_NAME = "ชื่อ";

            public const string TXT_LAST_NAME = "นามสกุล";

            public const string TXT_MID_NAME = "ชื่อกลาง";

            public const string TXT_MEMBER_TYPE = "ประเภทสมาชิก";

            public const string TXT_EMAIL = "อีเมล";

            public const string TXT_DEPARTMENT = "หน่วยงานต้นสังกัด";

            public const string TXT_POSITION = "ตำแหน่ง";

            public const string TXT_ADDRESS = "ที่อยู่";

            public const string TXT_DOCUMENT_DELIVERY = "การจัดส่งเอกสาร";

            public const string TXT_REGISTER_SUCCESS = "ท่านสมัครสมาชิกเรียบร้อยแล้ว กรุณายืนยันตนผ่านอีเมลที่ท่านลงทะเบียนไว้";

            public const string TXT_PERSONALANDDEPART = "บุคคลและหน่วยงาน";

            public const string TXT_ATCONTACT = "ที่ต้องการติดต่อ";

            public const string TXT_CHANNELCOMMENT = "ช่องทางการรับฟังความคิดเห็น";

            public const string TXT_HEARING = "การรับฟังความคิดเห็น";

            public const string TXT_SUBMIT = "ส่งข้อความ";

        }

        public class EN
        {
            public const string TXT_ID_CARD = "Id card number";

            public const string TXT_TITLE = "Title";

            public const string TXT_FIRST_NAME = "First name";

            public const string TXT_LAST_NAME = "Last name";

            public const string TXT_MID_NAME = "Middle name";

            public const string TXT_MEMBER_TYPE = "Member type";

            public const string TXT_EMAIL = "Email";

            public const string TXT_DEPARTMENT = "Department";

            public const string TXT_POSITION = "Position";

            public const string TXT_ADDRESS = "Address";

            public const string TXT_DOCUMENT_DELIVERY = "Document delivery";

            public const string TXT_REGISTER_SUCCESS = "You have successfully subscribed. Please verify your identity through your registered email address.";

            public const string TXT_PERSONALANDDEPART = "Personal and Department";

            public const string TXT_ATCONTACT = "for contact";

            public const string TXT_CHANNELCOMMENT = "Channels for hearing opinions";

            public const string TXT_HEARING = "Hearing opinions";

            public const string TXT_SUBMIT = "Send Message";
        }
    }
}
