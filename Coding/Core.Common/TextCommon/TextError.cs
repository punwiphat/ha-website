﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common
{
    public static class TextError
    {
        public class TH
        {
            public const string TXT_PID_REGISTED = "เลขที่บัตรประชาชนของท่านเคยลงทะเบียนแล้ว!";
            public const string TXT_PASSWORD_MISMATCH = "Password และ Confirm Password ไม่ตรงกัน!";
            public const string TXT_HAS_BEEN_USED = " นี้ ถูกใช้งานแล้ว!";
            public const string TXT_AN_ERROR_OCCURRED = "พบข้อผิดพลาด กรุณาลองใหม่!";
            public const string TXT_AN_ERROR_OCCURRED_EMAIL = "เกิดข้อผิดพลาด กรุณาตรวจสอบอีเมล!";
            public const string TXT_NO_INT_AUTHEN = "ไม่พบข้อมูลสำหรับการยืนยันตัวตน กรุณาลองใหม่อีกครั้ง!";
            public const string TXT_SUCCESS_VERIFIED = "ท่านยืนยันตัวตนเสร็จเรียบร้อย กรุณาตรวจสอบข้อมูลผู้ใช้งานที่อีเมลที่ท่านลงทะเบียนไว้!";
            public const string TXT_AN_ERROR_OCCURRED_SEND_EMAIL = "เกิดข้อผิดพลาด การส่งอีเมลการยืนยันตัวตนไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!";
            public const string TXT_AN_ERROR_OCCURRED_AUTHEN = "เกิดข้อผิดพลาด การยืนยันตัวตนไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!";
            public const string TXT_VERIFIED = "ท่านเคยยืนยันตัวตนการสมัครสมาชิกแล้ว!";
            public const string TXT_MENU_NOT_FOUND = "ไม่พบเมนู!";
        }

        public class EN
        {
            public const string TXT_PID_REGISTED = "Citizen Id is registed!";
            public const string TXT_PASSWORD_MISMATCH = "Password and Confirm Password mismatch!";
            public const string TXT_HAS_BEEN_USED = " has been used!";
            public const string TXT_AN_ERROR_OCCURRED = "An error occurred Please try again!";
            public const string TXT_AN_ERROR_OCCURRED_EMAIL = "An error occurred Please check your email!";
            public const string TXT_NO_INT_AUTHEN = "No information found for authentication. Please try again!";
            public const string TXT_SUCCESS_VERIFIED = "You have successfully verified your identity. Please check the user information at your registered email address!";
            public const string TXT_AN_ERROR_OCCURRED_SEND_EMAIL = "An error occurred Failed to send verification email Please try again!";
            public const string TXT_AN_ERROR_OCCURRED_AUTHEN = "An error occurred Authentication failed. Please try again!";
            public const string TXT_VERIFIED = "You have already verified your membership identity!";
            public const string TXT_MENU_NOT_FOUND = "Menu not found!";
        }
    }
}
