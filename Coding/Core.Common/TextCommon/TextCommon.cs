﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common
{
    public static class TextCommon
    {
        public class TH
        {
            public const string TXT_HA = "บันทึกปริญญาวิทยาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ";

            public const string TXT_SEARCH = "ค้นหา";

            public const string TXT_DATA = "ข้อมูล";

            public const string TXT_FORM = "แบบฟอร์ม";

            public const string TXT_REGISTER = "สมัครสมาชิก";

            public const string TXT_LOGIN = "เข้าสู่ระบบ";

            public const string TXT_LOGOUT = "ออกจากระบบ";

            public const string TXT_REQUIRED_FIELD = "กรุณาระบุ";

            public const string TXT_SELECT_DATA = "เลือก";

            public const string TXT_IF_ANY = "ถ้ามี";

            public const string TXT_SHORTCUT_MENU = "เมนูทางลัด";

            public const string TXT_DISPLAY = "การแสดงผล";

            public const string TXT_TEL = "โทรศัพท์";

            public const string TXT_FAX = "โทรสาร";

            public const string TXT_EMAIL = "อีเมล";

            public const string TXT_EMAIL_SARABUN = "งานรับ-ส่งหนังสือ";

            public const string TXT_EMAIL_SARABUN2 = "งานสารบรรณ";

            public const string TXT_CONTACT_US_SRP = "ติดต่อสถาบัน สรพ.";

            public const string TXT_CONTACT_AGENCY = "ติดต่อหน่วยงาน";

            public const string TXT_WEB_LINK = "เว็บไซต์ที่เกี่ยวข้อง";

            public const string TXT_SITEMAP = "แผนผังเว็บไซต์";

            public const string TXT_ABOUT_US_SRP = "เกี่ยวกับ สรพ.";

            public const string TXT_CALEDAR = "ปฏิทินกิจกรรม";

            public const string TXT_ATTACH = "เอกสารแนบ";

            public const string TXT_DOWNLOAD = "ดาวน์โหลด";

            public const string TXT_SERVICEOUR = "บริการของเราทั้งหมด";

            public const string TXT_PREV = "ก่อนหน้า";

            public const string TXT_NEXT = "ถัดไป";

            public const string TXT_ADDRESS = "ที่อยู่";

            public const string TXT_MAP = "แผนที่ Google";

            public const string TXT_SENDCOMPLAINT = "ส่งเรื่องร้องเรียน";

            public const string TXT_TYPECOMPLAINT = "ประเภทเรื่องร้องเรียน";

            public const string TXT_FLNAME = "ชื่อ-นามสกุล";

            public const string TXT_DEPARTSTAFF = "แผนก (สำหรับเจ้าหน้าที่)";

            public const string TXT_DEPARTMENT = "สำนักงาน/แผนก";

            public const string TXT_SUBJECT = "หัวเรื่อง";

            public const string TXT_DESCRIPTION = "รายละเอียด";

            public const string TXT_PROVINCE = "จังหวัด";

            public const string TXT_ALLOW = "อนุญาตเฉพาะ";

            public const string TXT_ONLY = "เท่านั้น";

            public const string TXT_MAPONLY = "แผนที่เท่านั้น";

            public const string TXT_HOME = "หน้าหลัก";

            public const string TXT_RESULT_SEARCH = "ผลการค้นหา";

            public const string TXT_HOSPITAL_TYPE = "ประเภทโรงพยาบาล";

            public const string TXT_HOSPITAL_KIND = "ชนิดโรงพยาบาล";

            public const string TXT_LATLONG = "ละติจูด ลองจิจูด";

            public const string TXT_WEBSITE = "เว็บไซต์";

            public const string TXT_CER = "การประเมินและการรับรอง";

            public const string TXT_LEVEL = "ระดับ<br/>การรับรอง";

            public const string TXT_CER_DATE = " วันรับรอง";

            public const string TXT_CER_EXPIRY = "วันหมดอายุ";

            public const string TXT_ALL_HISTORY = "แสดงประวัติการรับรองทั้งหมด";

            public const string TXT_DATA_HISTORY = "ประวัติการรับรอง";

            public const string TXT_ROOTDEPARTMENT = "ต้นสังกัด";

            public const string TXT_POSITION = "ตำแหน่ง";

            public const string TXT_NEWS = "ข่าวสาร";

            public const string TXT_ALL = "แสดงทั้งหมด";
        }


        public class EN
        {
            public const string TXT_HA = "Graduate School of Srinakharinwirot University. ";

            public const string TXT_SEARCH = "Search";

            public const string TXT_DATA = "Data";

            public const string TXT_FORM = "Form";

            public const string TXT_REGISTER = "Register";

            public const string TXT_LOGIN = "Login";

            public const string TXT_LOGOUT = "Logout";

            public const string TXT_REQUIRED_FIELD = "required";

            public const string TXT_SELECT_DATA = "Select";

            public const string TXT_IF_ANY = "if any";

            public const string TXT_SHORTCUT_MENU = "Shortcuts";

            public const string TXT_DISPLAY = "Display";

            public const string TXT_TEL = "Tel";

            public const string TXT_FAX = "Fax";

            public const string TXT_EMAIL = "Email";

            public const string TXT_EMAIL_SARABUN = "านรับ-ส่งหนังสือ";

            public const string TXT_EMAIL_SARABUN2 = "Correspondence";

            public const string TXT_CONTACT_US_SRP = "Contact Us";

            public const string TXT_CONTACT_AGENCY = "Contact the agency";

            public const string TXT_WEB_LINK = "Weblink";

            public const string TXT_SITEMAP = "Sitemap";

            public const string TXT_ABOUT_US_SRP = "About Us";

            public const string TXT_CALEDAR = "Activity Calendar";

            public const string TXT_ATTACH = "Attach File";

            public const string TXT_DONWLOAD = "Download";

            public const string TXT_SERVICEOUR = "Our Service";

            public const string TXT_PREV = "Previous";

            public const string TXT_NEXT = "Next";

            public const string TXT_ADDRESS = "Address";

            public const string TXT_MAP = "Google Map";

            public const string TXT_SENDCOMPLAINT = "Send Complaint";

            public const string TXT_TYPECOMPLAINT = "Complaint Type";

            public const string TXT_FLNAME = "Firstname - Lastname";

            public const string TXT_DEPARTSTAFF = "Department (Staff)";

            public const string TXT_DEPARTMENT = "Department";

            public const string TXT_SUBJECT = "Subject";

            public const string TXT_DESCRIPTION = "Descriotion";

            public const string TXT_PROVINCE = "Province";

            public const string TXT_ALLOW = "Allow";

            public const string TXT_ONLY = "Only";

            public const string TXT_MAPONLY = "Map";

            public const string TXT_HOME = "Home";

            public const string TXT_RESULT_SEARCH = "Search Result";

            public const string TXT_HOSPITAL_TYPE = "Hospital Type";

            public const string TXT_HOSPITAL_KIND = "Hospital Kind";

            public const string TXT_LATLONG = "Latitude , Longitude";

            public const string TXT_WEBSITE = "Website";

            public const string TXT_CER = "Certificate";

            public const string TXT_LEVEL = "Level";

            public const string TXT_CER_DATE = "Start Date";

            public const string TXT_CER_EXPIRY = "Expiry Date";

            public const string TXT_ALL_HISTORY = "Show all history";

            public const string TXT_DATA_HISTORY = "History of certificate";

            public const string TXT_ROOTDEPARTMENT = "ต้นสังกัด";

            public const string TXT_POSITION = "Position";

            public const string TXT_NEWS = "News";

            public const string TXT_ALL = "SHOW ALL";
        }
    }
}
