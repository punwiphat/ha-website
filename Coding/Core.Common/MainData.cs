﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using MySqlConnector;

namespace Core.Common
{
    public static class MainData
    {
        public class Get
        {
            public static List<SocialLink> SocialList()
            {
                List<SocialLink> Obj = new List<SocialLink>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.SocialLink.Where(o=>o.Actived == true).ToList();
                }
                return Obj;
                //List<SocialLinkData> Obj = new List<BannerDetail>();
                //MySqlParameter[] param = new MySqlParameter[2];
                //param[0] = new MySqlParameter("p_MenuID", MenuID);
                //param[1] = new MySqlParameter("p_LangCode", LangCode);
                //Obj = GetStoredMySql.GetAllStored<BannerDetail>("bannerlist_bymenu", param).ToList();
                //return Obj;
            }
        }
    }
}
