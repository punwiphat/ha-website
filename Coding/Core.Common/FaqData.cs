﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public static class FaqData
    {
        public class Get
        {
            public static Faq MainFaqById(int ID)
            {
                Faq Obj = new Faq();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Faq.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static Faq MainFaqByMenuId(int MenuID)
            {
                Faq Obj = new Faq();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Faq.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<FaqDetail> ByMenu(int MenuID,string LangCode)
            {
                List<FaqDetail> Obj = new List<FaqDetail>();
                using (var Map = new CMSdbContext())
                {
                    Obj = (from Main in Map.Faq
                           join Faqs in Map.FaqDetail on Main.ID equals Faqs.FaqID
                           join Category in Map.Category on Main.CategoryID equals Category.ID into _it
                           from Category in _it.DefaultIfEmpty()
                           where Main.Deleted != true
                           && Faqs.LangCode == LangCode
                           && Main.MenuID == MenuID
                           select new
                           {
                                LangCode = Faqs.LangCode
                               ,Faqs.FaqID
                               ,Faqs.FaqDetailID
                               ,Faqs.Question
                               ,Faqs.Answer
                               ,CategoryName = Category!=null && !string.IsNullOrEmpty(Category.NameTH)? Category.NameTH:"ไม่สังกัดหมวดหมู่"
                               ,Sequence = Main.Sequence
                               ,UpdatedDate = Main.UpdatedDate
                               ,UpdatedBy = Main.UpdatedBy
                               ,Actived = Main.Actived      
                            }).ToList_list<FaqDetail>();


                }
                return !Obj.Any()? Obj.OrderByDescending(o => o.UpdatedDate).ToList(): Obj;
            }
            public static List<FaqDetail> FaqById(int Id,string LangCode = null)
            {
                try
                {
                    List<FaqDetail> Obj = new List<FaqDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_FaqID", Id);
                    Obj = GetStoredMySql.GetAllStored<FaqDetail>("B_FAQ_BY_MAINID", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            FaqDetail o = new FaqDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();

                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<Faq> FaqByActive(int MenuID)
            {
                List<Faq> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.Faq.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true ).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<Faq> SearchListByMenu(string searchString, int MenuID)
            {
                CMSdbContext context = new CMSdbContext();
                List<Faq> Faq = new List<Faq>();
                Faq = context.Faq.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                return Faq;
            }
            public static List<Faq> GetFaqList()
            {
                CMSdbContext context = new CMSdbContext();
                var objResult = context.Faq.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;
            }
            public static List<FaqDetail> FListFAQ(int MenuID, string LangCode)
            {
                List<FaqDetail> Lists = new List<FaqDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Lists = GetStoredMySql.GetAllStored<FaqDetail>("F_FAQ", param).ToList();
                return Lists;
            }
        }
        public class Post
        {
            public static Faq AddOrUpdate(Faq Obj,string Username)
            {
                bool mode = false;
                Faq saveObj = new Faq();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.Faq.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        saveObj.MenuID = Obj.MenuID;
                        var lmax = Map.Faq.Where(o=>o.MenuID == Obj.MenuID);
                        var max = lmax.Count()>0? lmax.Max(o => o.Sequence)+1:1;
                        saveObj.Sequence = max > 0 ? max : 1;
                    }
                    saveObj.CategoryID = Obj.CategoryID;
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<FaqDetail> DetailAddOrUpdate(List<FaqDetail> item, string Username)
            {

                try
                {
                    bool mode = false;
                    List<FaqDetail> saveLst = new List<FaqDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            FaqDetail saveObj = new FaqDetail();
                            if (Obj.FaqDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.FaqDetail.Find(Obj.FaqDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.FaqID = Obj.FaqID;
                            }
                            saveObj.Question = Obj.Question;
                            saveObj.Answer = Obj.Answer;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Faq.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Faq.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Faq.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
