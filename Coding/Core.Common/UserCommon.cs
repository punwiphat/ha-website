﻿using System;
using System.Collections.Generic;
using System.Text; 
namespace SMEs
{
  public  class UserCommon
    {

        public Guid UserId
        {
            get { return Guid.Parse(((ClaimsIdentity)User.Identity).FindFirst("UserId").Value); }
        }
        public string _FullName
        {
            get { return ((ClaimsIdentity)User.Identity).FindFirst("FullName").Value; }
        }
        public string _UserName
        {
            get { return ((ClaimsIdentity)User.Identity).FindFirst("username").Value; }
        }

    }
}
