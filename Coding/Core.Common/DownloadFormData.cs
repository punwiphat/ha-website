﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public static class DownloadFormData
    {
        public class Get
        {
            public static DownloadForm MainDownloadFormById(int ID)
            {
                DownloadForm Obj = new DownloadForm();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.DownloadForm.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static DownloadForm MainDownloadFormByMenuId(int MenuID)
            {
                DownloadForm Obj = new DownloadForm();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.DownloadForm.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<DownloadFormDetail> ByMenu(int MenuID,string LangCode)
            {
                List<DownloadFormDetail> Obj = new List<DownloadFormDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangID", LangCode);
                Obj = GetStoredMySql.GetAllStored<DownloadFormDetail>("B_DOWNLOAD_BY_MENU", param).ToList();
                return Obj;
            }
            public static List<DownloadFormDetail> DownloadFormById(int ID, string LangCode = null)
            {
                try
                {
                    List<DownloadFormDetail> Obj = new List<DownloadFormDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_DownloadID", ID);
                    Obj = GetStoredMySql.GetAllStored<DownloadFormDetail>("B_DOWNLOAD_BY_MAINID", param).ToList();
                   
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            DownloadFormDetail o = new DownloadFormDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }

                    }
                    
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                    foreach(var item in Obj)
                    {
                        item.lstFilesAttach = MediaFilesData.Get.ByCategoryMenu(4, item.MenuID).Where(o => o.RefID == item.DownloadFormDetailID.ToString()).ToList();
                    }
                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<DownloadForm> DownloadFormByActive(int MenuID)
            {
                List<DownloadForm> Obj = new List<DownloadForm>();
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.DownloadForm.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<DownloadForm> SearchListByMenu(string searchString, int MenuID)
            {
                CMSdbContext context = new CMSdbContext();
                List<DownloadForm> DownloadForm = new List<DownloadForm>();
                DownloadForm = context.DownloadForm.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                return DownloadForm;
            }
            public static List<DownloadForm> GetDownloadFormList()
            {
                CMSdbContext context = new CMSdbContext();
                var objResult = context.DownloadForm.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;
            }

            public static List<DownloadFormDetail> FListByMenu(int MenuID,int CateID, string LangCode)
            {
                List<DownloadFormDetail> Obj = new List<DownloadFormDetail>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_CateID", CateID);
                param[2] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<DownloadFormDetail>("F_DOWNLOAD", param).ToList();
                return Obj;
            }
        }
        public class Post
        {
            public static DownloadForm AddOrUpdate(DownloadForm Obj,string Username,string IpAddress,string Browser)
            {
                bool mode = false;
                DownloadForm saveObj = new DownloadForm();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.DownloadForm.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        saveObj.MenuID = Obj.MenuID;
                        var lmax = Map.DownloadForm.Where(o=>o.MenuID == Obj.MenuID);
                        var max = lmax.Count()>0? lmax.Max(o => o.Sequence)+1:1;
                        saveObj.Sequence = max > 0 ? max : 1;
                    }
                    saveObj.CategoryID = Obj.CategoryID;
                    saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                    saveObj.EndDate = Obj.EndDate_str.todateEn();
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<DownloadFormDetail> DetailAddOrUpdate(List<DownloadFormDetail> item, string Username, string IpAddress, string Browser)
            {

                try
                {
                    bool mode = false;
                    List<DownloadFormDetail> saveLst = new List<DownloadFormDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            DownloadFormDetail saveObj = new DownloadFormDetail();
                            if (Obj.DownloadFormDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.DownloadFormDetail.Find(Obj.DownloadFormDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.DownloadFormID = Obj.DownloadFormID;
                            }
                            saveObj.DownloadFormName = Obj.DownloadFormName;
                            saveObj.DownloadFormLink = Obj.DownloadFormLink;
                            saveObj.DownloadFormDescription = Obj.DownloadFormDescription;
                            saveObj.Files = Obj.Files;
                            saveObj.MenuID = Obj.MenuID;
                            saveObj.SUB_GUID = Obj.SUB_GUID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                        //foreach (var itemsave in saveLst)
                        //{
                        //    if (itemsave.Files != null)
                        //    {
                        //        var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 4, itemsave.DownloadFormDetailID.ToString(), Username, IpAddress, Browser);
                        //        if (deteted)
                        //            UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.DownloadFormDetailID, 4, 3, Username);
                        //    }
                        //}
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.DownloadForm.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.DownloadForm.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.DownloadForm.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
