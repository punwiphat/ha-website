﻿using System;
using System.Collections.Generic;
using Core.DAL.Models;
using Core.library;
using System.Linq;
using MySqlConnector;

namespace Core.Common
{
    public class LogTransaction
    {
        private static CMSdbContext context = new CMSdbContext();
        public class Get
        {
            public enum ActivityType
            {
                View,
                Add,
                Edit,
                Search,
                Delete,
                Export,
                Import,
                Sendemail,
                Error,
                Sequence,
                Sortby,
                SortType,
                ChangeStatus,
                AddDetail,
                EditDetail

            }
            public static List<LogActivities> all(string txtSearch, int MenuID)
            {
                new List<LogActivities>();
                return GetStoredMySql.GetAllStored<LogActivities>("B_LOG", new MySqlParameter[2]
                {
                new MySqlParameter("p_Txtsearch", txtSearch),
                new MySqlParameter("p_MenuID", MenuID)
                }).ToList();
            }
           
        }
        public static bool SaveLog(int refID, Get.ActivityType activityType, string menuName, string Username, string Description, string IP, string Browser)
        {
            using (var Map = new CMSdbContext())
            {
                LogActivities saveObj = new LogActivities();
                //saveObj.LogActivityId = 0;
                saveObj.RefId = refID;
                saveObj.ActivityType = activityType.ToString();
                saveObj.MenuName = menuName;
                //if (!string.IsNullOrEmpty(Username))
                //{
                //    var usr = Map.Users.Where(o => o.Username == Username).FirstOrDefault();
                //    if (usr != null)
                //    {
                //        //saveObj.Fullname = usr.FirstName + " "+usr.LastName;
                //        saveObj.ActionBy = usr.Username;
                //    }
                //}
                //else
                //    saveObj.ActionBy = "999999";
                saveObj.ActionBy = Username;
                saveObj.Browser = Browser;
                saveObj.ClientIp = IP;
                saveObj.DeviceName = Environment.MachineName;
                saveObj.Descriptions = Description;
                saveObj.ActionDate = DateTime.Now;
                Map.Add(saveObj);
                Map.SaveChanges();
                return true;
            }
        }


    }
}
