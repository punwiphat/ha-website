﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class RoleData
    {
        public static string MenuName = "กลุ่มสิทธิ์";
        public class Get
        {
            public static List<Role> Menurole(int RoleGroupID)
            {
                List<Role> Obj = new List<Role>();
                var Map = new CMSdbContext();
                Obj = Map.Role.Where(o => o.RoleGroupID == RoleGroupID).ToList();

                return Obj;
            }
            public static List<RoleGroup> All(String value)
            {
                List<RoleGroup> Obj = new List<RoleGroup>();
                int i = 1;
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.RoleGroup.Where(o => o.Deleted == false).OrderByDescending(o=>o.UpdatedDate).ToList();
                    if (!string.IsNullOrEmpty(value))
                    {
                        Obj = Obj.Where(o => o.RoleGroupName.Contains(value)).ToList();
                    }
                }
                return Obj.OrderByDescending(o=>o.CreatedDate).ToList();
            }
            public static List<RoleGroup> ByActive()
            {
                List<RoleGroup> Obj = new List<RoleGroup>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.RoleGroup.Where(o => o.Actived == true && o.Deleted != true).ToList();
                }
                return Obj;
            }
            public static RoleGroup ByID(int ID)
            {
                RoleGroup Obj = new RoleGroup();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.RoleGroup.Find(ID);
                }
                return Obj;
            }
            public static List<Role> RoleByGroupID(int RoleGroupID)
            {
                List<Role> Obj = new List<Role>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Role.Where(o=> o.RoleGroupID == RoleGroupID).ToList();
                }
                return Obj;
            }
            public static List<Tm_Menu> GetListMenu(int ParentId)
            {
                List<Tm_Menu> Obj = new List<Tm_Menu>();
                using (var Map = new CMSdbContext())
                {
                    if (ParentId != 0)
                    {
                        Obj = Map.Tm_Menu.Where(o => o.MENU_BACK_ID == ParentId && o.Actived == true).OrderBy(o => o.sort_back).ToList();
                    }
                    else
                    {
                        Obj = Map.Tm_Menu.Where(o => o.levels == 1 && o.Actived == true).OrderBy(o => o.sort_back).ToList();
                    }
                }
                return Obj;
            }
        }
        public static class Post
        {
            public static RoleGroup AddOrUpdate(RoleGroupModel Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                RoleGroup Obj = new RoleGroup();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.RoleGroupID != 0)
                        {
                            mode = true;
                            Obj = Map.RoleGroup.Find(Items.RoleGroupID);
                        }
                        else
                        {
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                        }
                        Obj.Actived = Items.Actived;
                        Obj.RoleGroupName = Items.RoleGroupName;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.RoleGroupID, STATE, MenuName, Username,"", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, MenuName, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }

            public static Boolean AddRoleMenu(int RoleGroupID, List<MenuModel> _list,string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var _temp = Map.Role.Where(o => o.RoleGroupID == RoleGroupID).ToList();
                        foreach (var items in _temp)
                        {
                            Map.Remove(items);
                            Map.SaveChanges();
                        }
                        foreach (var items in _list)
                        {
                            Role Obj = new Role();
                            Obj.MenuID = items.ID;
                            Obj.RoleGroupID = RoleGroupID;
                            Obj.RoleType = items.RoleType;
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(RoleGroupID, LogTransaction.Get.ActivityType.Add, MenuName, Username, "", IpAddress, Browser);
                    return true;
                }
                catch(Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, MenuName, Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }

            //public static Boolean Status(int RoleGroupID, Boolean Active, string username)
            //{
            //    using (var Map = new CMSdbContext())
            //    {
            //        var Obj = Map.TmMasterData.Find(RoleGroupID);
            //        Obj.Activated = Active;
            //        Obj.UpdatedBy = username;
            //        Obj.UpdatedDate = DateTime.Now;
            //        Map.Update(Obj);
            //        Map.SaveChanges();
            //    }
            //    return true;
            //}
            public static bool AddRoleNewMenu(Role items, string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Map.Add<Role>(items);
                        Map.SaveChanges();
                    }
                   
                    LogTransaction.SaveLog(items.RoleID, LogTransaction.Get.ActivityType.Add, "New Menu", Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "New Menu", Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static Boolean Delete(int RoleGroupID, string Username,string IpAddress,string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RoleGroup.Find(RoleGroupID);
                        Obj.Deleted = true;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(RoleGroupID, LogTransaction.Get.ActivityType.Add, MenuName, Username, "", IpAddress, Browser);
                    return true;
                }
                catch(Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, MenuName, Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RoleGroup.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RoleGroup.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        
    }
    }
}
