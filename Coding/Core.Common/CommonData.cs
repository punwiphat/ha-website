using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using MySqlConnector;
namespace Core.Common
{
    public static class CommonData
	{
		public class Get
		{
			public static string FGetConfigValue(string ConfigModule, string ConfigName)
			{
                List<ItemConfigValue> Obj = new List<ItemConfigValue>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ConfigModule", ConfigModule);
                param[1] = new MySqlParameter("p_ConfigName", ConfigName);
                Obj = GetStoredMySql.GetAllStored<ItemConfigValue>("GET_SYSTEM_CONFIGS", param).ToList();
                if (Obj.Count > 0)
                {
                    return Obj.FirstOrDefault().ConfigValue;
                }
                return "";
                
			}
		}

		public class Post
		{
		}
	}
}
