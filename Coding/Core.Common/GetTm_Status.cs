﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;

namespace Core
{
    public static class GetTm_Status
    {
         public class Get
        {
            public static List<TmStatus> ByActive(string value = null)
            {
                List<TmStatus> obj = new List<TmStatus>();
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        obj=  Map.TmStatus.Where(o => o.Activated == true && o.Deleted == false && o.StatusName.Contains(value)).OrderBy(o => o.Sequence).ToList();
                    }
                    else
                    {
                        obj = Map.TmStatus.Where(o => o.Activated == true && o.Deleted == false).OrderBy(o => o.Sequence).ToList();

                    }
                    return obj;
                }
            }
            public static TmStatus ByID(Guid ID)
            {
                TmStatus obj = new TmStatus();
                using (var Map = new CMSdbContext())
                {
                    obj  =Map.TmStatus.Find(ID); 
                }
                return obj;
            }

        }
        public static class save
        {
        }
        }
    }
