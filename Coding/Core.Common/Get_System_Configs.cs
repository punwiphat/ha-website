﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;

namespace Core
{
    public static class Get_System_Configs
    {
         public class Get
        {
            public static List<SystemConfigs> All(int type)
            {
                List<SystemConfigs> obj = new List<SystemConfigs>();
                using (var Map = new CMSdbContext())
                {
                    obj =  Map.SystemConfigs.Where(o => o.System_Config_Category == type && o.Activated == true).ToList();
                 }
                return obj;
            }
            public static SystemConfigs BySystemConfigId( Guid SystemConfigId)
            {
                SystemConfigs obj = new SystemConfigs();
                using (var Map = new CMSdbContext())
                {
                    obj = Map.SystemConfigs.Where(o => o.SystemConfigId == SystemConfigId).FirstOrDefault();
                }
                return obj;
            }


            
        }

        public static class save
        {

            public static void Update(Guid SystemConfigId, int value,string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.SystemConfigs.Find(SystemConfigId);
                    Obj.SystemConfigValue = value;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
            }
        }
    }
}
