﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public static class PostData
    {
        public class Get
        {
            public static Posts MainPostsById(int ID)
            {
                Posts Obj = new Posts();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Posts.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }

            public static List<PostsDetail> ByMenu(int MenuID,string LangCode)
            {
                
                    List<PostsDetail> Obj = new List<PostsDetail>();
                    MySqlParameter[] param = new MySqlParameter[2];
                    param[0] = new MySqlParameter("p_MenuID", (int)MenuID);
                    param[1] = new MySqlParameter("p_LangCode", LangCode);
                    Obj = GetStoredMySql.GetAllStored<PostsDetail>("B_POSTS_DETAIL", param).ToList();
                    return Obj;
                    //List<PostsDetail> Obj = new List<PostsDetail>();
                    //using (var Map = new CMSdbContext())
                    //{
                    //    Obj = (from Main in Map.Posts
                    //           join Postss in Map.PostsDetail on Main.ID equals Postss.PostID
                    //           join Media in Map.MediaFiles.Where(o => o.MediaCategoryID == 2 && o.Actived == true && o.Deleted!= true) on
                    //           new {
                    //                Key1 = Postss.PostDetailID.ToString()
                    //               ,Key2 = Main.MenuID
                    //           } equals
                    //           new {
                    //                 Key1 = Media.RefID
                    //                ,Key2 = Media.MenuID
                    //            } 
                    //           into _it
                    //           from Media in _it.DefaultIfEmpty()
                    //           where Main.Deleted != true
                    //           && Main.MenuID == MenuID
                    //           && Postss.LangCode == LangCode
                    //           select new
                    //           {
                    //                Postss.PostID
                    //               ,Postss.PostDetailID
                    //               ,Postss.Title
                    //               ,Postss.ShortDescriptions
                    //               ,Postss.Descriptions
                    //               ,Postss.UrlLink
                    //               ,Postss.TagKeywords
                    //               ,Sequence = Main.Sequence
                    //               ,Actived = Main.Actived
                    //               ,UpdatedDate = Main.UpdatedDate
                    //               ,UpdatedBy = Main.UpdatedBy
                    //               ,FileID = Media != null ? Media.MediaFileID : 0
                    //               ,FileName = Media != null ? Media.FileName : ""
                    //               ,FilePath = Media != null ? Media.PathFile : ""
                    //               ,OriginalFileName = Media != null ? Media.FileNameOri : ""
                    //           }).ToList_list<PostsDetail>();


                    //}
                    //return !Obj.Any()? Obj.OrderByDescending(o => o.UpdatedDate).ToList(): Obj;
                }
            public static List<PostsDetail> PostsById(int Id,string LangCode = null)
            {
                List<PostsDetail> Obj = new List<PostsDetail>();
                if (Id != 0)
                {
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_PostID", Id);
                    Obj = GetStoredMySql.GetAllStored<PostsDetail>("B_POSTS_BY_MAINID", param).ToList();
                    foreach(var item in Obj)
                    {
                        item.lstFiles = MediaFilesData.Get.ByCategoryMenu(5, item.MenuID).Where(o => o.RefID == item.PostDetailID.ToString()).ToList();
                        item.lstFilesAttach = MediaFilesData.Get.ByCategoryMenu(4, item.MenuID).Where(o => o.RefID == item.PostDetailID.ToString()).ToList();

                    }
                    //using (var Map = new CMSdbContext())
                    //{
                    //    Obj = (from Lang in Map.Languages
                    //           join Postss in Map.PostsDetail on Lang.LanguageCode equals Postss.LangCode into _Postss
                    //           from Postss in _Postss.DefaultIfEmpty()
                    //           join Main in Map.Posts on Postss.PostID equals Main.ID into _Main
                    //           from Main in _Main.DefaultIfEmpty()
                    //           join Media in Map.MediaFiles.Where(o => o.MediaCategoryID == 2) on
                    //           new
                    //           {
                    //               Key1 = Postss.PostDetailID.ToString()
                    //               , Key2 = Main.MenuID
                    //           } equals
                    //           new
                    //           {
                    //               Key1 = Media.RefID
                    //               , Key2 = Media.MenuID
                    //           }
                    //           into _it
                    //           from Media in _it.DefaultIfEmpty()
                    //           where Main.Deleted != true
                    //           && Media.Deleted != true
                    //           && Main.ID == Id
                    //           && Postss.PostID == Id
                    //           && Lang.Actived == true
                    //           select new
                    //           {
                    //               LangCode = Lang.LanguageCode
                    //               , Postss.PostID
                    //               , Postss.PostDetailID
                    //               , Postss.Title
                    //               , Postss.ShortDescriptions
                    //               , Postss.Descriptions
                    //               , Postss.UrlLink
                    //               , Postss.TagKeywords
                    //               , Sequence = Main.Sequence
                    //               , UpdatedDate = Main.UpdatedDate
                    //               , UpdatedBy = Main.UpdatedBy
                    //               , Actived = Main.Actived
                    //               , MenuID = Main.MenuID
                    //               , lstFiles = MediaFilesData.Get.ByCategoryMenu(5, Main.MenuID).Where(o=>o.RefID == Postss.PostDetailID.ToString()).ToList()
                    //               , lstFilesAttach = MediaFilesData.Get.ByCategoryMenu(4, Main.MenuID).Where(o => o.RefID == Postss.PostDetailID.ToString().ToList()
                    //               , FileID = Media != null ? Media.MediaFileID : 0
                    //               , FileName = Media != null ? Media.FileName : ""
                    //               , FilePath = Media != null ? Media.PathFile : ""
                    //               , OriginalFileName = Media != null ? Media.FileNameOri : ""
                    //           }).ToList_list<PostsDetail>();

                      

                    //}
                }
                else
                {
                    var lang = LanguageData.GetByActive();
                    if (!string.IsNullOrEmpty(LangCode))
                        lang = lang.Where(o => o.LanguageCode == LangCode).ToList();
                    foreach (var l in lang)
                    {
                        PostsDetail o = new PostsDetail();
                        o.LangCode = l.LanguageCode;
                        Obj.Add(o);
                    }
                }
                if (!string.IsNullOrEmpty(LangCode))
                    Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                return Obj;
            }
            public static List<Posts> PostsByActive(int MenuID)
            {
                List<Posts> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Posts.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                }
                return Obj;
            }
            public static List<Posts> SearchListByMenu(string searchString, int MenuID)
            {
                List<Posts> Posts = new List<Posts>();
                using (var Map = new CMSdbContext())
                {
                    Posts = Map.Posts.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                }
                return Posts;
            }
            public static List<Posts> GetPostsList()
            {

                List<Posts> Posts = new List<Posts>();
                using (var Map = new CMSdbContext())
                {
                    Posts = Map.Posts.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                }
                 return Posts;


            }
            public static List<PostsDetail> PostsActivedList()
            {
                try
                {
                    List<PostsDetail> objResult = new List<PostsDetail>(); // context.Posts.Where(o => o.Deleted == false && o.Actived == true).OrderByDescending(o => o.Ti).ToList();
                    using (var Map = new CMSdbContext())
                    {
                        objResult = (from main in Map.Posts
                                     join detail in Map.PostsDetail on main.ID equals detail.PostID
                                     where main.Deleted != true
                                     && main.Actived == true
                                     && detail.LangCode == "TH"
                                     select new
                                     {
                                         detail
                                     }).ToList_list<PostsDetail>();
                    }
                    return !objResult.Any() ? objResult : new List<PostsDetail>();
                }
                catch(Exception ex)
                {
                    string m = ex.Message;
                    return new List<PostsDetail>(); 
                }

            }
            public static List<PostsTag> TagsAll(string term)
            {
                List<PostsTag> Obj = new List<PostsTag>();
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(term))
                        Obj = Map.PostsTag.Where(o => o.TagName.Contains(term)).ToList();
                    else
                        Obj = Map.PostsTag.ToList();

                }
                return !Obj.Any() ? Obj.OrderBy(o=>o.TagName).ToList() : new List<PostsTag>();
            }
            public static List<PostsTag> TagsLangAll(string term,string LangID)
            {
                List<PostsTag> Obj = new List<PostsTag>();
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(term))
                        Obj = Map.PostsTag.Where(o => o.TagName.Contains(term) && o.LangID == LangID).ToList();
                    else
                        Obj = Map.PostsTag.Where(o => o.LangID == LangID).ToList();

                }
                return !Obj.Any() ? Obj.OrderBy(o => o.TagName).ToList() : new List<PostsTag>();
            }
            public static List<PostsDetail> FTopPosts(MenuList MenuID, string LangCode)
            {
                List<PostsDetail> Obj = new List<PostsDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", (int)MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<PostsDetail>("F_POSTS_TOP", param).ToList();
                return Obj;
            }
            public static List<PostsDetail> FListPosts(int MenuID, string LangCode)
            {
                List<PostsDetail> Obj = new List<PostsDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", (int)MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<PostsDetail>("F_POSTS", param).ToList();
                return Obj;
            }
            public static List<PostsDetail> FListPostsTag(int MenuID, string LangCode,string tags)
            {
                List<PostsDetail> Obj = new List<PostsDetail>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_MenuID", (int)MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                param[2] = new MySqlParameter("p_Tags", tags);
                Obj = GetStoredMySql.GetAllStored<PostsDetail>("F_POSTS_TAG", param).ToList();
                return Obj;
            }
            public static PostsDetail FPostByID(int MenuID, int ID,string LangCode)
            {
                PostsDetail Obj = new PostsDetail();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_PostID", ID);
                param[2] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<PostsDetail>("F_POSTS_DETAIL", param).FirstOrDefault();
                return Obj;
            }
            public static List<PostsDetail> FListRelatePosts(int MenuID, int ID, string LangCode)
            {
                List<PostsDetail> Obj = new List<PostsDetail>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_PostID", ID);
                param[2] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<PostsDetail>("F_POSTS_RELATED", param).ToList();
                return Obj;
            }
            public static bool FViews(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_PostDetailID", ID);
                var Obj = GetStoredMySql.Executenon("F_POSTS_VIEW", param);
                return Obj;
            }
        }
        public class Post
        {
            public static Posts AddOrUpdate(Posts Obj,string Username, string IpAddress, string Browser)
            {
                try
                {
                    bool mode = false;
                    Posts saveObj = new Posts();
                    using (var Map = new CMSdbContext())
                    {
                        if (Obj.ID != 0)
                        {
                            mode = true;
                            saveObj = Map.Posts.Find(Obj.ID);
                        }
                        else
                        {
                            saveObj.Deleted = false;
                            saveObj.CreatedDate = DateTime.Now;
                            saveObj.CreatedBy = Username;
                            saveObj.MenuID = Obj.MenuID;
                            var lmax = Map.Posts.Where(o => o.MenuID == Obj.MenuID);
                            var max = lmax.Count() > 0 ? lmax.Max(o => o.Sequence) : 1;
                            saveObj.Sequence = max > 0 ? max : 1;
                        }
                        saveObj.RelatedPost = Obj.RelatedPost;// String.Join(",",Obj.RelatedID);
                        saveObj.InputDate = ((!string.IsNullOrEmpty(Obj.InputDate_str)) ? Utility.todateEn(Obj.InputDate_str) : new DateTime?(DateTime.Now));

                        saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                        saveObj.EndDate = Obj.EndDate_str.todateEn();
                        saveObj.UpdatedDate = DateTime.Now;
                        saveObj.UpdatedBy = Username;
                        saveObj.Actived = Obj.Actived;
                        saveObj.Menuname = Obj.Menuname;
                        if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveObj;
                }
                catch (Exception ex)
                {
                    string ms = ex.Message;
                    return new Posts();
                }
            }

            public static List<PostsTag> TagAddOrUpdate(List<PostsTag> ListTags)
            {
                List<PostsTag> result = new List<PostsTag>();
                try
                {
                        using (var Map = new CMSdbContext())
                        {
                            foreach (var item in ListTags)
                            {
                                PostsTag o = new PostsTag();
                                o.TagName = item.TagName;
                                o.LangID = item.LangID;
                                Map.Add(o);
                                result.Add(o);
                            }
                            Map.SaveChanges();

                        }
                    return result;
                }
                catch (Exception ex)
                {
                    string ms = ex.Message;
                    return result;
                }

            }
            public static List<PostsDetail> DetailAddOrUpdate(List<PostsDetail> item, string Username, string IpAddress, string Browser)
            {
                try
                {

                    bool mode = false;
                    List<PostsDetail> saveLst = new List<PostsDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        List<PostsTag> list2 = new List<PostsTag>();
                        foreach (var Obj in item)
                        {
                            PostsDetail saveObj = new PostsDetail();
                            if (Obj.PostDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.PostsDetail.Find(Obj.PostDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.PostID = Obj.PostID;
                            }
                            saveObj.Title = Obj.Title;
                            saveObj.ShortDescriptions = Obj.ShortDescriptions;
                            saveObj.Descriptions = Obj.Descriptions;
                            saveObj.UrlLink = Obj.UrlLink;
                            saveObj.TagKeywords = Obj.TagKeywords;
                            saveObj.Files = Obj.Files;
                            saveObj.MenuID = Obj.MenuID;
                            saveObj.SUB_GUID = Obj.SUB_GUID;
                            if (Obj.TagsName != null)
                            {
                                foreach (string item3 in Obj.TagsName)
                                {
                                    PostsTag postsTag = new PostsTag();
                                    postsTag.LangID = Obj.LangCode;
                                    postsTag.TagName = item3;
                                    list2.Add(postsTag);
                                }
                            }
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                            if (list2.Count() > 0)
                            {
                                TagAddOrUpdate(list2);
                            }
                            
                        }
                        Map.SaveChanges();
                        foreach (var itemsave in saveLst)
                        {
                            if (itemsave.Files != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.PostDetailID.ToString(), Username, IpAddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.PostDetailID, 2, 4, Username);
                            }
                        }
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    string ms = ex.Message;
                    return new List<PostsDetail>();
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Posts.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Posts.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Posts.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }

    }
}
