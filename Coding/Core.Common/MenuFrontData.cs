﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public class MenuFrontData
    {
        public class Get
        {
            public static Tm_Menu ByID(int ID, string LangCode)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", ID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_ID", param).ToList();
                return obj.Any() ? obj.FirstOrDefault() : new Tm_Menu();
            }
            public static Tm_Menu ByID(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", ID);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_ID", param).ToList();
                return obj.Any() ? obj.FirstOrDefault() : new Tm_Menu();
            }
            public static Tm_Menu ByPermalink(string Permalink, string LangCode)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Permalink", Permalink);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PERMALINK", param).ToList();
                return obj.FirstOrDefault();// obj.Any() ? obj.FirstOrDefault() : new Tm_Menu();
            }

            public static List<Tm_Menu> ListNAV(int MenuID, string LangCode)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PERMALINK_NAV", param).ToList();
                return obj;// obj.Any() ? obj.FirstOrDefault() : new Tm_Menu();
            }
            public static List<Tm_Menu> FMenuByModule(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                return RootMenu;

            }
            public static List<Tm_Menu> FMenuF(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList(); foreach (var item in RootMenu) // Level2
                {
                    item.Sub_Menu = FMenuByParentID(item.ID, LangCode);

                }
                return RootMenu;
            }

            public static List<Tm_Menu> FMenu(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList(); foreach (var item in RootMenu) // Level2
                {
                    item.Sub_Menu = FMenuByParentID(item.ID, LangCode);
                    foreach (var item2 in RootMenu) // Level3
                    {
                        item2.Sub_Menu = FMenuByParentID(item2.ID, LangCode);
                        foreach (var item3 in RootMenu) // Level4
                        {
                            item3.Sub_Menu = FMenuByParentID(item3.ID, LangCode);
                            foreach (var item4 in RootMenu) // Level5
                            {
                                item4.Sub_Menu = FMenuByParentID(item4.ID, LangCode);
                            }
                        }
                    }
                }
                return RootMenu;
            }
            public static List<Tm_Menu> FSubMenu(int ParentID, string LangCode)
            {

                List<Tm_Menu> AllMenu = new List<Tm_Menu>();
                AllMenu = FMenuByALL(ParentID, LangCode);

                List<Tm_Menu> RootMenu = AllMenu.Where(o => o.MENU_FRONT_ID == ParentID).ToList();
                foreach (var item in RootMenu) // Level2
                {
                    item.Sub_Menu = AllMenu.Where(o => o.MENU_FRONT_ID == item.ID).ToList();
                    foreach (var item2 in item.Sub_Menu) // Level3
                    {
                        item2.Sub_Menu = AllMenu.Where(o => o.MENU_FRONT_ID == item2.ID).ToList();
                        foreach (var item3 in item2.Sub_Menu) // Level4
                        {
                            item3.Sub_Menu = AllMenu.Where(o => o.MENU_FRONT_ID == item3.ID).ToList();
                            foreach (var item4 in RootMenu) // Level5
                            {
                                item4.Sub_Menu = AllMenu.Where(o => o.MENU_FRONT_ID == item4.ID).ToList();
                            }
                        }
                    }
                }
                return RootMenu;
            }
            public static List<Tm_Menu> FMainMenu(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                return RootMenu;
            }

            public static List<Tm_Menu> FMenuByParentID(int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PARENT", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> FMenuByALL(int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_ALL", param).ToList();
                return RootMenu;
            }

            public static List<Tm_Menu> FModuleByParentID(int ModuleID, int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[2] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE_PARENT", param).ToList();
                return RootMenu;
            }



            public static List<Tm_Menu> FMenuByParentID(MenuList ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PARENT", param).ToList();
                return RootMenu;
            }
            public static List<ShortcutDetail> ServiceManageList(int ModuleID, string LangCode)
            {
                List<ShortcutDetail> RootMenu = new List<ShortcutDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<ShortcutDetail>("shortcut_list", param).ToList();
                return RootMenu;
            }

            public static Shortcut MainById(int ID)
            {
                Shortcut Obj = new Shortcut();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_ShortcutID", ID);
                Obj = GetStoredMySql.GetAllStored<Shortcut>("shortcut_byid", param).FirstOrDefault();
                return Obj;
            }
            public static List<ShortcutDetail> ShortcutById(int ID, string LangCode = null)
            {
                try
                {
                    List<ShortcutDetail> Obj = new List<ShortcutDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_ShortcutID", ID);
                    Obj = GetStoredMySql.GetAllStored<ShortcutDetail>("shortcut_byid", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            ShortcutDetail o = new ShortcutDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();

                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
       public class Post
        {
            public static Shortcut AddOrUpdate(Shortcut Obj, string Username, string IPaddress, string Browser)
            {
                try
                {
                    
                    bool mode = false;
                    Shortcut saveObj = new Shortcut();
                    using (var Map = new CMSdbContext())
                    {
                        if (Obj.ID != 0)
                        {
                            mode = true;
                            saveObj = Map.Shortcut.Find(Obj.ID); //MenuFrontData.Get.MainById(Obj.ID);// Map.Shortcut.Find(Obj.ID);
                        }
                        else
                        {
                            saveObj.Deleted = false;
                            saveObj.CreatedDate = DateTime.Now;
                            saveObj.CreatedBy = Username;
                            //
                            saveObj.ModuleID = Obj.ModuleID;
                            var lmax = Map.Shortcut.Where(o => o.ModuleID == Obj.ModuleID);
                            var max = lmax.Count() > 0 ? lmax.Max(o => o.Sequence) + 1 : 1;
                            saveObj.Sequence = max > 0 ? max : 1;
                        }
                  
                        saveObj.ParentID = Obj.ParentID;
                        saveObj.MenuID = Obj.MenuID;
                        saveObj.UpdatedDate = DateTime.Now;
                        saveObj.UpdatedBy = Username;
                        saveObj.Actived = Obj.Actived;
                        saveObj.Menuname = Obj.Menuname;
                        saveObj.MainMenuID = Obj.MainMenuID;

                        if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveObj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                   
                }
                return null;
            }
            public static List<ShortcutDetail> DetailAddOrUpdate(List<ShortcutDetail> item, string Username, string IPaddress, string Browser,int size=0)
            {

                try
                {
                    bool mode = false;
                    List<ShortcutDetail> saveLst = new List<ShortcutDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            ShortcutDetail saveObj = new ShortcutDetail();
                            if (Obj.ShortcutDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.ShortcutDetail.Find(Obj.ShortcutDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.ShortcutID = Obj.ShortcutID;
                            }
                            saveObj.ServiceName = Obj.ServiceName;
                            saveObj.URL = Obj.URL;
                            saveObj.IconType = Obj.IconType;
                            saveObj.IconName = Obj.IconName;
                            saveObj.Files = Obj.Files;
                            saveObj.MenuID = Obj.MenuID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                        MediaFiles files = new MediaFiles();
                        foreach (var itemsave in saveLst)
                        {
                            if (itemsave.Files != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.ShortcutDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    files = UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.ShortcutDetailID, 2, size, Username);
                            }
                            var t = Updatepath(itemsave.ShortcutDetailID, files.PathFile);

                        }

                    }
                    return saveLst;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static bool Updatepath(int ID, string path)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ShortcutDetail.Find(ID);
                        if (Obj != null)
                        {
                            Obj.IconName = path;
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                     return false;
                }
            }

            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Shortcut.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Shortcut.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Shortcut.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
