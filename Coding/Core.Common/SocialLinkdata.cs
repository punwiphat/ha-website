﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Core.Common;
using static Core.Common.LogTransaction.Get;
using MySqlConnector;

namespace Core
{
    public static class SocialLinkData
    {
        public class Get
        {
            public static List<SocialLink> ListSocialLink()
            {
               
                    List<SocialLink> Obj = new List<SocialLink>();
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.SocialLink.ToList();
                    }
                    return Obj;
            }
            public static List<SocialLink> ByActive()
            {
                
                    List<SocialLink> Obj = new List<SocialLink>();
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.SocialLink.Where(o=>o.Actived==true).ToList();
                    }
                    return Obj;
            }
        }
        public static class Post
        {
           
            public static List<SocialLink> AddOrUpdate(List<SocialLink> item, string Username, string IPaddress, string Browser)
            {

                try
                {
                    bool mode = false;
                    List<SocialLink> saveLst = new List<SocialLink>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            SocialLink saveObj = new SocialLink();
                            if (Obj.ID != 0)
                            {
                                mode = true;
                                saveObj = Map.SocialLink.Find(Obj.ID);
                            }
                            //saveObj.SocialIcon = Obj.SocialIcon;
                            //saveObj.SocialName = Obj.SocialName;
                            saveObj.SocialURL = Obj.SocialURL;
                            //saveObj.Actived = Obj.Actived;
                            saveObj.MenuID = Obj.MenuID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveLst;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }
    }
}