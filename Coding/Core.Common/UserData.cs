﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core
{
    public static class UsersData
    {
        public static class Get
        {
            public static Users By_Email(string Email)
            {
                Users Obj = new Users();
                var Map = new CMSdbContext();
                Obj = Map.Users.Where(o => o.Email.Trim() == Email.Trim() && o.Actived == true && o.Deleted == false).FirstOrDefault();
                
                return Obj;
            }
            public static List<Users> GetAll(string value = "")
            {
                List<Users> Obj = new List<Users>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_Txtsearch", value);
                Obj = GetStoredMySql.GetAllStored<Users>("B_USERS", param).ToList();
                return Obj;
            }

          
            public static bool EmailValidation(String Email, int UserID)
            {
                List<Users> Obj = new List<Users>();
                var Map = new CMSdbContext();
                Obj = Map.Users.Where(o => o.Email.ToUpper() == Email.ToUpper() && o.UserID != UserID).ToList();

                return Obj.Count() == 0 ? false : true;
            }
            public static Users ActiveByID(string Username)
            {
                Users Obj = new Users();
                var Map = new CMSdbContext();
                Obj = Map.Users.Where(o => o.Username == Username && o.Actived && o.Deleted != true).FirstOrDefault();
                return Obj;
            }

            public static Users ByID(int UserID)
            {
                Users Obj = new Users();
                var Map = new CMSdbContext();

                Obj = Map.Users.Find(UserID);

                if (Obj != null)
                {
                    if (Obj.RoleGroupID != 0)
                    {
                        var roleGr = Map.RoleGroup.Where(s => s.RoleGroupID == Obj.RoleGroupID).FirstOrDefault();
                        if (roleGr != null)
                        {
                            Obj.RoleGroupName = roleGr.RoleGroupName;
                        }
                    }
                }
                return Obj;
            }
            public static Users ByUserName(string UserName)
            {
                Users Obj = new Users();
                var Map = new CMSdbContext();

                Obj = Map.Users.Where(o => o.Username == UserName).FirstOrDefault();
                if (Obj != null)
                {
                    if (Obj.RoleGroupID != 0)
                    {
                        var roleGr = Map.RoleGroup.Where(s => s.RoleGroupID == Obj.RoleGroupID).FirstOrDefault();
                        if (roleGr != null)
                        {
                            Obj.RoleGroupName = roleGr.RoleGroupName;
                        }
                    }
                }
                return Obj;
            }
            public static Users Login(string username, string password)
            {
                Users Obj = new Users();
                var Map = new CMSdbContext();
                var _result = Map.Users.Where(o => o.Username.Trim().ToUpper() == username.Trim().ToUpper() && o.Password == Encryption.Encrypt(password) && o.Actived == true && o.Deleted == false).FirstOrDefault();
                if (_result != null)
                {
                    Obj = _result;
                    if (Obj.RoleGroupID != 0)
                    {
                        Obj.RoleGroupName = "";
                        var roleGr = Map.RoleGroup.Where(s => s.RoleGroupID == Obj.RoleGroupID).FirstOrDefault();
                        if (roleGr != null)
                        {
                            Obj.RoleGroupName = roleGr.RoleGroupName;
                        }
                    }
                }
                else
                {

                    Obj = null;
                }

                return Obj;
            }

        }
        public static class save
        {
            public static bool AddOrUpdate_password(int UserID, string oldpassword, String password, string username)
            {
                Users Obj = new Users();
                bool status = false;

                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Users.Find(UserID);
                    if (Obj.Password == Encryption.Encrypt(oldpassword))
                    {
                        Obj.Password = Encryption.Encrypt(password);
                        Obj.UpdatedBy = username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                        status = true;
                    }

                }
                return status;
            }

            private static Random random = new Random();
            private static string RandomString(int length)
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                return new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
            public static Users resetpassword(int UserID, string username)
            {
                Users Obj = new Users();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Users.Find(UserID);
                    Obj.Password = Encryption.Encrypt(RandomString(8));
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }

            public static Users AddOrUpdate_profile(Users Items, string username)
            {
                Users Obj = new Users();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Users.Find(Items.UserID);
                   // Obj.TitleNameCode = Items.TitleNameCode;
                    Obj.FirstName = Items.FirstName;
                    //Obj.MiddleName = Items.MiddleName;
                    Obj.LastName = Items.LastName;
                    Obj.Email = Items.Email;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }

            public static Users AddOrUpdate(Users Items, string Username, string IPaddress, string Browser)
            {

                Users Obj = new Users();
                string text = "";
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items != null && Items.UserID != 0)
                        {
                            mode = true;
                            Obj = Map.Users.Find(Items.UserID);
                        }
                        else
                        {
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                            Obj.Username = Items.Username;
                            Obj.Password = Encryption.Encrypt(RandomString(8));
                        }
                        //Obj.TitleNameCode = Items.TitleNameCode;
                        Obj.FirstName = Items.FirstName;
                        //Obj.MiddleName = Items.MiddleName;
                        Obj.LastName = Items.LastName;
                        Obj.RoleGroupID = Items.RoleGroupID;
                        Obj.Email = Items.Email;
                        //Obj.MobileNo = Items.MobileNo;
                        //Obj.DepartmentAdmin = Items.DepartmentAdmin;
                        //Obj.Position_Id = Items.Position_Id;
                        Obj.Actived = Items.Actived;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        text = "Username : " + Obj.Username + " Name : " + Obj.FullName;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                        LogTransaction.SaveLog(Obj.UserID, (!mode) ? LogTransaction.Get.ActivityType.Add : LogTransaction.Get.ActivityType.Edit, "ผู้ใช้งาน", Username, text, IPaddress, Browser);
                    }

                }
			catch (Exception ex)
			{
                    LogTransaction.SaveLog(Obj.UserID, LogTransaction.Get.ActivityType.Error, "ผู้ใช้งาน", Username, ex.Message, IPaddress, Browser);
            }
                return Obj;
            }
            public static Boolean Delete(int UserID, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Users.Find(UserID);
                    Obj.Deleted = true;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }
            public static Boolean Status(int UserID, Boolean Active, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Users.Find(UserID);
                    Obj.Actived = Active;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }
            public static ResponseJson UpdateStatus(int ID, bool Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson responseJson = new ResponseJson();
                string description = "";

                try
                {
                    using (CMSdbContext cMSdbContext = new CMSdbContext())
                    {
                        Users user = cMSdbContext.Users.Find(ID);

                        if (user == null)
                        {
                            description = "ไม่พบข้อมูล ID : " + ID;
                            responseJson.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            string currentStatus = user.Actived ? "เปิดการใช้งาน" : "ปิดการใช้งาน";
                            string newStatus = Active ? "เปิดการใช้งาน" : "ปิดการใช้งาน";

                            description = $"ทำการเปลี่ยนสถานะจาก {currentStatus} เป็น {newStatus}";
                            user.Actived = Active;

                            cMSdbContext.Update(user);
                            cMSdbContext.SaveChanges();

                            responseJson.Result = true;
                        }
                    }

                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.ChangeStatus, "ผู้ใช้งาน", Username, description, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "ผู้ใช้งาน", Username, ex.Message, IpAddress, Browser);
                    responseJson.Result = false;
                    responseJson.MessageError = ex.Message;
                }

                return responseJson;
            }

            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                string description = "";
                ResponseJson responseJson = new ResponseJson();

                try
                {
                    using (CMSdbContext cMSdbContext = new CMSdbContext())
                    {
                        Users user = cMSdbContext.Users.Find(ID);

                        if (user == null)
                        {
                            description = "ไม่พบข้อมูล ID : " + ID;
                            responseJson.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            description = "ทำการลบข้อมูล ID : " + ID;
                            user.Deleted = true;
                            user.UpdatedBy = Username;
                            user.UpdatedDate = DateTime.Now;

                            cMSdbContext.Update(user);
                            cMSdbContext.SaveChanges();

                            responseJson.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, "ผู้ใช้งาน", Username, description, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, "ผู้ใช้งาน", Username, ex.Message, IpAddress, Browser);
                    responseJson.MessageError = ex.Message;
                }

                return responseJson;
            }

        }
    }
}
