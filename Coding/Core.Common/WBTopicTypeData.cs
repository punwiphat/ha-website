﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class WBTopicTypeData
    {
        public class Get
        {
            public static List<TopicType> ByActive()
            {
                List<TopicType> objResult = new List<TopicType>();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopicType.Where(o => o.Actived == true && o.Deleted != true).ToList();
                }
                return objResult;
            }

        }
        public class Post
        {
           
        }
    }
}
