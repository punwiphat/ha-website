﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;
using static Core.Common.LogTransaction.Get;

namespace Core.Common
{
    public static class ChannelCommentTypeData
    {
        public class Get
        {
            public static List<ChannelCommentType> ByActive(bool? Active = true)
            {
                List<ChannelCommentType> list = new List<ChannelCommentType>();
                using (var Map = new CMSdbContext())
                {
                    list = Map.ChannelCommentTypes.Where(o => o.Actived == Active && o.Deleted != true).ToList();
                }
                return list;
            }

            public static List<ChannelCommentType> ALL()
            {
                List<ChannelCommentType> list = new List<ChannelCommentType>();
                using (var Map = new CMSdbContext())
                {
                    list = Map.ChannelCommentTypes.Where(o => o.Deleted != true).ToList();
                }
                return list;
            }

            public static ChannelCommentType ByID(int ID)
            {
                ChannelCommentType obj = new ChannelCommentType();
                using (var Map = new CMSdbContext())
                {
                    obj = Map.ChannelCommentTypes.Find(ID);
                }
                return obj;
            }
        }
        public class Post
        {
            public static ChannelCommentType AddOrUpdate(ChannelCommentType Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                ChannelCommentType Obj = new ChannelCommentType();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.ChannelCommentTypes.Find(Items.ID);
                        }
                        else
                        {
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                            Obj.MenuID = Items.MenuID;
                        }
                        Obj.Actived = Items.Actived;
                        Obj.NameTH = Items.NameTH;
                        Obj.NameEN = Items.NameEN;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Obj.Menuname = Items.Menuname;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username, "", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ChannelCommentTypes.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ChannelCommentTypes.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
