﻿using System;
using System.Collections.Generic;
using Core.DAL.Models;
using Core.library;
using System.Linq;
using MySqlConnector;
using static Core.Common.LogTransaction.Get;
using static System.Net.Mime.MediaTypeNames;

namespace Core.Common
{
    public class MenuBackofficeData
    {
        public class Get
        {
            public static int GetMaxLevel()
            {
                using (var context = new CMSdbContext())
                {
                   return context.Tm_Menu.Max(o => o.levels.Value);
                }
            }
            public static Tm_Menu ByID(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", ID);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("MENU_BY_ID", param).ToList();
                return obj.Any()?obj.FirstOrDefault():new Tm_Menu();
            }
            //public static List<ddlmaster> MenuTypeList()
            //{
            //    List<ddlmaster> lst = new List<ddlmaster>();
            //    lst = GetStoredMySql.GetAllStoredNonparam<ddlmaster>("menutypelist").ToList();
            //    return lst;
            //}
            //public static List<ddlmaster> MenuSectionList()
            //{
            //    List<ddlmaster> lst = new List<ddlmaster>();
            //    lst = GetStoredMySql.GetAllStoredNonparam<ddlmaster>("menusectionlist").ToList();
            //    return lst;
            //}
           
            public static MenuType LinkByCode(string Code)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_Code", Code);
                var obj = GetStoredMySql.GetAllStored<MenuType>("menutypelist_bycode", param).ToList();
                return obj.Any() ? obj.FirstOrDefault() : new MenuType();
            }
            public static List<Tm_Menu> GetMenuBackOfficeListActive()
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", 0);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("B_SIDEMENU", param).ToList();

                List<Tm_Menu> objResult = new List<Tm_Menu>();
                objResult = RootMenu.Where(o => o.levels == 1).ToList();
                foreach (var p in objResult)
                {
                   p.Sub_Menu = RootMenu.Where(o => o.levels == 2 && o.MENU_BACK_ID == p.ID).ToList();
                   foreach (var q in p.Sub_Menu)
                   {
                      q.Sub_Menu = RootMenu.Where(o => o.levels == 3 && o.MENU_BACK_ID == q.ID).ToList();
                      foreach (var r in q.Sub_Menu)
                      {
                          r.Sub_Menu = RootMenu.Where(o => o.levels == 4 && o.MENU_BACK_ID == r.ID).ToList();
                          foreach (var s in r.Sub_Menu)
                          {
                            s.Sub_Menu = RootMenu.Where(o => o.levels == 5 && o.MENU_BACK_ID == s.ID).ToList();
                          }
                      }
                   }   
                }
                return objResult;
            }
            public static List<Tm_Menu> GetMenuBackOfficeListActiveByname(string Menuname)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_Level", 0);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                param[2] = new MySqlParameter("p_Menuname", Menuname);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("menulist_backoffice_active_by_name", param).ToList();

                return RootMenu;
            }
            public static List<Tm_Menu> GetMenuBackOfficeList()
        {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", 0);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("menulist_backoffice", param).ToList();

                List<Tm_Menu> objResult = new List<Tm_Menu>();
                objResult = RootMenu.Where(o => o.levels == 1).ToList();
                foreach (var p in objResult)
                {
                    p.Sub_Menu = RootMenu.Where(o => o.levels == 2 && o.MENU_BACK_ID == p.ID).ToList();
                    foreach (var q in p.Sub_Menu)
                    {
                        q.Sub_Menu = RootMenu.Where(o => o.levels == 3 && o.MENU_BACK_ID == q.ID).ToList();
                        foreach (var r in q.Sub_Menu)
                        {
                            r.Sub_Menu = RootMenu.Where(o => o.levels == 4 && o.MENU_BACK_ID == r.ID).ToList();
                            foreach (var s in r.Sub_Menu)
                            {
                                s.Sub_Menu = RootMenu.Where(o => o.levels == 5 && o.MENU_BACK_ID == s.ID).ToList();
                            }
                        }
                    }
                }
                return objResult;
            }
            public static List<Tm_Menu> GetMenuManageList()
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", 0);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("B_MENULIST_FOR_MANAGE", param).ToList();

                List<Tm_Menu> objResult = new List<Tm_Menu>();
                objResult = RootMenu.Where(o => o.levels == 1).ToList();
                foreach (var p in objResult)
                {
                    p.Sub_Menu = RootMenu.Where(o => o.levels == 2 && o.MENU_BACK_ID == p.ID).ToList();
                    foreach (var q in p.Sub_Menu)
                    {
                        q.Sub_Menu = RootMenu.Where(o => o.levels == 3 && o.MENU_BACK_ID == q.ID).ToList();
                        foreach (var r in q.Sub_Menu)
                        {
                            r.Sub_Menu = RootMenu.Where(o => o.levels == 4 && o.MENU_BACK_ID == r.ID).ToList();
                            foreach (var s in r.Sub_Menu)
                            {
                                s.Sub_Menu = RootMenu.Where(o => o.levels == 5 && o.MENU_BACK_ID == s.ID).ToList();
                            }
                        }
                    }
                }
                return objResult;
            }
            public static List<Tm_Menu> GetMenuListActive(int level)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", level);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("menulist_backoffice_active", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> MenuEnabledBelong(int level)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", level);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("B_MENULIST_FOR_MANAGE_BELONG_DDL", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> GetMenuListManageActive(int level)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Level", level);
                param[1] = new MySqlParameter("p_RootMenuID", 0);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("menulist_backoffice_manage_active", param).ToList();
                return RootMenu;
            }
            public static Tm_Menu ByPermalink(string Permalink)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Permalink", Permalink);
                param[1] = new MySqlParameter("p_LangCode", "TH");
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("B_MENU_BY_PERMALINK", param).ToList();
                return obj.FirstOrDefault();
            }
            public static Tm_Menu ByPermalinkWithRole(string Permalink, string username)
            {
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_Permalink", Permalink);
                param[1] = new MySqlParameter("p_LangCode", "TH");
                param[2] = new MySqlParameter("p_Username", username);
                var obj = GetStoredMySql.GetAllStored<Tm_Menu>("B_MENU_BY_PERMALINK_WITH_ROLE", param).ToList();
                return obj.FirstOrDefault();
            }

            public static List<Tm_Menu> FMenuByModule(int ModuleID,string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                return RootMenu;

            }
            public static List<Tm_Menu> FMenu(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                foreach(var item in RootMenu) // Level2
                {
                    item.Sub_Menu = FMenuByParentID(item.ID, LangCode);
                    foreach (var item2 in RootMenu) // Level3
                    {
                        item2.Sub_Menu = FMenuByParentID(item2.ID, LangCode);
                        foreach (var item3 in RootMenu) // Level4
                        {
                            item3.Sub_Menu = FMenuByParentID(item3.ID, LangCode);
                            foreach (var item4 in RootMenu) // Level5
                            {
                                item4.Sub_Menu = FMenuByParentID(item4.ID, LangCode);
                            }
                        }
                    }
                }
                return RootMenu;
            }
            public static List<Tm_Menu> FSubMenu(int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                //MySqlParameter[] param = new MySqlParameter[2];
                //param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                //param[1] = new MySqlParameter("p_LangCode", LangCode);
                //RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                using (var context = new CMSdbContext())
                {
                    RootMenu = context.Tm_Menu.Where(o => o.ID == ParentID).ToList();
                }
                foreach (var item in RootMenu) // Level2
                {
                    item.Sub_Menu = FMenuByParentID(item.ID, LangCode);
                    foreach (var item2 in RootMenu) // Level3
                    {
                        item2.Sub_Menu = FMenuByParentID(item2.ID, LangCode);
                        foreach (var item3 in RootMenu) // Level4
                        {
                            item3.Sub_Menu = FMenuByParentID(item3.ID, LangCode);
                            foreach (var item4 in RootMenu) // Level5
                            {
                                item4.Sub_Menu = FMenuByParentID(item4.ID, LangCode);
                            }
                        }
                    }
                }
                return RootMenu;
            }
            public static List<Tm_Menu> FMainMenu(int ModuleID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE", param).ToList();
                foreach (var item in RootMenu) // Level2
                {
                    item.Sub_Menu = FModuleByParentID(ModuleID,item.ID, LangCode);
                    foreach (var item2 in item.Sub_Menu) // Level3
                    {
                        item2.Sub_Menu = FModuleByParentID(ModuleID,item2.ID, LangCode);
                        foreach (var item3 in item2.Sub_Menu) // Level4
                        {
                            item3.Sub_Menu = FModuleByParentID(ModuleID,item3.ID, LangCode);
                            foreach (var item4 in item3.Sub_Menu) // Level5
                            {
                                item4.Sub_Menu = FModuleByParentID(ModuleID,item4.ID, LangCode);
                            }
                        }
                    }
                }
                return RootMenu;
            }
            public static List<Tm_Menu> FMenuByParentID(int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PARENT", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> FModuleByParentID(int ModuleID, int ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_ModuleID", ModuleID);
                param[1] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[2] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_MODULE_PARENT", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> FMenuByParentID(MenuList ParentID, string LangCode)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ParentID", (int)ParentID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("F_MENU_BY_PARENT", param).ToList();
                return RootMenu;
            }
            public static string GeneratePermalink(string Permalink,int MenuID)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_Permalink", Permalink);
                param[1] = new MySqlParameter("p_MenuID", MenuID);
                string p_Permalink = GetStoredMySql.GetAllStored<DataReturn>("B_GENARATE_LINK", param).FirstOrDefault().c_permalink.ToString();
                return p_Permalink;
            }
            #region GET MENU BACK OFFICE
            public static int GetMenuIDByPermalink(string Menuname)
        {
            using (var context = new CMSdbContext())
            {
                return context.Tm_Menu.Where(m => m.Actived == true && m.show_back == true && m.permalink_back == Menuname).FirstOrDefault()?.ID??0; //m.levels == level &&
               
            }
        }
        public static List<Tm_Menu> GetByAllMenu()
        {
            using (var context = new CMSdbContext())
            {
                return context.Tm_Menu.Where(m => m.Actived == true && m.show_back == true).ToList(); //m.levels == level &&

            }
        }

        #endregion
        public static Tm_Menu GetMenuBackOfficeById(int Id)
        {
            try
            {
                using (var context = new CMSdbContext())
                {

                    var obj = context.Tm_Menu.Where(o => o.ID == Id).FirstOrDefault();

                    return obj;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<Tm_Menu> GetChildMenuByparentId(int parentId, int level)
        {
            try
            {
                using (var context = new CMSdbContext())
                {

                    var obj = context.Tm_Menu.Where(o => o.MENU_BACK_ID == parentId && o.levels == level).ToList();

                    return obj;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
      }
        public static class Post
        {
            public static List<Tm_Menu> ADDCATE(Tm_Menu Items, string Username, string IPaddress, string Browser)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[4];
                param[0] = new MySqlParameter("p_Menuname", Items.MenuNameTH);
                param[1] = new MySqlParameter("p_MenunameEN", Items.MenuNameEN);
                param[2] = new MySqlParameter("p_CreatedBy", Username);
                param[3] = new MySqlParameter("p_ParentID", Items.MENU_BACK_ID);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("INSERT_MENU_CATE", param).ToList();
                return RootMenu;
            }
            public static List<Tm_Menu> UPDATECATE(Tm_Menu Items, string Username, string IPaddress, string Browser)
            {
                List<Tm_Menu> RootMenu = new List<Tm_Menu>();
                MySqlParameter[] param = new MySqlParameter[6];
                param[0] = new MySqlParameter("p_Menuname", Items.MenuNameTH);
                param[1] = new MySqlParameter("p_MenunameEN", Items.MenuNameEN);
                param[2] = new MySqlParameter("p_CreatedBy", Username);
                param[3] = new MySqlParameter("p_ParentID", Items.MENU_BACK_ID);
                param[4] = new MySqlParameter("p_MenuID", Items.ID);
                param[5] = new MySqlParameter("p_Permalink", Items.permalink_back);
                RootMenu = GetStoredMySql.GetAllStored<Tm_Menu>("UPDATE_MENU_CATE", param).ToList();
                return RootMenu;
            }
            public static Tm_Menu AddOrUpdate(Tm_Menu Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                int mainID = Items.ID;
                string text = "";
                Tm_Menu Obj = new Tm_Menu();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.Tm_Menu.Find(Items.ID);
                            if(Obj.MenuTypeCode == "DOWNLOAD" && Obj.MenuTypeCode != Items.MenuTypeCode)
                            {
                                DELCATE(Obj.ID, Username);
                            }
                        }
                        else
                        {
                            mainID = Map.Tm_Menu.Max(o=>o.ID)+1;
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                            //if (Items.MenuTypeCode != "CATEGORY")
                            //{
                                Obj.sort_front = Items.MENU_FRONT_ID != null && Items.MENU_FRONT_ID != 0 ? Map.Tm_Menu.Where(o => o.MENU_FRONT_ID == Items.MENU_FRONT_ID).Max(o => o.sort_front) + 1 : Map.Tm_Menu.Where(o => o.MENU_FRONT_ID == null).Max(o => o.sort_front) + 1;
                                Obj.self_front = true;//Items.self_back;
                                Obj.show_front = true;
                            //}
                            Obj.sort_back = Items.MENU_BACK_ID != null && Items.MENU_BACK_ID != 0 ? Map.Tm_Menu.Where(o => o.MENU_BACK_ID == Items.MENU_BACK_ID).Max(o=>o.sort_back)+1 : Map.Tm_Menu.Where(o => o.MENU_BACK_ID == null).Max(o => o.sort_back) + 1;
                            Obj.BelongID = 0;
                            Obj.Disabled = false;
                            Obj.IsEdit = true;
                            Obj.self_back = true;// Items.self_back;
                        }
                        if (Items.MENU_BACK_ID != null)
                        {
                            Obj.MENU_BACK_ID = Items.MENU_BACK_ID;
                            Obj.MENU_FRONT_ID = Items.MENU_BACK_ID;
                        }
                       
                        Obj.levels = Items.MENU_BACK_ID != null && Items.MENU_BACK_ID != 0 ? Map.Tm_Menu.Where(o => o.ID == Items.MENU_BACK_ID).FirstOrDefault().levels + 1 : 1;
                       
                        Obj.type_show = Items.type_show;
                        if (Items.type_show == 1)
                            Obj.MenuTypeCode = Items.MenuTypeCode;
                        else
                        {
                            Obj.MenuTypeCode = "LINK";
                            Obj.link_front = Items.link_front;
                        }
                        Obj.section_name_back = Items.section_name_back;
                        Obj.menu_name_back = Items.MenuNameTH;
                        Obj.MenuNameTH = Items.MenuNameTH;
                        Obj.MenuNameEN = Items.MenuNameEN;
                        string Permalink = Utility.ToPermalink(Items.MenuNameTH);
                        string newPermalink = Get.GeneratePermalink(Permalink, Items.ID);
                        //string newPermalink = count != 0 ? Permalink + (count + 1).ToString() : Permalink;
                        Obj.permalink_back = newPermalink;
                        var TypeCode = new[] { "SPECIAL", "STRUCTURE", "LINK"};

                        if (TypeCode.All(n => n != Items.MenuTypeCode))
                        {
                           var MenuType = Get.LinkByCode(Items.MenuTypeCode);
                            Obj.controller_back = MenuType.Controller_Back;
                            Obj.action_back = MenuType.Action_Back;
                            //if (Items.MenuTypeCode != "CATEGORY")
                            //{
                                Obj.controller_front = MenuType.Controller_Front;
                                Obj.action_front = MenuType.Action_Front;
                            //}
                        }

                        //Obj.show_back = Items.show_back;
                        switch (Obj.levels)
                        {
                            case 1:
                                Obj.icon_back = "ft-activity";
                                break;
                            case 2:
                                Obj.icon_back = "ft-chevron-right";
                                break;
                            case 3:
                                Obj.icon_back = "ft-chevrons-right";
                                break;
                            case 4:
                                Obj.icon_back = "ft-list";
                                break;
                            case 5:
                                Obj.icon_back = "ft-circle";
                                break;
                        }

                        Obj.show_back = Items.MenuTypeCode != "LINK";
                        Obj.Actived = Items.Actived;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;

                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                      
                    }
                    //if (Items.MenuTypeCode != "CATEGORY")
                    //{
                        var res = AddOrUpdateDetail(Obj, Username, IPaddress, Browser);
                    //}
                    if (text != "DOWNLOAD" && Items.MenuTypeCode == "DOWNLOAD")
                    {
                        UPDATECATE(Obj, Username, IPaddress, Browser);
                    }
                    if (Obj.levels == 1)
                    {
                        RoleData.Post.AddRoleNewMenu(new Role
                        {
                            MenuID = Obj.ID,
                            RoleGroupID = 1,
                            RoleType = "M"
                        }, Username, IPaddress, Browser);
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username, STATE.ToString() + " MENU " + Items.MenuTypeCode, IPaddress, Browser);

                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
            public static Tm_Menu UpdateDisabled(Tm_Menu Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                Tm_Menu Obj = new Tm_Menu();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.Tm_Menu.Find(Items.ID);
                        if (Items.MENU_BACK_ID != null)
                        {
                            Obj.MENU_FRONT_ID = Items.MENU_BACK_ID;
                        }
                        Obj.levels = Items.MENU_BACK_ID != null && Items.MENU_BACK_ID != 0 ? Map.Tm_Menu.Where(o => o.ID == Items.MENU_BACK_ID).FirstOrDefault().levels + 1 : 1;
                        Obj.MenuNameTH = ((Obj.menu_name_back != Items.MenuNameTH) ? Items.MenuNameTH : Obj.MenuNameTH);
                        Obj.MenuNameEN = Items.MenuNameEN;
                        Obj.menu_name_back = Items.MenuNameTH;
                        if (Obj.menu_name_back != Items.MenuNameTH || Obj.permalink_back == null)
                        {
                            Obj.permalink_back = Get.GeneratePermalink(Items.MenuNameTH, Items.ID);
                        }
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username, "UPDATE DISABLED MENU", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
            public static bool DELCATE(int BelongID, string Username)
            {
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_BelongID", BelongID);
                param[1] = new MySqlParameter("p_UpdatedBy", Username);
                return GetStoredMySql.Executenon("B_MENU_CHANGE_NOT_DOWNLOAD", param);
            }
            public static bool AddOrUpdateDetail(Tm_Menu Main, string Username, string IPaddress, string Browser)
            {
                bool res = false;
                try
                {
                    foreach (var lang in LanguageData.GetByActive())
                    {

                    ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                    Tm_MenuDetail Obj = new Tm_MenuDetail();
                    
                        using (var Map = new CMSdbContext())
                        {
                            Boolean mode = false;
                            if (Main.ID != 0)
                            {
                                mode = true;
                                Obj = Map.Tm_MenuDetail.Where(o => o.LangCode == lang.LanguageCode && o.MenuID == Main.ID).FirstOrDefault();
                                Obj = Obj!=null?Obj: new Tm_MenuDetail(); 
                            }
                            Obj.MenuID = Main.ID; //!= 0 ? Main.ID : mainID;
                            Obj.LangCode = lang.LanguageCode;
                           // var menu = Main.GetValObjDy("MenuName" + lang.LanguageCode); // Obj.GetType().GetProperty("MenuName" + lang.LanguageCode).GetValue(Obj, null).ToString();
                            Obj.Menuname = (lang.LanguageCode == "TH" ? Main.MenuNameTH : Main.MenuNameEN);
                            string Permalink = Utility.ToPermalink(Obj.Menuname);
                            string newPermalink = Get.GeneratePermalink(Permalink, Main.ID);
                            Obj.Permalink = newPermalink;

                            if (mode)
                            {
                                Map.Update(Obj);
                            }
                            else
                            {
                                Map.Add(Obj);
                            }
                            Map.SaveChanges();
                        }

                        LogTransaction.SaveLog(Obj.ID, STATE, "Menu Detail", Username, "", IPaddress, Browser);
                        res = true;
                    }
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "Menu Detail", Username, ex.Message, IPaddress, Browser);
                    res = false;
                }
                return res;

            }

            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Tm_Menu.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.sort_back = Seq;
                            Obj.sort_front = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Tm_Menu.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Tm_Menu.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static bool DragUpdateSequence(int ID,int Levels,int Sequence,int? ParentID)
            {
                MySqlParameter[] param = new MySqlParameter[4];
                param[0] = new MySqlParameter("p_MenuID", ID);
                param[1] = new MySqlParameter("p_Levels", Levels);
                param[2] = new MySqlParameter("p_Sequence", Sequence);
                param[3] = new MySqlParameter("p_ParentID", ParentID!=null ?Convert.ToInt32(ParentID):DBNull.Value);
                var obj = GetStoredMySql.Executenon("UPDATE_MENU_SEQUENCE", param);
                return obj;
            }
        }
    }
}
