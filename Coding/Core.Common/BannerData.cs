﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Wangkanai.Detection;

namespace Core.Common
{
    public static class BannerData
    {
        public class Get
        {
            public static Banner MainBannerById(int ID)
            {
                Banner Obj = new Banner();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Banner.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static Banner MainBannerByMenuId(int MenuID)
            {
                Banner Obj = new Banner();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Banner.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<BannerDetail> ByMenu(int MenuID,string LangCode)
            {
                List<BannerDetail> Obj = new List<BannerDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<BannerDetail>("B_BANNER_BY_MENU", param).ToList();
                return Obj;
            }
            public static List<BannerDetail> ActiveByMenu(int MenuID, string LangCode)
            {
                List<BannerDetail> Obj = new List<BannerDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<BannerDetail>("F_BANNER", param).ToList();
                return Obj;
            }

            public static List<BannerDetail> ActiveByMenu(MenuList MenuID, string LangCode)
            {
                List<BannerDetail> Obj = new List<BannerDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", (int)MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<BannerDetail>("F_BANNER", param).ToList();
                return Obj;
            }
            public static List<BannerDetail> BannerById(int ID,string LangCode = null)
            {
                try
                {
                    List<BannerDetail> Obj = new List<BannerDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_BannerID", ID);
                    Obj = GetStoredMySql.GetAllStored<BannerDetail>("B_BANNER_BY_MAINID", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            BannerDetail o = new BannerDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();

                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<Banner> BannerByActive(int MenuID)
            {
                List<Banner> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.Banner.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<Banner> SearchListByMenu(string searchString, int MenuID)
            {
                IQueryable<BannerDetail> Obj;
                CMSdbContext context = new CMSdbContext();
                List<Banner> Banner = new List<Banner>();
                Banner = context.Banner.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                return Banner;
            }
            public static List<Banner> GetBannerList()
            {
                CMSdbContext context = new CMSdbContext();
                var objResult = context.Banner.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;
            }
            public static bool CountView(int BannerDeatilID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_DetailID", (int)BannerDeatilID);
                GetStoredMySql.Executenon("F_BANNER_COUNT", param);
                return true;
            }
        }
        public class Post
        {
            public static Banner AddOrUpdate(Banner Obj,string Username, string IPaddress, string Browser)
            {
                string description = "";
                bool mode = false;
                try
                {
                    Banner saveObj = new Banner();

                    using (var Map = new CMSdbContext())
                    {
                        if (Obj.ID != 0)
                        {
                            mode = true;
                            saveObj = Map.Banner.Find(Obj.ID);
                        }
                        else
                        {
                            saveObj.Deleted = false;
                            saveObj.CreatedDate = DateTime.Now;
                            saveObj.CreatedBy = Username;
                            saveObj.MenuID = Obj.MenuID;
                            var lmax = Map.Banner.Where(o => o.MenuID == Obj.MenuID);
                            var max = lmax.Count() > 0 ? lmax.Max(o => o.Sequence) + 1 : 1;
                            saveObj.Sequence = max > 0 ? max : 1;
                        }
                        saveObj.CategoryID = Obj.CategoryID;
                        saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                        saveObj.EndDate = Obj.EndDate_str.todateEn();
                        saveObj.UpdatedDate = DateTime.Now;
                        saveObj.UpdatedBy = Username;
                        saveObj.Actived = Obj.Actived;
                        saveObj.Menuname = Obj.Menuname;
                        saveObj.lstBanner = Obj.lstBanner;
                        saveObj.MenuID = Obj.MenuID;
                        if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                        }
                        Map.SaveChanges();
                        description = $"Banner ID : {saveObj.ID} Banner name : {saveObj.lstBanner.FirstOrDefault(b => b.LangCode == "TH")?.BannerName}";
                        LogTransaction.SaveLog(saveObj.ID, mode ? LogTransaction.Get.ActivityType.Edit : LogTransaction.Get.ActivityType.Add, saveObj.Menuname, Username, description, IPaddress, Browser);
                    }
                    return saveObj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(Obj.ID, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                }

                return Obj;
            }
            public static List<BannerDetail> DetailAddOrUpdate(List<BannerDetail> item, string Username,int size, string IPaddress, string Browser)
            {

                try
                {
                    bool mode = false;
                    List<BannerDetail> saveLst = new List<BannerDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            BannerDetail saveObj = new BannerDetail();
                            if (Obj.BannerDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.BannerDetail.Find(Obj.BannerDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.BannerID = Obj.BannerID;
                            }
                            saveObj.BannerName = Obj.BannerName;
                            saveObj.BannerLink = Obj.BannerLink;
                            saveObj.BannerDescription = Obj.BannerDescription;
                            saveObj.Files = Obj.Files;
                            saveObj.FileAttach = Obj.FileAttach;
                            saveObj.MenuID = Obj.MenuID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                            var logDescription = $"Banner ID : {saveObj.BannerID} Banner Detail ID : {saveObj.BannerDetailID} Banner name : {saveObj.BannerName}";
                            LogTransaction.SaveLog(saveObj.BannerDetailID, mode ? LogTransaction.Get.ActivityType.EditDetail : LogTransaction.Get.ActivityType.AddDetail, saveObj.Menuname, Username, logDescription, IPaddress, Browser);
                        }

                        Map.SaveChanges();
                        foreach (var itemsave in saveLst)
                        {
                            if (itemsave.Files != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.BannerDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.BannerDetailID, 2, size, Username);
                            }
                            if (itemsave.FileAttach != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 4, itemsave.BannerDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.FileAttach, itemsave.MenuID, itemsave.BannerDetailID, 4, 0, Username);
                            }
                        }
                    }
                    return saveLst;
                }

                catch (Exception ex)
                {
                    LogTransaction.SaveLog(item.FirstOrDefault()?.BannerID ?? 0, LogTransaction.Get.ActivityType.Error, item.FirstOrDefault()?.Menuname ?? string.Empty, Username, ex.Message, IPaddress, Browser);
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Banner.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                            var description = $"ทำการเปลี่ยนลำดับจาก {Obj.Sequence} เป็น {Seq}";
                            LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Sequence, Menuname, Username, description, IpAddress, Browser);
                        }
                    }
                    //LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Banner.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Banner.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
