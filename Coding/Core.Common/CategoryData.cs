﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class CategoryData
    {
        public class Get
        {
            public static List<Category> ByMenuID(int MenuID)
            {
                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("B_CATEGORY_BYMENU", param).ToList();
                return Obj;
            }
            public static List<Category> ByListID(int ListMenuID)
            {
                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", ListMenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("B_CATEGORY_BY_LIST_MENU", param).ToList();
                return Obj;
            }
            public static List<Category> All()
            {
                List<Category> Obj = new List<Category>();
               
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Category.Where(o => o.Deleted != true).OrderByDescending(o=>o.UpdatedDate).ToList();
                   
                }
                return Obj.OrderByDescending(o=>o.CreatedDate).ToList();
            }
            public static List<Category> ByMenuIdActive(int MenuID)
            {

                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("F_CATEGORY_BY_MENU", param).ToList();
                return Obj;
            }
           
            public static List<Category> ByActive()
            {
                List<Category> Obj = new List<Category>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Category.Where(o => o.Actived == true && o.Deleted != true).ToList();
                }
                return Obj;
            }
            public static Category ByID(int ID)
            {
                Category Obj = new Category();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Category.Find(ID);
                }
                return Obj;
            }
            public static List<Category> ByBannerMenuIdActive(int MenuID)
            {
                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("F_CATEGORY_BANNER_BY_MENU", param).ToList();
                return Obj;
            }

            public static List<Category> ByCourseMenuIdActive(int MenuID)
            {
                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("F_CATEGORY_COURSE_BY_MENU", param).ToList();
                return Obj;
            }

            public static List<Category> ByProjectMenuIdActive(int MenuID)
            {
                List<Category> Obj = new List<Category>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                Obj = GetStoredMySql.GetAllStored<Category>("F_CATEGORY_PROJECT_BY_MENU", param).ToList();
                return Obj;
            }

         

        }
        public static class Post
        {
            public static Category AddOrUpdate(Category Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                Category Obj = new Category();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.Category.Find(Items.ID);
                        }
                        else
                        {
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                            Obj.MenuID = Items.MenuID;
                            var lmax = Map.Category.Where(o => o.MenuID == Obj.MenuID);
                            var max = lmax.Count() > 0 ? lmax.Max(o => o.Sequence) + 1 : 1;
                            Obj.Sequence = max > 0 ? max : 1;
                        }
                        Obj.Actived = Items.Actived;
                        Obj.NameTH = Items.NameTH;
                        Obj.NameEN = Items.NameEN;
                        Obj.EmailTo = Items.EmailTo;
                        Obj.RSSUrl = Items.RSSUrl;
                        Obj.Sequence = Items.Sequence;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Obj.Menuname = Items.Menuname;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username,"", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                string description = "";
                ResponseJson responseJson = new ResponseJson();
                try
                {
                    using (CMSdbContext cMSdbContext = new CMSdbContext())
                    {
                        Category category = cMSdbContext.Category.Find(ID);
                        if (category == null)
                        {
                            description = "ไม่พบข้อมูล ID : " + ID;
                            responseJson.MessageError = "ไม\u0e48พบข\u0e49อม\u0e39ล";
                        }
                        else
                        {
                            description = "ทำการเปลี่ยนลำดับจาก" + category.Sequence + " เป็น" + Seq;
                            category.Sequence = Seq;
                            cMSdbContext.Update(category);
                            cMSdbContext.SaveChanges();
                            responseJson.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Sequence, Menuname, Username, description, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    responseJson.Result = false;
                    responseJson.MessageError = ex.Message;
                }
                return responseJson;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Category.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Category.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
