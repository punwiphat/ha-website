﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Core.Common;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class SettingFileData
    {
        public class Get
        {
            public static SettingFiles All( )
            {
                var Obj =  new SettingFiles();
                using (var Map = new CMSdbContext())
                {
                    var Temp = Map.SettingFiles.FirstOrDefault();
                    Obj = Temp != null ? Temp : Obj;
                }
                return Obj;
            }
        }
        public static class save
        {
            public static SettingFiles AddOrUpdate(SettingFiles Items, string Username,string IPaddress, string Browser)
            {
                SettingFiles Obj = new SettingFiles();
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Temp = Map.SettingFiles.FirstOrDefault();
                        Obj = Temp != null ? Temp : Obj;
                        Obj.AllowTypeFile = Items.AllowTypeFile;
                        Obj.MaxSizeFile = Items.MaxSizeFile;
                        Obj.MaxQuantityFile = Items.MaxQuantityFile;
                        Obj.SizeFile = Items.SizeFile;
                        Obj.AllowTypeFile = Items.AllowTypeFile;

                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (Temp != null)
                        {
                            STATE = LogTransaction.Get.ActivityType.Edit;
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, "ตั้งค่าไฟล์", Username,"", IPaddress, Browser);
                    return Obj;
                }
                catch(Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "ตั้งค่าไฟล์", Username,ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
        }
    }
}
