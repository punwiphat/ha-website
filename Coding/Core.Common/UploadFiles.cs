﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SixLabors;
using System.Threading;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp;
using System.Threading.Tasks;
using Core.Common;

namespace Core
{

    public static class UploadFiles
    {
        static string uploadDirecotroy = "wwwroot//fileupload";
        private static CMSdbContext context = new CMSdbContext();
        static string sPath = Directory.GetCurrentDirectory();
        static bool delFile = false;

        public static List<MediaFiles> GetMediaFilesList()
        {
            // var objResult = _context.TmMenu.s("EXECUTE dbo.GetMostPopularBlogsForUser @filterByUser=@user", langID).Tomaplist<TmMenu>();
            using (var _db = new CMSdbContext())
            {
                var objResult = _db.MediaFiles.Where(o => o.Deleted == false).ToList();
                return objResult;
            }
        }
        public static List<MediaFiles> GetMediaFilesList_byRef_Id(string RefID, int MenuID)
        {
            using (var _db = new CMSdbContext())
            {
                var objResult = _db.MediaFiles.Where(o => o.RefID == RefID && o.MenuID == MenuID && o.Deleted == false).ToList();
                return objResult;
            }
        }


        public static bool AddUploadAllFile(IFormFile uploadFile, string RowID, string categoryName, string filename, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);



            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);
            // + "_" + uploadFile.FileName;

            string _filename = uploadFile.FileName;
            if (!string.IsNullOrEmpty(filename))
            {
                _filename = filename + Path.GetExtension(uploadFile.FileName);
            }
            var filePath = Path.Combine(uploadOriPath, uniqueFileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                uploadFile.CopyToAsync(fileStream);
            }


            using (var Map = new CMSdbContext())
            {
                MediaFiles saveObj = new MediaFiles();
                saveObj.CreatedDate = DateTime.Now;
                saveObj.CreatedBy = userName;

                saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
                saveObj.RefID = RowID.Trim();
                saveObj.Deleted = false;
                saveObj.Actived = true;
                saveObj.PathFile = filePath.Replace(sPath, ""); ;
                saveObj.FolderName = RowID.ToString();
                saveObj.TitleName = categoryName;
                saveObj.FileNameOri = _filename;
                saveObj.FileName = uniqueFileName;
                saveObj.FileExtension = uploadFile.ContentType;
                saveObj.FileSize = uploadFile.Length;
                ///saveObj.Downloads = uploadFile.Downloads;
                //saveObj.Sequence = uploadFile.Sequence;

                saveObj.UpdatedDate = DateTime.Now;
                saveObj.UpdatedBy = userName;
                Map.Add(saveObj);
                Map.SaveChanges();
            }


            return true;

        }



        public static bool AddUploadCarouselFile(IFormFile uploadFile, string RowID, string categoryName, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            uploadThumbnailPath = uploadThumbnailPath + @"\" + categoryName + @"\Thumbnail\" + RowID.ToString();

            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);
            if (!Directory.Exists(uploadThumbnailPath))
                Directory.CreateDirectory(uploadThumbnailPath);

            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);
            // + "_" + uploadFile.FileName;
            //using (var fileStream = new FileStream(oriPath, FileMode.Create))
            //{
            //    uploadFile.CopyTo(fileStream);
            //    fileStream.Flush();
            //}

            var oriPath = Path.Combine(uploadOriPath, uniqueFileName);
            using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // image.Mutate(x => x.Resize(946, 689));688, 689
            image.Mutate(x => x.Resize(688, 689));//480x360
            image.Save(oriPath);

            var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);

            using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // thumbImage.Mutate(x => x.Resize(174, 89));
            thumbImage.Mutate(x => x.Resize(1900, 620));
            thumbImage.Save(thumbFile);


            MediaFiles saveObj = new MediaFiles();
            saveObj.CreatedDate = DateTime.Now;
            saveObj.CreatedBy = userName;

            saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
            saveObj.RefID = RowID.Trim();
            saveObj.Deleted = false;
            saveObj.Actived = true;

            saveObj.PathFile = thumbFile.Replace(sPath, "");
            saveObj.FolderName = RowID.ToString();
            saveObj.TitleName = categoryName;
            saveObj.FileNameOri = uploadFile.FileName;
            saveObj.FileName = uniqueFileName;
            saveObj.FileExtension = uploadFile.ContentType;
            saveObj.FileSize = uploadFile.Length;
            saveObj.UpdatedDate = DateTime.Now;
            saveObj.UpdatedBy = userName;
            context.Add(saveObj);
            context.SaveChanges();
            return true;

        }

        public static bool AddUploadRelateLinkFile(IFormFile uploadFile, string RowID, string categoryName, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            uploadThumbnailPath = uploadThumbnailPath + @"\" + categoryName + @"\Thumbnail\" + RowID.ToString();

            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);
            if (!Directory.Exists(uploadThumbnailPath))
                Directory.CreateDirectory(uploadThumbnailPath);

            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);
            // + "_" + uploadFile.FileName;
            //using (var fileStream = new FileStream(oriPath, FileMode.Create))
            //{
            //    uploadFile.CopyTo(fileStream);
            //    fileStream.Flush();
            //}

            var oriPath = Path.Combine(uploadOriPath, uniqueFileName);
            using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // image.Mutate(x => x.Resize(946, 689));688, 689
            image.Mutate(x => x.Resize(688, 689));//480x360
            image.Save(oriPath);

            var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);

            using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // thumbImage.Mutate(x => x.Resize(174, 89));
            thumbImage.Mutate(x => x.Resize(220, 194));
            thumbImage.Save(thumbFile);


            MediaFiles saveObj = new MediaFiles();
            saveObj.CreatedDate = DateTime.Now;
            saveObj.CreatedBy = userName;

            saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
            saveObj.RefID = RowID.Trim();
            saveObj.Deleted = false;
            saveObj.Actived = true;

            saveObj.PathFile = thumbFile.Replace(sPath, "");
            saveObj.FolderName = RowID.ToString();
            saveObj.TitleName = categoryName;
            saveObj.FileNameOri = uploadFile.FileName;
            saveObj.FileName = uniqueFileName;
            saveObj.FileExtension = uploadFile.ContentType;
            saveObj.FileSize = uploadFile.Length;
            ///saveObj.Downloads = uploadFile.Downloads;
            //saveObj.Sequence = uploadFile.Sequence;

            saveObj.UpdatedDate = DateTime.Now;
            saveObj.UpdatedBy = userName;
            context.Add(saveObj);

            context.SaveChanges();

            return true;

        }

        public static bool AddUploadThumbnailFile(IFormFile uploadFile, string RowID, string categoryName, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            uploadThumbnailPath = uploadThumbnailPath + @"\" + categoryName + @"\Thumbnail\" + RowID.ToString();

            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);
            if (!Directory.Exists(uploadThumbnailPath))
                Directory.CreateDirectory(uploadThumbnailPath);

            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);
            // + "_" + uploadFile.FileName;
            //using (var fileStream = new FileStream(oriPath, FileMode.Create))
            //{
            //    uploadFile.CopyTo(fileStream);
            //    fileStream.Flush();
            //}

            var oriPath = Path.Combine(uploadOriPath, uniqueFileName);
            using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // image.Mutate(x => x.Resize(946, 689));688, 689
            image.Mutate(x => x.Resize(688, 689));//480x360
            image.Save(oriPath);

            var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);

            using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // thumbImage.Mutate(x => x.Resize(174, 89));
            thumbImage.Mutate(x => x.Resize(480, 380));
            thumbImage.Save(thumbFile);


            MediaFiles saveObj = new MediaFiles();
            saveObj.CreatedDate = DateTime.Now;
            saveObj.CreatedBy = userName;

            saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
            saveObj.RefID = RowID.Trim();
            saveObj.Deleted = false;
            saveObj.Actived = true;

            saveObj.PathFile = thumbFile.Replace(sPath, "");
            saveObj.FolderName = RowID.ToString();
            saveObj.TitleName = categoryName;
            saveObj.FileNameOri = uploadFile.FileName;
            saveObj.FileName = uniqueFileName;
            saveObj.FileExtension = uploadFile.ContentType;
            saveObj.FileSize = uploadFile.Length;
            ///saveObj.Downloads = uploadFile.Downloads;
            //saveObj.Sequence = uploadFile.Sequence;

            saveObj.UpdatedDate = DateTime.Now;
            saveObj.UpdatedBy = userName;
            context.Add(saveObj);

            context.SaveChanges();

            return true;

        }

        public static bool AddUploadGalleryFile(IFormFile uploadFile, string RowID, string categoryName, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;


            var uploadOriPath = Path.Combine(sPath, uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            uploadThumbnailPath = uploadThumbnailPath + @"\" + categoryName + @"\Thumbnail\" + RowID.ToString();

            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);
            if (!Directory.Exists(uploadThumbnailPath))
                Directory.CreateDirectory(uploadThumbnailPath);

            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);// + "_" + uploadFile.FileName;

            //using (var fileStream = new FileStream(oriFile, FileMode.Create))
            //{

            //    uploadFile.CopyTo(fileStream);
            //    fileStream.Flush();
            //}
            var oriFile = Path.Combine(uploadOriPath, uniqueFileName);
            var filetype = Path.GetExtension(uploadFile.FileName).ToLower();
            if (filetype.IndexOf("pdf") != -1)
            {
                using (var fileStream = new FileStream(oriFile, FileMode.Create))
                {
                    uploadFile.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            else if (filetype.IndexOf("jpg") != -1 || filetype.IndexOf("jpeg") != -1 || filetype.IndexOf("gif") != -1 || filetype.IndexOf("png") != -1 || filetype.IndexOf("bmp") != -1 || filetype.IndexOf("tiff") != -1)
            {
                using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                // image.Mutate(x => x.Resize(946, 689));//174,93   //89,89
                image.Mutate(x => x.Resize(688, 689));
                image.Save(oriFile);


                var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);
                using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                //thumbImage.Mutate(x => x.Resize(174, 89));
                thumbImage.Mutate(x => x.Resize(480, 360));//321
                thumbImage.Save(thumbFile);
            }
            else//word/excel/access,ptt,media,csv,text
            {
                using (var fileStream = new FileStream(oriFile, FileMode.Create))
                {
                    uploadFile.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }


            //if (File.Exists(oriFile))
            //    File.Delete(oriFile);



            MediaFiles saveObj = new MediaFiles();
            saveObj.CreatedDate = DateTime.Now;
            saveObj.CreatedBy = userName;

            saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
            saveObj.RefID = RowID.Trim();
            saveObj.Deleted = false;
            saveObj.Actived = true;

            saveObj.PathFile = oriFile.Replace(sPath, "");//save original
            saveObj.FolderName = RowID.ToString();
            saveObj.TitleName = categoryName;
            saveObj.FileNameOri = uploadFile.FileName;
            saveObj.FileName = uniqueFileName;
            saveObj.FileExtension = uploadFile.ContentType;
            saveObj.FileSize = uploadFile.Length;
            ///saveObj.Downloads = uploadFile.Downloads;
            //saveObj.Sequence = uploadFile.Sequence;

            saveObj.UpdatedDate = DateTime.Now;
            saveObj.UpdatedBy = userName;
            context.Add(saveObj);

            context.SaveChanges();

            return true;

        }
        public static bool AddUploadLandingFile(IFormFile uploadFile, string RowID, string categoryName, string userName)
        {
            if (uploadFile == null)
                return true;

            var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
            string uniqueFileName = null;

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
            var uploadThumbnailPath = uploadOriPath;

            uploadOriPath = uploadOriPath + @"\" + categoryName + @"\Original\" + RowID.ToString();
            uploadThumbnailPath = uploadThumbnailPath + @"\" + categoryName + @"\Thumbnail\" + RowID.ToString();

            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);
            if (!Directory.Exists(uploadThumbnailPath))
                Directory.CreateDirectory(uploadThumbnailPath);

            Thread.Sleep(1000);
            uniqueFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(uploadFile.FileName);
            // + "_" + uploadFile.FileName;
            //using (var fileStream = new FileStream(oriPath, FileMode.Create))
            //{
            //    uploadFile.CopyTo(fileStream);
            //    fileStream.Flush();
            //}

            var oriPath = Path.Combine(uploadOriPath, uniqueFileName);
            using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // image.Mutate(x => x.Resize(946, 689));688, 689
            image.Mutate(x => x.Resize(688, 689));//480x360
            image.Save(oriPath);

            var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);

            using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
            // thumbImage.Mutate(x => x.Resize(174, 89));
            thumbImage.Mutate(x => x.Resize(1200, 680));
            thumbImage.Save(thumbFile);


            MediaFiles saveObj = new MediaFiles();
            saveObj.CreatedDate = DateTime.Now;
            saveObj.CreatedBy = userName;

            saveObj.MediaCategoryID = ObjCat.MediaCategoryID;
            saveObj.RefID = RowID.ToString();
            saveObj.Deleted = false;
            saveObj.Actived = true;

            saveObj.PathFile = thumbFile.Replace(sPath, "");
            saveObj.FolderName = RowID.ToString();
            saveObj.TitleName = categoryName;
            saveObj.FileNameOri = uploadFile.FileName;
            saveObj.FileName = uniqueFileName;
            saveObj.FileExtension = uploadFile.ContentType;
            saveObj.FileSize = uploadFile.Length;
            ///saveObj.Downloads = uploadFile.Downloads;
            //saveObj.Sequence = uploadFile.Sequence;

            saveObj.UpdatedDate = DateTime.Now;
            saveObj.UpdatedBy = userName;
            context.Add(saveObj);

            context.SaveChanges();

            return true;

        }

        public static bool DeleteFile(string RefID, string categoryName, string UpdatedBy)
        {
            using (var _db = new CMSdbContext())
            {
                var ObjCat = _db.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();

                if (delFile)
                {
                    var Obj = context.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.RefID == RefID && o.Deleted == false).ToList();


                    foreach (var it in Obj)
                    {
                        try
                        {
                            string sFilePaht = Directory.GetCurrentDirectory() + it.PathFile;

                            if (File.Exists(sFilePaht))
                                File.Delete(sFilePaht);
                        }
                        catch { }
                        //Obj.Deleted = true;
                        //Obj.UpdatedBy = UpdatedBy;
                        //Obj.UpdatedDate = DateTime.Now;
                        context.Remove(it);
                        context.SaveChanges();
                    }

                    return true;
                }
                else
                {
                    var Obj = context.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.RefID == RefID && o.Deleted == false).ToList();
                    foreach (var it in Obj)
                    {
                        try
                        {
                            string sFilePaht = Directory.GetCurrentDirectory() + it.PathFile;



                            if (File.Exists(sFilePaht))
                                File.Delete(sFilePaht);
                        }
                        catch { }
                        it.Deleted = true;
                        it.UpdatedBy = UpdatedBy;
                        it.UpdatedDate = DateTime.Now;
                        context.Update(it);
                        context.SaveChanges();
                    }


                    return true;
                }
            }


        }

        public static Boolean Delete(int MediaFileID, string username)
        {
            using (var entity = new CMSdbContext())
            {
                MediaFiles Obj = entity.MediaFiles.Where(o => o.MediaFileID == MediaFileID).FirstOrDefault();
                Obj.Deleted = true; 
                entity.Update(Obj);
                entity.SaveChanges();

 
            }
            return true;
        }


        public static bool UpdateDownload(int MediaFileID)
        {
            using (var _db = new CMSdbContext())
            {
                var objFile = context.MediaFiles.Where(o => o.MediaFileID == MediaFileID).FirstOrDefault();
                if (objFile.Downloads == null)
                {
                    objFile.Downloads = 1;
                }
                else
                {
                    objFile.Downloads = objFile.Downloads + 1;
                }

                //objFile.UpdatedBy = UpdatedBy;
                //objFile.UpdatedDate = DateTime.Now;
                context.Update(objFile);
                context.SaveChanges();
                return true;
            }

        }

        public static MediaFiles RenderByFileName(string RowID, string categoryName)
        {
            using (var _db = new CMSdbContext())
            {
                var ObjCat = _db.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
                var objFile = _db.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.RefID == RowID && o.Deleted == false).FirstOrDefault();
                return objFile;
            }
        }
        public static MediaFiles GetThumbnailFileByRowID(string RowID, string categoryName)
        {
            using (var _db = new CMSdbContext())
            {
                var ObjCat = _db.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
                var objFile = _db.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.RefID == RowID && o.Deleted == false).FirstOrDefault();
                return objFile;
            }
        }
        public static List<MediaFiles> GetGalleryFilesByRowID(string RowID, string categoryName)
        {
            using (var _db = new CMSdbContext())
            {
                var ObjCat = _db.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
                var objFile = _db.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.RefID == RowID && o.Deleted == false).ToList();
                return objFile;
            }
        }
        public static MediaFiles GetMediaFilesById(int MediaFileID, string categoryName)
        {
            using (var _db = new CMSdbContext())
            {
                var ObjCat = context.MediaCategorys.Where(o => o.MediaCategoryName == categoryName).FirstOrDefault();
                var objFile = context.MediaFiles.Where(o => o.MediaCategoryID == ObjCat.MediaCategoryID && o.MediaFileID == MediaFileID && o.Deleted == false).FirstOrDefault();
              
                
                return objFile;
            }
        }
        public static string UploadFileOnly(IFormFile uploadFile)
        {
            if (uploadFile == null)
                return "";

            var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy) + "/Import";


            if (!Directory.Exists(uploadOriPath))
                Directory.CreateDirectory(uploadOriPath);


            string uniqueFileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadFile.FileName);

            var filePath = Path.Combine(uploadOriPath, uniqueFileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                uploadFile.CopyToAsync(fileStream);
            }

            return filePath;

        }
        public static MediaFiles UploadFile(IFormFile uploadFile, int MenuID, int RefID, int MediaCategoryID,int size, string Username, string newfilename = null)
        {
           // string p = Path.Combine(Directory.GetCurrentDirectory(), "LOG_ERROR/") + "log.txt";
           // File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": STARTED" + Environment.NewLine);
            try
            {
                string path = MenuID.ToString("D5");
                if (uploadFile == null)
                    return new MediaFiles();

                var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
                var Cate = MediaFilesData.Get.CategorysById(MediaCategoryID);
                if (Cate != null)
                {
                    path = Cate.MediaCategoryName + "/" + path;
                }
                uploadOriPath = uploadOriPath + @"/" + path;

                if (!Directory.Exists(uploadOriPath))
                    Directory.CreateDirectory(uploadOriPath);


                string uniqueFileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadFile.FileName);

                var filePath = Path.Combine(uploadOriPath, uniqueFileName);
                //using (var fileStream = new FileStream(filePath, FileMode.Create))
                //{
                //    uploadFile.CopyTo(fileStream);
                //   // File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": PROCESSING : Length (" + fileStream.Length.ToString() + ")" + Environment.NewLine);
                //}
                var oriFile = Path.Combine(uploadOriPath, uniqueFileName);
                var filetype = Path.GetExtension(uploadFile.FileName).ToLower();
                if (filetype.IndexOf("pdf") != -1)
                {
                    using (var fileStream = new FileStream(oriFile, FileMode.Create))
                    {
                        uploadFile.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                }
                else if (filetype.IndexOf("jpg") != -1 || filetype.IndexOf("jpeg") != -1 || filetype.IndexOf("gif") != -1 || filetype.IndexOf("png") != -1 || filetype.IndexOf("bmp") != -1 || filetype.IndexOf("tiff") != -1)
                {
                    using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                    // image.Mutate(x => x.Resize(946, 689));//174,93   //89,89
                    switch(size)
                    {
                        case 1:
                            image.Mutate(x => x.Resize(2880, 900));
                            break;
                        case 2:
                            image.Mutate(x => x.Resize(1600, 500));
                            break;
                        case 3:
                            image.Mutate(x => x.Resize(1600, 60));
                            break;
                        //case 2:
                        //    image.Mutate(x => x.Resize(1280, 400));
                        //    break;
                        case 4:
                            image.Mutate(x => x.Resize(1200, 700));
                            break;
                        case 5:
                            image.Mutate(x => x.Resize(882, 684));
                            break;
                        case 6:
                            image.Mutate(x => x.Resize(550, 360));
                            break;
                        case 7:
                            image.Mutate(x => x.Resize(150, 50));
                            break;
                        case 8:
                            image.Mutate(x => x.Resize(80, 80));
                            break;
                        case 9:
                            image.Mutate(x => x.Resize(50, 50));
                            break;
                        case 10:
                            image.Mutate(x => x.Resize(512, 512));
                            break;
                        case 11:
                            image.Mutate(x => x.Resize(138, 170));
                            break;

                    }
                    
                    image.Save(oriFile);
                    //var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);
                    //using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                    ////thumbImage.Mutate(x => x.Resize(174, 89));
                    //thumbImage.Mutate(x => x.Resize(480, 360));//321
                    //thumbImage.Save(thumbFile);
                }
                else//word/excel/access,ptt,media,csv,text
                {
                    using (var fileStream = new FileStream(oriFile, FileMode.Create))
                    {
                        uploadFile.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                }
                MediaFiles saveObj = new MediaFiles();

                using (var _map = new CMSdbContext())
                {
                    saveObj.CreatedDate = DateTime.Now;
                    saveObj.CreatedBy = string.IsNullOrEmpty(Username) ? "" : Username;
                    saveObj.MenuID = MenuID;

                    saveObj.MediaCategoryID = MediaCategoryID;
                    saveObj.RefID = RefID.ToString();
                    saveObj.Deleted = false;
                    saveObj.Actived = true;

                    saveObj.PathFile = path + "/" + uniqueFileName;
                    saveObj.FolderName = Cate.MediaCategoryName;
                    saveObj.TitleName = !string.IsNullOrEmpty(newfilename) ? newfilename : uploadFile.FileName;
                    saveObj.FileNameOri = uploadFile.FileName;
                    saveObj.FileName = uniqueFileName;
                    saveObj.FileExtension = uploadFile.ContentType;
                    saveObj.FileSize = uploadFile.Length;
                    saveObj.Downloads = 0;
                    saveObj.Sequence = _map.MediaFiles.Where(o => o.MenuID == MenuID).Max(o => o.Sequence) + 1;
                    saveObj.UpdatedDate = DateTime.Now; 
                    saveObj.UpdatedBy = string.IsNullOrEmpty(Username)?"": Username;
                    _map.Add(saveObj);

                    _map.SaveChanges();
                }
                // File.WriteAllText(p, "SUCCESSFULLY");
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": SUCCESSFULLY to path : " + saveObj.PathFile + Environment.NewLine);
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": END" + Environment.NewLine);

                return saveObj;
            }
            catch (Exception ex)
            {
                //File.WriteAllText(p, ex.Message.ToString());
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": ERROR message : " + ex.Message.ToString() + Environment.NewLine);
                // LogTransaction.SaveLog(new Guid(), "UPLOAD", LogTransaction.Get.ActivityType.Import, new Guid(), ":::", "", ex.Message.ToString());
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "", Username, ex.Message, "", "");

                MediaFiles saveObj = new MediaFiles();
                saveObj.FileName = ex.Message.ToString();
                return saveObj;

            }
        }
        public static MediaFiles UploadFileState(IFormFile uploadFile, int MenuID, string RefID, int MediaCategoryID, int size, string Username, string newfilename = null)
        {
            // string p = Path.Combine(Directory.GetCurrentDirectory(), "LOG_ERROR/") + "log.txt";
            // File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": STARTED" + Environment.NewLine);
            try
            {
                string path = MenuID.ToString("D5");
                if (uploadFile == null)
                    return new MediaFiles();

                var uploadOriPath = Path.Combine(Directory.GetCurrentDirectory(), uploadDirecotroy);
                var Cate = MediaFilesData.Get.CategorysById(MediaCategoryID);
                if (Cate != null)
                {
                    path = Cate.MediaCategoryName + "/" + path;
                }
                uploadOriPath = uploadOriPath + @"/" + path;

                if (!Directory.Exists(uploadOriPath))
                    Directory.CreateDirectory(uploadOriPath);


                string uniqueFileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadFile.FileName);

                var filePath = Path.Combine(uploadOriPath, uniqueFileName);
                //using (var fileStream = new FileStream(filePath, FileMode.Create))
                //{
                //    uploadFile.CopyTo(fileStream);
                //   // File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": PROCESSING : Length (" + fileStream.Length.ToString() + ")" + Environment.NewLine);
                //}
                var oriFile = Path.Combine(uploadOriPath, uniqueFileName);
                var filetype = Path.GetExtension(uploadFile.FileName).ToLower();
                if (filetype.IndexOf("pdf") != -1)
                {
                    using (var fileStream = new FileStream(oriFile, FileMode.Create))
                    {
                        uploadFile.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                }
                else if (filetype.IndexOf("jpg") != -1 || filetype.IndexOf("jpeg") != -1 || filetype.IndexOf("gif") != -1 || filetype.IndexOf("png") != -1 || filetype.IndexOf("bmp") != -1 || filetype.IndexOf("tiff") != -1)
                {
                    using var image = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                    // image.Mutate(x => x.Resize(946, 689));//174,93   //89,89
                    switch (size)
                    {
                        case 1:
                            image.Mutate(x => x.Resize(1920, 1080));
                            break;
                        case 2:
                            image.Mutate(x => x.Resize(1280, 400));
                            break;
                        case 3:
                            image.Mutate(x => x.Resize(1200, 700));
                            break;
                        case 4:
                            image.Mutate(x => x.Resize(882, 684));
                            break;
                        case 5:
                            image.Mutate(x => x.Resize(550, 360));
                            break;
                        case 6:
                            image.Mutate(x => x.Resize(80, 80));
                            break;
                        case 7:
                            image.Mutate(x => x.Resize(50, 50));
                            break;
                    }

                    image.Save(oriFile);
                    //var thumbFile = Path.Combine(uploadThumbnailPath, uniqueFileName);
                    //using var thumbImage = SixLabors.ImageSharp.Image.Load(uploadFile.OpenReadStream());
                    ////thumbImage.Mutate(x => x.Resize(174, 89));
                    //thumbImage.Mutate(x => x.Resize(480, 360));//321
                    //thumbImage.Save(thumbFile);
                }
                else//word/excel/access,ptt,media,csv,text
                {
                    using (var fileStream = new FileStream(oriFile, FileMode.Create))
                    {
                        uploadFile.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                }
                MediaFiles saveObj = new MediaFiles();

                using (var _map = new CMSdbContext())
                {
                    saveObj.CreatedDate = DateTime.Now;
                    saveObj.CreatedBy = Username;
                    saveObj.MenuID = MenuID;

                    saveObj.MediaCategoryID = MediaCategoryID;
                    saveObj.RefID = RefID;
                    saveObj.Deleted = false;
                    saveObj.Actived = false;

                    saveObj.PathFile = path + "/" + uniqueFileName;
                    saveObj.FolderName = Cate.MediaCategoryName;
                    saveObj.TitleName = !string.IsNullOrEmpty(newfilename) ? newfilename : uploadFile.FileName;
                    saveObj.FileNameOri = uploadFile.FileName;
                    saveObj.FileName = uniqueFileName;
                    saveObj.FileExtension = uploadFile.ContentType;
                    saveObj.FileSize = uploadFile.Length;
                    saveObj.Downloads = 0;
                    saveObj.Sequence = _map.MediaFiles.Where(o => o.MenuID == MenuID).Max(o => o.Sequence) + 1;
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    _map.Add(saveObj);

                    _map.SaveChanges();
                }
                // File.WriteAllText(p, "SUCCESSFULLY");
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": SUCCESSFULLY to path : " + saveObj.PathFile + Environment.NewLine);
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": END" + Environment.NewLine);

                return saveObj;
            }
            catch (Exception ex)
            {
                //File.WriteAllText(p, ex.Message.ToString());
                //File.AppendAllText(p, DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ": ERROR message : " + ex.Message.ToString() + Environment.NewLine);
                // LogTransaction.SaveLog(new Guid(), "UPLOAD", LogTransaction.Get.ActivityType.Import, new Guid(), ":::", "", ex.Message.ToString());
                LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "", Username, ex.Message, "", "");

                MediaFiles saveObj = new MediaFiles();
                saveObj.FileName = ex.Message.ToString();
                return saveObj;

            }
        }
    }
}
