﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class WBReplyData
    {
        public class Get
        {
            public static List<WebboardReply> GetReplyByTopic(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_ID", ID);
                return GetStoredMySql.GetAllStored<WebboardReply>("F_WBDETAIL", param).ToList();
            }
        }
        public class Post
        {
            public static WebboardReply ReplyAddOrUpdate(WebboardReply Obj, string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                WebboardReply saveObj = new WebboardReply();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.WBReplyID != 0)
                    {
                        mode = true;
                        saveObj = Map.WebboardReply.Find(Obj.WBReplyID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                    }
                    saveObj.WBPostID = Obj.WBPostID;
                    saveObj.Description = Obj.Description;
                    saveObj.TIPAddress = IPaddress;
                    saveObj.DeviceName = Obj.DeviceName;
                    saveObj.AliasName = Obj.AliasName;
                    saveObj.LangCode = Obj.LangCode;
                    saveObj.Deleted = Obj.Deleted;
               


                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = saveObj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                var ReplyCount = WBTopicData.Get.FReplyTopic(saveObj.WBPostID);
                return saveObj;
            }


            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardReply.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            var ReplyCount = WBTopicData.Get.FReplyTopic(Obj.WBPostID);
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardReply.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            var ReplyCount = WBTopicData.Get.FReplyTopic(Obj.WBPostID);
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
