﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
namespace Core
{
    public static class Get_Tm_Master_Data_Type
    {
        private static CMSdbContext Map = new CMSdbContext();
        public class Get
        {

            public enum MasterType
            {
                dimansion,
                Titlename,
                Information,
                Metrictype,
                measure,
                vision,
                mission,
                strategies,
                substrategies,
                subdimension,
                    PMQA
            }
            private static string CheckType(MasterType masterType)
            {
                string value = "";
                switch (masterType.ToString().ToUpper())
                {
                    case "DIMANSION" : { value = "ข้อมูลมิติตัวชี้วัด"; break; }
                    case "TITLENAME": { value = "ข้อมูลคำนำหน้าชื่อ"; break; }
                    case "INFORMATION": { value = "ข้อมูลวิธีการประเมินตัวชี้วัด"; break; }
                    case "METRICTYPE": { value = "ข้อมูลประเภทตัวชี้วัด"; break; }
                    case "MEASURE": { value = "ข้อมูลหน่วยวัด"; break; }
                    case "VISION": { value = "ข้อมูลวิสัยทัศน์"; break; }
                    case "MISSION": { value = "ข้อมูลพันธกิจ"; break; }
                    case "STRATEGIES": { value = "ข้อมูลยุทธศาสตร์"; break; }
                    case "SUBSTRATEGIES": { value = "ข้อมูลกลยุทธ์"; break; }
                    case "SUBDIMENSION": { value = "ข้อมูลด้าน"; break; }
                    case "PMQA": { value = "ข้อมูลหมวดหมู่"; break; }
                }
                return value;
            }
            public static TmMasterDataType ByName(MasterType masterType)
            {
                TmMasterDataType obj = new TmMasterDataType();
                var db  = new CMSdbContext();
                obj = db.TmMasterDataType.Where(o => o.MasterTypeName == CheckType(masterType)).FirstOrDefault();
                
                return obj;

            }
            public static List<TmMasterDataType> Lists()
            {
                var db = new CMSdbContext();
                return db.TmMasterDataType.Where(o => o.Deleted != true).OrderBy(o => o.MasterTypeName).ToList();

            }
        }
        public static class save
        {
            //public static TmMasterData AddOrUpdate(Tm_Master_Data_Model Items)
            //{
            //    TmMasterData Obj = new TmMasterData();
            //    Boolean mode = false;
            //    if (Items.MasterId != null)
            //    {
            //        mode = true;
            //        Obj = Map.TmMasterData.Find(Items.MasterId);
            //    }
            //    else
            //    {

            //        Obj.Deleted = false;
            //        Obj.CreateDate = DateTime.Now;
            //        Obj.CreateBy = "system";
            //    }
            //    Obj.Activated = true;
            //    Obj.MasterName = Items.MasterName;
            //    Obj.UpdateBy = "system";
            //    Obj.UpdateDate = DateTime.Now;
            //    if (mode)
            //    {
            //        Map.Update(Obj);
            //    }
            //    else
            //    {
            //        Map.Add(Obj);
            //    }
            //    Map.SaveChanges();

            //    return Obj;
            //}


        }
    }
}
