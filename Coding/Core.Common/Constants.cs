﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common
{
   public  class Constants
    {

        public class Config
        {
            public static string PicturePath = "~/BACKOFFICE";
            public static string PictureBack = "~/";
            public static string ServerPath = "http://localhost:6633/BACKOFFICE/";
            public static string PathImageBanner = "BANNER";
            public static string PathAttach = "ATTACH";
            public static string PathGallery = "GALLERY";
            public static string PathVDO = "MEDIAFILE";
            public static string PathOriginal = "ORIGINAL";
            public static string Host = "http://localhost:6633";
            public static string NameNoImage = "NoImage.jpg";
            public static string PathNoImage = "/img/NoImage.jpg";
            public static string PathMember = "Member";
            public static string fileupload = "fileupload";
            public static int pageSize = 10;

            public static string AdminRole = "79EFB532-3DA2-4C05-ABB6-F7977007A479";
            public static int Cookies_Menu = 1;

        }
    }
}
