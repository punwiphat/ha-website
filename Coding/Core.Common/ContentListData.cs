﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;

namespace Core.Common
{
    public static class ContentListData
    {
       
         public static IQueryable<Contents> SearchListByMenu(string searchString, Guid menuId)
        {
            CMSdbContext dbcontext = new CMSdbContext();
            if (!string.IsNullOrEmpty(searchString))
                    return dbcontext.Contents.Where(s => (s.TopicName.Contains(searchString) || s.Descriptions.Contains(searchString)) && s.MenuId == menuId && s.Deleted == false).OrderBy(o => o.UpdateDate);
                else
                    return dbcontext.Contents.Where(s => s.Deleted == false && s.MenuId == menuId).OrderByDescending(o => o.UpdateDate);
            
        }
        public static bool UpdateActiveStatus(Guid bannerId, string updateBy)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.Contents.Find(bannerId);

                Obj.Activated = !Obj.Activated;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                context.Update(Obj);
                context.SaveChanges();
                return true;
            }

        }

        public static Contents AddOrUpdate(Banners update)
        {
            using (var context = new CMSdbContext())
            {
                Contents saveObj = new Contents();
                Boolean mode = false;
                if (update.BannerId.ToString() != "00000000-0000-0000-0000-000000000000")
                {
                    mode = true;
                    saveObj = context.Contents.Find(update.BannerId);
                }
                else
                {

                    saveObj.Deleted = false;
                    saveObj.CreateDate = DateTime.Now;
                    saveObj.CreateBy = update.CreateBy;
                }
                saveObj.TopicName = update.TopicName;
                saveObj.MenuId = update.MenuId;
                saveObj.Descriptions = string.IsNullOrEmpty(update.Descriptions) ? "" : update.Descriptions.Replace("&nbsp;", " ");
                saveObj.ShortDescriptions = update.ShortDescriptions;
                //saveObj.UrlLink = update.UrlLink;
                //saveObj.DateInput = update.DateInput;
                //saveObj.EffectiveDate = update.EffectiveDate;
                //saveObj.EndDate = update.EndDate;
                ////saveObj.TypeSort = update.TypeSort;
                ////saveObj.StyleSort = update.StyleSort;
                ////saveObj.OpenComment = update.OpenComment;
                ////saveObj.OpenRating = update.OpenRating;
                //saveObj.MemberOnly = update.MemberOnly;
                //saveObj.Recommended = update.Recommended;
                //saveObj.Views = update.Views;
                //saveObj.Vote = update.Vote;
                //saveObj.Rating = update.Rating;
                //saveObj.Sequence = update.Sequence;
                saveObj.Activated = update.Activated;


                saveObj.UpdateDate = DateTime.Now;
                saveObj.UpdateBy = update.CreateBy;
                if (mode)
                {
                    context.Update(saveObj);
                }
                else
                {
                    context.Add(saveObj);
                }
                context.SaveChanges();

                return saveObj;
            }
        }

        public static Contents GetBannerById(Guid Id)
        {
            CMSdbContext context = new CMSdbContext();
            return context.Contents.Find(Id);
            
        }

        public static bool DeleteContentList(Guid Id, string updateBy)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.Contents.Find(Id);
                Obj.Deleted = true;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                context.Update(Obj);
                context.SaveChanges();
                return true;
            }
        }

        public static List<Contents> GetContentList()
        {
            CMSdbContext context = new CMSdbContext();
            var objResult = context.Contents.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdateDate).ToList();

                return objResult;
            
        }
       

    }
}
