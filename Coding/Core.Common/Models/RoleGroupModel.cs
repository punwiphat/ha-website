﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RoleGroupModel
    {
        public int RoleGroupID { get; set; }
        public string RoleGroupName { get; set; }
        public bool Actived { get; set; }
    }
}
