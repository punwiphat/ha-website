﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Tm_Master_Data_Model
    {
        public Guid? MasterId { get; set; }
        public Guid MasterTypeId { get; set; }
        public Guid? MasterRootId { get; set; }
        public Guid? MasterParentId { get; set; }

        public string MasterName { get; set; }
        public int? Sequence { get; set; }
        public bool Activated { get; set; } 
        public int? Level { get; set; }
        public string MasterCode { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
    }
}
