﻿using System;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
namespace SMEs.Common.Models
{
    public class UserSingleton
    {
        public const string SessionUserKey = "UserCMS_";
        private static readonly UserSingleton _mySingletonServiceInstance;
        UserSingleton() { }
        Guid _userId ;
        string _userName = "";
        string _password = "";
        string _departmentCode = "";
        string _firstName = "";
        string _lastName = "";
        Guid _roleGroupId ;
        string _email="";
        public Guid UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }
        public string Username
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }
        public Guid Role_Group_Id
        {
            get
            {
                return _roleGroupId;
            }
            set
            {
                _roleGroupId = value;
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        public static UserSingleton GetInstance()
        {
           _mySingletonServiceInstance = (UserSingleton) HttpContext.Session.Get("EmployeeDetails");
           if (_mySingletonServiceInstance == null)
            {
                _mySingletonServiceInstance = new UserSingleton();
                HttpContext.Session.SetComplexData("UserData", _mySingletonServiceInstance);
            }
            return _mySingletonServiceInstance;
        }
        public static UserSingleton GetCurrentSingleton()
        {
            UserSingleton oSingleton;

            if (null == HttpContext.Session[SESSION_SINGLETON])
            {
                oSingleton = new UserSingleton();
                HttpContext.Session[SESSION_SINGLETON] = oSingleton;
            }
            else
            {
                oSingleton = (UserSingleton)HttpContext.Session[SESSION_SINGLETON];
            }
            return oSingleton;
        }
    }
   
}
public static class SessionExtensions
{
    public static T GetComplexData<T>(this ISession session, string key)
    {
        var data = session.GetString(key);
        if (data == null)
        {
            return default(T);
        }
        return JsonConvert.DeserializeObject<T>(data);
    }

    public static void SetComplexData(this ISession session, string key, object value)
    {
        session.SetString(key, JsonConvert.SerializeObject(value));
    }
}