﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Common.Models
{
    public class ChangPasswordModel
    {
        public int UserID { get; set; }

        public string UserName { get; set; }

        [Display(Name = "CurrentPassword"), Required(ErrorMessage = "กรุณากรอกข้อมูล")]
        public string CurrentPassword { get; set; }

        [Display(Name = "NewPassword"), Required(ErrorMessage = "กรุณากรอกข้อมูล")]
        public string NewPassword { get; set; }

        [Display(Name = "ConfirmPassword"), Required(ErrorMessage = "กรุณากรอกข้อมูล")]
        public string ConfirmPassword { get; set; }
    }
}
