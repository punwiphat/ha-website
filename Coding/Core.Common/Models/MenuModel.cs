﻿using Core.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class MenuModel
    {
        public MenuModel() { RoleType = "N"; }
        public int ID { get; set; }
        public int? MENU_BACK_ID { get; set; }
        public string menu_name_back { get; set; }
        public int? levels { get; set; }
        public string RoleType { get; set; }
        public List<Role> TempRole { get; set; }
        public string CountItem { get; set; }
    }
}
