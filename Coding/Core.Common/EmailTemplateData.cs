﻿using System.Linq;
using Core.Common;
using Core.DAL.Models;
using MySqlConnector;

namespace Core
{
    public static class EmailTemplateData
    {
        public class Get
        {
            public static EmailTemplate ByID(TemplateNum templateID,string LangID)
            {
                int tem = (int)templateID;

                EmailTemplate Obj = new EmailTemplate();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_TemplateTypeID", tem);
                param[1] = new MySqlParameter("p_LangCode", LangID);
                Obj = GetStoredMySql.GetAllStored<EmailTemplate>("B_EMAIL_TEMPLATE", param).FirstOrDefault();
                //using (var Map = new CMSdbContext())
                //{
                //    Obj = Map.EmailTemplate.Where(o => o.LangCode == LangID && o.TemplateTypeID == tem && o.Actived==true && o.Deleted != true).FirstOrDefault();
                //}
                return Obj;
            }
        }
    }
}
