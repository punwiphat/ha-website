﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Core.Common;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class ComplaintTypeData
    {
        public class Get
        {
            public static List<ComplaintType> All()
            {
                List<ComplaintType> Obj = new List<ComplaintType>();
               
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.ComplaintType.Where(o => o.Deleted != true).OrderByDescending(o=>o.UpdatedDate).ToList();
                   
                }
                return Obj.OrderByDescending(o=>o.CreatedDate).ToList();
            }

            public static List<ComplaintType> ByActive()
            {
                List<ComplaintType> Obj = new List<ComplaintType>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.ComplaintType.Where(o => o.Actived == true && o.Deleted != true).ToList();
                }
                return Obj;
            }
            public static ComplaintType ByID(int ID)
            {
                ComplaintType Obj = new ComplaintType();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.ComplaintType.Find(ID);
                }
                return Obj;
            }
            
        }
        public static class Post
        {
            public static ComplaintType AddOrUpdate(ComplaintType Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                ComplaintType Obj = new ComplaintType();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.ComplaintType.Find(Items.ID);
                        }
                        else
                        {
                            Obj.Deleted = false;
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                        }
                        Obj.Actived = Items.Actived;
                        Obj.NameTH = Items.NameTH;
                        Obj.NameEN = Items.NameEN;
                        Obj.EmailNotification = Items.EmailNotification;
                        Obj.IsDirector = Items.IsDirector;


                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username,"", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }


            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ComplaintType.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ComplaintType.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
