﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core.Common
{
  public  static  class commonbase
    { 
        public static string connect()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")).Build();
           return configuration.GetConnectionString("MSSqlConnection");
        }
    }
}
