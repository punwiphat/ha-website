﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Core.Common;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class SettingEmailData
    {
        public class Get
        {
            public static SettingEmail All( )
            {
                var Obj =  new SettingEmail();
                using (var Map = new CMSdbContext())
                {
                    var Temp = Map.SettingEmail.FirstOrDefault();
                    Obj = Temp != null ? Temp : Obj;
                }
                return Obj;
            }
        }
        public static class save
        {
            public static SettingEmail AddOrUpdate(SettingEmail Items, string Username,string IPaddress, string Browser)
            {
                SettingEmail Obj = new SettingEmail();
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Temp = Map.SettingEmail.FirstOrDefault();
                        Obj = Temp != null ? Temp : Obj;
                        Obj.EmailAlias = Items.EmailAlias;
                        Obj.Emailname = Items.Emailname;
                        Obj.Smtp = Items.Smtp;
                        Obj.Port = Items.Port;
                        if (!string.IsNullOrEmpty(Items.Password))
                        {
                            Obj.Password = Encryption.Encrypt(Items.Password);
                        }
                        Obj.UseDefaultCredentials = Items.UseDefaultCredentials;
                        Obj.EnabledSSL = Items.EnabledSSL;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (Temp != null)
                        {
                            STATE = LogTransaction.Get.ActivityType.Edit;
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.EmailId, STATE, "ตั้งค่าอีเมล์", Username,"", IPaddress, Browser);
                    return Obj;
                }
                catch(Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "ตั้งค่าอีเมล์", Username,ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
        }
    }
}
