﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
namespace Core.Common
{

    public static class PostsBKData
    {
       
        public static List<PostsTag> GetPostsTag()
        {
            CMSdbContext context = new CMSdbContext();
            var objResult = context.PostsTag.ToList();
            return objResult;
        }
        public static List<Posts> GetNewsList(Guid menuId)
        {
            CMSdbContext context = new CMSdbContext();
            var objResult = context.Posts.Where(o => o.MenuId == menuId && o.Deleted == false).ToList();
            return objResult;
        }
        public static Posts GetPostsById(Guid postId)
        {
            CMSdbContext context = new CMSdbContext();
            return context.Posts.Where(o => o.PostId == postId).FirstOrDefault();
        }
        public static List<Posts> GetPostsList(Guid menuId)
        {

            CMSdbContext context = new CMSdbContext();
            var objResult = context.Posts.Where(o => o.MenuId == menuId && o.Deleted == false).OrderByDescending(o => o.UpdateDate).ToList();

            return objResult;


        }
        public static IQueryable<Posts> SearchPostsListByTopicName(Guid menuId, string searchString)
        {
            CMSdbContext context = new CMSdbContext();
            if (!string.IsNullOrEmpty(searchString))
                return context.Posts.Where(s => (s.TopicName.Contains(searchString) || s.Descriptions.Contains(searchString) || s.Keywords.Contains(searchString)) && s.MenuId == menuId && s.Deleted == false).OrderByDescending(o => o.UpdateDate);
            else
                return context.Posts.Where(s => s.MenuId == menuId && s.Deleted == false).OrderByDescending(o => o.UpdateDate);

        }
        public static Posts AddPost(Posts update)
        {
            CMSdbContext context = new CMSdbContext();
            Posts saveObj = new Posts();



            saveObj.Deleted = false;
            saveObj.CreateDate = DateTime.Now;
            saveObj.CreateBy = update.CreateBy;
            saveObj.PostId = update.PostId;


            saveObj.TopicName = update.TopicName;
            saveObj.PostRelatedId = update.PostRelatedId;
            saveObj.MenuId = update.MenuId;
            saveObj.Descriptions = string.IsNullOrEmpty(update.Descriptions) ? "" : update.Descriptions.Replace("&nbsp;", " ");
            saveObj.ShortDescriptions = update.ShortDescriptions;
            saveObj.UrlLink = update.UrlLink;
            saveObj.Keywords = update.Keywords;

            if (update.DateInput_str != null)
                saveObj.DateInput = update.DateInput_str.todateEn();
            if (update.EffectiveDate_str != null)
                saveObj.EffectiveDate = update.EffectiveDate_str.todateEn();
            if (update.EndDate_str != null)
                saveObj.EndDate = update.EndDate_str.todateEn();

            //saveObj.TypeSort = update.TypeSort;
            //saveObj.StyleSort = update.StyleSort;
            //saveObj.OpenComment = update.OpenComment;
            //saveObj.OpenRating = update.OpenRating;
            saveObj.MemberOnly = update.MemberOnly;
            //saveObj.Recommended = update.Recommended;
            //saveObj.Views = update.Views;
            //saveObj.Vote = update.Vote;
            //saveObj.Rating = update.Rating;
            //saveObj.Sequence = update.Sequence;
            saveObj.Activated = update.Activated; ;


            saveObj.UpdateDate = DateTime.Now;
            saveObj.UpdateBy = update.CreateBy;
            context.Add(saveObj);

            context.SaveChanges();

            return saveObj;
        }
        public static Posts UpdatePost(Posts update)
        {
            CMSdbContext context = new CMSdbContext();
            Posts saveObj = new Posts();
            saveObj = context.Posts.Find(update.PostId);

            saveObj.TopicName = update.TopicName;
            saveObj.PostRelatedId = update.PostRelatedId;
            // saveObj.MenuId = update.MenuId;
            saveObj.Descriptions = string.IsNullOrEmpty(update.Descriptions) ? "" : update.Descriptions.Replace("&nbsp;", " ");
            //saveObj.ShortDescriptions = update.ShortDescriptions;
            saveObj.UrlLink = update.UrlLink;
            saveObj.Keywords = update.Keywords;

            if (update.DateInput_str != null)
                saveObj.DateInput = update.DateInput_str.todateEn();
            if (update.EffectiveDate_str != null)
                saveObj.EffectiveDate = update.EffectiveDate_str.todateEn();
            if (update.EndDate_str != null)
                saveObj.EndDate = update.EndDate_str.todateEn();
            //saveObj.TypeSort = update.TypeSort;
            //saveObj.StyleSort = update.StyleSort;
            //saveObj.OpenComment = update.OpenComment;
            //saveObj.OpenRating = update.OpenRating;
            saveObj.MemberOnly = update.MemberOnly;
            //saveObj.Recommended = update.Recommended;
            //saveObj.Views = update.Views;
            //saveObj.Vote = update.Vote;
            //saveObj.Rating = update.Rating;
            //saveObj.Sequence = update.Sequence;
            saveObj.Activated = update.Activated;
            saveObj.UpdateBy = update.UpdateBy;
            saveObj.UpdateDate = DateTime.Now;

            context.Update(saveObj);
            context.SaveChanges();

            return saveObj;
        }
        public static bool DeletePost(Guid MasterId, string updateBy)
        {
            CMSdbContext context = new CMSdbContext();
            var Obj = context.Posts.Find(MasterId);
            Obj.Deleted = true;
            Obj.UpdateBy = updateBy;
            Obj.UpdateDate = DateTime.Now;
            context.Update(Obj);
            context.SaveChanges();
            return true;
        }


        public static bool UpdateActiveStatus(Guid postId, string updateBy)
        {
            CMSdbContext context = new CMSdbContext();
            var Obj = context.Posts.Find(postId);

            Obj.Activated = !Obj.Activated;
            Obj.UpdateBy = updateBy;
            Obj.UpdateDate = DateTime.Now;
            context.Update(Obj);
            context.SaveChanges();
            return true;

        }
        public static bool UpdateMemberStatus(Guid postId, string updateBy)
        {
            CMSdbContext context = new CMSdbContext();
            var Obj = context.Posts.Find(postId);

            Obj.MemberOnly = !Obj.MemberOnly;
            Obj.UpdateBy = updateBy;
            Obj.UpdateDate = DateTime.Now;
            context.Update(Obj);
            context.SaveChanges();
            return true;

        }
        public static bool UpdateViewCount(Guid postId)
        {
            CMSdbContext context = new CMSdbContext();
            var Obj = context.Posts.Find(postId);

            if (Obj.Views == null)
            {
                Obj.Views = 1;

            }
            else
            {
                Obj.Views = Obj.Views + 1;
            }


            context.Update(Obj);
            context.SaveChanges();
            return true;
        }
    }
}
