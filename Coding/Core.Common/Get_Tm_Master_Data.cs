﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
namespace Core
{
   public static class Get_Tm_Master_Data
    {
         public class Get
        {
            public static List<TmMasterData> ByType(Get_Tm_Master_Data_Type.Get.MasterType masterType, string value = null)
            {
                List<TmMasterData> Obj;

                CMSdbContext Map = new CMSdbContext();
                    if (!string.IsNullOrEmpty(value))
                    {
                        Obj =  Map.TmMasterData.Where(o => o.MasterTypeId == Get_Tm_Master_Data_Type.Get.ByName(masterType).MasterTypeId && o.Deleted != true && o.MasterName.Contains(value)).OrderBy(c => c.Sequence).ThenBy(n => n.CreateDate).ToList();
                    }
                    else
                    {
                        Obj = Map.TmMasterData.Where(o => o.MasterTypeId == Get_Tm_Master_Data_Type.Get.ByName(masterType).MasterTypeId && o.Deleted != true).OrderBy(c => c.Sequence).ThenBy(n => n.CreateDate).ToList();
                    }
                
                return Obj;
            }

            public static List<TmMasterData> ByPMQA()
            {
                List<TmMasterData> Obj;

                CMSdbContext Map = new CMSdbContext();
                //if (!string.IsNullOrEmpty(value))
                //{
                //    Obj = Map.TmMasterData.Where(o => o.MasterTypeId == Get_Tm_Master_Data_Type.Get.ByName(masterType).MasterTypeId && o.Deleted != true && o.MasterName.Contains(value)).ToList();
                //}
                //else
                //{
                    Obj = Map.TmMasterData.Where(o => o.MasterTypeId == Guid.Parse("237ABC7A-9670-4FB7-93C8-BD97BB5D7143") && o.Activated == true && o.Deleted != true).OrderBy(o=>o.Sequence).ToList();
                //}

                return Obj;
            }

            public static TmMasterData ByID(Guid ID)
            {
                TmMasterData Obj = new TmMasterData();
                CMSdbContext Map = new CMSdbContext();
                Obj =  Map.TmMasterData.Find(ID);
                
                return Obj;
            }
            public static List<TmMasterData> Lists(string value = null)
            {
                List<TmMasterData> Obj;

                CMSdbContext Map = new CMSdbContext();
                if (!string.IsNullOrEmpty(value))
                {
                    Obj = Map.TmMasterData.Where(o => o.Deleted != true && o.MasterName.Contains(value)).ToList();
                }
                else
                {
                    Obj = Map.TmMasterData.Where(o => o.Deleted != true).ToList();
                }

                return Obj;
            }
        }
        public static class save
        {
            public static void UpdateSeq(TmMasterData Items, string username)
            {
                using (var entity = new CMSdbContext())
                {
                    Items.UpdateBy = username;
                    Items.UpdateDate = DateTime.Now;
                    entity.Update(Items);
                    entity.SaveChanges();
                }
            }

            public static TmMasterData AddOrUpdate(Tm_Master_Data_Model Items, string username)
            {
                TmMasterData Obj = new TmMasterData();
                using (var Map = new CMSdbContext())
                {
                    Boolean mode = false;
                    if (Items.MasterId != null)
                    {
                        mode = true;
                        Obj = Map.TmMasterData.Find(Items.MasterId);
                    }
                    else
                    {
                        Obj.Deleted = false;
                        Obj.CreateDate = DateTime.Now;
                        Obj.CreateBy = username;
                        Obj.MasterTypeId = Items.MasterTypeId;
                    }
                    Obj.Activated = true;
                    Obj.MasterName = Items.MasterName;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    if (mode)
                    {
                        Map.Update(Obj);
                    }
                    else
                    {
                        Map.Add(Obj);
                    }
                    Map.SaveChanges();
                }
                return Obj;
            }
            public static Boolean Status(Guid MasterId,Boolean Active, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.TmMasterData.Find(MasterId);
                    Obj.Activated = Active;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }

            public static Boolean Delete(Guid MasterId, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.TmMasterData.Find(MasterId);
                    Obj.Deleted = true;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }

        }

    }
}
