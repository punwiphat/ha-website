﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.CodeAnalysis;
using MySqlConnector;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public class MediaFilesData
    {
        public class Post
        {
            public MediaFiles Add(MediaFiles Item, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Item.MediaFileID != 0)
                        {
                            mode = true;
                            Item = Map.MediaFiles.Where(o => o.MediaFileID == Item.MediaFileID).FirstOrDefault();
                        }
                        else
                        {
                            Item.Deleted = false;
                            Item.CreatedDate = DateTime.Now;
                        }
                        Item.Actived = Item.Actived;
                        Item.UpdatedDate = DateTime.Now;
                        if (mode)
                        {
                            Map.Update(Item);
                        }
                        else
                        {
                            Map.Add(Item);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Item.MediaFileID, STATE, "SAVE MEDIA FILE", Item.UpdatedBy, "", IPaddress, Browser);
                    return Item;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "SAVE MEDIA FILE", Item.UpdatedBy, ex.Message, IPaddress, Browser);
                    return Item;
                }
            }
            public static bool UpdateStatus(int ID,string RefID,string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.MediaFiles.Find(ID);
                        Obj.RefID = RefID;
                        Obj.Actived = true;
                        Obj.Deleted = false;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Add, "DELETE MEDIA FILE", Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "DELETE MEDIA FILE", Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static Boolean DeleteByFile(int Id, string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.MediaFiles.Find(Id);
                        Obj.Deleted = true;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Id, LogTransaction.Get.ActivityType.Add, "DELETE MEDIA FILE", Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "DELETE MEDIA FILE", Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static Boolean DeleteByIDMenuCategory(int MenuID ,int CategoryID,string RefID, string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.MediaFiles.Where(o => o.MenuID == MenuID && o.MediaCategoryID == CategoryID && o.RefID == RefID);
                        foreach (var item in Obj)
                        {
                            item.Deleted = true;
                            item.UpdatedBy = Username;
                            item.UpdatedDate = DateTime.Now;
                            Map.Update(item);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(CategoryID, LogTransaction.Get.ActivityType.Add, "DELETE BY ID MENU CATEGORY MEDIA FILE", Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(CategoryID, LogTransaction.Get.ActivityType.Error, "DELETE BY ID MENU  CATEGORY MEDIA FILE", Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static Boolean DeleteByCategory(int CategoryId, string Username, string IpAddress, string Browser)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.MediaFiles.Where(o=>o.MediaCategoryID == CategoryId);
                        foreach(var item in Obj)
                        {
                            item.Deleted = true;
                            item.UpdatedBy = Username;
                            item.UpdatedDate = DateTime.Now;
                            Map.Update(item);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(CategoryId, LogTransaction.Get.ActivityType.Add, "DELETE BY CATEGORY MEDIA FILE", Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(CategoryId, LogTransaction.Get.ActivityType.Error, "DELETE BY CATEGORY MEDIA FILE", Username, ex.Message, IpAddress, Browser);
                    return false;
                }
            }
            public static bool CountView(int p_MediaFileID)
            {
                
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_MediaFileID", p_MediaFileID);
                bool Obj = GetStoredMySql.Executenon("F_FILE_COUNT", param);
                return Obj;
                
            }
        }
        public class Get
        {
            public static MediaCategorys CategorysById(int ID)
            {
                using (var context = new CMSdbContext())
                {
                    return context.MediaCategorys.Find(ID);
                }
            }
            public List<MediaFiles> GetAll()
            {
                List<MediaFiles> Obj = new List<MediaFiles>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaFiles.Where(o=>o.Actived==true && o.Deleted!=true).ToList();
                }
                return Obj;
            }
            public MediaFiles GetById(int Id)
            {
                MediaFiles Obj = new MediaFiles();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaFiles.Find(Id);
                }
                return Obj;
            }
            public List<MediaFiles> GetByRef(string RefId)
            {
                List<MediaFiles> Obj = new List<MediaFiles>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaFiles.Where(o=>o.RefID == RefId && o.Actived == true && o.Deleted != true).ToList();
                }
                return Obj;
            }
            public static List<MediaFiles> ByCategoryMenu(int CategoryID,int MenuID)
            {
                List<MediaFiles> Obj = new List<MediaFiles>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaFiles.Where(o => o.MenuID== MenuID && o.MediaCategoryID== CategoryID && o.Actived == true && o.Deleted != true).ToList();
                }
                return Obj;
            }
            public static List<MediaFiles> FListMedia(int MenuID,int CategoryID, int RefID)
            {
                List<MediaFiles> Obj = new List<MediaFiles>();
                MySqlParameter[] param = new MySqlParameter[3];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_RefID", RefID);
                param[2] = new MySqlParameter("p_CategoryID", CategoryID);
                Obj = GetStoredMySql.GetAllStored<MediaFiles>("F_MEDIAFILE", param).ToList();
                return Obj;
            }

            public static List<MediaFiles> FListMediaMenuCategory(int MenuID, int CategoryID)
            {
                List<MediaFiles> Obj = new List<MediaFiles>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_CategoryID", CategoryID);
                Obj = GetStoredMySql.GetAllStored<MediaFiles>("F_MEDIAFILE_BY_MENU_CATEGORY", param).ToList();
                return Obj;
            }
        }

    }
}
