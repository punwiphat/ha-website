﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class RSSFeedData
    {
        public class Get
        {
            public static RSSFeed MainRSSFeedById(int ID)
            {
                RSSFeed Obj = new RSSFeed();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.RSSFeed.Where(o=>o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static RSSFeed MainRSSFeedByMenuId(int MenuID)
            {
                RSSFeed Obj = new RSSFeed();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.RSSFeed.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<RSSFeedDetail> ByMenu(int MenuID,string LangCode)
            {
                List<RSSFeedDetail> Obj = new List<RSSFeedDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<RSSFeedDetail>("RSSFeedlist_bymenu", param).ToList();
                return Obj;
            }
            public static List<RSSFeedDetail> RSSFeedById(int ID,string LangCode = null)
            {
                try
                {
                    
                    List<RSSFeedDetail> Obj = new List<RSSFeedDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_RSSFeedID", ID);
                    Obj = GetStoredMySql.GetAllStored<RSSFeedDetail>("RSSFeedlist_byid", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            RSSFeedDetail o = new RSSFeedDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();

                    return Obj;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static List<RSSFeed> RSSFeedByActive(int MenuID)
            {
                List<RSSFeed> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.RSSFeed.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<RSSFeedDetail> RSSFeedByCateActive(int CateID, string LangCode)
            {
                List<RSSFeedDetail> Obj = new List<RSSFeedDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_CateID", CateID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<RSSFeedDetail>("F_RSSFEED", param).ToList();
                return Obj;
            }
            public static List<RSSFeed> SearchListByMenu(string searchString, int MenuID)
            {
                IQueryable<RSSFeedDetail> Obj;
                CMSdbContext context = new CMSdbContext();

                List<RSSFeed> RSSFeed = new List<RSSFeed>();
                RSSFeed = context.RSSFeed.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();

                return RSSFeed;
            }
            public static List<RSSFeed> GetRSSFeedList()
            {

                CMSdbContext context = new CMSdbContext();
                var objResult = context.RSSFeed.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;


            }
        }
        public class Post
        {
            public static RSSFeed AddOrUpdate(RSSFeed Obj,string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                RSSFeed saveObj = new RSSFeed();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.RSSFeed.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        saveObj.MenuID = Obj.MenuID;
                        var lmax = Map.RSSFeed.Where(o=>o.MenuID == Obj.MenuID);
                        var max = lmax.Count()>0? lmax.Max(o => o.Sequence)+1:1;
                        saveObj.Sequence = max > 0 ? max : 1;
                    }
                    saveObj.CategoryID = Obj.CategoryID;
                    saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                    saveObj.EndDate = Obj.EndDate_str.todateEn();
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = Obj.Actived;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<RSSFeedDetail> DetailAddOrUpdate(List<RSSFeedDetail> item, string Username, string IPaddress, string Browser)
            {

                try
                {
                    bool mode = false;
                    List<RSSFeedDetail> saveLst = new List<RSSFeedDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            RSSFeedDetail saveObj = new RSSFeedDetail();
                            if (Obj.RSSFeedDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.RSSFeedDetail.Find(Obj.RSSFeedDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.RSSFeedID = Obj.RSSFeedID;
                            }
                            saveObj.RSSFeedName = Obj.RSSFeedName;
                            saveObj.RSSFeedLink = Obj.RSSFeedLink;
                            saveObj.RSSFeedDescription = Obj.RSSFeedDescription;
                            saveObj.MenuID = Obj.MenuID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                        
                    }
                    return saveLst;
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RSSFeed.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RSSFeed.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser,string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.RSSFeed.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
