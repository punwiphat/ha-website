﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;

namespace Core
{
    public static class MemberData
    {
        public static class Get
        {
            public static Members By_Email(string Email)
            {
                Members Obj = new Members();
                var Map = new CMSdbContext();
                Obj = Map.Members.Where(o => o.Email.Trim() == Email.Trim() && o.Actived == true && o.Deleted == false).FirstOrDefault();
                return Obj;
            }

            public static List<ddlmaster> TypeMember()
            {
                List<ddlmaster> Obj = new List<ddlmaster>();
                Obj.Add(new ddlmaster() { ID = 1, Name = "สมาชิกตลอดชีพ" });
                Obj.Add(new ddlmaster() { ID = 2, Name = "สมาชิกรายปี" });
                return Obj;

            }
            public static List<Members> GetAll(String value = null)
            {
                List<Members> obj = new List<Members>();
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        obj = Map.Members.Where(o =>o.Deleted == false && (o.FirstName.Contains(value) || o.LastName.Contains(value))).ToList();
                    }
                    else
                    {
                        obj = Map.Members.Where(o =>o.Deleted == false).ToList();
                    }
                    foreach (var item in obj)
                    {
                        item.TypeMemberName = item.TypeMemberID == 1 ? "สมาชิกตลอดชีพ" : (item.TypeMemberID == 2 ? "สมาชิกรายปี" : "-");
                    }
                }
                return obj.OrderByDescending(o => o.CreatedDate).ToList();
            }


            public static bool EmailValidation(String Email, int MemberID)
            {
                List<Members> Obj = new List<Members>();
                var Map = new CMSdbContext();
                Obj = Map.Members.Where(o => o.Email.ToUpper() == Email.ToUpper() && o.MemberID != MemberID).ToList();

                return Obj.Count() == 0 ? false : true;
            }

            public static Members ByID(int MemberID)
            {
                Members Obj = new Members();
                var Map = new CMSdbContext();

                Obj = Map.Members.Find(MemberID);
                return Obj;
            }
            public static Members ByUserName(string UserName)
            {
                Members Obj = new Members();
                var Map = new CMSdbContext();
                Obj = Map.Members.Where(o => o.Username == UserName).FirstOrDefault();
                return Obj;
            }
            public static Members CheckLogin(string username, string password)
            {
                var Map = new CMSdbContext();
                Members Obj = Map.Members.Where(o => o.Username.Trim().ToUpper() == username.Trim().ToUpper() && o.Password == Encryption.Encrypt(password) && o.Actived == true && o.Deleted != true).FirstOrDefault();
                return Obj;
            }

            public static bool CheckDupplicateByIdentityNo(string IdentityNo)
            {
                bool status = false;
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Members.Where(o => o.Deleted != true && o.IdentityNo == IdentityNo).FirstOrDefault();
                    if (Obj != null)
                        status = true;
                }
                return status;
            }
            public static bool CheckDupplicateByUserName(string UserName)
            {
                bool status = false;
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Members.Where(o => o.Deleted != true && o.Username.ToLower() == UserName.ToLower()).FirstOrDefault();
                    if (Obj != null)
                        status = true;
                }
                return status;
            }
        }
        public static class Post
        {
            public static bool AddOrUpdate_password(int MemberID, string oldpassword, String password, string username)
            {
                Members Obj = new Members();
                bool status = false;

                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Members.Find(MemberID);
                    if (Obj.Password == Encryption.Encrypt(oldpassword))
                    {
                        Obj.Password = Encryption.Encrypt(password);
                        Obj.UpdatedBy = username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                        status = true;
                    }

                }
                return status;
            }

            public static Members UpdateActivated(string key,string lanId = "TH")
            {
                if (string.IsNullOrEmpty(key))
                    return null;
                Members Obj = new Members();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Members.Where(o => o.KeyActivated.ToLower() == key.ToLower()).FirstOrDefault();
                    if(Obj == null)
                        return null;

                    Obj.Msg = "";
                    if (Obj.Activated == true){
                        Obj.Msg = lanId == "TH" ? TextError.TH.TXT_VERIFIED : TextError.EN.TXT_VERIFIED;
                    }
                    else
                    {
                        Obj.Activated = true;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    return Obj;
                }
            }

            public static Members UnUpdateActivated(Members member)
            {
                Members Obj = new Members();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Members.Find(member.MemberID);
                    Obj.Activated = false;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }

            private static Random random = new Random();
            private static string RandomString(int length)
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                return new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
            public static Members resetpassword(int MemberID, string username)
            {
                Members Obj = new Members();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Members.Find(MemberID);
                    Obj.Password = Encryption.Encrypt(RandomString(8));
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }

            public static Members AddOrUpdate_profile(Members Items, string username)
            {
                Members Obj = new Members();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Members.Find(Items.MemberID);
                   // Obj.TitleNameCode = Items.TitleNameCode;
                    Obj.FirstName = Items.FirstName;
                    //Obj.MiddleName = Items.MiddleName;
                    Obj.LastName = Items.LastName;
                    Obj.Email = Items.Email;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }

            public static Members AddOrUpdate(Members Items, string username, string IpAddress, string Browser)
            {

                Members Obj = new Members();
                using (var Map = new CMSdbContext())
                {
                    Boolean mode = false;
                    if (Items != null && Items.MemberID != 0)
                    {
                        mode = true;
                        Obj = Map.Members.Find(Items.MemberID);
                    }
                    else
                    {
                        Obj.Deleted = false;
                        Obj.CreatedDate = DateTime.Now;
                        Obj.KeyActivated = Guid.NewGuid().ToString();
                        Obj.CreatedBy = username;
                        Obj.Username = Items.Username;
                        Obj.Password = Encryption.Encrypt((string.IsNullOrEmpty(Items.Password) ? "12345678" : Items.Password));
                    }
                    //Obj.TitleNameCode = Items.TitleNameCode;
                    Obj.IdentityNo = Items.IdentityNo;
                    Obj.FirstName = Items.FirstName;
                    Obj.MiddleName = Items.MiddleName;
                    Obj.LastName = Items.LastName;
                    Obj.TitleID = Items.TitleID;
                   // Obj.RoleGroupID = Items.RoleGroupID;
                    Obj.Email = Items.Email;
                    //Obj.MobileNo = Items.MobileNo;
                    //Obj.DepartmentAdmin = Items.DepartmentAdmin;
                    //Obj.Position_Id = Items.Position_Id;
                    Obj.TypeMemberID = Items.TypeMemberID;
                    Obj.Tel = Items.Tel;
                    Obj.Position = Items.Position;
                    Obj.Department = Items.Department;
                    Obj.Address = Items.Address;
                    Obj.AddressSend = Items.AddressSend;
                    Obj.Actived = Items.Actived;
                    Obj.Activated = Items.Activated;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    if (mode)
                    {
                        Map.Update(Obj);
                    }
                    else
                    {
                        Map.Add(Obj);
                    }
                    Map.SaveChanges();
                }
                return Obj;
            }
            public static Boolean Delete(int MemberID, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Members.Find(MemberID);
                    Obj.Deleted = true;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }


            public static Boolean Remove(int MemberID)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Members.Find(MemberID);
                    Map.Remove(Obj);
                    Map.SaveChanges();
                }
                return true;
            }



            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Members.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static Boolean Status(int MemberID, Boolean Active, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.Members.Find(MemberID);
                    Obj.Actived = Active;
                    Obj.UpdatedBy = username;
                    Obj.UpdatedDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Members.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

        }
    }
}
