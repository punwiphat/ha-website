﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
namespace Core
{
    public static class Get_Webboard
    {
         public class Get
        {
            public static WebboardTopic Topic(Guid? WebboardTopicId)
            {
                WebboardTopic Obj = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    Obj =  Map.WebboardTopic.Find(WebboardTopicId);
                }
                return Obj;
            }
            public static List<WebboardReservedWord> WebboardReservedWord_list(string value = null)
            {
                List<WebboardReservedWord> Obj;
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        Obj = Map.WebboardReservedWord.Where(o => o.Deleted == false && (o.ReservedWord.Contains(value) || o.ReplaceWord.Contains(value) )).OrderByDescending(o => o.CreateDate).ToList();
                    }
                    else
                    {
                        Obj = Map.WebboardReservedWord.Where(o => o.Deleted == false).OrderByDescending(o => o.CreateDate).ToList();

                    }
                }
                return Obj;
            }
            public static WebboardReservedWord ReservedWord(Guid ReservedWordId)
            {
                WebboardReservedWord Obj = new WebboardReservedWord();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.WebboardReservedWord.Find(ReservedWordId);
                }
                return Obj;
            }

            public static List<WebboardTopic> Topic_list(string value = null)
            {
                List<WebboardTopic> Obj;
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        Obj = Map.WebboardTopic.Where(o => o.Deleted == false && (o.WebboardTopicName.Contains(value) || o.WebboardTopicDecriptions.Contains(value) || o.Keywords.Contains(value))).OrderByDescending(o => o.CreateDate).ToList();
                    }
                    else
                    {
                        Obj = Map.WebboardTopic.Where(o => o.Deleted == false).OrderByDescending(o => o.CreateDate).ToList();

                    }
                }
                return Obj;
            }

            public static List<WebboardTopic> Topic_list_by_reports(string value = null)
            {
                List<WebboardTopic> obj;
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        obj =  Map.WebboardTopic.Where(o => o.Deleted == false && o.Reports == true && (o.WebboardTopicName.Contains(value) || o.WebboardTopicDecriptions.Contains(value) || o.Keywords.Contains(value))).OrderByDescending(o => o.CreateDate).ToList();
                    }
                    else
                    {
                        obj = Map.WebboardTopic.Where(o => o.Deleted == false && o.Reports == true).OrderByDescending(o => o.CreateDate).ToList();
                     }
                }
                return obj;


            }


            public static List<WebboardTopic> Topic_list_Create_by(string CreateBy, string value)
            {
                List<WebboardTopic> Obj;
                using (var Map = new CMSdbContext())
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        Obj=  Map.WebboardTopic.Where(o => o.Deleted == false && o.CreateBy == CreateBy && (o.WebboardTopicName.Contains(value) || o.WebboardTopicDecriptions.Contains(value) || o.Keywords.Contains(value))).OrderByDescending(o => o.CreateDate).ToList();
                    }
                    else
                    {
                        Obj = Map.WebboardTopic.Where(o => o.Deleted == false && o.CreateBy == CreateBy).OrderByDescending(o => o.CreateDate).ToList();
                    }

                }
                return Obj;
            }
            public static List<WebboardReservedWord> Reserved()
            {
                List<WebboardReservedWord> obj = new List<WebboardReservedWord>(0);
                using (var Map = new CMSdbContext())
                {
                    obj =  Map.WebboardReservedWord.Where(o => o.Activated == true && o.Deleted == false).OrderByDescending(o => o.CreateDate).ToList();

                }
                return obj;
            }
            public static List<WebboardReply> reply_list(Guid WebboardTopicId)
            {
                List<WebboardReply> obj = new List<WebboardReply>();
                using (var Map = new CMSdbContext())
                {
                    obj =  Map.WebboardReply.Where(o => o.WebboardTopicId == WebboardTopicId && o.Activated == true && o.Deleted == false).ToList();

                }
                return obj;

            }
        } 


        public static class save
        {
            public static void Topic_Count(Guid WebboardTopicId)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardTopic.Find(WebboardTopicId);
                    Obj.Views = Obj.Views + 1;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
            }

            public static Boolean Topic_reports(Guid WebboardTopicId,string Description, string IpAddress, string DeviceName, string UserName)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardTopic.Find(WebboardTopicId);
                    Obj.Reports = true;
                    Map.WebboardTopic.Update(Obj);


                    WebboardReports webboardReports = new WebboardReports();
                    webboardReports.Webboard_Topic_Id = Obj.WebboardTopicId;
                    webboardReports.Description = Description;
                    webboardReports.IpAddress = IpAddress;
                    webboardReports.DeviceName = DeviceName;
                    webboardReports.CreateDate = DateTime.Now;
                    webboardReports.CreateBy = UserName;
                    Map.WebboardReports.Add(webboardReports);
                    Map.SaveChanges();
                }
                return true;
            }

            public static Boolean Topic_Approve(Guid WebboardTopicId, string UserName)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardTopic.Find(WebboardTopicId);
                    Obj.Reports = false;
                    Obj.UpdateBy = UserName;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }


            public static Boolean Topic_Delete(Guid WebboardTopicId,string UserName)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardTopic.Find(WebboardTopicId);
                    Obj.Deleted = true;
                    Obj.UpdateBy = UserName;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }

            public static WebboardReservedWord Webboard_Reserved_Word_AddOrUpdate(WebboardReservedWord items  , string UserName)
            {
                WebboardReservedWord Obj = new WebboardReservedWord();
                using (var Map = new CMSdbContext())
                {
                    Boolean mode = false;
                    if (items.ReservedWordId != null)
                    {
                        mode = true;
                        Obj = Map.WebboardReservedWord.Find(items.ReservedWordId);
                    }
                    else
                    {
                        Obj.Deleted = false;
                        Obj.CreateDate = DateTime.Now;
                        Obj.CreateBy = UserName; 
                    }
                    Obj.Activated = items.Activated; 
                    Obj.ReservedWord = items.ReservedWord;
                    Obj.ReplaceWord = items.ReplaceWord; 
                    Obj.UpdateBy = UserName;
                    Obj.UpdateDate = DateTime.Now;
                    if (mode)
                    {
                        Map.Update(Obj);
                    }
                    else
                    {
                        Map.Add(Obj);
                    }
                    Map.SaveChanges();
                }
                return Obj;
            }


            public static WebboardTopic Topic_AddOrUpdate(  Guid?     WebboardTopicId ,  string WebboardTopicName , string WebboardTopicDecriptions , string Keywords,  bool Islock ,  bool Activated,string IpAddress, string DeviceName,string UserName)
            {
                WebboardTopic Obj = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    Boolean mode = false;
                    if (WebboardTopicId != null)
                    {
                        mode = true;
                        Obj = Map.WebboardTopic.Find(WebboardTopicId);
                    }
                    else
                    {
                        Obj.Deleted = false;
                        Obj.CreateDate = DateTime.Now;
                        Obj.CreateBy = UserName;
                        Obj.Views = 0;
                        Obj.Reply = 0;
                        Obj.Reports = false;
                    }
                    Obj.Activated = true;
                    //Obj.WebboardCategoryId = Items.WebboardCategoryId;
                    Obj.WebboardTopicName = WebboardTopicName;
                    Obj.WebboardTopicDecriptions = WebboardTopicDecriptions;
                    //    Obj.Pin = Items.Pin;  
                    Obj.IpAddress = IpAddress;
                    Obj.DeviceName = DeviceName;
                    Obj.Islock = Islock;
                    Obj.Keywords = Keywords;
                    //  Obj.IsAlias = Items.IsAlias;
                    //  Obj.AliasName = Items.AliasName;
                    //   Obj.TypeSort = Items.TypeSort;
                    //  Obj.StyleSort = Items.StyleSort; 

                    Obj.UpdateBy = UserName;
                    Obj.UpdateDate = DateTime.Now;
                    if (mode)
                    {
                        Map.Update(Obj);
                    }
                    else
                    {
                        Map.Add(Obj);
                    }
                    Map.SaveChanges();
                }
                return Obj;
            }
            public static WebboardReply Webboard_Reply_Add(Guid WebboardTopicId, string ReplyDescriptions, string IpAddress, string DeviceName,string UserName)
            {
                WebboardReply Obj = new WebboardReply();
                using (var Map = new CMSdbContext())
                {
                    Obj.WebboardTopicId = WebboardTopicId;
                    Obj.Deleted = false;
                    Obj.CreateDate = DateTime.Now;
                    Obj.CreateBy = UserName;
                    Obj.Activated = true;
                    Obj.ReplyDescriptions = ReplyDescriptions;
                    Obj.IpAddress = IpAddress;
                    Obj.DeviceName = DeviceName;
                    Obj.UpdateBy = UserName;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Add(Obj);
                    Map.SaveChanges();
                }
                return Obj;
            }


            public static Boolean Revers_Status(Guid MasterId, Boolean Active, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardReservedWord.Find(MasterId);
                    Obj.Activated = Active;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }

            public static Boolean Revers_Delete(Guid MasterId, string username)
            {
                using (var Map = new CMSdbContext())
                {
                    var Obj = Map.WebboardReservedWord.Find(MasterId);
                    Obj.Deleted = true;
                    Obj.UpdateBy = username;
                    Obj.UpdateDate = DateTime.Now;
                    Map.Update(Obj);
                    Map.SaveChanges();
                }
                return true;
            }


        }
    }
}
