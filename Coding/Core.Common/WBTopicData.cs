﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class WBTopicData
    {
        public class Get
        {
            public static WebboardTopic ByID(int ID)
            {
                WebboardTopic objResult = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopic.Find(ID);
                }
                return objResult;
            }
            public static List<WebboardTopic> Lists(string LangCode)
            {
                List<WebboardTopic> objResult = new List<WebboardTopic>();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopic.Where(o => o.LangCode == LangCode && o.Deleted == false && o.Actived == true && o.Approved == true && o.Forbidden != true).OrderByDescending(o => o.UpdatedDate).ToList();
                    if (objResult.Count > 0)
                    {
                        var type = Map.WebboardTopicType.ToList();
                        if (type.Count > 0)
                        {
                            foreach (var item in objResult)
                            {
                                var temp = type.Where(o => o.ID == item.TopicTypeID).FirstOrDefault();
                                item.TopicTypeName = temp != null ? temp.NameTH : "";
                            }
                        }

                    }
                }
                return objResult;
            }
            public static List<WebboardTopic> WaittingLists(string LangCode)
            {
                List<WebboardTopic> objResult = new List<WebboardTopic>();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopic.Where(o => o.LangCode == LangCode && o.Deleted == false && o.Actived == true && o.Approved != true).OrderByDescending(o => o.UpdatedDate).ToList();
                    if (objResult.Count > 0)
                    {
                        var type = Map.WebboardTopicType.ToList();
                        if (type.Count > 0)
                        {
                            foreach (var item in objResult)
                            {
                                var temp = type.Where(o => o.ID == item.TopicTypeID).FirstOrDefault();
                                item.TopicTypeName = temp != null ? temp.NameTH : "";
                            }
                        }
                    }
                }
                return objResult;
            }
            public static List<WebboardTopic> FListByCategory(int CateID, string LangCode)
            {
                List<WebboardTopic> Obj = new List<WebboardTopic>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_CateID", CateID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<WebboardTopic>("F_WBTOPIC", param).ToList();
                return Obj;
            }
            public static List<WebboardCategory> FCateByLang(string LangCode)
            {
                List<WebboardCategory> Obj = new List<WebboardCategory>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<WebboardCategory>("F_WBCATE", param).ToList();
                return Obj;
            }

            public static List<WebboardCategory> CategoryWebboardByLang(string LangCode)
            {
                List<WebboardCategory> Obj = new List<WebboardCategory>();
                using (var Map = new CMSdbContext())
                {
                    Obj = (from m in Map.Category
                               where m.MenuID == 20 && m.Actived == true && m.Deleted != true
                               orderby m.Sequence
                               select new
                               {
                                   WBCategoryID = m.ID,
                                   TopicName = string.IsNullOrEmpty(LangCode) ? m.NameTH : ((LangCode.ToUpper() == "TH")? m.NameTH : m.NameEN),
                                   Descriptions = "",
                                   Sequence = 0,
                                   Activated = m.Actived,
                                   Deleted = m.Deleted,
                                   CreatedDate = m.CreatedDate,
                                   CreatedBy = m.CreatedBy,
                                   UpdatedDate = m.UpdatedDate,
                                   UpdatedBy = m.UpdatedBy
                               }).ToList_list<WebboardCategory>();
                }
                return Obj;
            }

            public static WebboardTopic FTopicByReply(int ID, bool? Active = null)
            {
                WebboardTopic objResult = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopic.Where(o => o.ID == ID).FirstOrDefault();
                }

                List<WebboardReply> Obj = new List<WebboardReply>();
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_ID", ID);
                if (Active == null)
                    Obj = GetStoredMySql.GetAllStored<WebboardReply>("F_WBDETAIL", param).ToList();
                else
                    Obj = GetStoredMySql.GetAllStored<WebboardReply>("F_WBDETAIL", param).Where(o => o.Actived == Active).ToList();
                objResult.lstReply = Obj;
                return objResult;
            }
            public static bool FViewTopic(int ID)
            {
                WebboardTopic objResult = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    objResult = Map.WebboardTopic.Find(ID);
                    objResult.Views = objResult.Views + 1;
                    Map.Update(objResult);
                    Map.SaveChanges();
                }
                return true;
            }
            public static bool FReplyTopic(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_WBPostID", ID);
                var Obj = GetStoredMySql.Executenon("F_WBREPLY_COUNT", param);
                return Obj;
            }
        }
        public class Post
        {
            public static WebboardTopic AddOrUpdate(WebboardTopic Obj, string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                WebboardTopic saveObj = new WebboardTopic();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.WebboardTopic.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.Deleted = false;
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                        saveObj.TopicTypeID = (Obj.TopicTypeID == 0) ? 1 : Obj.TopicTypeID;
                    }
                    saveObj.WBCategoryID = Obj.WBCategoryID;
                    saveObj.Topic = Obj.Topic;
                    saveObj.Description = Obj.Description;
                    saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                    saveObj.EndDate = Obj.EndDate_str.todateEn();
                    //saveObj.Pined = Obj.Pined;
                    saveObj.Keywords = Obj.Keywords;
                    saveObj.TIPAddress = Obj.TIPAddress;
                    saveObj.DeviceName = Obj.DeviceName;
                    saveObj.AliasName = Obj.AliasName;
                    saveObj.LangCode = Obj.LangCode;


                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Actived = true;
                    saveObj.ReplyCount = 0;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                       Map.Update(saveObj); 
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardTopic.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardTopic.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Approved(int ID, Boolean Approved, string RejectReason, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardTopic.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Approved = Approved;
                            Obj.Approver = Username;
                            Obj.RejectReason = RejectReason;
                            Obj.ApprovedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Forbidden(int ID, Boolean Forbidden, string ForbiddenReason, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardTopic.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Forbidden = Forbidden;
                            Obj.ForbiddenReason = ForbiddenReason;
                            Obj.Stopper = Username;
                            Obj.ForbiddenDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.WebboardTopic.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Sequence = Seq;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }


        }
    }
}
