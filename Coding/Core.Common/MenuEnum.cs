using System;
namespace Core.Common
{
    public enum MenuList
    {
        BannerMain = 6,
        BannerMini = 8,
        News = 14,
        EventsSWU = 525,
        Policy = 66,
        WebLink = 45,
        Complaint = 26,
        Landing = 5,
        Popup = 7,
        QuickMenu = 8,
        ChannelComment = 413,
        SuccessContact = 462,
        SuccessComplaint = 463,
        SuccessChannelComment = 464,
        SuccessRegister = 493,
        SuccessRegisterVerify = 494
    }
    public enum TemplateNum
    {
        Register = 1,
        ForgotPassword = 2,
        Complaint = 3,
        B_Register =4,
        B_ForgotPassword = 5,
        B_ResetPassword = 6,
        F_ContactUSPublic = 7,
        B_ContactUSStaff = 8,
        F_RegisterAfterSave = 9,
        F_RegisterAfterActivated = 10,
        F_ComplaintAfterSave = 11

    }
    public static class CookieName
    {
        public static string SocialList = "SocialList";
        public static string WebLink = "WebLink";
        public static string BannerMain = "BannerMain";
        public static string Graphic = "Graphic";
        public static string ServicesMain = "ServicesMain";
        public static string Services2 = "Services2";
        public static string ServicesOur = "ServicesOur";
        public static string MenuMain = "MenuMain";
        public static string MenuMainF = "MenuMainF";
        public static string Submenu = "Submenu";
        public static string Sitemap = "Sitemap";
        public static string Category = "Category";
        public static string WBCategory = "WBCategory";
        public static string ContactUS = "ContactUS";
        public static string Popup = "Popup";
        public static string PolicyMenu = "PolicyMenu";
        public static string QuickMenu = "QuickMenu";
        
        //public static string MenuMain = "MenuMain";

    }
}
