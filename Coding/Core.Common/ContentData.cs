﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core.Common
{
    public static class ContentData
    {
        public class Get
        {
            public static Content MainByMenuId(int MenuID)
            {
                Content Obj = new Content();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Content.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj??new Content();
            }

            public static List<ContentDetail> ByMenu(int MenuID, string LangCode)
            {
                List<ContentDetail> Obj = new List<ContentDetail>();
                using (var Map = new CMSdbContext())
                {
                    Obj = (from Main in Map.Content
                           join Contents in Map.ContentDetail on Main.ID equals Contents.ContentID
                           join Media in Map.MediaFiles on
                           new
                           {
                                Key1 = Contents.ContentDetailID.ToString()
                               ,Key2 = Main.MenuID
                           } equals
                           new
                           {
                                Key1 = Media.RefID
                               ,Key2 = Media.MenuID
                           }
                           into _it
                           from Media in _it.DefaultIfEmpty()
                           where Main.Deleted != true
                           && Media.Deleted != true
                           && Main.MenuID == MenuID
                           select new
                           {
                               Contents.ContentID
                               ,Contents.ContentDetailID
                               ,Contents.Title
                               ,Contents.Descriptions
                               ,Contents.HtmlContent
                               ,UpdatedDate = Main.UpdatedDate
                               ,UpdatedBy = Main.UpdatedBy
                               ,FileID = Media != null ? Media.MediaFileID : 0
                               ,FileName = Media != null ? Media.FileName : ""
                               ,FilePath = Media != null ? Media.PathFile : ""
                               ,OriginalFileName = Media != null ? Media.FileNameOri : ""
                           }).ToList_list<ContentDetail>();


                }
                return Obj;
            }
            public static ContentDetail ContentDetsilByContentId(int ID, string LangCode = "")
            {
                List<ContentDetail> Obj = new List<ContentDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ContentID", ID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<ContentDetail>("B_CONTENT", param).ToList();
                return Obj.FirstOrDefault();
            }
            public static List<ContentDetail> ContentById(int ID, string LangCode = "")
            {
                List<ContentDetail> Obj = new List<ContentDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_ContentID", ID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<ContentDetail>("B_CONTENT", param).ToList();
                foreach (var item in Obj)
                {
                    item.lstFiles = MediaFilesData.Get.ByCategoryMenu(4, item.MenuID).ToList();
                }
                if (Obj.Count != 2)
                {
                    List<Languages> lang = new List<Languages>();
                    if (Obj.Count == 1 && String.IsNullOrEmpty(Obj.FirstOrDefault().LangCode))
                        lang = LanguageData.GetByActive().Where(o => o.LanguageCode != Obj.FirstOrDefault().LangCode).ToList();
                    else
                        lang = LanguageData.GetByActive();
                    if (!string.IsNullOrEmpty(LangCode))
                        lang = lang.Where(o => o.LanguageCode == LangCode).ToList();
                    foreach (var l in lang)
                    {
                        ContentDetail o = new ContentDetail();
                        o.LangCode = l.LanguageCode;
                        Obj.Add(o);
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                }
                
                return Obj;
            }


            public static List<ContentDetail> FByMenu(int MenuID, string LangCode)
            {
                List<ContentDetail> Obj = new List<ContentDetail>();
                MySqlParameter[] param = new MySqlParameter[2];
                param[0] = new MySqlParameter("p_MenuID", MenuID);
                param[1] = new MySqlParameter("p_LangCode", LangCode);
                Obj = GetStoredMySql.GetAllStored<ContentDetail>("F_CONTENT", param).ToList();
                return Obj;
            }
        }
        public class Post
        {

            public static Content AddOrUpdate(Content Obj, string Username, string IPaddress, string Browser)
            {
                try
                {
                    string description = "";
                    bool mode = false;
                    Content saveObj = new Content();
                    using (var Map = new CMSdbContext())
                    {
                        if (Obj.ID != 0)
                        {
                            mode = true;
                            saveObj = Map.Content.Find(Obj.ID);
                        }
                        else
                        {
                            saveObj.MenuID = Obj.MenuID;
                            saveObj.Deleted = false;
                            saveObj.CreatedDate = DateTime.Now;
                            saveObj.CreatedBy = Username;
                        }
                        saveObj.Actived = Obj.Actived;

                        saveObj.UpdatedDate = DateTime.Now;
                        saveObj.UpdatedBy = Username;
                        saveObj.Menuname = Obj.Menuname;
                        description = "ID : " + saveObj.ID + " Title : " + Obj.lstContent.Where((ContentDetail o) => o.LangCode == "TH").FirstOrDefault().Title;

                        if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                            
                        }
                        Map.SaveChanges();
                    }
                    description = $"Content ID : {saveObj.ID} Content name : {saveObj.lstContent.FirstOrDefault(b => b.LangCode == "TH")?.Title}";
                     LogTransaction.SaveLog(saveObj.ID, mode ? LogTransaction.Get.ActivityType.Edit : LogTransaction.Get.ActivityType.Add, saveObj.Menuname, Username, description, IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                }
                    return Obj;
            }
            public static List<ContentDetail> DetailAddOrUpdate(List<ContentDetail> item, string Username, string IPaddress, string Browser)
            {
                try {
                    string text = "";
                    bool mode = false;
                List<ContentDetail> saveLst = new List<ContentDetail>();
                using (var Map = new CMSdbContext())
                {
                    foreach (var Obj in item)
                    {
                        ContentDetail saveObj = new ContentDetail();
                        if (Obj.ContentDetailID != 0)
                        {
                            mode = true;
                            saveObj = Map.ContentDetail.Find(Obj.ContentDetailID);
                        }
                        else
                        {
                                saveObj.ContentID = Obj.ContentID;
                                saveObj.LangCode = Obj.LangCode;
                            }
                        saveObj.Title = Obj.Title;
                        saveObj.Descriptions = Obj.Descriptions;
                        saveObj.HtmlContent = Obj.HtmlContent;
                        saveObj.ContentID = Obj.ContentID;
                        saveObj.MenuID = Obj.MenuID;
                        saveObj.SUB_GUID = Obj.SUB_GUID;
                            if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                        }
                            saveLst.Add(saveObj);
                            text = "ID : " + Obj.ContentID + " Detail ID : " + Obj.ContentDetailID + " Title : " + Obj.Title;
                            LogTransaction.SaveLog(Obj.ContentDetailID, (!mode) ? LogTransaction.Get.ActivityType.AddDetail : LogTransaction.Get.ActivityType.EditDetail, Obj.Menuname, Username, text, IPaddress, Browser);
                        }
                    Map.SaveChanges();
                }
                return saveLst;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(item.FirstOrDefault()?.ContentID ?? 0, LogTransaction.Get.ActivityType.Error, item.FirstOrDefault()?.Menuname ?? string.Empty, Username, ex.Message, IPaddress, Browser);
                    return null;
                }
            }
            public static bool UpdateActiveStatus(int ContentID, string Username)
            {
                try { 
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Content.Find(ContentID);
                        Obj.Actived = !Obj.Actived;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    return true;
                }
                catch(Exception ex)
                {
                    return false;
                }
            }

            public static bool Delete(int ContentID, string Username)
            {
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Content.Find(ContentID);
                        Obj.Deleted = true;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

        }

    }
}
