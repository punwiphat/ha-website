﻿using System;
using System.Linq;
using Core.DAL.Models;
using static Core.Common.LogTransaction.Get;

namespace Core.Common
{
    public static class MenuDisplayData
    {
        public static class Get
        {
            public static MenuDisplay ByMenu(int MenuID)
            {
                MenuDisplay Obj = new MenuDisplay();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MenuDisplay.Where(o => o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj?? new MenuDisplay();
            }
        }
        public static class Post
        {
            public static MenuDisplay AddOrUpdate(MenuDisplay Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                MenuDisplay Obj = new MenuDisplay();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.MenuDisplay.Find(Items.ID);
                        }
                        Obj.SortingBy = Items.SortingBy;
                        Obj.SortingType = Items.SortingType;
                        Obj.MenuID = Items.MenuID;
                        Obj.CategoryID = Items.CategoryID;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, "MENUID : "+Obj.MenuID+" SORTTING", Username, "", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "MENUID : " + Obj.MenuID + " SORTTING", Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }


            public static Boolean UpdateSortingType(int MenuDisplayID, int MenuID, Boolean SortingType, string Username, string IpAddress, string Browser)
            {
                MenuDisplay Obj = new MenuDisplay();
                string action_detail = "";
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.MenuDisplay.Find(MenuDisplayID);
                        if (Obj != null)
                        {
                            if (SortingType != Obj.SortingType)
                            {
                                action_detail = "เปลี่ยนลักษณะแสดงผลจาก " + (SortingType ? "DESC" : "ASC") + " เป็น " + (SortingType ? "ASC" : "DESC");
                                Obj.SortingType = SortingType;
                                Obj.UpdatedBy = Username;
                                Obj.UpdatedDate = DateTime.Now;
                                Map.Update(Obj);
                                Map.SaveChanges();
                            }
                            else
                            {
                                action_detail = "ไม่เปลี่ยนลักษณะแสดงผล";
                            }
                        }
                        else
                        {
                            Obj = new MenuDisplay();
                            Obj.MenuID = MenuID;
                            Obj.SortingType = SortingType;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Add(Obj);
                            Map.SaveChanges();
                            action_detail = "เพิ่มการเปลี่ยนลักษณะแสดงผล "+ (SortingType ? "ASC" : "DESC");
                        }

                    }
                    LogTransaction.SaveLog(MenuDisplayID, LogTransaction.Get.ActivityType.Edit, "MENUID : " + Obj.MenuID + " SortingType", Username, action_detail, IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "MENUID : " + Obj.MenuID + " SortingType", Username, ex.Message, IpAddress, Browser);
                    return false;

                }
            }
            public static Boolean UpdateSortingBy(int MenuDisplayID,int MenuID, int SortingBy, string Username, string IpAddress, string Browser)
            {
                MenuDisplay Obj = new MenuDisplay();
                string action_detail = "";
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.MenuDisplay.Find(MenuDisplayID);
                        if (Obj != null)
                        {
                            if (SortingBy != Obj.SortingBy)
                            {
                                action_detail = "เปลี่ยนลักษณะแสดงผลจาก " + CheckSortingBy(Obj.SortingBy.Value) + " เป็น " + CheckSortingBy(SortingBy);
                                Obj.SortingBy = SortingBy;
                                Obj.UpdatedBy = Username;
                                Obj.UpdatedDate = DateTime.Now;
                                Map.Update(Obj);
                                Map.SaveChanges();
                            }
                            else
                            {
                                action_detail = "ไม่เปลี่ยนลักษณะแสดงผล";
                            }
                        }
                        else
                        {
                            Obj = new MenuDisplay();
                            Obj.MenuID = MenuID;
                            Obj.SortingBy = SortingBy;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Add(Obj);
                            Map.SaveChanges();
                            action_detail = "เพิ่มการเปลี่ยนลักษณะแสดงผล";
                        }

                    }
                    LogTransaction.SaveLog(MenuDisplayID, LogTransaction.Get.ActivityType.Edit, "MENUID : " + Obj.MenuID + " SortingBy", Username, action_detail, IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "MENUID : " + Obj.MenuID + " SortingBy", Username, ex.Message, IpAddress, Browser);
                    return false;

                }
            }
            public static string CheckSortingBy(int SortingBy)
            {
                switch(SortingBy)
                {
                    case 1: return "เรียงจากวันที่แก้ไข";
                    case 2: return "เรียงจากชื่อ"; 
                    default: return "กำหนดเอง";
                }
            }
            //public static Boolean Delete(int MenuDisplayID, string Username, string IpAddress, string Browser)
            //{
            //    MenuDisplay Obj = new MenuDisplay();
            //    try
            //    {
            //        using (var Map = new CMSdbContext())
            //        {
            //            Obj = Map.MenuDisplay.Find(MenuDisplayID);
            //            Obj.Deleted = true;
            //            Obj.UpdatedBy = Username;
            //            Obj.UpdatedDate = DateTime.Now;
            //            Map.Update(Obj);
            //            Map.SaveChanges();
            //        }
            //        LogTransaction.SaveLog(MenuDisplayID, LogTransaction.Get.ActivityType.Edit, Obj.Menuname, Username, "", IpAddress, Browser);
            //        return true;
            //    }
            //    catch (Exception ex)
            //    {
            //        LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IpAddress, Browser);
            //        return false;
            //    }
            //}
        }

    }
}
