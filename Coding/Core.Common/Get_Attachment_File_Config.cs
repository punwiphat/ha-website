﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
namespace Core
{
    public static class Get_Attachment_File_Config
    {
         public class Get
        {

            public static Attachment_File_Config All()
            {
                using (var Map = new CMSdbContext())
                {
                    var Temp = Map.Attachment_File_Config.FirstOrDefault();

                    if (Temp != null)
                    {
                        return Temp;
                    }
                    else
                    {

                        return new Attachment_File_Config();
                    }
                }
            }
        }
        public static class save
        {
            public static Attachment_File_Config AddOrUpdate(Attachment_File_Config Items)
            {
                Attachment_File_Config Obj = new Attachment_File_Config();
                using (var Map = new CMSdbContext())
                {
                    Boolean mode = false;
                    var Temp = Map.Attachment_File_Config.FirstOrDefault();

                    if (Temp != null)
                    {
                        Obj = Temp;
                    }

                    Obj.Size_limit = Items.Size_limit;
                    Obj.Size_limit_Type = Items.Size_limit_Type;
                    Obj.Type_limit = Items.Type_limit;

                    Obj.UpdateBy = "system";
                    Obj.UpdateDate = DateTime.Now;
                    if (Temp != null)
                    {
                        Map.Update(Obj);
                    }
                    else
                    {
                        Map.Add(Obj);
                    }
                    Map.SaveChanges();
                }
                return Obj;
            }

        }
    }
}
