﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
namespace Core.Common
{
    public static class EventData
    {


        public static Events AddOrUpdate(Events Items)
        {
            CultureInfo ukCulture = new CultureInfo("en-GB");

            ukCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";

            Thread.CurrentThread.CurrentCulture = ukCulture;
            Thread.CurrentThread.CurrentUICulture = ukCulture;

            Events Obj = new Events();
            using (var context = new CMSdbContext())
            {
                Boolean mode = false;
                if (Items.EventId.ToString() != "00000000-0000-0000-0000-000000000000" && Items.EventId != null)
                {
                    mode = true;
                    Obj = context.Events.Find(Items.EventId);
                }
                else
                {

                    Obj.Deleted = false;
                    Obj.CreateDate = DateTime.Now;
                    Obj.CreateBy = Items.CreateBy;
                }
                Obj.Activated = Items.Activated;
                Obj.TopicName = Items.TopicName;
                Obj.Descriptions = Items.Descriptions;
                Obj.LocationName = Items.LocationName;
                if (Items.StartDate_Str != null)
                    Obj.StartDate = Items.StartDate_Str.todateEn();
                if (Items.EndDate_Str != null)
                    Obj.EndDate = Items.EndDate_Str.todateEn();
                //Obj.StartDate = (DateTime)Items.StartDate_Str.todateEn();
                //Obj.EndDate = (DateTime)Items.EndDate_Str.todateEn();
                Obj.s_hour = Items.s_hour == "" ? "08" : Items.s_hour;
                Obj.s_min = Items.s_min == "" ? "00" : Items.s_min;
                Obj.e_hour = Items.e_hour == "" ? "18" : Items.e_hour;
                Obj.e_min = Items.e_min == "" ? "00" : Items.e_min;
                Obj.UpdateBy = Items.UpdateBy;
                Obj.UpdateDate = DateTime.Now;
                if (mode)
                {
                    context.Update(Obj);
                }
                else
                {
                    context.Add(Obj);
                }
                context.SaveChanges();
            }
            return Obj;
        }

        public static Events GeteventById(Guid Id)
        {
            var db = new CMSdbContext();
            return db.Events.Find(Id);
        }

        public static bool DeleteEvents(Guid Id, string updateBy)
        {
            var db = new CMSdbContext();
            var Obj = db.Events.Find(Id);
            if (Obj != null)
            {
                Obj.Activated = false;
                Obj.Deleted = true;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                db.Update(Obj);
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public static List<Events> GeEventsList()
        {
            CMSdbContext context = new CMSdbContext();
            var objResult = context.Events.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdateDate).ToList();

            return objResult;

        }
        public static IQueryable<Events> SearchListByName(string searchString)
        {
            var db = new CMSdbContext();

            if (!string.IsNullOrEmpty(searchString))
                return db.Events.Where(s => (s.TopicName.Contains(searchString) || s.Descriptions.Contains(searchString)) && s.Deleted == false).OrderBy(o => o.UpdateDate);
            else
                return db.Events.Where(s => s.Deleted == false).OrderByDescending(o => o.UpdateDate);


        }
        public static List<Events> SearchListByMonth()
        {
            var db = new CMSdbContext();


            return db.Events.Where(s => s.Deleted == false && s.Activated==true).OrderByDescending(o => o.UpdateDate).ToList();


        }
        public static bool UpdateActiveStatus(Guid eventId, string updateBy)
        {
            using (var context = new CMSdbContext())
            {
                var Obj = context.Events.Find(eventId);
                Obj.Activated = !Obj.Activated;
                Obj.UpdateBy = updateBy;
                Obj.UpdateDate = DateTime.Now;
                context.Update(Obj);
                context.SaveChanges();
                return true;
            }

        }

    }
}
