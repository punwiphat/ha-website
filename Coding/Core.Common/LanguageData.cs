﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
namespace Core.Common
{
    public static class LanguageData
    {
        
        public static List<Languages> GetByActive()
        {
            //List<Languages> Obj = new List<Languages>();
            //using (var Map = new CMSdbContext())
            //{
            //    Obj = Map.Languages.Where(o => o.Actived == true).ToList();
            //}
            List<Languages> Obj = new List<Languages>();
            Obj.Add(new Languages { LanguageID = 1,LanguageCode="TH", LanguageName = "ภาษาไทย" ,LanguageFlag= "/BACKOFFICE/image/flags/th.png" });
            Obj.Add(new Languages { LanguageID = 2,LanguageCode="EN", LanguageName = "English", LanguageFlag = "/BACKOFFICE/image/flags/en.png" });
            return Obj.OrderBy(o => o.Sequence).ToList();
        }
    }
}
