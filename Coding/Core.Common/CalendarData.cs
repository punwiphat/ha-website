﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;
using Wangkanai.Detection;
using Calendar = Core.DAL.Models.Calendar;

namespace Core.Common
{
    public static class CalendarData
    {
        public class Get
        {
            public static Calendar MainCalendarById(int ID)
            {
                Calendar Obj = new Calendar();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Calendar.Where(o => o.Deleted != true && o.ID == ID).FirstOrDefault();
                }
                return Obj;
            }
            public static Calendar MainCalendarByMenuId(int MenuID)
            {
                Calendar Obj = new Calendar();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Calendar.Where(o => o.Deleted != true && o.MenuID == MenuID).FirstOrDefault();
                }
                return Obj;
            }
            public static List<CalendarModel> ByMenu(int MenuID, string LangCode)
            {
                try
                {
                    List<CalendarModel> Obj = new List<CalendarModel>();
                    using (var Map = new CMSdbContext())
                    {
                        Obj = (from Main in Map.Calendar
                               join Calendars in Map.CalendarDetail on Main.ID equals Calendars.CalendarID
                               where Main.Deleted != true && Calendars.LangCode == LangCode
                               && Main.MenuID == MenuID
                               select new
                               {
                                   ID = Calendars.CalendarID
                                   ,
                                   title = Calendars.EventName
                                   ,
                                   desc = Calendars.EventDescription
                                   ,
                                   start = Main.EventDate.ToString("s")
                                   ,
                                   end = Main.EventEndDate.AddHours(24).ToString("s")
                                   ,
                                   Active = Main.Actived
                                   ,
                                   allDay = true
                                   ,
                                   color = Main.Actived ? "#ACC9FF" : "#B7B7B7"
                               }).ToList_list<CalendarModel>();


                    }
                    return Obj;
                }
                catch (Exception ex)
                {
                    string e = ex.Message;
                    return new List<CalendarModel>();
                }
            }
            public static List<CalendarDetail> CalendarById(int Id, string LangCode = null)
            {
                try
                {
                    List<CalendarDetail> Obj = new List<CalendarDetail>();
                    if (Id != 0)
                    {
                        using (var Map = new CMSdbContext())
                        {
                            Obj = (from Lang in Map.Languages
                                   join Calendars in Map.CalendarDetail on Lang.LanguageCode equals Calendars.LangCode into _Calendars
                                   from Calendars in _Calendars.DefaultIfEmpty()
                                   join Main in Map.Calendar on Calendars.CalendarID equals Main.ID into _Main
                                   from Main in _Main.DefaultIfEmpty()
                                   where Calendars.CalendarID == Id
                                   && Lang.Actived == true
                                   select new
                                   {
                                       LangCode = Lang.LanguageCode
                                       ,
                                       Calendars.CalendarID
                                       ,
                                       Calendars.CalendarDetailID
                                       ,
                                       Calendars.EventLink
                                       ,
                                       Calendars.EventName
                                       ,
                                       Calendars.EventDescription
                                       ,
                                       lstFiles = MediaFilesData.Get.ByCategoryMenu(4, Main.MenuID).ToList()
                                   }).ToList_list<CalendarDetail>();
                        }
                    }
                    else
                    {
                        var lang = LanguageData.GetByActive();
                        if (!string.IsNullOrEmpty(LangCode))
                            lang = lang.Where(o => o.LanguageCode == LangCode).ToList();
                        foreach (var l in lang)
                        {
                            CalendarDetail o = new CalendarDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();
                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static List<Calendar> CalendarByActive(int MenuID)
            {
                List<Calendar> Obj;
                string curDate = DateTime.Now.ToString("yyyy/MM/dd");
                CMSdbContext context = new CMSdbContext();
                Obj = context.Calendar.Where(s => s.MenuID == MenuID && s.Deleted != true && s.Actived == true && s.EffectiveDate <= DateTime.Parse(curDate) && DateTime.Parse(curDate) <= s.EndDate).OrderByDescending(o => o.UpdatedDate).ToList();
                return Obj;
            }
            public static List<Calendar> SearchListByMenu(string searchString, int MenuID)
            {
                CMSdbContext context = new CMSdbContext();
                List<Calendar> Calendar = new List<Calendar>();
                Calendar = context.Calendar.Where(o => o.Deleted == false && o.MenuID == MenuID).ToList();
                return Calendar;
            }
            public static List<Calendar> GetCalendarList()
            {

                CMSdbContext context = new CMSdbContext();
                var objResult = context.Calendar.Where(o => o.Deleted == false).OrderByDescending(o => o.UpdatedDate).ToList();
                return objResult;


            }
            public static List<CalendarDetail> ListCalendar(string LangCode, int MenuID)
            {
                try
                {
                    List<CalendarDetail> Obj = new List<CalendarDetail>();
                    MySqlParameter[] param = new MySqlParameter[2];
                    param[0] = new MySqlParameter("p_LangCode", LangCode);
                    param[1] = new MySqlParameter("p_MenuID", MenuID);
                    Obj = GetStoredMySql.GetAllStored<CalendarDetail>("F_EVENT", param).ToList();
                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static List<CalendarDetail> ListCalendarCurrent(string LangCode, int MenuID)
            {
                try
                {
                    List<CalendarDetail> Obj = new List<CalendarDetail>();
                    MySqlParameter[] param = new MySqlParameter[2];
                    param[0] = new MySqlParameter("p_LangCode", LangCode);
                    param[1] = new MySqlParameter("p_MenuID", MenuID);
                    Obj = GetStoredMySql.GetAllStored<CalendarDetail>("F_EVENT_CURRENT", param).ToList();
                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }

            }
        }
        public class Post
        {
            public static Calendar AddOrUpdate(Calendar Obj, string Username, string IPaddress, string Browser)
            {
                string description = "";
                bool mode = false;
                try
                {
                    Calendar saveObj = new Calendar();
                    using (var Map = new CMSdbContext())
                    {
                        if (Obj.ID != 0)
                        {
                            mode = true;
                            saveObj = Map.Calendar.Find(Obj.ID);
                        }
                        else
                        {
                            saveObj.Deleted = false;
                            saveObj.CreatedDate = DateTime.Now;
                            saveObj.CreatedBy = Username;
                            saveObj.MenuID = Obj.MenuID;
                        }

                        saveObj.EffectiveDate = Obj.EffectiveDate_str.todateEn();
                        saveObj.EndDate = Obj.EndDate_str.todateEn();
                        saveObj.EventDate = Obj.EventDate_str.todateEn().Value;
                        saveObj.EventEndDate = Obj.EventEndDate_str.todateEn().Value;
                        saveObj.UpdatedDate = DateTime.Now;
                        saveObj.UpdatedBy = Username;
                        saveObj.Actived = Obj.Actived;
                        saveObj.Menuname = Obj.Menuname;
                        if (mode)
                        {
                            Map.Update(saveObj);
                        }
                        else
                        {
                            Map.Add(saveObj);
                        }
                        Map.SaveChanges();
                        description = $"Calendar ID : {saveObj.ID} Calendar name : {saveObj.lstCalendar.FirstOrDefault(b => b.LangCode == "TH")?.EventName}";
                        LogTransaction.SaveLog(saveObj.ID, mode ? LogTransaction.Get.ActivityType.Edit : LogTransaction.Get.ActivityType.Add, saveObj.Menuname, Username, description, IPaddress, Browser);
                    }
                    return saveObj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(Obj.ID, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                }
                return Obj;
            }
            public static List<CalendarDetail> DetailAddOrUpdate(List<CalendarDetail> item, string Username, string IPaddress, string Browser)
            {
                try
                {
                    bool mode = false;
                    List<CalendarDetail> saveLst = new List<CalendarDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            CalendarDetail saveObj = new CalendarDetail();
                            if (Obj.CalendarDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.CalendarDetail.Find(Obj.CalendarDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.CalendarID = Obj.CalendarID;
                            }
                            saveObj.EventName = Obj.EventName;
                            saveObj.EventLink = Obj.EventLink;
                            saveObj.EventDescription = Obj.EventDescription;
                            saveObj.EventPlace = Obj.EventPlace;
                            saveObj.MenuID = Obj.MenuID;
                            saveObj.SUB_GUID = Obj.SUB_GUID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                    }
                    return saveLst;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static ResponseJson UpdateEvent(int ID, DateTime DateStart, DateTime DateEnd, string Username, string IpAddress, string Browser)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Calendar.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";

                        }
                        else
                        {
                            Obj.EventDate = DateStart;
                            Obj.EventEndDate = DateEnd;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, "CALENDAR", Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, "CALENDAR", Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }

            public static ResponseJson UpdateStatus(int ID, Boolean Active, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Calendar.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Actived = Active;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Edit, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.Result = false;
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.Calendar.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
