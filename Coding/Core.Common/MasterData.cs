using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using MySqlConnector;

public static class MasterData
{
	public class Get
	{
		public static List<Masters> ByMenuID(int MenuID)
		{
            List<Masters> Obj = new List<Masters>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_MenuID", MenuID);
            Obj = GetStoredMySql.GetAllStored<Masters>("B_MASTER_BYMENU", param).ToList();
            return Obj;
          
		}

		public static List<Masters> All()
		{
			List<Masters> source = new List<Masters>();
			using (CMSdbContext cMSdbContext = new CMSdbContext())
			{
				source = (from o in cMSdbContext.Masters
					where o.Deleted != (bool?)true
					orderby o.UpdatedDate descending
					select o).ToList();
			}
			return source.OrderByDescending((Masters o) => o.CreatedDate).ToList();
		}

		public static Masters ByID(int ID)
		{
			Masters masters = new Masters();
			using CMSdbContext cMSdbContext = new CMSdbContext();
			return cMSdbContext.Masters.Find(ID);
		}
	}

	public static class Post
	{
		public static Masters AddOrUpdate(Masters Items, string Username, string IPaddress, string Browser)
		{
			LogTransaction.Get.ActivityType activityType = LogTransaction.Get.ActivityType.Add;
			Masters Obj = new Masters();
			try
			{
				using (CMSdbContext cMSdbContext = new CMSdbContext())
				{
					bool flag = false;
					if (Items.ID != 0)
					{
						flag = true;
						Obj = cMSdbContext.Masters.Find(Items.ID);
					}
					else
					{
						Obj.Deleted = false;
						Obj.CreatedDate = DateTime.Now;
						Obj.CreatedBy = Username;
						Obj.MenuID = Items.MenuID;
						List<Masters> source = cMSdbContext.Masters.Where((Masters o) => o.MenuID == Obj.MenuID).ToList();
						int num = ((source.Count() <= 0) ? 1 : (source.Max((Masters o) => o.Sequence) + 1));
						Obj.Sequence = ((num <= 0) ? 1 : num);
					}
					Obj.Actived = Items.Actived;
					Obj.NameTH = Items.NameTH;
					Obj.NameEN = Items.NameEN;
					Obj.UpdatedBy = Username;
					Obj.UpdatedDate = DateTime.Now;
					Obj.Menuname = Items.Menuname;
					if (flag)
					{
						cMSdbContext.Update(Obj);
					}
					else
					{
						cMSdbContext.Add(Obj);
					}
					cMSdbContext.SaveChanges();
				}
				LogTransaction.SaveLog(Obj.ID, activityType, Obj.Menuname, Username, "", IPaddress, Browser);
				return Obj;
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
				return Obj;
			}
		}

		public static ResponseJson UpdateSequence(int ID, int Seq, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson responseJson = new ResponseJson();
			try
			{
				using (CMSdbContext cMSdbContext = new CMSdbContext())
				{
					Masters masters = cMSdbContext.Masters.Find(ID);
					if (masters == null)
					{
						responseJson.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						masters.Sequence = Seq;
						cMSdbContext.Update(masters);
						cMSdbContext.SaveChanges();
						responseJson.Result = true;
					}
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Sequence, Menuname, Username, responseJson.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				responseJson.Result = false;
				responseJson.MessageError = ex.Message;
			}
			return responseJson;
		}

		public static ResponseJson UpdateStatus(int ID, bool Active, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson responseJson = new ResponseJson();
			try
			{
				using (CMSdbContext cMSdbContext = new CMSdbContext())
				{
					Masters masters = cMSdbContext.Masters.Find(ID);
					if (masters == null)
					{
						responseJson.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						masters.Actived = Active;
						cMSdbContext.Update(masters);
						cMSdbContext.SaveChanges();
						responseJson.Result = true;
					}
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.ChangeStatus, Menuname, Username, responseJson.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				responseJson.Result = false;
				responseJson.MessageError = ex.Message;
			}
			return responseJson;
		}

		public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
		{
			ResponseJson responseJson = new ResponseJson();
			try
			{
				using (CMSdbContext cMSdbContext = new CMSdbContext())
				{
					Masters masters = cMSdbContext.Masters.Find(ID);
					if (masters == null)
					{
						responseJson.MessageError = "ไม่พบข้อมูล";
					}
					else
					{
						masters.Deleted = true;
						masters.UpdatedBy = Username;
						masters.UpdatedDate = DateTime.Now;
						cMSdbContext.Update(masters);
						cMSdbContext.SaveChanges();
						responseJson.Result = true;
					}
				}
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, responseJson.MessageError, IpAddress, Browser);
			}
			catch (Exception ex)
			{
				LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
				responseJson.MessageError = ex.Message;
			}
			return responseJson;
		}
	}
}
