﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;

namespace Core
{
    public static class ContactData
    {
        public class Get
        {
            public static Contact Content()
            {
                Contact Obj = new Contact();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Contact.OrderByDescending(o => o.UpdatedDate).FirstOrDefault();
                }
                return Obj;

            }
            public static List<ContactDetail> ContactById(int ID, string LangCode = null)
            {
                try
                {
                    List<ContactDetail> Obj = new List<ContactDetail>();
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_ContactID", ID);
                    Obj = GetStoredMySql.GetAllStored<ContactDetail>("contactlist_byid", param).ToList();
                    if (Obj.Count == 0)
                    {
                        var lang = LanguageData.GetByActive();
                        foreach (var l in lang)
                        {
                            ContactDetail o = new ContactDetail();
                            o.LangCode = l.LanguageCode;
                            Obj.Add(o);
                        }
                    }
                    if (!string.IsNullOrEmpty(LangCode))
                        Obj = Obj.Where(o => o.LangCode == LangCode).ToList();

                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static List<ContactForm> ContactFormList()
            {
                try
                {
                    List<ContactForm> Obj = new List<ContactForm>();
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.ContactForm.Where(o => o.Deleted != true).OrderByDescending(o => o.CreatedDate).ToList();
                    }
                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            public static ContactForm ContactFormById(int Id)
            {
                ContactForm Obj = new ContactForm();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.ContactForm.Find(Id);
                    }
                    return Obj;
                }
                catch (Exception ex)
                {
                    return Obj;
                }
            }
            public static ContactDetail FContactUS(string LangCode)
            {
                try
                {
                    MySqlParameter[] param = new MySqlParameter[1];
                    param[0] = new MySqlParameter("p_LangCode", LangCode);
                    ContactDetail Obj = GetStoredMySql.GetAllStored<ContactDetail>("F_CONTACTUS", param).FirstOrDefault();
                    return Obj;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static class Post
        {
            public static Contact AddOrUpdate(Contact Obj, string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                Contact saveObj = new Contact();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.Contact.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                    }
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }
            public static List<ContactDetail> DetailAddOrUpdate(List<ContactDetail> item, string Username, string IPaddress, string Browser)
            {

                try
                {
                    bool mode = false;
                    List<ContactDetail> saveLst = new List<ContactDetail>();
                    using (var Map = new CMSdbContext())
                    {
                        foreach (var Obj in item)
                        {
                            ContactDetail saveObj = new ContactDetail();
                            if (Obj.ContactDetailID != 0)
                            {
                                mode = true;
                                saveObj = Map.ContactDetail.Find(Obj.ContactDetailID);
                            }
                            else
                            {
                                saveObj.LangCode = Obj.LangCode;
                                saveObj.ContactID = Obj.ContactID;
                            }
                            saveObj.InstitutionTitle = Obj.InstitutionTitle;
                            saveObj.InstitutionEmail = Obj.InstitutionEmail;
                            saveObj.InstitutionTel = Obj.InstitutionTel;
                            saveObj.InstitutionAddress = Obj.InstitutionAddress;
                            saveObj.InstitutionFax = Obj.InstitutionFax;
                            saveObj.GoogleMap = Obj.GoogleMap;
                            saveObj.Files = Obj.Files;
                            saveObj.MenuID = Obj.MenuID;
                            if (mode)
                            {
                                Map.Update(saveObj);
                            }
                            else
                            {
                                Map.Add(saveObj);
                            }
                            saveLst.Add(saveObj);
                        }
                        Map.SaveChanges();
                        foreach (var itemsave in saveLst)
                        {
                            if (itemsave.Files != null)
                            {
                                var deteted = MediaFilesData.Post.DeleteByIDMenuCategory(itemsave.MenuID, 2, itemsave.ContactDetailID.ToString(), Username, IPaddress, Browser);
                                if (deteted)
                                    UploadFiles.UploadFile(itemsave.Files, itemsave.MenuID, itemsave.ContactDetailID, 2, 3, Username);
                            }
                        }
                    }
                    return saveLst;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            public static ContactForm FormAddOrUpdate(ContactForm Obj, string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                ContactForm saveObj = new ContactForm();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.ContactForm.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                    }
                    saveObj.Email = Obj.Email;
                    saveObj.Telephone = Obj.Telephone;
                    saveObj.Fullname = Obj.Fullname;
                    saveObj.Message = Obj.Message;
                    saveObj.MenuID = Obj.MenuID;
                    saveObj.Menuname = Obj.Menuname;
                    saveObj.LangCode = Obj.LangCode;
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                return saveObj;
            }

            public static ContactForm UpdateContactFormReply(ContactForm Obj, string Username)
            {
                ContactForm updateObj = new ContactForm();
                using (var Map = new CMSdbContext())
                {
                    updateObj = Map.ContactForm.Find(Obj.ID);
                    updateObj.MessageReply = Obj.MessageReply;
                    updateObj.UpdatedDate = DateTime.Now;
                    updateObj.UpdatedBy = Username;
                    Map.Update(updateObj);
                    Map.SaveChanges();
                }
                return updateObj;
            }
            public static ResponseJson DeleteContactForm(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ContactForm.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}