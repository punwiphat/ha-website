﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Globalization;
using System.Threading;
using MySqlConnector;

namespace Core.Common
{
    public static class ChannelCommentData
    {
        public class Get
        {
            public static List<ChannelComment> List()
            {
                List<ChannelComment> list = new List<ChannelComment>();
                using (var Map = new CMSdbContext())
                {
                    list = (from c in Map.ChannelComments.Where(o => o.Deleted != true)
                            join ct in Map.ChannelCommentTypes on c.TypeID equals ct.ID
                            orderby c.CreatedDate descending
                            select new ChannelComment
                            {
                                ID = c.ID,
                                LangCode = c.LangCode,
                                Fullname = c.Fullname,
                                Email = c.Email,
                                Telephone = c.Telephone,
                                Comment = c.Comment,
                                Address = c.Address,
                                Deleted = c.Deleted,
                                TypeID = c.TypeID,
                                CreatedBy = c.CreatedBy,
                                CreatedDate = c.CreatedDate,
                                UpdatedBy = c.UpdatedBy,
                                UpdatedDate = c.UpdatedDate,
                                TypeName = ct.NameTH
                            }).ToList();
                }
                return list;

            }


            public static ChannelComment ByID(int ID)
            {
                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_Id", ID);
                return GetStoredMySql.GetAllStored<ChannelComment>("F_CHANNELCOMMENT_BY_ID", param).FirstOrDefault();
            }
        }
        public class Post
        {
            public static ChannelComment AddOrUpdate(ChannelComment Obj, string Username, string IPaddress, string Browser)
            {
                bool mode = false;
                ChannelComment saveObj = new ChannelComment();
                using (var Map = new CMSdbContext())
                {
                    if (Obj.ID != 0)
                    {
                        mode = true;
                        saveObj = Map.ChannelComments.Find(Obj.ID);
                    }
                    else
                    {
                        saveObj.CreatedDate = DateTime.Now;
                        saveObj.CreatedBy = Username;
                    }
                    saveObj.Fullname = Obj.Fullname;
                    saveObj.TypeID = Obj.TypeID;
                    saveObj.Telephone = Obj.Telephone;
                    saveObj.Email = Obj.Email;
                    saveObj.Address = Obj.Address;
                    saveObj.Comment = Obj.Comment;
                    saveObj.LangCode = Obj.LangCode;
                    saveObj.UpdatedDate = DateTime.Now;
                    saveObj.UpdatedBy = Username;
                    saveObj.Menuname = Obj.Menuname;
                    if (mode)
                    {
                        Map.Update(saveObj);
                    }
                    else
                    {
                        Map.Add(saveObj);
                    }
                    Map.SaveChanges();
                }
                if (Obj.Files != null)
                {
                    UploadFiles.UploadFile(Obj.Files, Obj.MenuID, saveObj.ID, 4, 0, Username);
                }
                return saveObj;
            }

            public static ResponseJson Delete(int ID, string Username, string IpAddress, string Browser, string Menuname)
            {
                ResponseJson Res = new ResponseJson();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        var Obj = Map.ChannelComments.Find(ID);
                        if (Obj == null)
                        {
                            Res.MessageError = "ไม่พบข้อมูล";
                        }
                        else
                        {
                            Obj.Deleted = true;
                            Obj.UpdatedBy = Username;
                            Obj.UpdatedDate = DateTime.Now;
                            Map.Update(Obj);
                            Map.SaveChanges();
                            Res.Result = true;
                        }
                    }
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Delete, Menuname, Username, Res.MessageError, IpAddress, Browser);
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(ID, LogTransaction.Get.ActivityType.Error, Menuname, Username, ex.Message, IpAddress, Browser);
                    Res.MessageError = ex.Message;
                }
                return Res;
            }
        }
    }
}
