﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Core.DAL.Models;
using Core.library;
using Core.Models;
using System.Linq;
using Core.Common;
namespace Core
{
    public class MediaCategorysData
    {
        public class Post
        {
            public MediaCategorys Add(MediaCategorys FileUpload)
            {
                using (var Map = new CMSdbContext())
                {
                    FileUpload.Deleted = false;
                    Map.Add(FileUpload);
                    Map.SaveChanges();
                }
                return FileUpload;
            }
            public MediaCategorys Update(MediaCategorys FileUpload)
            {
                MediaCategorys x = new MediaCategorys();
                using (var Map = new CMSdbContext())
                {
                    x = Map.MediaCategorys.Find(FileUpload.MediaCategoryID);
                    x.Deleted = false;
                    Map.Add(x);
                    Map.SaveChanges();
                }
                return x;
              
            }
            public MediaCategorys Delete(int Id, string Username)
            {
                MediaCategorys x = new MediaCategorys();
                using (var Map = new CMSdbContext())
                {
                    x = Map.MediaCategorys.Find(Id);
                    x.Deleted = true;
                    x.UpdatedBy = Username;
                    x.UpdatedDate = DateTime.Now;
                    Map.Add(x);
                    Map.SaveChanges();
                }
                return x;
            }
        }
        public class Get
        {
            public MediaCategorys GetById(int Id)
            {
                MediaCategorys Obj = new MediaCategorys();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaCategorys.Find(Id);
                }
                return Obj;
            }
            public List<MediaCategorys> GetData()
            {
                List<MediaCategorys> Obj = new List<MediaCategorys>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.MediaCategorys.Where(o => o.Deleted != true).ToList();
                }
                return Obj;
            }
        }

    }
}
