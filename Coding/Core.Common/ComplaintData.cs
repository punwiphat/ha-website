﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using MySqlConnector;
using static Core.Common.LogTransaction.Get;

namespace Core
{
    public static class ComplaintData
    {
        public class Get
        {

            // ดึงข้อมูลตรวจสอบเรื่องสิทธิ์เรื่องร้องทุกข์ผู้บริหาร
            public static List<Complaint> AllByCheckRoleAdmin(int userRoleGroupId)
            {
                List<Complaint> Obj = new List<Complaint>();
                using (var db = new CMSdbContext())
                {
                    if (userRoleGroupId == 6) // สิทธิ์ผู้ดูแลระบบ
                    {
                       Obj = (from c in db.Complaint
                               join ct in db.ComplaintType on c.ComplainTypeID equals ct.ID
                               select new Complaint
                               {
                                   ID = c.ID,
                                   ComplainTypeName = ct.NameTH,
                                   ComplainTypeID = ct.ID,
                                   Name = c.Name,
                                   Division = c.Division,
                                   Email = c.Email,
                                   ProvinceID = c.ProvinceID,
                                   CountryID = c.CountryID,
                                   Title = c.Title,
                                   Description = c.Description,
                                   LangCode = c.LangCode,
                                   ResponseMessage = c.ResponseMessage,
                                   CreatedDate = c.CreatedDate
                               }).ToList();;
                    }
                    else
                    {
                        Obj = (from c in db.Complaint
                               join ct in db.ComplaintType on c.ComplainTypeID equals ct.ID
                               where ct.IsDirector != true
                               select new Complaint
                               {
                                   ID = c.ID,
                                   ComplainTypeName = ct.NameTH,
                                   ComplainTypeID = ct.ID,
                                   Name = c.Name,
                                   Division = c.Division,
                                   Email = c.Email,
                                   ProvinceID = c.ProvinceID,
                                   CountryID = c.CountryID,
                                   Title = c.Title,
                                   Description = c.Description,
                                   LangCode = c.LangCode,
                                   ResponseMessage = c.ResponseMessage,
                                   CreatedDate = c.CreatedDate
                               }).ToList();
                    }

                }
                return Obj.OrderByDescending(o => o.Responsed).ThenByDescending(o => o.CreatedDate).ToList();
            }

            public static List<Complaint> All()
            {
                List<Complaint> Obj = new List<Complaint>();
                using (var Map = new CMSdbContext())
                {
                    //Obj = Map.Complaint.OrderByDescending(o=>o.CreatedDate).ToList();
                  
                     Obj = (from Main in Map.Complaint
                            join Cate in Map.ComplaintType on Main.ComplainTypeID equals Cate.ID into _it
                            from Cate in _it.DefaultIfEmpty()
                            select new
                            {
                                ComplainTypeName = Cate.NameTH
                                ,Main.ID 
                                ,Main.ComplainTypeID 
       
                                ,Main.Name 
                                ,Main.Division 
                                ,Main.Email 
        
                                ,Main.ProvinceID 
                                ,Main.CountryID 
                                ,Main.Title 
                                ,Main.Description 
                                ,Main.LangCode 
                                ,Main.ResponseMessage 
                            }).ToList_list<Complaint>();
                }
                return Obj.OrderByDescending(o=> o.Responsed).ThenByDescending(o=> o.CreatedDate).ToList();
            }
            public static List<Complaint> ByResponse()
            {
                List<Complaint> Obj = new List<Complaint>();
                using (var Map = new CMSdbContext())
                {
                    Obj = Map.Complaint.Where(o => !string.IsNullOrEmpty(o.ResponseMessage)).ToList();
                }
                return Obj;
            }
            public static Complaint ByID(int ID)
            {

                MySqlParameter[] param = new MySqlParameter[1];
                param[0] = new MySqlParameter("p_complaintId", ID);
                return GetStoredMySql.GetAllStored<Complaint>("F_COMPLAINT_BY_ID", param).FirstOrDefault();
            }
            
        }
        public static class Post
        {
            public static Complaint AddOrUpdate(Complaint Items, string Username, string IPaddress, string Browser)
            {
                ActivityType STATE = LogTransaction.Get.ActivityType.Add;
                Complaint Obj = new Complaint();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Boolean mode = false;
                        if (Items.ID != 0)
                        {
                            mode = true;
                            Obj = Map.Complaint.Find(Items.ID);
                        }
                        else
                        {
                            Obj.CreatedDate = DateTime.Now;
                            Obj.CreatedBy = Username;
                        }
                        Obj.MenuID = Items.MenuID;
                        Obj.LangCode = Items.LangCode;
                        Obj.ComplainTypeID = Items.ComplainTypeID;
                        Obj.Name = Items.Name;
                        Obj.Email = Items.Email;
                        Obj.Division = Items.Division;
                        Obj.Title = Items.Title;
                        Obj.Description = Items.Description;
                        Obj.CurrentAddr = Items.CurrentAddr;
                        Obj.ProvinceID = Items.ProvinceID;
                        Obj.CountryID = 1;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        if (mode)
                        {
                            Map.Update(Obj);
                        }
                        else
                        {
                            Map.Add(Obj);
                        }
                       
                        Map.SaveChanges();
                    }
                    if (Items.Files != null)
                    {
                        UploadFiles.UploadFile(Items.Files, Items.MenuID, Obj.ID, 4, 0, Username);
                    }
                    LogTransaction.SaveLog(Obj.ID, STATE, Obj.Menuname, Username, "", IPaddress, Browser);
                    return Obj;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IPaddress, Browser);
                    return Obj;
                }
            }
            public static Boolean Responsed(int ComplaintID, string Message, string Username, string IpAddress, string Browser)
            {
                Complaint Obj = new Complaint();
                try
                {
                    using (var Map = new CMSdbContext())
                    {
                        Obj = Map.Complaint.Find(ComplaintID);
                        Obj.ResponseMessage = Message;
                        Obj.UpdatedBy = Username;
                        Obj.UpdatedDate = DateTime.Now;
                        Map.Update(Obj);
                        Map.SaveChanges();
                    }
                    LogTransaction.SaveLog(ComplaintID, LogTransaction.Get.ActivityType.Edit, Obj.Menuname, Username, "", IpAddress, Browser);
                    return true;
                }
                catch (Exception ex)
                {
                    LogTransaction.SaveLog(0, LogTransaction.Get.ActivityType.Error, Obj.Menuname, Username, ex.Message, IpAddress, Browser);
                    return false;

                }
            }

           
        }
    }
}
