﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class ProcurementController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ProcurementController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string Menuname, string LangID, int CategoryID,string searchString, int? pageNumber)
        {

            ViewBag.CategoryIDParam = CategoryID;
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title2 = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;


            ViewBag.CategoryMenuID = menuObj.CategoryID != 0 ? menuObj.CategoryID : menuObj.ID;
            Category cate = new Category();
            if (CategoryID != 0)
            {
                cate = CategoryData.Get.ByID(CategoryID);
            }
            else
            {
                cate = CategoryData.Get.ByMenuIdActive(menuObj.CategoryID).FirstOrDefault();
                CategoryID = cate != null ? cate.ID : 0;
            }
            ViewBag.CategoryID = CategoryID;
            if (cate != null)
            {
                string nTH = !string.IsNullOrEmpty(cate.NameTH) ? cate.NameTH : "";
                string nEN = !string.IsNullOrEmpty(cate.NameEN) ? cate.NameEN : "";
                ViewBag.Title = (!string.IsNullOrEmpty(LangID) ? LangID : "TH") == "TH"
                        ? nTH : nEN;
            }
            int newmenu = menuObj.newMenuDownloadID != 0 ? menuObj.newMenuDownloadID : menuObj.ID;

            var Content = DownloadFormData.Get.FListByMenu(newmenu, CategoryID, LangID).ToList();
            if (cate != null && !string.IsNullOrEmpty(cate.RSSUrl))
            {
                List<DownloadFormDetail> RSSContent = new List<DownloadFormDetail>();
                string url = cate.RSSUrl;
                if (!string.IsNullOrEmpty(url))
                {
                    try
                    {
                        WebClient client = new WebClient();
                        Stream stream = client.OpenRead(url);
                        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                        Encoding srcEncoding = Encoding.GetEncoding(874);
                        using var reader = XmlReader.Create(new StreamReader(stream, srcEncoding));// XmlReader.Create(url);

                        var feed = SyndicationFeed.Load(reader);

                        List<DownloadFormDetail> a = (from Main in feed.Items.ToList()
                                                      select new
                                                      {
                                                          DownloadFormName = Main.Title.Text,
                                                          DownloadFormDescription = Main.Summary.Text,
                                                          FilePath = (Main.Links.Count > 0 ? Main.Links.FirstOrDefault().Uri.ToString() : "#"),
                                                          UpdatedDate = Main.PublishDate != null ? Main.PublishDate.UtcDateTime : new DateTime(),
                                                          IsRSS = true
                                                      }).ToList_list<DownloadFormDetail>();
                        RSSContent = RSSContent.Union(a).ToList();
                    }
                    catch (Exception ex)
                    {
                        var esx = ex.Message;
                    }
                }

                Content = Content.Union(RSSContent).ToList();
            }
            foreach (var item in Content)
            {
                //item.lstFilesAttach = MediaFilesData.Get.ByCategoryMenu(4, obj.ID).Where(o => o.RefID == item.DownloadFormDetailID.ToString()).ToList();
                item.lstFilesAttach = MediaFilesData.Get.FListMedia(newmenu, 4, item.DownloadFormDetailID);

            }
          
            return View(await PaginatedList<DownloadFormDetail>.CreateAsync(Content, pageNumber ?? 1, Constants.Config.pageSize));

        }
    }
}
