﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using SWU.Frontend.Models;
using Wangkanai.Detection;

namespace SWU.Frontend.Controllers
{
    public class CoursesController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public CoursesController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string Menuname, string LangID, int CategoryID, string searchString, int? pageNumber)
        {
            ViewBag.CategoryIDParam = CategoryID;
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title2 = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);

            ViewBag.CategoryMenuID = MenuDisplay.CategoryID != 0 ? MenuDisplay.CategoryID : menuObj.ID;
            Category cate = new Category();
            if (CategoryID != 0)
            {
                cate = CategoryData.Get.ByID(CategoryID);
            }
            else
            {
                cate = CategoryData.Get.ByMenuIdActive(menuObj.CategoryID).FirstOrDefault();
                CategoryID = cate != null ? cate.ID : 0;
            }
            ViewBag.CategoryID = CategoryID;
            if (cate != null)
            {
                string nTH = !string.IsNullOrEmpty(cate.NameTH) ? cate.NameTH : "";
                string nEN = !string.IsNullOrEmpty(cate.NameEN) ? cate.NameEN : "";
                ViewBag.Title = (!string.IsNullOrEmpty(LangID) ? LangID : "TH") == "TH"
                        ? nTH : nEN;
            }
            int newmenu = menuObj.newMenuDownloadID != 0 ? menuObj.newMenuDownloadID : menuObj.ID;

            var Content = ProjectsData.Get.FListProjects(MenuID, LangID,"").ToList();
          
           
            return View(await PaginatedList<ProjectsDetail>.CreateAsync(Content, pageNumber ?? 1, Constants.Config.pageSize));

        }
    }
}
