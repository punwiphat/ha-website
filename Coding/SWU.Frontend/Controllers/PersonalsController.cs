﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class PersonalsController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public PersonalsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string Menuname, string LangID, int CategoryID, string searchString, int? pageNumber)
        {
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            //if (obj == null)
            //    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            //ViewBag.MenuID = obj.ID;
            //ViewBag.ParentID = obj.MENU_FRONT_ID;
            //ViewBag.ParentName = obj.ParentMenuname;
            //Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            List<StaffDetail> Content = StaffData.Get.FListStaff(MenuID, LangID, searchString).ToList();
                foreach (var it in Content)
                {
                    it.lstFilesAttach = MediaFilesData.Get.FListMedia(MenuID, 4, it.StaffDetailID);
                }

            return View(Content);// await PaginatedList<StaffDetail>.CreateAsync(Content, pageNumber ?? 1, Constants.Config.pageSize));

        }
    }
}
