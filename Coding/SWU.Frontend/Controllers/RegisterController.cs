﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.ReCaptcha;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class RegisterController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public RegisterController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            ViewBag.IsError = false;
            ViewBag.MsgError = "";
            ViewBag.IsErrorLoging = false;
            ViewBag.MsgErrorLoging = "";
            bool IsLogin = false;
            if (!string.IsNullOrEmpty(F_Username))
                IsLogin = true;
            ViewBag.IsLogin = IsLogin;
            Members Obj = new Members();
            Obj.Menuname = Menuname;
            return View(Obj);
        }
        [ValidateReCaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Members obj, string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            ViewBag.IsError = false;
            ViewBag.MsgError = "";
            ViewBag.IsErrorLoging = false;
            ViewBag.MsgErrorLoging = "";
            bool IsLogin = false;
            if (!string.IsNullOrEmpty(F_Username))
                IsLogin = true;
            ViewBag.IsLogin = IsLogin;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                if (obj.Password != obj.ConfirmPassword)
                {
                    ViewBag.IsError = true;
                    ViewBag.MsgError = LangID == "TH" ? TextError.TH.TXT_PASSWORD_MISMATCH : TextError.EN.TXT_PASSWORD_MISMATCH;
                    return View(obj);
                }
                try
                {
                    if (MemberData.Get.CheckDupplicateByIdentityNo(obj.IdentityNo))
                    {
                        ViewBag.IsError = true;
                        ViewBag.MsgError = LangID == "TH" ? TextError.TH.TXT_PID_REGISTED : TextError.EN.TXT_PID_REGISTED;
                        return View(obj);
                    }

                    if (MemberData.Get.CheckDupplicateByUserName(obj.Username))
                    {
                        ViewBag.IsError = true;
                        ViewBag.MsgError = "Username " + obj.Username + (LangID == "TH" ? TextError.TH.TXT_HAS_BEEN_USED : TextError.EN.TXT_HAS_BEEN_USED);
                        return View(obj);
                    }
                    obj.Actived = true;
                    obj.Activated = false;
                    var main = MemberData.Post.AddOrUpdate(obj, UserName, IpAddress, Browser);
                    try
                    {
                        var Template = EmailTemplateData.Get.ByID(TemplateNum.F_RegisterAfterSave, (string.IsNullOrEmpty(LangID) ? "TH" : LangID));
                        //string url = "https://"+_accessor.HttpContext.Request.Host.Value + Url.Action("Verify", "Register", new { token = main.KeyActivated });
                        string url = _accessor.HttpContext.Request.Host.Value + Url.Action("Verify", "Register", new { token = main.KeyActivated });
                        string _html = Template.HTMLContent.Replace("[@FullName]", main.FirstName + " " + main.LastName).Replace("[@URL]", url);
                        var _mail = SettingEmailData.Get.All();
                        SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, Encryption.Decrypt(_mail.Password), _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                        sendEmail.SendMail(main.Email, Template.Subject, _html);
                        return RedirectToAction("Success", "Register", new { Menuname = Menuname, LangID = LangID });
                    }
                    catch (Exception ex)
                    {
                        var remove = MemberData.Post.Remove(main.MemberID);
                        ViewBag.IsError = true;
                        ViewBag.MsgError = LangID == "TH" ? TextError.TH.TXT_AN_ERROR_OCCURRED_EMAIL : TextError.EN.TXT_AN_ERROR_OCCURRED_EMAIL;
                        return View(obj);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.IsError = true;
                    ViewBag.MsgError = LangID == "TH" ? TextError.TH.TXT_AN_ERROR_OCCURRED : TextError.EN.TXT_AN_ERROR_OCCURRED;
                    return View(obj);
                }
            }
            else
            {
                if (errors.Count() > 0)
                {
                    ViewBag.IsError = true;
                    ViewBag.MsgError = errors.FirstOrDefault().ErrorMessage;
                }
            }
            return View(obj);
        }
        public IActionResult Success(string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            var Content = ContentData.Get.FByMenu(Convert.ToInt32(MenuList.SuccessRegister), LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(Convert.ToInt32(MenuList.SuccessChannelComment), 4, Content.ContentDetailID);
            return View(Content);

        }


        [HttpGet]
        public IActionResult Verify(string token, string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            ViewBag.Message = "";
            try
            {
                var main = MemberData.Post.UpdateActivated(token, LangID);
                if (main == null)
                {
                    ViewBag.Message = LangID == "TH" ? TextError.TH.TXT_NO_INT_AUTHEN : TextError.EN.TXT_NO_INT_AUTHEN;
                }
                else
                {
                    if (!string.IsNullOrEmpty(main.Msg))
                    {
                        ViewBag.Message = main.Msg;
                    }
                    else
                    {
                        try
                        {
                            var Template = EmailTemplateData.Get.ByID(TemplateNum.F_RegisterAfterActivated, (string.IsNullOrEmpty(LangID) ? "TH" : LangID));
                            string url = _accessor.HttpContext.Request.Host.Value + Url.Action("Index", "Register");
                            string _html = Template.HTMLContent.Replace("[@FullName]", main.FirstName + " " + main.LastName).Replace("[@UserName]", main.Username).Replace("[@Password]", Encryption.Decrypt(main.Password)).Replace("[@URL]", url);
                            var _mail = SettingEmailData.Get.All();
                            SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, Encryption.Decrypt(_mail.Password), _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                            sendEmail.SendMail(main.Email, Template.Subject, _html);
                            ViewBag.Message = LangID == "TH" ? TextError.TH.TXT_SUCCESS_VERIFIED : TextError.EN.TXT_SUCCESS_VERIFIED;
                        }
                        catch (Exception ex)
                        {
                            var ErrorMail = MemberData.Post.UnUpdateActivated(main);
                            ViewBag.Message = LangID == "TH" ? TextError.TH.TXT_AN_ERROR_OCCURRED_SEND_EMAIL : TextError.EN.TXT_AN_ERROR_OCCURRED_SEND_EMAIL;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = LangID == "TH" ? TextError.TH.TXT_AN_ERROR_OCCURRED_AUTHEN : TextError.EN.TXT_AN_ERROR_OCCURRED_AUTHEN;
            }
            var Content = ContentData.Get.FByMenu(Convert.ToInt32(MenuList.SuccessRegisterVerify), LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(Convert.ToInt32(MenuList.SuccessChannelComment), 4, Content.ContentDetailID);
            return View(Content);
            // return View();
        }
    }
}
