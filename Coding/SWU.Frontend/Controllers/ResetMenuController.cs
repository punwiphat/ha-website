﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWU.Frontend.Controllers
{
    public class ResetMenuController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ResetMenuController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {

            string cookiename = CookieName.MenuMain + "TH";
            _cache.Remove(cookiename);
            cookiename = CookieName.MenuMain + "EN";
            _cache.Remove(cookiename);
            cookiename = CookieName.Sitemap + "TH";
            _cache.Remove(cookiename);
            cookiename = CookieName.Sitemap + "EN";
            _cache.Remove(cookiename);

            // cookiename = CookieName.Submenu + ParentID.ToString() + LangID;
            //_cache.Remove(cookiename);

            // cookiename = CookieName.Submenu + ParentID.ToString() + LangID;

            return View();
        }
    }
}
