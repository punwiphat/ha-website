﻿using Core;
using Core.DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Wangkanai.Detection;

namespace SWU.Frontend.Controllers
{
    public class LoginController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public LoginController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Members obj , string Menuname, string LangID)
        {
            ViewBag.Title = obj.Menuname;
            ViewBag.Menuname = obj.Menuname;
            ViewBag.IsError = false;
            ViewBag.MsgError = "";
            ViewBag.IsErrorLoging = false;
            ViewBag.MsgErrorLoging = "";
            bool IsLogin = false;
            if (!string.IsNullOrEmpty(F_Username))
                IsLogin = true;
            ViewBag.IsLogin = IsLogin;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var main = MemberData.Get.CheckLogin(obj.Username, obj.Password);
                if (main == null){
                    ViewBag.IsErrorLoging = true;
                    ViewBag.MsgErrorLoging = "Username หรือ Password ไม่ถูกต้อง กรุณาลองใหม่!";
                }
                else if(main.Activated == false){
                    ViewBag.IsErrorLoging = true;
                    ViewBag.MsgErrorLoging = "ท่านยังไม่ได้ยืนยันตัวตนการสมัครสมาชิก กรุณาตรวจสอบข้อมูลอีเมล!";
                }
                else{
                    var claims = new List<Claim>();
                    claims.Add(new Claim("F_FullName", main.FirstName + "  " + main.LastName));
                    claims.Add(new Claim("F_Username", main.Username));
                    var grandmaIdentity = new ClaimsIdentity(claims, "F_User Identity");
                    var userPrincipal = new ClaimsPrincipal(new[] { grandmaIdentity });
                    HttpContext.SignInAsync(userPrincipal);
                    return RedirectToAction("Index", "Home", new { Menuname = "หน้าหลัก", LangID = (string.IsNullOrEmpty(LangID)? "TH" : LangID) });
                }
            }
            else
            {
                if (errors.Count() > 0)
                {
                    ViewBag.IsErrorLoging = true;
                    ViewBag.MsgErrorLoging = errors.FirstOrDefault().ErrorMessage;
                }
            }
            return View("~/Views/Register/Index.cshtml", obj);
        }

        [HttpGet]
        public IActionResult Signout(string Menuname, string LangID)
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Register");
        }

    }
}
