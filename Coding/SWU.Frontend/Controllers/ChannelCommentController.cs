﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AspNetCore.ReCaptcha;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class ChannelCommentController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ChannelCommentController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }

        public IActionResult Index(string Menuname,string LangID)
        {

            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;

            ViewBag.Error = "";
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            ViewBag.LangID = LangID;
            ChannelComment Obj = new ChannelComment();
            Obj.Menuname = Menuname;
            Obj.LangCode = LangID;
            return View(Obj);
        }
        [ValidateReCaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ChannelComment obj , string Menuname, string LangID)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            ViewBag.Title = obj.Menuname;
            ViewBag.Menuname = obj.Menuname;
            ViewBag.Error = "";
            if (ModelState.IsValid)
            {
                try{
                    obj.MenuID = (int)MenuList.ChannelComment;
                    obj.LangCode = !string.IsNullOrEmpty(obj.LangCode) ? obj.LangCode : "TH";
                    var main = ChannelCommentData.Post.AddOrUpdate(obj, UserName, IpAddress, Browser);
                    return RedirectToAction("Success", "ChannelComment", new { Menuname = Menuname, LangID = LangID });
                }
                catch(Exception ex){
                    ViewBag.Error = "บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่อีกครั้ง";
                }
            }
            else{
                if (errors.Count() > 0)
                    ViewBag.Error = errors.FirstOrDefault().ErrorMessage;
            }
            return View(obj);
        }
        public IActionResult Success(string Menuname, string LangID)
        {
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;

            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            var Content = ContentData.Get.FByMenu(Convert.ToInt32(MenuList.SuccessChannelComment), LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(Convert.ToInt32(MenuList.SuccessChannelComment), 4, Content.ContentDetailID);
            return View(Content);
            //return View();
        }

    }
}
