﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Core;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using MySqlConnector;

namespace SWU.Frontend.Controllers
{

    public class InitializeController : Controller
    {
        public static readonly string DATE_INPUT_FORMAT = "dd/MM/yyyy";
        public static bool hasManageRole = false;
        public static string MenuId = "0";
        public static string _LangID = "TH";



        public ActionResult RedirectToLocalized()
        {
            return RedirectPermanent("/en");
        }
        public static SelectList GetTitle(string LangID)
        {
            List<ddlmastername> Obj = new List<ddlmastername>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_MenuID", 411);
            Obj = GetStoredMySql.GetAllStored<ddlmastername>("B_MASTER_BY_MENUID_ACTIVE", param).ToList();
            return new SelectList(Obj, "ID", "Name" + (LangID == "TH" ? "TH" : "EN"));
        }
        public static SelectList GetTypeMember(string LangID)
        {
            List<ddlmaster> Obj = new List<ddlmaster>();
            Obj.Add(new ddlmaster() { ID = 1, Name = "สมาชิกตลอกชีพ", NameEN = "lifetime membership" });
            Obj.Add(new ddlmaster() { ID = 2, Name = "สมาชิกรายปี", NameEN = "yearly membership" });

            return new SelectList(Obj, "ID", "Name" + (LangID == "TH" ? "" : "EN"));
        }
        public static SelectList GetProvince(string LangID, string selectedValues = "")
        {
            List<ddlmaster> Obj = new List<ddlmaster>();
            MySqlParameter[] param = new MySqlParameter[1];
            param[0] = new MySqlParameter("p_LangID", LangID);
            Obj = GetStoredMySql.GetAllStored<ddlmaster>("F_PROVINCE_LIST", param).ToList();
            return new SelectList(Obj, "ID", "Name", selectedValues);
        }
        public static SelectList GetComplaintType(string LangID,string selectedValues = "")
        {
            List<ComplaintType> Obj = new List<ComplaintType>();
            Obj = ComplaintTypeData.Get.ByActive();
            return new SelectList(Obj, "ID", "Name"+LangID, selectedValues);
        }
        public static SelectList GetCateWB(string LangID, string selectedValues = "")
        {
            List<WebboardCategory> Obj = new List<WebboardCategory>();
            Obj = WBTopicData.Get.CategoryWebboardByLang(LangID);
            return new SelectList(Obj, "WBCategoryID", "TopicName", selectedValues);
        }

        protected string GetLangID()
        {
            return HttpContext.Request.RouteValues["LangID"].ToString();
        }
        protected string GetPermalink()
        {
            return HttpContext.Request.RouteValues["Menuname"].ToString();
        }
        public int UserID
        {
            get
            {
                int user_id = 0;
                if (HttpContext.User.Claims != null)
                {
                    var u = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "UserId");
                    user_id = u != null ? Int32.Parse(u.Value) : 0;
                }
                return user_id;

            }
        }

        public static string F_FullName { get; set; }
        public static string F_Username { get; set; }

        public string FullName 
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "FullName")?.Value : ""; }
        }
        public string UserName
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "username")?.Value : ""; }
        }

        public int UserRoleGroupId
        {
            get { return HttpContext.User.Claims != null ? Int32.Parse(HttpContext.User.Claims.FirstOrDefault(x => x.Type == "RoleGroupID")?.Value) : 0; }
        }

        public string UserRoleGroupName
        {
            get { return HttpContext.User.Claims != null ? HttpContext.User.Claims.FirstOrDefault(x => x.Type == "RoleGroupName")?.Value : ""; }
        }

        protected string Getdevice_client()
        {
            return Request.Headers["User-Agent"].ToString();
        }
        protected string GetIpAddress()
        {
            return HttpContext.Connection.RemoteIpAddress.ToString();
        }
        public string LangCode
        {
            get
            {
                _LangID = RouteData.Values["LangID"] != null ? RouteData.Values["LangID"].ToString() : "TH";
                
                return _LangID;

            }
        }
       
        private static string GetMachineNameFromIPAddress(string ipAdress)
        {
            string machineName = string.Empty;
            try
            {
                System.Net.IPHostEntry hostEntry = System.Net.Dns.GetHostEntry(ipAdress);
                machineName = hostEntry.HostName;
            }
            catch (Exception ex)
            {
                //log here
            }
            return machineName;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            F_FullName = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "F_FullName")?.Value;
            F_Username = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "F_Username")?.Value;
            base.OnActionExecuting(context);
        }
    }
    public class SetLangAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string LangID = filterContext.RouteData.Values["LangID"]?.ToString() ?? "TH";
            string UI = "th-TH";
            if (LangID == "EN")
            {
                UI = "en-US";
            }
            filterContext.HttpContext.Response.Cookies.Append(
             CookieRequestCultureProvider.DefaultCookieName,
             CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(UI)),
             new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
         );
            base.OnActionExecuting(filterContext);
        }
    }
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var Username = "";
            if (!string.IsNullOrEmpty(Username))
            {
                filterContext.Result = new RedirectResult("~/Home/Index");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonSerializer.Deserialize<T>(value);
        }
    }
   
}
