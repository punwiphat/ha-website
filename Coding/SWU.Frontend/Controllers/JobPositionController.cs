﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.Common;
using Core.DAL.Models;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class JobPositionController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public JobPositionController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string Menuname,string LangID, string searchString, int? pageNumber)
        {
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);

            //if (obj == null)
            //    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            //ViewBag.MenuID = obj.ID;
            //ViewBag.ParentID = obj.MENU_FRONT_ID;
            //ViewBag.ParentName = obj.ParentMenuname;
            //Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            var Content = JobPositionData.Get.FListPosition(17, LangID).ToList();
            var HeadContent = ContentData.Get.ContentDetsilByContentId(8, LangID);
            foreach (var item in Content)
            {
                item.HeadContent = HeadContent;
                item.lstFiles = MediaFilesData.Get.FListMedia(17, 4, item.JobPositionDetailID);
            }
            return View(await PaginatedList<JobPositionDetail>.CreateAsync(Content, pageNumber ?? 1, Constants.Config.pageSize));
           // return View(Content);
        }
    }
}
