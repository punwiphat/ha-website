﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using Core;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class EventsController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public EventsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string Menuname, string LangID)
        {
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            return View();
        }
        private string ChangeDateTime(string date, string time)
        {
            string[] sArrDate = date.Split("T");

            return sArrDate[0] + "T" + time;

        }
        // ใช้เฉพาะหน้า Home เท่านั้น
        public JsonResult GetCalendarEventsHomeOnly(string LangID)
        {
            var obj = CalendarData.Get.ListCalendar(LangID,18);

            var eventList = from e in obj
                            select new
                            {
                                id = e.CalendarID,
                                title = string.IsNullOrEmpty(e.EventName) ? "" : e.EventName,
                                // title = "&lt;span&gt;" + e.s_time + " - " + e.e_time + " น.&lt;/span&gt;&lt;br&gt;" + e.TopicName,
                                description = string.IsNullOrEmpty(e.EventDescription) ? "" : e.EventDescription,
                                start = e.EventDate.ToString("yyyy-MM-dd", new CultureInfo("en-GB")),
                                end = (e.EventEndDate == e.EventDate) ? e.EventEndDate.ToString("yyyy-MM-dd", new CultureInfo("en-GB")) : e.EventEndDate.AddDays(1).ToString("yyyy-MM-dd", new CultureInfo("en-GB")),
                                //                    extendedProps: {
                                //    place = string.IsNullOrEmpty(e.LocationName) ? "" : e.LocationName;
                                //},

                                // color = e.EventColor,
                                //Active = e.Activated,
                                //time = e.s_time + " - " + e.e_time,
                                //startTime = e.s_time,
                                //endTime = e.e_time,
                                // call = "",
                                eventlink = string.IsNullOrEmpty(e.EventLink)? "" : e.EventLink,
                                allDay = true
                            };
            var rows = eventList.ToArray();

            return Json(rows);
        }

        public JsonResult GetCalendarEventsByMonthYear(int month, int year, string LangID)
        {

            try
            {
                //DateTime monthName = new DateTime(year, month, 1);

                DateTime monthName = DateTime.ParseExact("01/" + month.ToString("0#") + "/" + year, "dd/MM/yyyy", new System.Globalization.CultureInfo("en-GB"));

                var obj = CalendarData.Get.ListCalendar(LangID,18);

                obj = obj.Where(s => ((DateTime)s.EventDate).ToString("yyyyMM") == monthName.ToString("yyyyMM") || ((DateTime)s.EventDate).ToString("yyyyMM") == monthName.ToString("yyyyMM")).ToList();

                if (obj.Count > 0)
                    obj = obj.OrderBy(s => s.EventDate).ToList();

                var files = MediaFilesData.Get.FListMediaMenuCategory(18, 4).ToList();
                var eventList = from e in obj
                                select new
                                {
                                    id = e.CalendarID,
                                    title = string.IsNullOrEmpty(e.EventName) ? "" : e.EventName,
                                    description = string.IsNullOrEmpty(e.EventDescription) ? "" : e.EventDescription,
                                    //start = todateTH(e.StartDate.ToString()),
                                    //end = todateTH(e.EndDate.ToString()),
                                    start = e.EventDate.ToString("dd MMM yy", new CultureInfo("th-TH")),
                                    startDay = e.EventDate.ToString("dd", new CultureInfo("th-TH")),
                                    startMonth = e.EventDate.ToString("MMM", new CultureInfo("th-TH")),
                                    end = e.EventEndDate.ToString("dd MMM yy", new CultureInfo("th-TH")),

                                    startInt = e.EventDate.ToString("yyyyMMdd", new CultureInfo("th-TH")),
                                    endInt = e.EventEndDate.ToString("yyyyMMdd", new CultureInfo("th-TH")),
                                    // place = string.IsNullOrEmpty(e.) ? "" : e.LocationName,
                                    //   color = e.EventColor,
                                    Active = e.Actived,
                                    //  time = e.s_time + " - " + e.e_time,
                                    //   stime = e.s_time,
                                    //  etime = e.e_time,
                                    call = "",
                                    lstFiles = files.Where(o => o.RefID == e.CalendarDetailID.ToString()).ToList(),
                                    eventlink = string.IsNullOrEmpty(e.EventLink) ? "" : e.EventLink,
                                    allDay = true
                                };

                var rows = eventList.ToArray();

                return Json(rows);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());

            }
        }
        public JsonResult GetCalendarEventsByMonthYearNew(int month, int year, string LangID,int MenuID)
        {

            try
            {
                //DateTime monthName = new DateTime(year, month, 1);

                DateTime monthName = DateTime.ParseExact("01/" + month.ToString("0#") + "/" + year, "dd/MM/yyyy", new System.Globalization.CultureInfo("en-GB"));

                var obj = CalendarData.Get.ListCalendar(LangID, MenuID);

                obj = obj.Where(s => ((DateTime)s.EventDate).ToString("yyyyMM") == monthName.ToString("yyyyMM") || ((DateTime)s.EventDate).ToString("yyyyMM") == monthName.ToString("yyyyMM")).ToList();

                if (obj.Count > 0)
                    obj = obj.OrderBy(s => s.EventDate).ToList();

                var files = MediaFilesData.Get.FListMediaMenuCategory(MenuID, 4).ToList();
                var eventList = from e in obj
                                select new
                                {
                                    id = e.CalendarID,
                                    title = string.IsNullOrEmpty(e.EventName) ? "" : e.EventName,
                                    description = string.IsNullOrEmpty(e.EventDescription) ? "" : e.EventDescription,
                                    //start = todateTH(e.StartDate.ToString()),
                                    //end = todateTH(e.EndDate.ToString()),
                                    start = e.EventDate.ToString("dd MMM yy", new CultureInfo("th-TH")),
                                    startDay = e.EventDate.ToString("dd", new CultureInfo("th-TH")),
                                    startMonth = e.EventDate.ToString("MMM", new CultureInfo("th-TH")),
                                    end = e.EventEndDate.ToString("dd MMM yy", new CultureInfo("th-TH")),

                                    startInt = e.EventDate.ToString("yyyyMMdd", new CultureInfo("th-TH")),
                                    endInt = e.EventEndDate.ToString("yyyyMMdd", new CultureInfo("th-TH")),
                                    // place = string.IsNullOrEmpty(e.) ? "" : e.LocationName,
                                    //   color = e.EventColor,
                                    Active = e.Actived,
                                    //  time = e.s_time + " - " + e.e_time,
                                    //   stime = e.s_time,
                                    //  etime = e.e_time,
                                    call = "",
                                    lstFiles = files.Where(o => o.RefID == e.CalendarDetailID.ToString()).ToList(),
                                    eventlink = string.IsNullOrEmpty(e.EventLink) ? "" : e.EventLink,
                                    allDay = true
                                };

                var rows = eventList.ToArray();

                return Json(rows);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());

            }
        }
        private string todateTH(string value)
        {
            string[] months = new string[] { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิการยน", "ธันวาคม" };
            string[] shortmonths = new string[] { "ม.ค", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย", "ก.ค", "ส.ค", "ก.ย", "ต.ค", "พ.ย", "ธ.ค" };
            if (!string.IsNullOrEmpty(value))
            {
                CultureInfo ukCulture = new CultureInfo("en-GB");
                ukCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                string tem = string.Format("{0:dd/MM/yyyy}", value);
                string[] dateString = tem.Split('/');
                int iYear = int.Parse(dateString[2].Substring(0, 4));
                string sTime = dateString[2].Substring(4, dateString[2].Length - 4);
                int nMonth = int.Parse(dateString[1]);
                int NowYear = DateTime.Now.Year;
                if (Math.Abs(iYear - NowYear) < 100)
                    iYear = iYear + 543;

                string result = dateString[0] + " " + shortmonths[nMonth] + " " + iYear; //+ sTime;
                return result;
            }
            else
                return null;
        }
      
        private int dateTHToInt(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string tem = string.Format("{0:dd/MM/yyyy}", value);
                string[] dateString = tem.Split('/');
                int iYear = int.Parse(dateString[2].Substring(0, 4));
                string sTime = dateString[2].Substring(4, dateString[2].Length - 4);
                int NowYear = DateTime.Now.Year;
                if (Math.Abs(iYear - NowYear) < 100)
                    iYear = iYear + 543;

                string result = iYear + dateString[1] + dateString[0];

                return int.Parse(result);
            }
            else
                return -1;
        }
        private string SetText(string sDate)
        {

            var result = "<div class=\"col-xs-12 no-padding\" style=\"font-weight: bolder;cursor:pointer;\"><span class=\"tooltipss\" title=\"" + "\">" + "</span></div>";
            result += "<div class=\"col-xs-12 no-padding\"><i class=\"fa fa-phone\" aria-hidden=\"true\"></i><span class=\"tooltipss\" title=\"" + "\">&nbsp;&nbsp;" + "</span></div><br/>";
            try
            {
                result += "<script>  $('.tooltipss').tooltipster();</script> ";
            }
            catch (Exception ex) { }
            return result;
        }

    }
}
