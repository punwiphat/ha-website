﻿using System;
using System.Diagnostics;
using System.Linq;
using Core;
using Core.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using SWU.Frontend.Models;

namespace SWU.Frontend.Controllers
{
    [SetLangAttribute]
    public class HomeController : InitializeController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStringLocalizer<HomeController> _localizer;

        public HomeController(ILogger<HomeController> logger, IStringLocalizer<HomeController> localizer)
        {
            _logger = logger;
            _localizer = localizer;
        }

        public IActionResult Index(string menuname,string LangID)
        {
            ViewBag.Menuname = LangID == "EN" ? "Home" : "หน้าหลัก";
            var Content = ContentData.Get.FByMenu(419, LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(419, 4, Content.ContentDetailID);
            return View(Content);
        }
        public IActionResult ChangeLang(string URL,string LangID)
        {
            return Redirect(URL);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult SetThai()
        {
            // กำหนดค่าใน RequestCulture ว่าจะให้เป็นภาษาอะไร ในที่นี้ต้องการเปลี่ยนเป็นภาษาไทย เลยตั้งเป็น "th-TH"
            var cookieValue = CookieRequestCultureProvider.MakeCookieValue(new RequestCulture("th-TH"));

            // อายุของ Cookie 
            var option = new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) };

            // เซตค่าให้กับ Cookie ซึ่งชื่อจะเป็นตัวเดียวกับที่ระบุไว้ในขั้นตอนที่สอง
            Response.Cookies.Append("Web.Language", cookieValue, option);

            return RedirectToAction(nameof(Index));
        }
    }
}
