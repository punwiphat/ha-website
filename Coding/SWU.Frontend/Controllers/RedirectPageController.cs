﻿using Core;
using Core.Common;
using Microsoft.AspNetCore.Mvc;

namespace SWU.Frontend.Controllers
{
    public class RedirectPageController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index(int ID,string url)
        {
            var res = BannerData.Get.CountView(ID);
            return Redirect(url);

        }
        [HttpPost]
        public JsonResult Download(int ID)
        {
            var res = MediaFilesData.Post.CountView(ID);
            return Json(new { status = true, message = "พบข้อผิดพลาดกรุณาลองใหม่ หรือ ติดต่อผู้ดูแลระบบ" });
        }
    }
}
