﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class ContactUsController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ContactUsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public IActionResult Index(string Menuname,string LangID, string searchString, int? pageNumber)
        {
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;


            ContactDetail Content = ContactData.Get.FContactUS(LangID);
            ContactDetailPage ContentNew = new ContactDetailPage();
            if (Content!=null)
            {
                ContentNew = Content.Tomaplist<ContactDetailPage>();
                var liststaff = StaffData.Get.FListStaff(36,LangID, searchString).Where(o => o.Actived == true).ToList();
                foreach (var it in liststaff)
                {
                    it.lstFilesAttach = MediaFilesData.Get.FListMedia(36, 4, it.StaffDetailID);
                }
                //ContentNew.lstFiles = MediaFilesData.Get.FListMedia(36, 2, ContentNew.ContactDetailID);
                ContentNew.lstStaff = PaginatedList<StaffDetail>.CreateSync(liststaff, pageNumber ?? 1, Constants.Config.pageSize);
            }

            return View(ContentNew);
        }

    }
}
