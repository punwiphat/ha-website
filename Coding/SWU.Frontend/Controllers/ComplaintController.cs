﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AspNetCore.ReCaptcha;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class ComplaintController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public ComplaintController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string Menuname, string LangID)
        {
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            //if (obj == null)
            //    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            //ViewBag.MenuID = obj.ID;
            //ViewBag.ParentID = obj.MENU_FRONT_ID;
            //ViewBag.ParentName = obj.ParentMenuname;
            //Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            var Content = ContentData.Get.FByMenu(MenuID, LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(menuObj.ID, 4, Content.ContentDetailID);
            return View(Content);
        }
        // GET: /<controller>/
        public IActionResult Form(string Menuname,string LangID)
        {
            ContactDetail Content = ContactData.Get.FContactUS(LangID);
            Complaint Obj = new Complaint();
            Obj.ObjContact = Content;
            Tm_Menu obj = MenuFrontData.Get.ByID(26, LangID);
            Obj.Menuname = obj.Menuname;
            Obj.LangCode = LangID;
            ViewBag.MenuID = 999999;
            ViewBag.ParentID = obj.ID;
            ViewBag.ParentName = obj.Menuname;
            ViewBag.Title = obj.Menuname;
            ViewBag.Menuname = obj.Menuname;
            ViewBag.IsError = false;
            ViewBag.MsgError = "";
            return View(Obj);
        }
        [ValidateReCaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(Complaint Obj, string Menuname, string LangID)
        {
            LangID = string.IsNullOrEmpty(LangID) ? "TH" : LangID;
            Tm_Menu objM = MenuFrontData.Get.ByID(26, LangID);
            Obj.Menuname = objM.Menuname;
            Obj.LangCode = LangID;
            ViewBag.MenuID = 999999;
            ViewBag.ParentID = objM.ID;
            ViewBag.ParentName = objM.Menuname;
            ViewBag.Title = objM.Menuname;
            ViewBag.Menuname = objM.Menuname;
            ViewBag.IsError = false;
            ViewBag.MsgError = "";
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                try
                {
                    Obj.MenuID = (int)MenuList.Complaint;
                    Obj.LangCode = !string.IsNullOrEmpty(LangID) ? LangID : "TH";
                    var main = ComplaintData.Post.AddOrUpdate(Obj, UserName, IpAddress, Browser);
                    if (main.ID != 0)
                    {
                        try
                        {
                            var ComplaintType = ComplaintTypeData.Get.ByID(main.ComplainTypeID);
                            if (ComplaintType != null)
                            {
                                if (!string.IsNullOrEmpty(ComplaintType.EmailNotification))
                                {
                                    var ComplaintItem = ComplaintData.Get.ByID(main.ID);
                                    string file = "-";
                                    if (!string.IsNullOrEmpty(ComplaintItem.FilePath))
                                    {
                                        file = "<a href='" + ComplaintItem.FilePath + "' target='_blank'>เอกสารแนบ</a>";
                                    }
                                    var Template = EmailTemplateData.Get.ByID(TemplateNum.F_ComplaintAfterSave, (string.IsNullOrEmpty(LangID) ? "TH" : LangID));
                                    string _html = Template.HTMLContent.Replace("[@ComplaintType]", ComplaintItem.ComplainTypeName)
                                                   .Replace("[@FullName]", main.Name).Replace("[@Departmant]", main.Division)
                                                   .Replace("[@Email]", main.Email).Replace("[@Topic]", main.Title)
                                                   .Replace("[@Details]", main.Description).Replace("[@Address]", main.CurrentAddr)
                                                   .Replace("[@ProvinceName]", ComplaintItem.Province).Replace("[@File]", file);
                                    var _mail = SettingEmailData.Get.All();
                                    SendEmail sendEmail = new SendEmail(_mail.EmailAlias, _mail.Emailname, Encryption.Decrypt(_mail.Password), _mail.Smtp, _mail.Port, _mail.EnabledSSL, _mail.UseDefaultCredentials);
                                    sendEmail.SendMail(ComplaintType.EmailNotification, Template.Subject, _html);
                                }
                            }

                        }
                        catch (Exception ex){}
                    }
                    return RedirectToAction("Success", "Complaint", new { Menuname = objM.Menuname, LangID = LangID });
                }
                catch(Exception ex)
                {
                    ViewBag.IsError = true;
                    ViewBag.MsgError = "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง";
                }   
            }
            else
            {
                if (errors.Count() > 0)
                {
                    ViewBag.IsError = true;
                    ViewBag.MsgError = errors.FirstOrDefault().ErrorMessage;
                }
            }
            ContactDetail Content = ContactData.Get.FContactUS(LangID);
            Obj.ObjContact = Content;
            return View(Obj);
        }
        public IActionResult Success(string Menuname, string LangID)
        {
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;

            var Content = ContentData.Get.FByMenu(Convert.ToInt32(MenuList.SuccessComplaint), LangID).FirstOrDefault();
            if (Content != null && Content.ContentDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(Convert.ToInt32(MenuList.SuccessComplaint), 4, Content.ContentDetailID);
            return View(Content);
           // return View();
        }
    }
}
