﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Wangkanai.Detection;
using Microsoft.Extensions.Caching.Memory;
using Core.DAL.Models;

namespace SWU.Frontend.Controllers
{
    public class SitemapController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public SitemapController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        public IActionResult Index(string LangID,string Menuname)
        {
            string cookiename4 = CookieName.Sitemap + LangID;
            if (!_cache.TryGetValue(cookiename4, out List<Tm_Menu> obj))
            {
                string cookienameMain = CookieName.MenuMain + LangID;
                //List<Tm_Menu> obj = MenuFrontData.Get.FMainMenu(1, LangID);
                if (!_cache.TryGetValue(cookienameMain, out List<Tm_Menu> MainMenu))
                {
                    MainMenu = MenuFrontData.Get.FMainMenu(1, LangID);
                    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                    };
                    _cache.Set(cookienameMain, MainMenu, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
                }
                foreach (var item in MainMenu)
                {
                    item.Sub_Menu = MenuFrontData.Get.FSubMenu(item.ID, LangID);
                }
                MemoryCacheEntryOptions options2 = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                obj = MainMenu;
                _cache.Set(cookiename4, MainMenu, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            }
            //return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/Default.cshtml", obj));
            return View(obj);
        }

    }
}
