﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AspNetCore.ReCaptcha;
using Core;
using Core.Common;
using Core.DAL.Models;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class WebboardController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public WebboardController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string Menuname, string LangID, int CategoryID,string searchString, int? pageNumber)
        {
            ViewBag.CategoryIDParam = CategoryID;
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);

            //if (obj == null || obj.ID == 0)
            //    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            //ViewBag.MenuID = obj.ID;
            //ViewBag.ParentID = obj.MENU_FRONT_ID;
            //ViewBag.ParentName = obj.ParentMenuname;
            //Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            ViewBag.CategoryID = CategoryID;
            var Content = WBTopicData.Get.FListByCategory(CategoryID, LangID).ToList();
            if (string.IsNullOrEmpty(F_Username))
                Content = Content.Where(o => o.TopicTypeID == 1).ToList();
            return View(await PaginatedList<WebboardTopic>.CreateAsync(Content, pageNumber ?? 1, Constants.Config.pageSize));

        }
        public IActionResult Detail(string Menuname,string LangID,int ID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            ViewBag.Error = "";
           // Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu obj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);

            if (obj == null || obj.ID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            ViewBag.MenuID = obj.ID;
            ViewBag.ParentID = obj.MENU_FRONT_ID;
            ViewBag.ParentName = obj.ParentMenuname;

            var Content = WBTopicData.Get.FTopicByReply(ID,true);
            var Views = WBTopicData.Get.FViewTopic(ID);
            var Obj = new WebboardReply();
            Obj.objTopic = Content;
            Obj.WBPostID = ID;
            Obj.LangCode = LangID;
            return View(Obj);
        }
        [ValidateReCaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Detail(WebboardReply Obj , string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            ViewBag.Error = "";
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var main = WBReplyData.Post.ReplyAddOrUpdate(Obj, UserName, IpAddress, Browser);
                return RedirectToAction("Detail", "Webboard", new { Menuname = main.Menuname, LangID = Obj.LangCode });
            }
            return View(Obj);
        }

        public IActionResult Topic(string Menuname, string LangID)
        {
            ViewBag.Error = "";
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu obj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);

            if (obj == null || obj.ID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            var listTopic = WBTopicTypeData.Get.ByActive();
            ViewBag.TopicType = listTopic;
            ViewBag.MenuID = obj.ID;
            ViewBag.ParentID = obj.MENU_FRONT_ID;
            ViewBag.ParentName = obj.ParentMenuname;
            var Obj = new WebboardTopic();
            Obj.Menuname = Menuname;
            Obj.LangCode = LangID;
            Obj.TopicTypeID = (listTopic.FirstOrDefault() == null)? 0 : listTopic.FirstOrDefault().ID;
            ViewBag.LangCode = LangID;
            return View(Obj);

        }
        [ValidateReCaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Topic(WebboardTopic Obj, string Menuname, string LangID)
        {
            ViewBag.Error = "";
            bool IsError = false;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid){
                try{
                    if (string.IsNullOrEmpty(Obj.Description))
                    {
                        IsError = true;
                        ViewBag.Error = "กรุณากรอกข้อมูล รายละเอียด";
                    }
                    else
                    {
                        var main = WBTopicData.Post.AddOrUpdate(Obj, F_Username, IpAddress, Browser);
                        return RedirectToAction("TopicSuccess", "Webboard", new { Menuname = main.Menuname, LangID = Obj.LangCode, ID = main.ID });
                    }
                }
                catch(Exception ex){
                    IsError = true;
                    ViewBag.Error = "บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่อีกครั้ง";
                }
            }
            else{
                IsError = true;
                if (errors.Count() > 0)
                    ViewBag.Error = errors.FirstOrDefault().ErrorMessage;
            }
            if (IsError){
                Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);

                if (obj == null || obj.ID == 0)
                    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
                var listTopic = WBTopicTypeData.Get.ByActive();
                ViewBag.TopicType = listTopic;
                ViewBag.MenuID = obj.ID;
                ViewBag.ParentID = obj.MENU_FRONT_ID;
                ViewBag.ParentName = obj.ParentMenuname;
                ViewBag.LangCode = LangID;
            }
            return View(Obj);

        }


        public IActionResult TopicSuccess(string Menuname, string LangID)
        {
            ViewBag.Title = Menuname;
            ViewBag.Menuname = Menuname;
            return View();
        }
    }

}
