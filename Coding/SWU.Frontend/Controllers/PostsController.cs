﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Core;
using Core.Common;
using Core.DAL.Models;
using Core.library;
using SWU.Frontend.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Wangkanai.Detection;


namespace SWU.Frontend.Controllers
{
    public class PostsController : InitializeController
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _accessor;
        private readonly IBrowserResolver _browser;
        private static string IpAddress;
        private static string Browser;
        private readonly IMemoryCache _cache;
        public PostsController(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor, IBrowserResolver browserDetector, IMemoryCache memoryCache)
        {
            _environment = environment;
            _accessor = httpContextAccessor;
            _browser = browserDetector;
            IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            Browser = _browser.Browser.Type.ToString();
            _cache = memoryCache;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index(string Menuname, string LangID, string searchString, int? pageNumber, string tags)
        {
            //ViewBag.Title = Menuname;
            //ViewBag.Menuname = Menuname;
            //Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);

            //if (obj == null)
            //    return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            //ViewBag.MenuID = obj.ID;
            //ViewBag.ParentID = obj.MENU_FRONT_ID!=null?obj.MENU_FRONT_ID: obj.ID;
            //ViewBag.ParentName = obj.ParentMenuname;
            ViewBag.LangID = LangID;

            //Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = menuObj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = MenuID;
            ViewBag.Menuname = menuObj.FPermalink;
            ViewBag.ParentID = menuObj.MENU_FRONT_ID;
            ViewBag.ParentName = menuObj.ParentMenuname;
            List<PostsDetail> Content = new List<PostsDetail>();
            var list_rss = RSSFeedData.Get.RSSFeedByCateActive(MenuID, LangID);
            List<PostsDetail> RSSContent = new List<PostsDetail>();
            foreach (var item in list_rss)
            {
                //string url = "https://www.cgd.go.th/cs/intranet/getRssById?asset_type=News_P&parent_id=1428491443848&site_id=1428504480102";
                string url = item.RSSFeedLink;
                if (!string.IsNullOrEmpty(url))
                {
                    try
                    {
                        WebClient client = new WebClient();
                        Stream stream = client.OpenRead(url);
                        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                        Encoding srcEncoding = Encoding.GetEncoding(874);
                        using var reader = XmlReader.Create(new StreamReader(stream, srcEncoding));// XmlReader.Create(url);

                        var feed = SyndicationFeed.Load(reader);

                        List<PostsDetail> a = (from Main in feed.Items.ToList()
                                               select new
                                               {
                                                   Title = Main.Title.Text,
                                                   ShortDescriptions = Main.Summary.Text,
                                                   FilePath = feed.ImageUrl.ToString(),// .ElementExtensions.Select(e => e.GetObject<XElement>().Attribute("url").Value),
                                                   //FilePath =Main.ElementExtensions.Where(p => (p.OuterName.StartsWith("http://") && (p.OuterName.EndsWith(".jpg") || p.OuterName.EndsWith(".png") || p.OuterName.EndsWith(".gif"))) || p.OuterName == "thumbnail").First().GetObject<XElement>().Attribute("url").Value,
                                                   //UpdatedDate = Main.PublishDate.DateTime,
                                                   //PostID = Main.Id,

                                                   UrlLink = (Main.Links.Count > 0 ? Main.Links.FirstOrDefault().Uri.ToString() : "#"),
                                                   UpdatedDate = Main.PublishDate != null ? Main.PublishDate.DateTime : new DateTime(),
                                                   IsRSS = true
                                               }).ToList_list<PostsDetail>();
                        RSSContent = RSSContent.Union(a).ToList();
                    }
                    catch (Exception ex)
                    {
                        var esx = ex.Message;
                    }
                }
            }

            if (string.IsNullOrEmpty(tags))
            {
                Content = PostData.Get.FListPosts(MenuID, LangID).ToList();
            }
            else
            {
                ViewBag.Tags = tags;
                Content = PostData.Get.FListPostsTag(MenuID, LangID, tags).ToList();
            }
            Content = Content.Union(RSSContent).ToList();
            ViewBag.UrlLink = CommonData.Get.FGetConfigValue("SETTING", "FRONTEND_URL");
            return View(await PaginatedList<PostsDetail>.CreateAsync(Content, pageNumber ?? 1, 12));
            // return View(Content);
        }
        public IActionResult Details(string Menuname, string LangID, int ID)
        {

            // Tm_Menu obj = MenuFrontData.Get.ByPermalink(Menuname, LangID);
            Tm_Menu obj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(Menuname), LangID);
            if (obj == null)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            string name_menu = obj?.Menuname ?? "ไม่พบเมนู";
            ViewBag.Title = name_menu;
            ViewBag.MenuID = obj.ID;
            ViewBag.Menuname = obj.FPermalink;
            ViewBag.ParentID = obj.MENU_FRONT_ID;
            ViewBag.ParentName = obj.ParentMenuname;

            var Content = PostData.Get.FPostByID(obj.ID, ID, LangID);
            if (Content == null)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });

            Content.MediaLst = MediaFilesData.Get.FListMedia(obj.ID, 5, Content.PostDetailID);
            Content.lstRelate = PostData.Get.FListRelatePosts(obj.ID, Content.PostID, LangID);
            if (Content != null && Content.PostDetailID != 0)
                Content.lstFiles = MediaFilesData.Get.FListMedia(obj.ID, 4, Content.PostDetailID);
            var Views = PostData.Get.FViews(Content.PostDetailID);
            return View(Content);
        }

        [ResponseCache(Duration = 1200)]
        public async Task<ActionResult> Rss(int ID, string LangID)
        {
            Tm_Menu menuObjs = MenuFrontData.Get.ByID(ID, LangID);
            //  Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(menuObjs.FPermalink, LangID);
            Tm_Menu menuObj = MenuFrontData.Get.ByPermalink(HttpUtility.UrlDecode(menuObjs.FPermalink), LangID);
            int MenuID = menuObj?.ID ?? 0;
            if (MenuID == 0)
                return RedirectToAction("Index", "Errorpage", new { MenuName = "404" });
            string name_menu = menuObj?.FPermalink ?? "";

            UriBuilder builder = new UriBuilder("https://www.ha.or.th");
            var feed = new SyndicationFeed("Title", "Description", builder.Uri, "RSSUrl", DateTime.Now);
            
            feed.Title = new TextSyndicationContent(name_menu);
            feed.Description = new TextSyndicationContent("");
            feed.ImageUrl = new Uri("https://www.ha.or.th/images/logo.png");

            feed.Copyright = new TextSyndicationContent($"{DateTime.Now.Year} " + (LangID == "EN" ? TextCommon.EN.TXT_HA : TextCommon.TH.TXT_HA));
            
            feed.LastUpdatedTime = DateTime.Now;
            var items = new List<SyndicationItem>();
            var postings = PostData.Get.FListPosts(ID, LangID).ToList(); //_blogDataService.ListBlogForRss();
            foreach (var item in postings.Take(20))
            {
                var postUrl = Url.Action("Detail", "Posts", new { ID = item.PostID }, HttpContext.Request.Scheme);
                var title = item.Title;
                var description = item.ShortDescriptions;
                items.Add(new SyndicationItem(title, description, new Uri(postUrl), item.PostID.ToString(), item.UpdatedDate));
            }

            feed.Items = items;
            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };
            using (var stream = new MemoryStream())
            {
                using (var xmlWriter = XmlWriter.Create(stream, settings))
                {
                    var rssFormatter = new Rss20FeedFormatter(feed, false);
                    rssFormatter.WriteTo(xmlWriter);
                    xmlWriter.Flush();
                }
                return File(stream.ToArray(), "application/xml; charset=utf-8");
            }

        }
    }
}
