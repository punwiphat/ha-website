// Footer Show/Hide on mobile
jQuery(document).ready(function($){
    $("#ft-links-1").on("click", function() {
        $("#ft-links-1 ul").toggleClass("show");
    });
    $("#ft-links-2").on("click", function() {
        $("#ft-links-2 ul").toggleClass("show");
    });
    $("#ft-links-3").on("click", function() {
        $("#ft-links-3 ul").toggleClass("show");
    });
});


// Go To Top
jQuery(document).ready(function($){ 
    $(window).scroll(function() {
    var gotop = $('.gotop');
    var top = 200;
    if ($(window).scrollTop() >= top) {
        gotop.addClass('show');
    } else {
        gotop.removeClass('show');
    }
});
});


// Sidebar 
$(document).ready(function(){
    $('.menu-item-level-one').click(function(){
        console.log( $(this).next());
        if($(this).next().hasClass('show-menu')){
            $(this).removeClass('up');
            $(this).next().removeClass('show-menu');
        }else {
            $(this).next().addClass('show-menu');
            $(this).addClass('up');
        }  
    });
});


function loadStaticDataTable(id) {
    console.log('s');
    $(id).DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': false,
        'ordering': true,
        'order': [],
        'info': true,
        'autoWidth': false,
        'language': {
            "sEmptyTable": "ไม่มีข้อมูลในตาราง",
            "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
            "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 รายการ",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกรายการ)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "แสดง _MENU_ รายการ",
            "sLoadingRecords": "กำลังโหลดข้อมูล...",
            "sProcessing": "กำลังดำเนินการ...",
            "sSearch": "ค้นหา: ",
            "sZeroRecords": "ไม่พบข้อมูล",
            "oPaginate": {
                "sFirst": "หน้าแรก",
                "sPrevious": "ก่อนหน้า",
                "sNext": "ถัดไป",
                "sLast": "หน้าสุดท้าย"
            }
        },
        'columnDefs': [{
            "targets": 'no-sort',
            "orderable": false
        }
        ],
        "fnDrawCallback": function () {
            $(".bt-status").bootstrapSwitch({
                animate: true,
                size: 'small',
                onColor: 'success',
                offColor: 'secondary',
                onText: 'ACTIVE',
                offText: 'INACTIVE',
                labelText: '&nbsp',
                handleWidth: '120',
                labelWidth: 'auto',
                baseClass: 'bootstrap-switch',
                wrapperClass: 'wrapper'
            });
        }
    })

    $('[data-toggle="tooltip"]').tooltip();
    UnloadingUI();
}
function onSuccessLoadDataTable(tableid) {

    loadStaticDataTable('#' + tableid);
}
function UnloadingUI() {
    $('.blockUI').fadeOut();
}
function loadingDIV() {
    var block_ele = $(this).closest('section');

    $(block_ele).block({
        message:
            '<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; Loading ...</div>',
        fadeIn: 1000,
        fadeOut: 1000,
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: "10px 15px",
            color: "#fff",
            width: "auto",
            backgroundColor: "#333"
        }
    });
}
function loadingUI() {
    var block_ele = $(this).closest('.tab-pane');

    $(block_ele).block({
        message:
            '<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; Loading ...</div>',
        fadeIn: 1000,
        fadeOut: 1000,
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: "10px 15px",
            color: "#fff",
            width: "auto",
            backgroundColor: "#333"
        }
    });
}