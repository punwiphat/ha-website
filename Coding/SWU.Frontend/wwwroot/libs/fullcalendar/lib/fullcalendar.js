$(document).ready(function(){

	/************************************
	*				Default				*
	************************************/
	var calendarEl = document.getElementById('fc-default');
	var fcCalendar = new FullCalendar.Calendar(calendarEl, {
		header: {
			left: 'prev,next today',
			center: 'title',
			right: "dayGridMonth,timeGridWeek,timeGridDay"
		},
		defaultDate: '2020-08-24',
		editable: true,
		selectable: true,
		plugins: ["dayGrid", "timeGrid", "interaction"],
		eventLimit: true, // allow "more" link when too many events
		eventClick: function (calEvent, jsEvent, view) {
			todoNewTasksidebar.addClass('show');
			appContentOverlay.addClass('show');
			sideBarLeft.removeClass('show');
			avatarUserImage.addClass("d-none");
			assignedAvatarContent.removeClass("d-none");
			selectUsersName.val(null).trigger('change');
			selectAssignLable.val(null).trigger('change');
			updateTodo.addClass("d-none");
			addTodo.removeClass("d-none");
			markCompleteBtn.addClass("d-none");
			newTaskTitle.removeClass("d-none");
		},
		selectable: true,
		select: function (start, end, allDay) {
			todoNewTasksidebar.addClass('show');
			appContentOverlay.addClass('show');
			sideBarLeft.removeClass('show');
			avatarUserImage.addClass("d-none");
			assignedAvatarContent.removeClass("d-none");
			selectUsersName.val(null).trigger('change');
			selectAssignLable.val(null).trigger('change');
			updateTodo.addClass("d-none");
			addTodo.removeClass("d-none");
			markCompleteBtn.addClass("d-none");
			newTaskTitle.removeClass("d-none");
			

		},
		events: [
			{
				title: 'All Day Event',
				start: '2016-06-01'
			},
			{
				title: 'Long Event',
				start: '2016-06-07',
				end: '2016-06-10'
			},
			{
				id: 999,
				title: 'Repeating Event',
				start: '2016-06-09T16:00:00'
			},
			{
				id: 999,
				title: 'Repeating Event',
				start: '2016-06-16T16:00:00'
			},
			{
				title: 'Conference',
				start: '2016-06-11',
				end: '2016-06-13'
			},
			{
				title: 'Meeting',
				start: '2016-06-12T10:30:00',
				end: '2016-06-12T12:30:00'
			},
			{
				title: 'Lunch',
				start: '2016-06-12T12:00:00'
			},
			{
				title: 'Meeting',
				start: '2016-06-12T14:30:00'
			},
			{
				title: 'Happy Hour',
				start: '2016-06-12T17:30:00'
			},
			{
				title: 'Dinner',
				start: '2016-06-12T20:00:00'
			},
			{
				title: 'Birthday Party',
				start: '2016-06-13T07:00:00'
			},
			{
				title: 'Click for Google',
				url: 'http://google.com/',
				start: '2016-06-28'
			}
		],
		selectHelper: true,
	
	});

	fcCalendar.render();


});
/* ADDING EVENTS */
var currColor = "#3c8dbc"; //Red by default
//Color chooser button
var colorChooser = $("#color-chooser-btn");
$("#color-chooser > li > a").click(function (e) {
	e.preventDefault();
	//Save color

	currColor = $(this).css("color");
	$('#color').val(currColor);
	// console.log($('#color').val());
	//Add color effect to button
	$('#button-save').css({ "background-color": currColor, "border-color": currColor });
});
