using System;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Common;

namespace SWU.Frontend.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
    public static class ReplaceWord
    {
        public static string Filter(this string input)
        {
            string[] badWords = WBReservedWordData.Get.Lists().Select(o => o.OffensiveWord).ToArray();
            var re = new Regex(
                @"\b("
                + string.Join("|", badWords.Select(word =>
                    string.Join(@"\s*", word.ToCharArray())))
                + @")\b", RegexOptions.IgnoreCase);
            return re.Replace(input, match =>
            {
                return new string('*', match.Length);
            });
        }
    }
}
