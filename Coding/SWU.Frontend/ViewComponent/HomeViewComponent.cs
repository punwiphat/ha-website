﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Backoffice.Controllers
{

    public class SocialListViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public SocialListViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<SocialLink> obj = SocialLinkData.Get.ByActive();
           // if (!_cache.TryGetValue(CookieName.SocialList, out List<SocialLink> obj))
           // {
           //     obj = SocialLinkData.Get.ByActive();
           //     MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
           //     {
           //         AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
           //         SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
           //     };
           //     _cache.Set(CookieName.SocialList, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
           //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/SocialList.cshtml", obj));
        }
    }
    public class WebLinkViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public WebLinkViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
           // string cookiename = CookieName.WebLink+ LangID;
            List<BannerDetail> obj = BannerData.Get.ActiveByMenu(MenuList.WebLink, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<BannerDetail> obj))
            //{
            //    obj = BannerData.Get.ActiveByMenu(MenuList.WebLink, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/WebLink.cshtml", obj));
        }
    }
    public class BannerMainViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public BannerMainViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            //string cookiename = CookieName.BannerMain+ LangID;
            List<BannerDetail> obj = BannerData.Get.ActiveByMenu(MenuList.BannerMain, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<BannerDetail> obj))
            //{
            //    obj = BannerData.Get.ActiveByMenu(MenuList.BannerMain, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/BannerMain.cshtml", obj));
        }
    }
    public class BannerMiniViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public BannerMiniViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            //string cookiename = CookieName.BannerMain+ LangID;
            List<BannerDetail> obj = BannerData.Get.ActiveByMenu(MenuList.BannerMini, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<BannerDetail> obj))
            //{
            //    obj = BannerData.Get.ActiveByMenu(MenuList.BannerMain, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/BannerMini.cshtml", obj));
        }
    }
    public class ServicesMainViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public ServicesMainViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(5, LangID);
           // string cookiename = CookieName.ServicesMain + LangID;
           //// List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(5, LangID);
           // if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
           // {
           //     obj = MenuBackofficeData.Get.FMenuByModule(5, LangID);
           //     MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
           //     {
           //         AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
           //         SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
           //     };
           //     _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
           // }
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/ServicesMain.cshtml", obj));
        }
    }
    public class Services2ViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public Services2ViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(6, LangID);
            //string cookiename = CookieName.Services2 + LangID;
            ////List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(6, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuBackofficeData.Get.FMenuByModule(6, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/Services2.cshtml", obj));
        }
    }
    public class ServicesOurViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public ServicesOurViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List <Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(7, LangID);
            //string cookiename = CookieName.ServicesOur + LangID;
            ////List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(7, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuBackofficeData.Get.FMenuByModule(7, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/ServicesOur.cshtml", obj));
        }
    }
    public class PostNewsViewComponent : ViewComponent
    {
        //private readonly IMemoryCache _cache;
        public PostNewsViewComponent(IMemoryCache memoryCache)
        {
           // _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List<PostsDetail> obj = PostData.Get.FTopPosts(MenuList.News, LangID);
            //string cookiename = "PostNewsHome" + LangID;
            //if (!_cache.TryGetValue(cookiename, out List<PostsDetail> obj))
            //{
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //_cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/PostNewsHome.cshtml", obj));
        }
    }
    public class EvnetListViewComponent : ViewComponent
    {
        //private readonly IMemoryCache _cache;
        public EvnetListViewComponent(IMemoryCache memoryCache)
        {
            // _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List<PostsDetail> obj = PostData.Get.FTopPosts(MenuList.EventsSWU, LangID);
            //string cookiename = "PostNewsHome" + LangID;
            //if (!_cache.TryGetValue(cookiename, out List<PostsDetail> obj))
            //{
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //_cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/EventList.cshtml", obj));
        }
    }
    public class PopupViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public PopupViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.Popup + LangID;
            List<BannerDetail> obj = BannerData.Get.ActiveByMenu(MenuList.Popup, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<BannerDetail> obj))
            //{
            //    obj = BannerData.Get.ActiveByMenu(MenuList.Popup, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/Popup.cshtml", obj));
        }
    }
    //public class GraphicViewComponent : ViewComponent
    //{
    //    private readonly IMemoryCache _cache;
    //    public GraphicViewComponent(IMemoryCache memoryCache)
    //    {
    //        _cache = memoryCache;
    //    }
    //    public async Task<IViewComponentResult> InvokeAsync(string LangID)
    //    {
    //        string cookiename = CookieName.Graphic + LangID;
    //        //if (!_cache.TryGetValue(cookiename, out List<BannerDetail> obj))
    //        //{
              
    //        //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
    //        //    {
    //        //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
    //        //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
    //        //    };
    //        //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
    //        //}
    //        List<BannerDetail>  obj = BannerData.Get.ActiveByMenu(MenuList.Graphic, LangID);
    //        return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Home/Graphic.cshtml", obj));
    //    }
    //}
 
}
