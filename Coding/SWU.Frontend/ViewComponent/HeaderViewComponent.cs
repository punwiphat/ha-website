﻿using System;
using System.Threading.Tasks;
using Core.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Backoffice.Controllers
{
    public class HeaderViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public HeaderViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = "ContactUS" + LangID;
            if (!_cache.TryGetValue(cookiename, out ContactDetail obj))
            {
                obj = ContactData.Get.FContactUS(LangID);
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(365), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(365) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename, obj, TimeSpan.FromDays(365));
            }
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Header/Default.cshtml", obj));
        }

    }
  
}
