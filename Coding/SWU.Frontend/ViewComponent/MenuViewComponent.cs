﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Core.Common;
using Core.DAL.Models;
using Core;
using Core.library;

namespace Core.Backoffice.Controllers
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public MenuViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.MenuMain + LangID;
            List<Tm_Menu> obj = MenuFrontData.Get.FMainMenu(1, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuFrontData.Get.FMainMenu(1, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/Default.cshtml", obj));
        }

    }
    public class SubMenuViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public SubMenuViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int ParentID, string LangID)
        {
            string cookiename = CookieName.Submenu + ParentID.ToString() + LangID;
            List<Tm_Menu> obj = MenuFrontData.Get.FSubMenu(ParentID, LangID);

            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuFrontData.Get.FSubMenu(ParentID, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/SubMenu.cshtml", obj));
        }

    }
    public class CategoryViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public CategoryViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int MenuID, string LangID)
        {
            //string cookiename = CookieName.Category + MenuID.ToString() + LangID;
            // var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            List<Category> obj = CategoryData.Get.ByMenuIdActive(MenuID);

            ////List<Category> obj = CategoryData.Get.ByMenuIdActive(MenuDisplay.CategoryID);
            //if (!_cache.TryGetValue(cookiename, out List<Category> obj))
            //{
            //    var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            //    obj = CategoryData.Get.ByMenuIdActive(MenuDisplay.CategoryID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/Category.cshtml", obj));
        }

    }
    public class WBCategoryViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public WBCategoryViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            List<WebboardCategory> obj = WBTopicData.Get.CategoryWebboardByLang(LangID);
            //string cookiename = CookieName.WBCategory + LangID;
            //if (!_cache.TryGetValue(cookiename, out List<WebboardCategory> obj))
            //{
            //    //obj = WBTopicData.Get.FCateByLang(LangID);
            //    obj = WBTopicData.Get.CategoryWebboardByLang(LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/WBCategory.cshtml", obj));
        }

    }
    public class SitemapViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public SitemapViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.Sitemap + LangID;

            if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> ListMenu))
            {
                string cookienameMain = CookieName.MenuMainF + LangID;
                //List<Tm_Menu> obj = MenuFrontData.Get.FMainMenu(1, LangID);
                if (!_cache.TryGetValue(cookienameMain, out List<Tm_Menu> obj))
                {
                    obj = MenuFrontData.Get.FMenu(1, LangID);
                    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                    };
                    _cache.Set(cookienameMain, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
                }
                foreach (var item in obj)
                {
                    foreach (var item2 in item.Sub_Menu) // Level3
                    {
                        item2.Sub_Menu = MenuFrontData.Get.FMenuByParentID(item2.ID, LangID);
                        foreach (var item3 in item2.Sub_Menu) // Level4
                        {
                            item3.Sub_Menu = MenuFrontData.Get.FMenuByParentID(item3.ID, LangID);
                            foreach (var item4 in item3.Sub_Menu) // Level5
                            {
                                item4.Sub_Menu = MenuFrontData.Get.FMenuByParentID(item4.ID, LangID);
                            }
                        }
                    }
                    // item.Sub_Menu = MenuFrontData.Get.FSubMenu(item.ID, LangID);
                }
                MemoryCacheEntryOptions options2 = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            }
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/Sitemap.cshtml", ListMenu));
        }

    }
    public class QuickMenuViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public QuickMenuViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.QuickMenu + LangID;
            List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/QuickMenu.cshtml", obj));
        }
    }
    public class QuickMenu2ViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public QuickMenu2ViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.QuickMenu + LangID;
            List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/QuickMenu2.cshtml", obj));
        }
    }
    public class QuickMenu3ViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public QuickMenu3ViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.QuickMenu + LangID;
            List<Tm_Menu> obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuBackofficeData.Get.FMenuByModule(8, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/QuickMenu3.cshtml", obj));
        }
    }
    public class SubMenuXHospitalViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public SubMenuXHospitalViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int ParentID, string LangID)
        {
            List<Tm_Menu> obj = MenuFrontData.Get.FSubMenu(110, LangID);
            //string cookiename = CookieName.Submenu + ParentID.ToString() + LangID;
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuFrontData.Get.FSubMenu(110, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/SubMenuXHospital.cshtml", obj));
        }

    }
    public class CategoryXProcurementViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public CategoryXProcurementViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int MenuID, string LangID)
        {
            var MenuDisplay = MenuDisplayData.Get.ByMenu(MenuID);
            List<Category> obj = CategoryData.Get.ByMenuIdActive(MenuDisplay.CategoryID);
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/CategoryXProcurement.cshtml", obj));
        }

    }
    public class SubMenuXComplaintViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public SubMenuXComplaintViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int ParentID, string LangID)
        {
            List<Tm_Menu> obj = MenuFrontData.Get.FSubMenu(ParentID, LangID);
            //string cookiename = CookieName.Submenu + ParentID.ToString() + LangID;
            //if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            //{
            //    obj = MenuFrontData.Get.FSubMenu(ParentID, LangID);
            //    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            //    {
            //        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
            //        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
            //    };
            //    _cache.Set(cookiename, obj, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            //}
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/SubMenuXComplaint.cshtml", obj));
        }

    }
    public class NavViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public NavViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(int MenuID, string LangID)
        {
            List<Tm_Menu> obj = MenuFrontData.Get.ListNAV(MenuID, LangID);
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Menu/nav.cshtml", obj));
        }

    }
}
