﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Common;
using Core.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Core.Backoffice.Controllers
{
    public class FooterViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public FooterViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = CookieName.ContactUS + LangID;
            if (!_cache.TryGetValue(cookiename, out ContactDetail obj))
            {
                obj = ContactData.Get.FContactUS(LangID);
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(1) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename, obj, TimeSpan.FromDays(1));
            }
            string cookiename2 = CookieName.PolicyMenu+ LangID;
            if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj2))
            {
                obj2 = MenuBackofficeData.Get.FMenuByParentID(MenuList.Policy, LangID);
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(1) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename2, obj2, TimeSpan.FromDays(1));
            }
            string cookiename3 = CookieName.SocialList;
            if (!_cache.TryGetValue(cookiename, out List<SocialLink> obj3))
            {
                obj3 = SocialLinkData.Get.ByActive();
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(1) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename3, obj, TimeSpan.FromDays(1));
            }
            string cookiename4 = CookieName.Sitemap + LangID;

            if (!_cache.TryGetValue(cookiename4, out List<Tm_Menu> Obj4))
            {
                string cookienameMain = CookieName.MenuMainF + LangID;
                //List<Tm_Menu> obj = MenuFrontData.Get.FMainMenu(1, LangID);
                if (!_cache.TryGetValue(cookienameMain, out List<Tm_Menu> MainMenu))
                {
                    MainMenu = MenuFrontData.Get.FMenuF(1, LangID);
                    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                        SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                    };
                    _cache.Set(cookienameMain, MainMenu, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
                }
                foreach (var item in MainMenu)
                {
                    item.Sub_Menu = MenuFrontData.Get.FSubMenu(item.ID, LangID);
                }
                MemoryCacheEntryOptions options2 = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(Constants.Config.Cookies_Menu), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(Constants.Config.Cookies_Menu) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                Obj4 = MainMenu;
                _cache.Set(cookiename4, MainMenu, TimeSpan.FromDays(Constants.Config.Cookies_Menu));
            }
            FooterModel Main = new FooterModel();
            Main.obj_contact = obj;
            Main.list_Policy = obj2;
            Main.list_Social = obj3;
            Main.list_Menu = Obj4;
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Footer/Default.cshtml", Main));
        }

    }
    public class PolicyViewComponent : ViewComponent
    {
        private readonly IMemoryCache _cache;
        public PolicyViewComponent(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public async Task<IViewComponentResult> InvokeAsync(string LangID)
        {
            string cookiename = "PolicyMenu" + LangID;
            if (!_cache.TryGetValue(cookiename, out List<Tm_Menu> obj))
            {
                obj = MenuBackofficeData.Get.FMenuByParentID(MenuList.Policy,LangID);
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1), // cache will expire in 25 seconds   TimeSpan.FromSeconds(25)
                    SlidingExpiration = TimeSpan.FromDays(1) // caceh will expire if inactive for 5 seconds    FromSeconds(5)
                };
                _cache.Set(cookiename, obj, TimeSpan.FromDays(1));
            }
            return await Task.FromResult((IViewComponentResult)View("~/Views/Components/Footer/PolicyMenu.cshtml", obj));
        }

    }
}
